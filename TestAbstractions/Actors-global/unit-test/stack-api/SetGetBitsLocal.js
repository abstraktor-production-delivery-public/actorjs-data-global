
const ActorApi = require('actor-api');
const {BitByte} = require('stack-api');


class SetGetBitsLocal extends ActorApi.ActorLocal {
  *run() {
    const value1 = 0;
    const a1 = BitByte.setBits(value1, 0, 3, 11);
    VERIFY_VALUE(176, a1);
    const b1 = BitByte.setBits(a1, 4, 7, 2);
    VERIFY_VALUE(178, b1);
    VERIFY_VALUE(178, BitByte.getBits(b1, 0, 7));
    
    const value2 = 129;
    const a2 = BitByte.setBits(value2, 2, 3, 3);
    VERIFY_VALUE(177, a2);
    const b2 = BitByte.setBits(a2, 5, 6, 2);
    VERIFY_VALUE(181, b2);
    VERIFY_VALUE(90, BitByte.getBits(b2, 0, 6));
  }
}

module.exports = SetGetBitsLocal;
