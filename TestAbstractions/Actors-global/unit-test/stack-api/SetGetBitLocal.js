
const ActorApi = require('actor-api');
const {BitByte} = require('stack-api');


class SetGetBitLocal extends ActorApi.ActorLocal {
  *run() {
    const value1 = 0;
    VERIFY_VALUE(1, BitByte.getBit(BitByte.setBit(value1, 0), 0));
    VERIFY_VALUE(1, BitByte.getBit(BitByte.setBit(value1, 1), 1));
    VERIFY_VALUE(1, BitByte.getBit(BitByte.setBit(value1, 2), 2));
    VERIFY_VALUE(1, BitByte.getBit(BitByte.setBit(value1, 3), 3));
    VERIFY_VALUE(1, BitByte.getBit(BitByte.setBit(value1, 4), 4));
    VERIFY_VALUE(1, BitByte.getBit(BitByte.setBit(value1, 5), 5));
    VERIFY_VALUE(1, BitByte.getBit(BitByte.setBit(value1, 6), 6));
    VERIFY_VALUE(1, BitByte.getBit(BitByte.setBit(value1, 7), 7));

    const value2 = 0;
    VERIFY_VALUE(0, BitByte.getBit(BitByte.setBit(value1, 0), 1));
    VERIFY_VALUE(0, BitByte.getBit(BitByte.setBit(value1, 1), 2));
    VERIFY_VALUE(0, BitByte.getBit(BitByte.setBit(value1, 2), 3));
    VERIFY_VALUE(0, BitByte.getBit(BitByte.setBit(value1, 3), 4));
    VERIFY_VALUE(0, BitByte.getBit(BitByte.setBit(value1, 4), 5));
    VERIFY_VALUE(0, BitByte.getBit(BitByte.setBit(value1, 5), 6));
    VERIFY_VALUE(0, BitByte.getBit(BitByte.setBit(value1, 6), 7));
    VERIFY_VALUE(0, BitByte.getBit(BitByte.setBit(value1, 7), 0));
    
    const value3 = 255;
    VERIFY_VALUE(0, BitByte.getBit(BitByte.setBit(value1, 7, 0), 7));
    
    let value = 0;
    
    value = BitByte.setBit(value, 7);
    value = BitByte.setBit(value, 6);
    value = BitByte.setBit(value, 3);
    value = BitByte.setBit(value, 2);
    
    VERIFY_VALUE(value, 51);
    VERIFY_VALUE(0, BitByte.getBit(value, 0));
    VERIFY_VALUE(0, BitByte.getBit(value, 1));
    VERIFY_VALUE(1, BitByte.getBit(value, 2));
    VERIFY_VALUE(1, BitByte.getBit(value, 3));
    VERIFY_VALUE(0, BitByte.getBit(value, 4));
    VERIFY_VALUE(0, BitByte.getBit(value, 5));
    VERIFY_VALUE(1, BitByte.getBit(value, 6));
    VERIFY_VALUE(1, BitByte.getBit(value, 7));
  }
}

module.exports = SetGetBitLocal;
