

const ActorApi = require('actor-api');
const {BitByte} = require('stack-api');


class SetBitsLocal extends ActorApi.ActorLocal {
  *run() {
    const value1 = 0;
    VERIFY_VALUE(240, BitByte.setBits(value1, 0, 3, 15));
    VERIFY_VALUE(112, BitByte.setBits(value1, 0, 3, 7));
    VERIFY_VALUE(16, BitByte.setBits(value1, 0, 3, 1));
    
    const value2 = 255;
    VERIFY_VALUE(255, BitByte.setBits(value2, 0, 3, 15));
    VERIFY_VALUE(127, BitByte.setBits(value2, 0, 3, 7));
    VERIFY_VALUE(31, BitByte.setBits(value2, 0, 3, 1));
    
    VERIFY_VALUE(12, BitByte.setBits(0, 3, 6, 6));
    VERIFY_VALUE(231, BitByte.setBits(255, 3, 4, 0));
    VERIFY_VALUE(186, BitByte.setBits(170, 2, 4, 7));
  }
}

module.exports = SetBitsLocal;
