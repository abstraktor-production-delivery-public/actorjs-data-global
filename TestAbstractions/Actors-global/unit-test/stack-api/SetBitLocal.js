
const ActorApi = require('actor-api');
const {BitByte} = require('stack-api');


class SetBitLocal extends ActorApi.ActorLocal {
  *run() {
    const value1 = 0;
    VERIFY_VALUE(128, BitByte.setBit(value1, 0));
    VERIFY_VALUE(64, BitByte.setBit(value1, 1));
    VERIFY_VALUE(32, BitByte.setBit(value1, 2));
    VERIFY_VALUE(16, BitByte.setBit(value1, 3));
    VERIFY_VALUE(8, BitByte.setBit(value1, 4));
    VERIFY_VALUE(4, BitByte.setBit(value1, 5));
    VERIFY_VALUE(2, BitByte.setBit(value1, 6));
    VERIFY_VALUE(1, BitByte.setBit(value1, 7));
    
    const value2 = 128;
    VERIFY_VALUE(136, BitByte.setBit(value2, 4));
    VERIFY_VALUE(132, BitByte.setBit(value2, 5));
    VERIFY_VALUE(130, BitByte.setBit(value2, 6));
    VERIFY_VALUE(129, BitByte.setBit(value2, 7));
    
    const value3 = 170;
    VERIFY_VALUE(234, BitByte.setBit(value3, 1));
    VERIFY_VALUE(186, BitByte.setBit(value3, 3));
    VERIFY_VALUE(174, BitByte.setBit(value3, 5));
    VERIFY_VALUE(171, BitByte.setBit(value3, 7));
    
    let sum = 0;
    sum = BitByte.setBit(sum, 0)
    VERIFY_VALUE(128, sum);
    sum = BitByte.setBit(sum, 1)
    VERIFY_VALUE(192, sum);
    sum = BitByte.setBit(sum, 2)
    VERIFY_VALUE(224, sum);
    sum = BitByte.setBit(sum, 3)
    VERIFY_VALUE(240, sum);
    sum = BitByte.setBit(sum, 4)
    VERIFY_VALUE(248, sum);
    sum = BitByte.setBit(sum, 5)
    VERIFY_VALUE(252, sum);
    sum = BitByte.setBit(sum, 6)
    VERIFY_VALUE(254, sum);
    sum = BitByte.setBit(sum, 7)
    VERIFY_VALUE(255, sum);
    
    let sum2 = 255;
    sum = BitByte.setBit(sum, 0, 0)
    VERIFY_VALUE(127, sum);
    sum = BitByte.setBit(sum, 1, 0)
    VERIFY_VALUE(63, sum);
    sum = BitByte.setBit(sum, 2, 0)
    VERIFY_VALUE(31, sum);
    sum = BitByte.setBit(sum, 3, 0)
    VERIFY_VALUE(15, sum);
    sum = BitByte.setBit(sum, 4, 0)
    VERIFY_VALUE(7, sum);
    sum = BitByte.setBit(sum, 5, 0)
    VERIFY_VALUE(3, sum);
    sum = BitByte.setBit(sum, 6, 0)
    VERIFY_VALUE(1, sum);
    sum = BitByte.setBit(sum, 7, 0)
    VERIFY_VALUE(0, sum);

  }
}

module.exports = SetBitLocal;
