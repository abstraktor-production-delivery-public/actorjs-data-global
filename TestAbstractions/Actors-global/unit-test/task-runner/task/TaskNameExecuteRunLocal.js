
const TaskRunner = require('z-build-project/project/server/task-runner');
const ActorApi = require('actor-api');


class TaskNameExecuteRunLocal extends ActorApi.ActorLocal {
  constructor() {
    super();
    this.taskNames = [];
    this.taskRunOrder = [];
  }
  
  *data() {
    this.taskNames = this.getTestDataStrings('task-names');
    this.taskRunOrder = this.getTestDataStrings('task-run-order');
  }
  
  *run() {
    const myTaskRunner = new TaskRunner();
    let resultingTaskNames = [];
    let pendings = this.taskNames.length;
    this.callback((cb) => {
      this.taskNames.forEach((taskName) => {
        myTaskRunner.task(taskName, (cbTask) => {
          resultingTaskNames.push(taskName);
          cbTask();
        })((err) => {
          if(0 === --pendings) {
            cb();
          }
        });
      });
    });
    VERIFY_VALUE(this.taskNames, resultingTaskNames);
    resultingTaskNames = [];
    this.callback((cb) => {
      let pendings = this.taskRunOrder.length;
      this.taskRunOrder.forEach((taskName) => {
        myTaskRunner.run(taskName, (err) => {
          if(0 === --pendings) {
            cb();
          }
        });
      });
    });
    VERIFY_VALUE(this.taskRunOrder, resultingTaskNames);
  }
}

module.exports = TaskNameExecuteRunLocal;
