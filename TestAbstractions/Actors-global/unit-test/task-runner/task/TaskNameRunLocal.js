
const TaskRunner = require('z-build-project/project/server/task-runner');
const ActorApi = require('actor-api');


class TaskNameRunLocal extends ActorApi.ActorLocal {
  constructor() {
    super();
    this.taskNames = [];
    this.taskRunOrder = [];
  }
  
  *data() {
    this.taskNames = this.getTestDataStrings('task-names');
    this.taskRunOrder = this.getTestDataStrings('task-run-order');
  }
  
  *run() {
    const myTaskRunner = new TaskRunner();
    const resultingTaskNames = [];
    this.taskNames.forEach((taskName) => {
      myTaskRunner.task(taskName, (cb) => {
        resultingTaskNames.push(taskName);
        cb();
      });
    });
    this.callback((cb) => {
      let pendings = this.taskRunOrder.length;
      this.taskRunOrder.forEach((taskName) => {
        myTaskRunner.run(taskName, (err) => {
          if(0 === --pendings) {
            cb();
          }
        });
      });
    });
    VERIFY_VALUE(this.taskRunOrder, resultingTaskNames);
  }
}

module.exports = TaskNameRunLocal;
