
const TaskRunner = require('z-build-project/project/server/task-runner');
const ActorApi = require('actor-api');


class TaskAnonymousExecuteLocal extends ActorApi.ActorLocal {
  constructor() {
    super();
    this.taskNames = [];
  }
  
  *data() {
    this.taskNames = this.getTestDataStrings('task-names');
  }
  
  *run() {
    const myTaskRunner = new TaskRunner();
    const resultingTaskNames = [];
    this.callback((cb) => {
      let pendings = this.taskNames.length;
      this.taskNames.forEach((taskName) => {
        myTaskRunner.task((taskCb) => {
          resultingTaskNames.push(taskName);
          taskCb();
        })((err) => {
          if(0 === --pendings) {
            cb();
          }
        });
      });
    });
    VERIFY_VALUE(this.taskNames, resultingTaskNames);
  }
}

module.exports = TaskAnonymousExecuteLocal;
