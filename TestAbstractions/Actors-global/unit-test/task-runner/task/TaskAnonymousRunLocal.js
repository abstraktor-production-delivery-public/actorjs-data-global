
const TaskRunner = require('z-build-project/project/server/task-runner');
const ActorApi = require('actor-api');


class TaskAnonymousRunLocal extends ActorApi.ActorLocal {
  constructor() {
    super();
    this.taskRunOrder = [];
  }
  
  *data() {
    this.taskRunOrder = this.getTestDataStrings('task-run-order');
  }
  
  *run() {
    const myTaskRunner = new TaskRunner();
    const resultingTaskNames = [];
    this.callback((cb) => {
      let pendings = this.taskRunOrder.length;
      this.taskRunOrder.forEach((taskName) => {
        myTaskRunner.run((taskCb) => {
          resultingTaskNames.push(taskName);
          taskCb();
        }, (err) => {
          if(0 === --pendings) {
            cb();
          }
        });
      });
    });
    VERIFY_VALUE(this.taskRunOrder, resultingTaskNames);
  }
}

module.exports = TaskAnonymousRunLocal;
