
const TaskRunner = require('z-build-project/project/server/task-runner');
const ActorApi = require('actor-api');


class ParallelMixedRunLocal extends ActorApi.ActorLocal {
  constructor() {
    super();
    this.taskNames = [];
    this.taskInstances = [];
  }
  
  *data() {
    this.taskNames = this.getTestDataStrings('task-names');
  }
  
  *run() {
    const myTaskRunner = new TaskRunner();
    const tasks = [];
    const resultingTaskNames = [];
    const expectingTaskNames = [];
    this.taskNames.forEach((taskName, index) => {
      const ticks = 2 * (this.taskNames.length - index - 1) + 1;
      myTaskRunner.task(`${taskName}-by-name`, (cb) => {
        this._tick(ticks, () => {
          resultingTaskNames.push(`${taskName}-by-name`);
          cb();
        });
      });
    });
    this.taskNames.forEach((taskName, index) => {
      const ticks = 2 * (this.taskNames.length - index - 1) + 2;
      tasks.push((taskCb) => {
        this._tick(ticks, () => {
          resultingTaskNames.push(taskName);
          taskCb();
        });
      });
      expectingTaskNames.push(taskName);
      tasks.push(`${taskName}-by-name`);
      expectingTaskNames.push(`${taskName}-by-name`);
    });
    this.callback((cb) => {
      myTaskRunner.run(myTaskRunner.parallel(...tasks), (err) => {
        cb();
      });
    });
    VERIFY_VALUE(expectingTaskNames.reverse(), resultingTaskNames);
  }
  
  _tick(ticks, cb) {
    process.nextTick(() => {
      if(0 === ticks) {
        cb();
      }
      else {
        this._tick(--ticks, cb);
      }
    });
  }
}

module.exports = ParallelMixedRunLocal;
