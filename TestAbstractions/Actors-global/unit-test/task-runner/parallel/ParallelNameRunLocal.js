
const TaskRunner = require('z-build-project/project/server/task-runner');
const ActorApi = require('actor-api');


class ParallelNameRunLocal extends ActorApi.ActorLocal {
  constructor() {
    super();
    this.taskNames = [];
    this.taskInstances = [];
  }
  
  *data() {
    this.taskNames = this.getTestDataStrings('task-names');
  }
  
  *run() {
    const myTaskRunner = new TaskRunner();
    const tasks = [];
    const resultingTaskNames = [];
    this.taskNames.forEach((taskName) => {
      const ticks =  this.taskNames.length - tasks.length + 1;
      tasks.push((taskCb) => {
        this._tick(ticks, () => {
          resultingTaskNames.push(taskName);
          taskCb();
        });
      });
    });
    this.callback((cb) => {
      myTaskRunner.run(myTaskRunner.parallel(...tasks), (err) => {
        cb();
      });
    });
    VERIFY_VALUE(this.taskNames.reverse(), resultingTaskNames);
  }
  
  _tick(ticks, cb) {
    process.nextTick(() => {
      if(0 === ticks) {
        cb();
      }
      else {
        this._tick(--ticks, cb);
      }
    });
  }
}

module.exports = ParallelNameRunLocal;
