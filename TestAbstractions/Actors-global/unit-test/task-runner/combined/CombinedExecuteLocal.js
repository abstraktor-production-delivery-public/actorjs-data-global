
const TaskRunner = require('z-build-project/project/server/task-runner');
const ActorApi = require('actor-api');


class CombinedExecuteLocal extends ActorApi.ActorLocal {
  constructor() {
    super();
  }
  
  *data() {
  }
  
  *run() {
    const myTaskRunner = new TaskRunner();
    const resultingTaskNames = [];
    myTaskRunner.task('a1', (cb) => {resultingTaskNames.push('a1');cb();});
    myTaskRunner.task('a2', (cb) => {resultingTaskNames.push('a2');cb();});
    myTaskRunner.task('a3', (cb) => {resultingTaskNames.push('a3');cb();});
    myTaskRunner.task('b1', (cb) => {resultingTaskNames.push('b1');cb();});
    myTaskRunner.task('b2', (cb) => {resultingTaskNames.push('b2');cb();});
    myTaskRunner.task('b3', (cb) => {resultingTaskNames.push('b3');cb();});
    myTaskRunner.task('c1', (cb) => {resultingTaskNames.push('c1');cb();});
    myTaskRunner.task('c2', (cb) => {resultingTaskNames.push('c2');cb();});
    myTaskRunner.task('c3', (cb) => {resultingTaskNames.push('c3');cb();});
    this.callback((cb) => {
      myTaskRunner.parallel(
        myTaskRunner.serial('a1', 'a2', 'a3'),
        myTaskRunner.serial('b1', 'b2', 'b3'),
        myTaskRunner.serial('c1','c2', 'c3'))((err) => {
        cb();
      });
    });
    VERIFY_VALUE(['a1', 'b1', 'c1', 'a2', 'b2', 'c2', 'a3', 'b3', 'c3'], resultingTaskNames);
  }
}

module.exports = CombinedExecuteLocal;
