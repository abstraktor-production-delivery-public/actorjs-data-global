
const TaskRunner = require('z-build-project/project/server/task-runner');
const ActorApi = require('actor-api');


class SerialNameRunLocal extends ActorApi.ActorLocal {
  constructor() {
    super();
    this.taskRunOrder = [];
  }
  
  *data() {
    this.taskRunOrder = this.getTestDataStrings('task-run-order');
  }
  
  *run() {
    const myTaskRunner = new TaskRunner();
    const tasks = [];
    const resultingTaskNames = [];
    this.taskRunOrder.forEach((taskName) => {
      tasks.push((taskCb) => {
        resultingTaskNames.push(taskName);
        taskCb();
      });
    });
    myTaskRunner.serial('my-serial-tasks', ...tasks);
    this.callback((cb) => {
      myTaskRunner.run('my-serial-tasks', (err) => {
        cb();
      });
    });
    VERIFY_VALUE(this.taskRunOrder, resultingTaskNames);
  }
}

module.exports = SerialNameRunLocal;
