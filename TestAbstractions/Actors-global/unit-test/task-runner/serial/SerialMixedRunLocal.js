
const TaskRunner = require('z-build-project/project/server/task-runner');
const ActorApi = require('actor-api');


class SerialMixedRunLocal extends ActorApi.ActorLocal {
  constructor() {
    super();
    this.taskRunOrder = [];
    this.taskInstances = [];
  }
  
  *data() {
    this.taskRunOrder = this.getTestDataStrings('task-run-order');
  }
  
  *run() {
    const myTaskRunner = new TaskRunner();
    const tasks = [];
    const resultingTaskNames = [];
    const expectingTaskNames = [];
    this.taskRunOrder.forEach((taskName) => {
      myTaskRunner.task(`${taskName}-by-name`, (cb) => {
        resultingTaskNames.push(`${taskName}-by-name`);
        cb();
      });
    });
    this.taskRunOrder.forEach((taskName) => {
      tasks.push((taskCb) => {
        resultingTaskNames.push(taskName);
        taskCb();
      });
      expectingTaskNames.push(taskName);
      tasks.push(`${taskName}-by-name`);
      expectingTaskNames.push(`${taskName}-by-name`);
    });
    this.callback((cb) => {
      myTaskRunner.run(myTaskRunner.serial(...tasks), (err) => {
        cb();
      });
    });
    VERIFY_VALUE(expectingTaskNames, resultingTaskNames);
  }
}

module.exports = SerialMixedRunLocal;
