
const TaskRunner = require('z-build-project/project/server/task-runner');
const ActorApi = require('actor-api');


class SerialMixedExecuteLocal extends ActorApi.ActorLocal {
  constructor() {
    super();
    this.taskNames = [];
    this.taskInstances = [];
  }
  
  *data() {
    this.taskNames = this.getTestDataStrings('task-names');
  }
  
  *run() {
    const myTaskRunner = new TaskRunner();
    const tasks = [];
    const resultingTaskNames = [];
    const expectingTaskNames = [];
    this.taskNames.forEach((taskName) => {
      myTaskRunner.task(`${taskName}-by-name`, (cb) => {
        resultingTaskNames.push(`${taskName}-by-name`);
        cb();
      });
    });
    this.taskNames.forEach((taskName) => {
      tasks.push((taskCb) => {
        resultingTaskNames.push(taskName);
        taskCb();
      });
      expectingTaskNames.push(taskName);
      tasks.push(`${taskName}-by-name`);
      expectingTaskNames.push(`${taskName}-by-name`);
    });
    this.callback((cb) => {
      myTaskRunner.serial(...tasks)((err) => {
        cb();
      });
    });
    VERIFY_VALUE(expectingTaskNames, resultingTaskNames);
  }
}

module.exports = SerialMixedExecuteLocal;
