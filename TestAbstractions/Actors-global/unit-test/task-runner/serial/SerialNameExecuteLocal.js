
const TaskRunner = require('z-build-project/project/server/task-runner');
const ActorApi = require('actor-api');


class SerialNameExecuteLocal extends ActorApi.ActorLocal {
  constructor() {
    super();
    this.taskNames = [];
    this.taskInstances = [];
  }
  
  *data() {
    this.taskNames = this.getTestDataStrings('task-names');
  }
  
  *run() {
    const myTaskRunner = new TaskRunner();
    const tasks = [];
    const resultingTaskNames = [];
    this.taskNames.forEach((taskName) => {
      tasks.push((taskCb) => {
        resultingTaskNames.push(taskName);
        taskCb();
      });
    });
    this.callback((cb) => {
      myTaskRunner.serial('my-task', ...tasks)((err) => {
        cb();
      });
    });
    VERIFY_VALUE(this.taskNames, resultingTaskNames);
  }
}

module.exports = SerialNameExecuteLocal;
