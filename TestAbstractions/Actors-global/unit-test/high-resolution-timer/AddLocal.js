
const ActorApi = require('actor-api');
const HighResolutionTimestamp = require('z-abs-corelayer-server/server/high-resolution-timestamp');


class AddLocal extends ActorApi.ActorLocal {
  constructor() {
    super();
  }
  
  *data() {
    this.term1 = this.getTestDataNumbers('term1');
    this.term2 = this.getTestDataNumbers('term2');
  }
  
  *run() {
    VERIFY_MANDATORY('result', HighResolutionTimestamp.add(this.term1, this.term2));
  }
}

module.exports = AddLocal;
