
const ActorApi = require('actor-api');
const NoneMs = require('./ms/NoneMs');


class PuppeteerHttpMockTerm extends ActorApi.ActorTerminating {
  constructor() {
    super(undefined, true);
    this.httpConnection = null;
  }
  
  *data() {
  }
    
  *initServer() {
    this.httpConnection = this.createServer('http', {
      messageSelector: new NoneMs()
    });
  }
  
  *run() {
    this.httpConnection.accept();
  }
  
  *exit(interrupted) {
    this.closeConnection(this.httpConnection);
  }
}

module.exports = PuppeteerHttpMockTerm;
