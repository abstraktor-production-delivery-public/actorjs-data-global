
const ActorApi = require('actor-api');


class SimplePageOrig extends ActorApi.ActorOriginating {
  constructor() {
    super();
    this.puppeteerConnection = null;
  }

  *data() {

  }
  
  *initClient() {
    this.puppeteerConnection = this.createConnection('puppeteer');
  }
  
  *run() {
    const page = this.puppeteerConnection.page;
  }
  
  *exit(interrupted) {
    this.closeConnection(this.puppeteerConnection);
  }
}

module.exports = SimplePageOrig;
