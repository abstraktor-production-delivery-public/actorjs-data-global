
const ActorApi = require('actor-api');
const HttpApi = require('http-stack-api');
const HttpGet200OkSiteResponse = require('./msg/HttpGet200OkSiteResponse');


class HttpStaticTerm extends ActorApi.ActorTerminating {
  constructor() {
    super();
    this.httpConnection = null;
    this.siteIndex = 0;
  }

  *data() {
    this.siteIndex = this.getTestDataNumber('site', this.siteIndex, {
      static: 0,
      image: 1,
      images2: 2,
      script: 3
    });
  }
    
  *initServer() {
    this.httpConnection = this.createServer('http');
  }
  
  *run() {
    this.httpConnection.accept();
    
    const request = this.httpConnection.receive();
    
    const site = HttpStaticTerm.SITES[this.siteIndex];
    this.httpConnection.send(new HttpGet200OkSiteResponse(site, 'text/html', site.length));
  }
  
  *exit(interrupted) {
    this.closeConnection(this.httpConnection);
  }
}

HttpStaticTerm.STATIC_SITE = `<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>title</title>
  </head>
  <body>
    <p>
      HELLO
    </p>
  </body>
</html>`;

HttpStaticTerm.STATIC_IMAGE_SITE = `<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>title</title>
  </head>
  <body>
    <p>
      HELLO
    </p>
    <img src="/example.png">
  </body>
</html>`;

HttpStaticTerm.STATIC_2_IMAGES_SITE = `<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>title</title>
  </head>
  <body>
    <p>
      HELLO
    </p>
    <img src="/example.png">
    <img src="/example2.png">
  </body>
</html>`;

HttpStaticTerm.STATIC_SCRIPT_SITE = `<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>title</title>
    <script src="/scripts/3ppLayer-client.js" type="text/javascript" defer></script>
  </head>
  <body>
    <p>
      HELLO
    </p>
  </body>
</html>`;

HttpStaticTerm.SITES = [
  HttpStaticTerm.STATIC_SITE,
  HttpStaticTerm.STATIC_IMAGE_SITE,
  HttpStaticTerm.STATIC_2_IMAGES_SITE,
  HttpStaticTerm.STATIC_SCRIPT_SITE
];

module.exports = HttpStaticTerm;
