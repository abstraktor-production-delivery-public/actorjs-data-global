
const ActorApi = require('actor-api');
const AllMs = require('./ms/AllMs');
const Sites = require('./sites/sites');
const HttpGet200OkSiteResponse = require('./msg/HttpGet200OkSiteResponse');


class PuppeteerHttpAllMockTerm extends ActorApi.ActorTerminating {
  constructor() {
    super(undefined, true);
    this.httpConnection = null;
    this.siteIndex = 0;
  }
  
  *data() {
    this.siteIndex = this.getTestDataNumber('site', this.siteIndex, {
      static: 0,
      image: 1,
      script: 2
    });
  }
  
  *initServer() {
    const srvAddress = this.getSrvAddress();
    console.log('srvAddress', srvAddress);
    const ifHost = `${srvAddress.domain}:${srvAddress.port}`;
    this.httpConnection = this.createServer('http', {
      messageSelector: new AllMs(ifHost)
    });
  }
  
  *run() {
    this.httpConnection.accept();
    
    const respone = this.httpConnection.receive();
    
    const site = Sites.SITES[this.siteIndex];
    this.httpConnection.send(new HttpGet200OkSiteResponse(site, 'text/html', site.length));
  }
  
  *exit(interrupted) {
    this.closeConnection(this.httpConnection);
  }
}

module.exports = PuppeteerHttpAllMockTerm;
