
const ActorApi = require('actor-api');
const ImageMs = require('./ms/ImageMs');
const HttpGet200OkSiteResponse = require('./msg/HttpGet200OkSiteResponse');


class PuppeteerHttpImageMockTerm extends ActorApi.ActorTerminating {
  constructor() {
    super();
    this.httpConnection = null;
    this.content = null;
    this.selectUri = '/example.png';
  }
  
  *data() {
    this.content = this.getContent('image', 'image');
    this.selectUri = this.getTestDataString('select-uri', this.selectUri);
  }
  
  *initServer() {
    this.httpConnection = this.createServer('http', {
      messageSelector: new ImageMs(this.selectUri)
    });
  }
  
  *run() {
    this.httpConnection.accept();

    const respone = this.httpConnection.receive();
    this.httpConnection.send(new HttpGet200OkSiteResponse(this.content, this.content.mime, this.content.size));
  }
  
  *exit(interrupted) {
    this.closeConnection(this.httpConnection);
  }
}

module.exports = PuppeteerHttpImageMockTerm;
