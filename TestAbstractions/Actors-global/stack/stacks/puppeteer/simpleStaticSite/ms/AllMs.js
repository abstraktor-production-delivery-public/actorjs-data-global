
'use strict';

const PuppeteerApi = require('puppeteer-stack-api');


class AllMs extends PuppeteerApi.PuppeteerMessageSelector {
  constructor(select) {
    super();
    this.select = select;
  }
  
  onSelect(msg, HttpApi, WebsocketApi) {
    if(msg instanceof HttpApi.Request) {
      console.log(msg.getHeader('Host'), this.select, this.select === msg.getHeader('Host'));
      return this.select === msg.getHeader('Host');
    }
    else {
      return false;
    }
  }
}

module.exports = AllMs;

