
'use strict';

const PuppeteerApi = require('puppeteer-stack-api');


class ImageMs extends PuppeteerApi.PuppeteerMessageSelector {
  constructor(select) {
    super();
    this.select = select;
  }
  
  onSelect(msg, HttpApi, WebsocketApi) {
    if(msg instanceof HttpApi.Request) {
      return msg.requestTarget.startsWith(this.select);
    }
    else {
      return false;
    }
  }
}

module.exports = ImageMs;

