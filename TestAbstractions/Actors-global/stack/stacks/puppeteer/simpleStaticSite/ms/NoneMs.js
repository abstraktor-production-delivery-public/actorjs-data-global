
'use strict';

const PuppeteerApi = require('puppeteer-stack-api');


class NoneMs extends PuppeteerApi.PuppeteerMessageSelector {
  constructor() {
    super();
  }
  
  onSelect(msg, HttpApi, WebsocketApi) {
    return false;
  }
}

module.exports = NoneMs;

