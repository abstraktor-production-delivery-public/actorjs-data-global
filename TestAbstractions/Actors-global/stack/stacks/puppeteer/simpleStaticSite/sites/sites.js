

class Sites {}

Sites.STATIC_SITE = `<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>title</title>
  </head>
  <body>
    <p>
      HELLO
    </p>
  </body>
</html>`;

Sites.STATIC_IMAGE_SITE = `<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>title</title>
  </head>
  <body>
    <p>
      HELLO
    </p>
    <img src="/example.jpg">
  </body>
</html>`;

Sites.STATIC_SCRIPT_SITE = `<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>title</title>
    <script src="/scripts/3ppLayer-client.js" type="text/javascript" defer></script>
  </head>
  <body>
    <p>
      HELLO
    </p>
  </body>
</html>`;

Sites.SITES = [
  Sites.STATIC_SITE,
  Sites.STATIC_IMAGE_SITE,
  Sites.STATIC_SCRIPT_SITE
];

module.exports = Sites;
