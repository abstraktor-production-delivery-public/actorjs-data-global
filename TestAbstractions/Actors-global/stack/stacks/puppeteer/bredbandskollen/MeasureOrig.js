
const ActorApi = require('actor-api');


class MeasureOrig extends ActorApi.ActorOriginating {
  constructor() {
    super();
    this.puppeteerConnection = null;
  }
  
  *data() {
  }
  
  *initClient() {
    this.puppeteerConnection = this.createConnection('puppeteer');
  }
  
  *run() {
    const page = this.puppeteerConnection.page;
    yield page.waitForFunction(() => {const button = document.getElementById("mainStartTest"); return button ? button.innerText === "Starta mätningen!" : false;});
    yield page.click('#mainStartTest');
    yield page.waitForFunction(() => {return document.getElementById("mainStartTest").innerText === "Mät igen";});
  }
  
  *exit(interrupted) {
    this.closeConnection(this.puppeteerConnection);
  }
}

module.exports = MeasureOrig;
