
const ActorApi = require('actor-api');


class StartPagePre extends ActorApi.ActorPartPre {
  constructor() {
    super();
  }
  
  *run() {
    const page = this.parent.puppeteerConnection.page;
    yield this.all([
      page.waitForNavigation({timeout: 10000}),
      page.click('.w-button');
    ]);
  }
}

module.exports = StartPagePre;
