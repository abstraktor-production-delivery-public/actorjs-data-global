
const ActorApi = require('actor-api');
const StackApi = require('stack-api');


class SocketObjectTerm extends ActorApi.ActorTerminating {
  constructor() {
    super();
    this.socketConnection = null;
    this.networkType = StackApi.NetworkType.TCP;
  }

  *data() {
    this.networkType = this.getTestDataNumber('network-type', 'tcp', {
      tcp: StackApi.NetworkType.TCP,
      udp: StackApi.NetworkType.UDP
    });
  }
    
  *initServer() {
    this.socketConnection = this.createServer('socket', {
      networkType: this.networkType
    });
  }
  
  *run() {
    this.socketConnection.accept();
    
    const cmd = this.socketConnection.receiveObject();
    
    VERIFY_VALUE('login', cmd.cmd);
    VERIFY_VALUE('Mercedes-Benz', cmd.name);
    
    this.socketConnection.sendObject({login: 'ok'});
  }
  
  *exit(interrupted) {
    this.closeConnection(this.socketConnection);  
  }
}

module.exports = SocketObjectTerm;
