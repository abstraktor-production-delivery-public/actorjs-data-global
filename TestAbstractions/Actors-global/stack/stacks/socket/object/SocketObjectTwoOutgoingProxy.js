
const ActorApi = require('actor-api');
const StackApi = require('stack-api');


class SocketObjectTwoOutgoingProxy extends ActorApi.ActorProxy {
  constructor() {
    super();
    this.socketServerConnection = null;
    this.socket1ClientConnection = null;
    this.socket2ClientConnection = null;
    this.networkTypeServer = StackApi.NetworkType.TCP;
    this.networkTypeClient = StackApi.NetworkType.TCP;
  }

  *data() {
    this.networkTypeServer = this.getTestDataNumber('network-type-server', 'tcp', {
      tcp: StackApi.NetworkType.TCP,
      udp: StackApi.NetworkType.UDP
    });
    this.networkTypeClient = this.getTestDataNumber('network-type-client', 'tcp', {
      tcp: StackApi.NetworkType.TCP,
      udp: StackApi.NetworkType.UDP
    });
  }
  
  *initServer() {
    this.socketServerConnection = this.createServer('socket', {
      networkType: this.networkTypeServer
    });
  }
  
  *initClient() {
    this.socket1ClientConnection = this.createConnection('socket', {
      networkType: this.networkTypeClient
    });
    this.socket2ClientConnection = this.createConnection('socket', {
      networkType: this.networkTypeClient,
      srcIndex: 1,
      dstIndex: 1
    });
  }
  
  *run() {
    this.socketServerConnection.accept();
    const cmd = this.socketServerConnection.receiveObject();
    
    VERIFY_VALUE('login', cmd.cmd);
    VERIFY_VALUE('Mercedes-Benz', cmd.name);
    
    this.socket1ClientConnection.sendObject(cmd);
    const result1 = this.socket1ClientConnection.receiveObject();
    VERIFY_VALUE('ok', result1.login);
    
    this.socket2ClientConnection.sendObject(cmd);
    const result2 = this.socket2ClientConnection.receiveObject();
    VERIFY_VALUE('ok', result2.login);

    this.socketServerConnection.sendObject(result1);
  }
  
  *exit(interrupted) {
    this.closeConnection(this.socket2ClientConnection);
    this.closeConnection(this.socket1ClientConnection);
    this.closeConnection(this.socketServerConnection);
  }
}


module.exports = SocketObjectTwoOutgoingProxy;
