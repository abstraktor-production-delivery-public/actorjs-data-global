
const ActorApi = require('actor-api');
const StackApi = require('stack-api');


class SocketObjectOrig extends ActorApi.ActorOriginating {
  constructor() {
    super();
    this.socketConnection = null;
    this.networkType = StackApi.NetworkType.TCP;
  }

  *data() {
    this.networkType = this.getTestDataNumber('network-type', 'tcp', {
      tcp: StackApi.NetworkType.TCP,
      udp: StackApi.NetworkType.UDP
    });
  }
  
  *initClient() {
    this.socketConnection = this.createConnection('socket', {
      networkType: this.networkType
    });
  }
  
  *run() {
    this.socketConnection.sendObject({cmd: 'login', name: 'Mercedes-Benz'});
    
    const result = this.socketConnection.receiveObject();
    VERIFY_VALUE('ok', result.login);
  }
  
  *exit(interrupted) {
    this.closeConnection(this.socketConnection);
  }
}

module.exports = SocketObjectOrig;
