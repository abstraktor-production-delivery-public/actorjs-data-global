
const ActorApi = require('actor-api');


class SocketReceiveSizeOrig extends ActorApi.ActorOriginating {
  constructor() {
    super();
    this.socketConnection = null;
    this.clientMsg = '';
    this.clientSendDelays = [];
  }

  *data() {
    this.clientMsg = this.getTestDataStrings('clientMsg', ['default client message.']);
    this.clientSendDelays = this.getTestDataNumbers('clientSendDelays', () => {
      const arrays = new Array(this.clientMsg.lengt);
      arrays.fill(-1, 0, this.clientMsg.length);
      return arrays;
    });
  }
  
  *initClient() {
    this.socketConnection = this.createConnection('socket');
  }
  
  *run() {
    if(0 == this.clientMsg.length) {
      let buffer = Buffer.allocUnsafe(4);
      buffer.writeUInt32BE(0, 0);  
      this.socketConnection.send(buffer);
    }
    else {
      let length = this.clientMsg.reduce((sum, currentValue) => {
        return sum + currentValue.length;
      }, 0);
      let buffer = Buffer.allocUnsafe(4 + this.clientMsg[0].length);
      buffer.writeUInt32BE(length, 0);
      buffer.write(this.clientMsg[0], 4);
      this.delay(this.clientSendDelays[0]);
      this.socketConnection.send(buffer);
      for(let i = 1; i < this.clientMsg.length; ++i) {
        let buffer = Buffer.allocUnsafe(this.clientMsg[i].length);
        buffer.write(this.clientMsg[i], 0);
        this.delay(this.clientSendDelays[i]);
        this.socketConnection.send(buffer);
      }
    }
    
    let responseSize = this.socketConnection.receiveSize(4);
    let size = responseSize.readUInt32BE(0);
    if(0 !== size) {
      let responseData = this.socketConnection.receiveSize(size);
      VERIFY_MANDATORY('serverMsg', responseData);
    }
  }

  *exit(interrupted) {
    this.closeConnection(this.socketConnection);
  }
}

module.exports = SocketReceiveSizeOrig;
