
'use strict';

const ActorApi = require('actor-api');


class SocketHalfOpenOrig extends ActorApi.ActorOriginating {
  constructor() {
    super();
    this.socketConnection = null;
    this.closingSide = '';
  }
  
  *data() {
    this.closingSide = this.getTestDataString('closing-side');
  }
  
  *initClient() {
    this.socketConnection = this.createConnection('socket');
  }
  
  *run() {
    if('server' === this.closingSide) {
      const msg = this.socketConnection.receiveLine();
      VERIFY_VALUE('HELLO-1', msg);
      this.socketConnection.sendLine('HELLO-1');
      this.waitForSharedData('HALFOPEN_CLOSED');
      this.socketConnection.sendLine('HELLO-2');
    }
    else if('client' === this.closingSide) {
      this.socketConnection.sendLine('HELLO-1');
      const msg = this.socketConnection.receiveLine();
      VERIFY_VALUE('HELLO-1', msg);
    }
  }
  
  *exit(interrupted) {
    if('server' === this.closingSide) {
      this.closeConnection(this.socketConnection);
    }
    else if('client' === this.closingSide) {
      this.socketConnection.close();
      this.setSharedData('HALFOPEN_CLOSED');
      const msg = this.socketConnection.receiveLine();
      VERIFY_VALUE('HELLO-2', msg);
      this.closeConnection(this.socketConnection);
    }
  }
}

module.exports = SocketHalfOpenOrig;
