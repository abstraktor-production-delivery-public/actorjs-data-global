
const ActorApi = require('actor-api');


class SocketReceiveLineOrig extends ActorApi.ActorOriginating {
  constructor() {
    super();
    this.socketConnection = null;
  }

  *initClient() {
    this.socketConnection = this.createConnection('socket');
  }
  
  *run() {
    this.socketConnection.sendLine('Hello World');
    const response = this.socketConnection.receiveLine();
    VERIFY_VALUE('Hello Europe', response);
  }
  
  *exit(interrupted) {
    this.socketConnection.close();
  }
}

module.exports = SocketReceiveLineOrig;
