
const ActorApi = require('actor-api');
const StackApi = require('stack-api');


class SocketLogTerm extends ActorApi.ActorTerminating {
  constructor() {
    super();
    this.socketConnection = null;
    this.command = '';
    this.networkType = StackApi.NetworkType.TCP;
  }
  
  *data() {
    this.command = this.getTestDataString('command');
    this.testdata = this._getCommandTestData();
    this.networkType = this.getTestDataNumber('transport', 'tcp', {
      tcp: StackApi.NetworkType.TCP,
      udp: StackApi.NetworkType.UDP
    });
  }
  
  *initServer() {
    this.socketConnection = this.createServer('socket', {
      networkType: this.networkType
    });
  }
  
  *run() {
    this.socketConnection.accept();
    switch(this.command) {
      case 'sendLine':
      {
        const request = this.socketConnection.receiveLine();
        break;
      }
      case 'sendObject':
      {
        const request = this.socketConnection.receiveObject();
        break;
      }
    }
  }
  
  *exit(interrupted) {
    this.closeConnection(this.socketConnection);
  }
  
  _getCommandTestData() {
    switch(this.command) {
      case 'sendLine':
        return this.getTestDataString('data');
      case 'sendObject':
        return this.getTestDataObject('data'); 
    }
  }
}

module.exports = SocketLogTerm;
