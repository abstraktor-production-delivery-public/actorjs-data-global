
const ActorApi = require('actor-api');


class EncodingTerm extends ActorApi.ActorTerminating {
  constructor() {
    super();
    this.socketConnection = null;
    this.encoding = '';
    this.text = '';
  }
  
  *data() {
    this.encoding = this.getTestDataString('encoding');
    this.text = this.getTestDataString('text');
  }
    
  *initServer() {
    this.socketConnection = this.createServer('socket');
  }
  
  *run() {
    this.socketConnection.accept();
    const msg = this.socketConnection.receiveLine();
    
    VERIFY_VALUE(this.text, msg);
  }
  
  *exit(interrupted) {
    this.closeConnection(this.socketConnection);
  }
}

module.exports = EncodingTerm;
