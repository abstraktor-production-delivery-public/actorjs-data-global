
const ActorApi = require('actor-api');


class SocketReceiveTerm extends ActorApi.ActorTerminating {
  constructor() {
    super();
    this.socketConnection = null;
    this.sendingSide = '';
    this.whatToSend = '';
  }
  
  *data() {
    this.sendingSide = this.getTestDataString('sending-side');
    this.whatToSend = this.getTestDataString('what-to-send');
  }
    
  *initServer() {
    this.socketConnection = this.createServer('socket');
  }
  
  *run() {
    this.socketConnection.accept();
    
    if('client' === this.sendingSide) {
      this.waitForSharedData('MSG_SENT');
      const msg = this.socketConnection.receive();
      if('BEFORE' === this.whatToSend) {
        VERIFY_VALUE('MESSAGE-1', msg);
      }
      else if('SINGLE' === this.whatToSend) {
        VERIFY_VALUE('MESSAGE-1', msg);
      }
      else if('DOUBLE' === this.whatToSend) {
        VERIFY_VALUE('MESSAGE-1MESSAGE-2', msg);
      }
      else if('DOUBLE_BOTH' === this.whatToSend) {
        VERIFY_VALUE('MESSAGE-1', msg);
        this.setSharedData('MSG_RECEIVED');
        const msg2 = this.socketConnection.receive();
        VERIFY_VALUE('MESSAGE-2', msg2);
      }
    }
    else if('server' === this.sendingSide) {
      if('BEFORE' === this.whatToSend) {
        this.setSharedData('MSG_SENT');
        this.socketConnection.send('MESSAGE-1');
      }
      else if('SINGLE' === this.whatToSend) {
        this.socketConnection.send('MESSAGE-1');
        this.setSharedData('MSG_SENT');
      }
      else if('DOUBLE' === this.whatToSend) {
        this.socketConnection.send('MESSAGE-1');
        this.socketConnection.send('MESSAGE-2');
        this.setSharedData('MSG_SENT');
      }
      else if('DOUBLE_BOTH' === this.whatToSend) {
        this.socketConnection.send('MESSAGE-1');
        this.setSharedData('MSG_SENT');
        this.waitForSharedData('MSG_RECEIVED');
        this.socketConnection.send('MESSAGE-2');
      }
    }
  }
  
  *exit(interrupted) {
    this.closeConnection(this.socketConnection);
  }
}

module.exports = SocketReceiveTerm;
