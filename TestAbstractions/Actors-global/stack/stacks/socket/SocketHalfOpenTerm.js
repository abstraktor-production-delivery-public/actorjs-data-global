
'use strict';

const ActorApi = require('actor-api');


class SocketHalfOpenTerm extends ActorApi.ActorTerminating {
  constructor() {
    super();
    this.socketConnection = null;
    this.closingSide = '';
  }
  
  *data() {
    this.closingSide = this.getTestDataString('closing-side');
  }
  
  *initServer() {
    this.socketConnection = this.createServer('socket');
  }
  
  *run() {
    this.socketConnection.accept();
    
    if('server' === this.closingSide) {
      this.socketConnection.sendLine('HELLO-1');
      const msg = this.socketConnection.receiveLine();
      VERIFY_VALUE('HELLO-1', msg);
    }
    else if('client' === this.closingSide) {
      const msg = this.socketConnection.receiveLine();
      VERIFY_VALUE('HELLO-1', msg);
      this.socketConnection.sendLine('HELLO-1');
      this.waitForSharedData('HALFOPEN_CLOSED');
      this.socketConnection.sendLine('HELLO-2');
    }
  }
  
  *exit(interrupted) {
    if('server' === this.closingSide) {
      this.socketConnection.close();
      this.setSharedData('HALFOPEN_CLOSED');
      const msg = this.socketConnection.receiveLine();
      VERIFY_VALUE('HELLO-2', msg);
      this.closeConnection(this.socketConnection);
    }
    else if('client' === this.closingSide) {
      this.closeConnection(this.socketConnection);
    }
  }
}

module.exports = SocketHalfOpenTerm;
