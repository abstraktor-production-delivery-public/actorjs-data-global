
const ActorApi = require('actor-api');


class SocketReceiveLineTerm extends ActorApi.ActorTerminating {
  constructor() {
    super();
    this.socketConnection = null;
  }
 
  *initServer() {
    this.socketConnection = this.createServer('socket');
  }
  
  *run() {
    this.socketConnection.accept();
    const request = this.socketConnection.receiveLine();
    VERIFY_VALUE('Hello World', request);
    this.socketConnection.sendLine('Hello Europe');
  }
  
  *exit(interrupted) {
    this.socketConnection.close();
  }
}

module.exports = SocketReceiveLineTerm;
