
const ActorApi = require('actor-api');
const StackApi = require('stack-api');


class SocketAcceptOrig extends ActorApi.ActorOriginating {
  constructor() {
    super();
    this.socketConnection = null;
    this.networkType = StackApi.NetworkType.TCP;
    this.acceptBeforeConnect = false;
  }
  
  *data() {
    this.networkType = this.getTestDataNumber('network_type', 'tcp', {
      tcp: StackApi.NetworkType.TCP,
      udp: StackApi.NetworkType.UDP
    });
    this.acceptBeforeConnect = this.getTestDataBoolean('accept_before_connect', false);
  }
  
  *initClient() {
    if(this.acceptBeforeConnect) {
      this.waitForSharedData('ACCEPTED');
    }
    this.socketConnection = this.createConnection('socket', {
      networkType: this.networkType
    });
    this.setSharedData('CONNECTED');
  }
  
  *run() {
  }
  
  *exit(interrupted) {
    this.closeConnection(this.socketConnection);
  }
}

module.exports = SocketAcceptOrig;
