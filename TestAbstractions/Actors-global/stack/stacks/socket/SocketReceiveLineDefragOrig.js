
const ActorApi = require('actor-api');
const StackApi = require('stack-api');


class SocketReceiveLineDefragOrig extends ActorApi.ActorOriginating {
  constructor() {
    super();
    this.socketConnection = null;
    this.clientNetworkType = StackApi.NetworkType.TCP;
    this.clientMsg = [['']];
    this.clientSendDelays = [[-1]];
    this.expectedServerMsg = [['']];
    this.clientReceiveDelays = [[-1]];
  }
  
  *data() {
    this.clientNetworkType = this.getTestDataNumber('transport', 'tcp', {
      tcp: StackApi.NetworkType.TCP,
      udp: StackApi.NetworkType.UDP
    });
    this.clientMsg = this.getTestDataArrayStrings('clientMsg');
    this.clientSendDelays = this.getTestDataArrayNumbers('clientSendDelays', () => {
      const arrays = [];
      this.clientMsg.forEach((array, index) => {
        arrays.push(Array(this.clientMsg[index].length).fill(-1));
      });
      return arrays;
    });
    this.expectedServerMsg = this.getVerificationData('clientMsg');
    this.clientReceiveDelays = this.getTestDataNumbers('clientReceiveDelays', () => {
      const arrays = [];
      this.expectedServerMsg.forEach((array, index) => {
        arrays.push(Array(this.expectedServerMsg[index].length).fill(-1));
      });
      return arrays;
    });
  }
  
  *initClient() {
    this.socketConnection = this.createConnection('socket', {
      networkType: this.clientNetworkType
    });
  }
  
  *run() {
    this.delay(this.preDelay);
    
    let iter = 0;
    let sendDone = false;
    let receiveDone = false;
    while(!sendDone || !receiveDone) {
      if(iter < this.clientMsg.length) {
        for(let j = 0; j < this.clientMsg[iter].length; ++j) {
          this.delay(this.clientSendDelays[iter][j]);
          this.socketConnection.send(this.clientMsg[iter][j]);
        }  
      }
      else {
        sendDone = true;
      }
      if(iter < this.expectedServerMsg.length) {
        for(let e = 0; e < this.expectedServerMsg[iter].length; ++e) {
          this.delay(this.clientReceiveDelays[iter][e]);
          let request = this.socketConnection.receiveLine();
          VERIFY_MANDATORY('serverMsg', request, iter, e);
        }
      }
      else {
        receiveDone = true;
      }
      ++iter;  
    }
  }
  
  *exit(interrupted) {
    this.closeConnection(this.socketConnection);
  }
}

module.exports = SocketReceiveLineDefragOrig;
