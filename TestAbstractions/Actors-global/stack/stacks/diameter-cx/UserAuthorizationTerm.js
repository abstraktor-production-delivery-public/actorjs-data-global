
const ActorApi = require('actor-api');
const DiameterApi = require('diameter-stack-api');
const UserAuthorizationAnswer = require('./msg/UserAuthorizationAnswer');


class UserAuthorizationTerm extends ActorApi.ActorTerminating {
  constructor() {
    super();
    this.diameterConnection = null;
  }

  *data() {
  }
  
  *initServer() {
    this.diameterConnection = this.createServer('diameter-cx');
  }
  
  *run() {
    this.diameterConnection.accept();
    
    const request = this.diameterConnection.receive();
    
    this.diameterConnection.send(new UserAuthorizationAnswer(DiameterApi.DiameterConstResultCodes.DIAMETER_SUCCESS));
  }
  
  *exit(interrupted) {
    this.closeConnection(this.diameterConnection);
  }
}

module.exports = UserAuthorizationTerm;
