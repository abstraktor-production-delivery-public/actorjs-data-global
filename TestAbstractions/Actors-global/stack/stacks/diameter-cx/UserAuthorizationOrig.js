
const ActorApi = require('actor-api');
const UserAuthorizationRequest = require('./msg/UserAuthorizationRequest');


class UserAuthorizationOrig extends ActorApi.ActorOriginating {
  constructor() {
    super();
    this.diameterConnection = null;
  }

  *data() {
  }
  
  *initClient() {
    this.diameterConnection = this.createConnection('diameter-cx');
  }
  
  *run() {
    this.diameterConnection.send(new UserAuthorizationRequest('Gunnar'));
    
    const answer = this.diameterConnection.receive();
  }
  
  *exit(interrupted) {
    this.closeConnection(this.diameterConnection);
  }
}

module.exports = UserAuthorizationOrig;
