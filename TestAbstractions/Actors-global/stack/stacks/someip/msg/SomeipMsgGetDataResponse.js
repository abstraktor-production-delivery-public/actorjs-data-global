
'use strict';

const SomeipApi = require('someip-stack-api');


class SomeipMsgGetDataResponse extends SomeipApi.SomeipMsgResponse {
  constructor() {
    super();
  }
}

module.exports = SomeipMsgGetDataResponse;
