
const ActorApi = require('actor-api');
const SomeipMsgGetDataRequest = require('./msg/SomeipMsgGetDataRequest');


class SomeIpConnectionBasicOrig extends ActorApi.ActorOriginating {
  constructor() {
    super();
    this.someipConnection = null;
  }
  
  *initClient() {
    this.someipConnection = this.createConnection('someip');
  }
  
  *run() {
    this.someipConnection.send(new SomeipMsgGetDataRequest(1, JSON.stringify({
      name: 'testname',
      id: 1
    })));
  }
  
  *exit(interrupted) {
    this.closeConnection(this.someipConnection);
  }
}

module.exports = SomeIpConnectionBasicOrig;
