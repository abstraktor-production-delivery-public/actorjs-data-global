
const ActorApi = require('actor-api');
const MqttApi = require('mqtt-stack-api');


class ConnectOrig extends ActorApi.ActorOriginating {
  constructor() {
    super();
    this.mqttConnection = null;
  }
  
  *data() {
  }
  
  *initClient() {
    this.mqttConnection = this.createConnection('mqtt');
  }
  
  *run() {
    this.mqttConnection.send(new MqttApi.msg.Subscribe());
  }
  
  *exit(interrupted) {
    this.closeConnection(this.mqttConnection);
  }
}

module.exports = ConnectOrig;
