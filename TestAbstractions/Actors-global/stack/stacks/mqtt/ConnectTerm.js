
const ActorApi = require('actor-api');


class ConnectTerm extends ActorApi.ActorTerminating {
  constructor() {
    super();
    this.mqttConnection = null;
  }
  
  *data() {
  }
  
  *initServer() {
    this.mqttConnection = this.createServer('mqtt');
  }
  
  *run() {
    this.mqttConnection.accept();
  }
  
  *exit(interrupted) {
    this.closeConnection(this.mqttConnection);
  }
}

module.exports = ConnectTerm;
