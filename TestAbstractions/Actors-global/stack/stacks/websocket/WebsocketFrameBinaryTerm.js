
const ActorApi = require('actor-api');
const StackApi = require('stack-api');
const HttpSwitchingProtocolsResponse = require('./msg/HttpSwitchingProtocolsResponse');


class WebsocketFrameBinaryTerm extends ActorApi.ActorTerminating {
  constructor() {
    super();
    this.httpConnection = null;
    this.websocketConnection = null;
    this.wsProtocol = 'demo';
    this.wsText = 'Hello';
  }
  
  *data() {
    this.wsProtocol = this.getTestDataString('ws-protocol', this.wsProtocol);
    this.wsText = this.getTestDataString('ws-text', this.wsText);
  }
  
  *initServer() {
    this.httpConnection = this.createServer('http');
  }
  
  *run() {
    this.httpConnection.accept();
    
    const request = this.httpConnection.receive();
    
    this.httpConnection.send(new HttpSwitchingProtocolsResponse(this.wsProtocol));
    this.websocketConnection = this.switchProtocol('websocket', this.httpConnection);
    
    const binaryFrame = this.websocketConnection.receive();
    const encode = this._encodeBinary()
    const decode = this._decodeBinary(1 === binaryFrame.mask ? binaryFrame.payloadDataUnmasked : binaryFrame.payloadData);
    VERIFY_VALUE(encode.a, decode.a);
    VERIFY_VALUE(encode.b, decode.b);
    VERIFY_VALUE(encode.c, decode.c);
    VERIFY_VALUE(encode.length, decode.length);
    VERIFY_VALUE(encode.data, decode.data);
    
    const closeFrame = this.websocketConnection.receive();
  }
  
  *exit(interrupted) {
    this.closeConnection(this.websocketConnection);
  }
  
  _encodeBinary() {
    return {
      a: 1,
      b: 1,
      c: 1,
      length: Buffer.byteLength(this.wsText),
      data: this.wsText
    };
  }
  
  _decodeBinary(payload) {
    const a = StackApi.BitByte.getBit(payload[0], 0);
    const b = StackApi.BitByte.getBit(payload[0], 1);
    const c = StackApi.BitByte.getBit(payload[0], 2);
    const length = StackApi.BitByte.getBits(payload[0], 3, 7);
    const data = payload.slice(1, length + 1);
    return {
      a: a,
      b: b,
      c: c,
      length: length,
      data: data
    };
  }
}

module.exports = WebsocketFrameBinaryTerm;
