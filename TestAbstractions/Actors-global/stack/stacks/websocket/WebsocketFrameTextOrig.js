
const ActorApi = require('actor-api');
const WebsocketStackApi = require('websocket-stack-api');
const HttpGetUpgradeRequest = require('./msg/HttpGetUpgradeRequest');


class WebsocketFrameTextOrig extends ActorApi.ActorOriginating {
  constructor() {
    super();
    this.httpConnection = null;
    this.websocketConnection = null;
    this.requistUri = 'http://example.com';
    this.wsProtocol = 'demo';
    this.wsMask = false;
    this.wsText = 'Hello';
  }
  
  *data() {
    this.requistUri = this.getTestDataString('request-uri', this.requistUri);
    this.wsProtocol = this.getTestDataString('ws-protocol', this.wsProtocol);
    this.wsMask = this.getTestDataBoolean('ws-mask', this.wsMask);
    this.wsText = this.getTestDataString('ws-text', this.wsText);
  }
  
  *initClient() {
    this.httpConnection = this.createConnection('http');
  }
  
  *run() {
    this.httpConnection.send(new HttpGetUpgradeRequest(this.requistUri, this.wsProtocol));
 
    const response = this.httpConnection.receive();
    this.websocketConnection = this.switchProtocol('websocket', this.httpConnection);
    
    this.websocketConnection.send(new WebsocketStackApi.TextFrame(this.wsText, this.wsMask));
    
    this.websocketConnection.send(new WebsocketStackApi.CloseFrame());
  }
  
  *exit(interrupted) {
    this.closeConnection(this.websocketConnection);
  }
}

module.exports = WebsocketFrameTextOrig;
