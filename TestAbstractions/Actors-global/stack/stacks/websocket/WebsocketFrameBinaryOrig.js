
const ActorApi = require('actor-api');
const StackApi = require('stack-api');
const WebsocketStackApi = require('websocket-stack-api');
const HttpGetUpgradeRequest = require('./msg/HttpGetUpgradeRequest');


class WebsocketFrameBinaryOrig extends ActorApi.ActorOriginating {
  constructor() {
    super();
    this.httpConnection = null;
    this.websocketConnection = null;
    this.requistUri = 'http://example.com';
    this.wsProtocol = 'demo';
    this.wsMask = false;
    this.wsText = 'Hello';
  }
  
  *data() {
    this.requistUri = this.getTestDataString('request-uri', this.requistUri);
    this.wsProtocol = this.getTestDataString('ws-protocol', this.wsProtocol);
    this.wsMask = this.getTestDataBoolean('ws-mask', this.wsMask);
    this.wsText = this.getTestDataString('ws-text', this.wsText);
  }
  
  *initClient() {
    this.httpConnection = this.createConnection('http');
  }
  
  *run() {
    this.httpConnection.send(new HttpGetUpgradeRequest(this.requistUri, this.wsProtocol));
 
    const response = this.httpConnection.receive();
    this.websocketConnection = this.switchProtocol('websocket', this.httpConnection);
    
    this.websocketConnection.send(new WebsocketStackApi.BinaryFrame(this._encodeBinary(), this.wsMask));
    
    this.websocketConnection.send(new WebsocketStackApi.CloseFrame());
  }
  
  *exit(interrupted) {
    this.closeConnection(this.websocketConnection);
  }
  
  _encodeBinary() {
    const dataLength = Buffer.byteLength(this.wsText);
    const buffer = Buffer.allocUnsafe(1 + dataLength);
    buffer[0] = StackApi.BitByte.setBit(buffer[0], 0);
    buffer[0] = StackApi.BitByte.setBit(buffer[0], 1);
    buffer[0] = StackApi.BitByte.setBit(buffer[0], 2);
    buffer[0] = StackApi.BitByte.setBits(buffer[0], 3, 7, dataLength);
    buffer.write(this.wsText, 1);
    return buffer;
  }
}

module.exports = WebsocketFrameBinaryOrig;
