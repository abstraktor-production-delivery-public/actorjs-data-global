
const ActorApi = require('actor-api');
const HttpSwitchingProtocolsResponse = require('./msg/HttpSwitchingProtocolsResponse');


class WebsocketFrameTextTerm extends ActorApi.ActorTerminating {
  constructor() {
    super();
    this.httpConnection = null;
    this.websocketConnection = null;
    this.wsProtocol = 'demo';
  }
  
  *data() {
    this.wsProtocol = this.getTestDataString('ws-protocol', this.wsProtocol);
  }
    
  *initServer() {
    this.httpConnection = this.createServer('http');
  }
  
  *run() {
    this.httpConnection.accept();
    
    const request = this.httpConnection.receive();
    
    this.httpConnection.send(new HttpSwitchingProtocolsResponse(this.wsProtocol));
    this.websocketConnection = this.switchProtocol('websocket', this.httpConnection);
    
    const textFrame = this.websocketConnection.receive();
    VERIFY_OPTIONAL('ws-text', 1 === textFrame.mask ? textFrame.payloadDataUnmasked : textFrame.payloadData);
    
    const closeFrame = this.websocketConnection.receive();
  }
  
  *exit(interrupted) {
    this.closeConnection(this.websocketConnection);
  }
}

module.exports = WebsocketFrameTextTerm;
