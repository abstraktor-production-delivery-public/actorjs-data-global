
const DiameterApi = require('diameter-stack-api');


class CapabilitiesExchangeRequest extends DiameterApi.msg.CapabilitiesExchangeRequest {
  constructor(originHost, originRealm, hostIPAddress, vendorId, productName) {
    super(16777216);
    this.addAvpOriginHost(originHost);
    this.addAvpOriginRealm(originRealm);
    this.addAvpHostIPAddress(hostIPAddress);
    this.addAvpVendorId(vendorId);
    this.addAvpProductName(productName);
  }
}

module.exports = CapabilitiesExchangeRequest;
