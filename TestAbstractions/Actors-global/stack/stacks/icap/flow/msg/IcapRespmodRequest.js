
'use strict';

const IcapApi = require('icap-stack-api');


class IcapRespmodRequest extends IcapApi.Request {
  constructor(requestTarget, host, httpRequest, httpResponse) {
    super(IcapApi.Method.RESPMOD, requestTarget, IcapApi.Version.ICAP_1_0);
    this.addHeader(IcapApi.Header.HOST, host);
    if(httpRequest) {
      this.addHttpRequest(httpRequest);
    }
    if(httpResponse) {
      this.addHttpResponse(httpResponse);
    }
  }
}

module.exports = IcapRespmodRequest;
