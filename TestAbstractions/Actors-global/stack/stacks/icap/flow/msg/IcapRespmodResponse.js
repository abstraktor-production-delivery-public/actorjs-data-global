
'use strict';

const IcapApi = require('icap-stack-api');


class IcapRespmodResponse extends IcapApi.Response {
  constructor(httpResponse, statusCode=IcapApi.StatusCode.OK, reasonPhrase=IcapApi.ReasonPhrase.OK) {
    super(IcapApi.Version.ICAP_1_0, statusCode, reasonPhrase);
    if(httpResponse) {
      this.addHttpResponse(httpResponse);
    }
  }
}

module.exports = IcapRespmodResponse;
