
'use strict';

const ActorApi = require('actor-api');
const IcapRespmodRequest = require('./msg/IcapRespmodRequest');
const HttpGetRequest = require('../../http/flow/msg/HttpGetRequest');
const HttpGet200Response = require('../../http/flow/msg/HttpGet200Response');


class IcapRespmodOkOrig extends ActorApi.ActorOriginating {
  constructor() {
    super();
    this.icapConnection = null;
    this.icapHost = 'actorjs.com'
    this.icapRequistUri = 'ai-image';
    this.httpHost = 'actorjs.com';
    this.requistUri = 'image';
    this.content = null;
  }
  
  *data() {
    this.icapHost = this.getTestDataString('icap-host', this.icapHost);
    this.icapRequistUri = this.getTestDataString('icap-request-uri', this.icapRequistUri);
    this.httpHost = this.getTestDataString('http-host', this.httpHost);
    this.requistUri = this.getTestDataString('http-request-uri', this.requistUri);
    this.content = this.getContentByName('flag_sweden', 'image');
  }
  
  *initClient() {
    this.icapConnection = this.createConnection('icap');
  }
  
  *run() {
    this.icapConnection.send(new IcapRespmodRequest(`http://${this.icapHost}/${this.icapRequistUri}`, this.icapHost, new HttpGetRequest(`http://${this.httpHost}/${this.requistUri}`, this.httpHost), new HttpGet200Response(this.content)));
    const request = this.icapConnection.receive();
  }
  
  *exit(interrupted) {
    this.closeConnection(this.icapConnection);
  }
}

module.exports = IcapRespmodOkOrig;
