
'use strict';

const ActorApi = require('actor-api');
const IcapApi = require('icap-stack-api');
const IcapRespmodResponse = require('./msg/IcapRespmodResponse');
const HttpGet200Response = require('./msg/HttpGet200Response');


class IcapRespmodOkTerm extends ActorApi.ActorTerminating {
  constructor() {
    super();
    this.icapConnection = null;
    this.content = null;
  }
  
  *data() {
    this.content = this.getContentByName('flag_denmark', 'image');
  }
    
  *initServer() {
    this.icapConnection = this.createServer('icap');
  }
  
  *run() {
    this.icapConnection.accept();
    
    const request = this.icapConnection.receive();
    const httpResponse = request.getHttpResponse();
    this.icapConnection.send(new IcapRespmodResponse(new HttpGet200Response(this.content)));
  }
  
  *exit(interrupted) {
    this.closeConnection(this.icapConnection);
  }
}

module.exports = IcapRespmodOkTerm;
