
const ActorApi = require('actor-api');
const HttpApi = require('http-stack-api');
const HttpGet200OkResponse = require('./msg/HttpGet200OkResponse');


class HttpGetOkServerProxy extends ActorApi.ActorProxy {
  constructor() {
    super();
    this.httpServerConnection = null;
    this.content = null;
  }
  
  *data() {
    this.content = this.getContent('content-name');
  }
  
  *initServer() {
    this.httpServerConnection = this.createServer('http');
  }
    
  *run() {
    this.httpServerConnection.accept();
    
    const request = this.httpServerConnection.receive();
   // VERIFY_VALUE(0, request.getHeaderNumber(HttpApi.Header.CONTENT_LENGTH), 'HTTP header Content-Length:');
    
    this.httpServerConnection.send(new HttpGet200OkResponse(this.content));
  }
  
  *exit(interrupted) {
    this.closeConnection(this.httpServerConnection);
  }
}

module.exports = HttpGetOkServerProxy;
