
'use strict';

const HttpApi = require('http-stack-api');


class HttpGetRequest extends HttpApi.Request {
  constructor(requestTarget, host) {
    super(HttpApi.Method.GET, requestTarget, HttpApi.Version.HTTP_1_1);
    this.addHeader(HttpApi.Header.HOST, host);
  }
}

module.exports = HttpGetRequest;
