
'use strict';

const ActorApi = require('actor-api');
const HttpGet200Response = require('./msg/HttpGet200Response');


class HttpGetOkTerm extends ActorApi.ActorTerminating {
  constructor() {
    super();
    this.httpConnection = null;
    this.content = null;
  }
  
  *data() {
    this.content = this.getContentByName('flag_sweden', 'image');
  }
    
  *initServer() {
    this.httpConnection = this.createServer('http');
  }
  
  *run() {
    this.httpConnection.accept();
    
    const request = this.httpConnection.receive();
    
    this.httpConnection.send(new HttpGet200Response(this.content));
  }
  
  *exit(interrupted) {
    this.closeConnection(this.httpConnection);
  }
}

module.exports = HttpGetOkTerm;
