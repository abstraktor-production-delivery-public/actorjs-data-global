
'use strict';

const ActorApi = require('actor-api');
const HttpGetRequest = require('./msg/HttpGetRequest');


class HttpGetOkOrig extends ActorApi.ActorOriginating {
  constructor() {
    super();
    this.httpConnection = null;
    this.httpHost = 'actorjs.com';
    this.requistUri = 'image';
  }
  
  *data() {
    this.httpHost = this.getTestDataString('http-host', this.httpHost);
    this.requistUri = this.getTestDataString('http-request-uri', this.requistUri);
  }
  
  *initClient() {
    this.httpConnection = this.createConnection('http');
  }
  
  *run() {
    this.httpConnection.send(new HttpGetRequest(`http://${this.httpHost}/${this.requistUri}`, this.httpHost));
    
    const response = this.httpConnection.receive();
  }
  
  *exit(interrupted) {
    this.closeConnection(this.httpConnection);
  }
}

module.exports = HttpGetOkOrig;
