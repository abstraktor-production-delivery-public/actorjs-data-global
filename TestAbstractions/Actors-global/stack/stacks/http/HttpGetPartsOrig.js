
const ActorApi = require('actor-api');
const HttpMsgGetRequest = require('./msg/HttpMsgGetRequest');


class HttpGetPartsOrig extends ActorApi.ActorOriginating {
constructor() {
    super();
    this.httpConnection = null;
    this.requistUri = '';
  }
  
  *data() {
    this.requistUri = this.getTestDataString('Request-URI', '/demo');
  }
    
  *initClient() {
    this.httpConnection = this.createConnection('http');
  }
  
  *run() {
    this.httpConnection.sendRequestLine(new HttpMsgGetRequest(this.requistUri));
    this.httpConnection.sendHeaders();
  }
  
  *exit(interrupted) {
    this.closeConnection(this.httpConnection);
  }
}

module.exports = HttpGetPartsOrig;
