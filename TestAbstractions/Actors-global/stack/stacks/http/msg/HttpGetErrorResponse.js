
'use strict';

const HttpApi = require('http-stack-api');


class HttpGetErrorResponse extends HttpApi.Response {
  constructor(statusCode, reasonPhrase) {
    super(HttpApi.Version.HTTP_1_1, statusCode, reasonPhrase);
    this.addHeader(HttpApi.Header.CONTENT_LENGTH, 0);
  }
}

module.exports = HttpGetErrorResponse;
