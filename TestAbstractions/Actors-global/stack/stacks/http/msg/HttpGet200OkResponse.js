
'use strict';

const HttpApi = require('http-stack-api');


class HttpGet200OkResponse extends HttpApi.Response {
  constructor(content) {
    super(HttpApi.Version.HTTP_1_1, HttpApi.StatusCode.OK, HttpApi.ReasonPhrase.OK);
    this.addHeader(HttpApi.Header.CONTENT_TYPE, content.mime);
    this.addHeader(HttpApi.Header.CONTENT_LENGTH, content.size);
    this.addBody(content);
  }
}

module.exports = HttpGet200OkResponse;
