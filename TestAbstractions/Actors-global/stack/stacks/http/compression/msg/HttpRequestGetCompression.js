
'use strict';

const HttpApi = require('http-stack-api');


class HttpRequestGetCompressionextends HttpApi.Request {
  constructor(requestUri) {
    super(HttpApi.Method.GET, requestUri, HttpApi.Version.HTTP_1_1);
    this.addHeader(HttpApi.Header.HOST, host);
    this.addHeader(HttpApi.Header.CONNECTION, 'keep-alive');
    this.addHeader(HttpApi.Header.ACCEPT, 'application/json');
    this.addHeader(HttpApi.Header.USER_AGENT, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4298.0 Safari/537.36');
    this.addHeader(HttpApi.Header.CONTENT_TYPE, 'application/json');
    this.addHeader(HttpApi.Header.ORIGIN, `http://${host}`);
    this.addHeader(HttpApi.Header.ACCEPT_ENCODING, 'gzip, deflate');
    this.addHeader(HttpApi.Header.ACCEPT_LANGUAGE, 'en-GB,en-US;q=0.9,en;q=0.8');
    
    const request = JSON.stringify(new ActorjsRequest(id, ...serviceActions));
    this.httpRequest.addHeader(HttpApi.Header.CONTENT_LENGTH, request.length);
    this.httpRequest.addBody(request);
  }
  
  _generateBody() {
    this.id = id;
    this.requests = serviceActions;
    let i = -1;
    this.requests.forEach((req) => {
      req.index = ++i;
    });
   
  }
}

module.exports = HttpRequestGetCompression;
