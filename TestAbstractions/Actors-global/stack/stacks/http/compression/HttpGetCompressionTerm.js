
const ActorApi = require('actor-api');


class HttpGetCompressionTerm extends ActorApi.ActorTerminating {
  constructor() {
    super();
    this.httpConnection = null;
  }
  
  *data() {
  }
    
  *initServer() {
    this.httpConnection = this.createServer('http');
  }
  
  *run() {
    this.httpConnection.accept();
  }
  
  *exit(interrupted) {
    this.closeConnection(this.httpConnection);
  }
}

module.exports = HttpGetCompressionTerm;
