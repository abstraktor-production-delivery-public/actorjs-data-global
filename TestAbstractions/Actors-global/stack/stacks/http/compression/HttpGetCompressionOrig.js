
const ActorApi = require('actor-api');


class HttpGetCompressionOrig extends ActorApi.ActorOriginating {
  constructor() {
    super();
    this.connection = null;
  }
  
  *data() {
  }
  
  *initClient() {
    this.connection = this.createConnection('[stack]');
  }
  
  *run() {
  }
  
  *exit(interrupted) {
    this.closeConnection(this.connection);
  }
}

module.exports = HttpGetCompressionOrig;
