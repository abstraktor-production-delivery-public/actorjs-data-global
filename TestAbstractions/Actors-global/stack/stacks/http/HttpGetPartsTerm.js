
const ActorApi = require('actor-api');
const HttpApi = require('http-stack-api');


class HttpGetPartsTerm extends ActorApi.ActorTerminating {
  constructor() {
    super();
    this.httpConnection = null;
  }
  
  *initServer() {
    this.httpConnection = this.createServer('http');
  }
  
  *run() {
    this.httpConnection.accept();
    
    const request = this.httpConnection.receive();
    VERIFY_VALUE(0, request.getHeaderNumber(HttpApi.Header.CONTENT_LENGTH));
  }
  
  *exit(interrupted) {
    this.closeConnection(this.httpConnection);
  }
}

module.exports = HttpGetPartsTerm;
