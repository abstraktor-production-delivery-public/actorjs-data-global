
const ActorApi = require('actor-api');
const HttpApi = require('http-stack-api');
const HttpGet200OkResponse = require('./msg/HttpGet200OkResponse');


class HttpGetOkTerm extends ActorApi.ActorTerminating {
  constructor() {
    super();
    this.httpConnection = null;
    this.content = null;
  }
  
  *data() {
    this.content = this.getContent('content-name');
  }
  
  *initServer() {
    this.httpConnection = this.createServer('http');
  }
  
  *run() {
    this.httpConnection.accept();
    
    const request = this.httpConnection.receive();
   // VERIFY_VALUE(0, request.getHeaderNumber(HttpApi.Header.CONTENT_LENGTH), 'HTTP header Content-Length:');
    
    this.httpConnection.send(new HttpGet200OkResponse(this.content));
  }
  
  *exit(interrupted) {
    this.closeConnection(this.httpConnection);
  }
}

module.exports = HttpGetOkTerm;
