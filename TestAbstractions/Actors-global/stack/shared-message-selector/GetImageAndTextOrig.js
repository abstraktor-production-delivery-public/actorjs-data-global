
const ActorApi = require('actor-api');
const StackApi = require('stack-api');
const ImageMsg = require('./msg/ImageMsg');
const TextMsg = require('./msg/TextMsg');


class GetImageAndTextOrig extends ActorApi.ActorOriginating {
  constructor() {
    super();
    this.socketConnection = null;
    this.networkType = StackApi.NetworkType.TCP;
    this.waitForSetup = [];
    this.waitForClose = [];
    this.messages = [];
  }
  
  *data() {
    this.networkType = this.getTestDataNumber('transport', 'tcp', {
      tcp: StackApi.NetworkType.TCP,
      udp: StackApi.NetworkType.UDP,
      mc: StackApi.NetworkType.MC
    });
    this.waitForSetup = this.getTestDataStrings('wait-for-setup', this.waitForSetup);
    this.waitForClose = this.getTestDataStrings('wait-for-close', this.waitForClose);
    this.messages = this.getTestDataStrings('messages', this.messages);
  }
  
  *initClient() {
    for(let i = 0; i < this.waitForSetup.length; ++i) {
      this.waitForSharedData(this.waitForSetup[i]);
    }
    this.setSharedData('CLIENT-CONNECT');
    this.socketConnection = this.createConnection('socket', {
      networkType: this.networkType
    });
  }
  
  *run() {
    for(let i = 0; i < this.messages.length; ++i) {
      if('Image' === this.messages[i]) {
        //console.log('SEND IMAGE');
        this.socketConnection.sendObject(new ImageMsg());
      }
      else if('Text' === this.messages[i]) {
        //console.log('SEND TEXT');
        this.socketConnection.sendObject(new TextMsg());
      }
    }
      //  this.delay(500);
  }
  
  *exit(interrupted) {
    if(!interrupted) {
      for(let i = 0; i < this.waitForClose.length; ++i) {
        this.waitForSharedData(this.waitForClose[i]);
      }
    }
    this.setSharedData('CLIENT-CLOSE');
    this.closeConnection(this.socketConnection);
  }
}

module.exports = GetImageAndTextOrig;
