
'use strict';

const StackApi = require('stack-api');


class GetImageMs extends StackApi.MessageSelector {
  constructor() {
    super();
  }
  
  onSelect(msg) {
    return false;
  }
}

module.exports = GetImageMs;
