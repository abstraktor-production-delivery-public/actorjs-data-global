
'use strict';

const StackApi = require('stack-api');


class GetTextMs extends StackApi.MessageSelector {
  constructor() {
    super();
  }
  
  onSelect(msg) {
    return false;
  }
}

module.exports = GetTextMs;
