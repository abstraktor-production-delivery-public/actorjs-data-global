
const ActorApi = require('actor-api');
const StackApi = require('stack-api');
const GetImageMs = require('./sms/GetImageMs');


class GetImageTerm extends ActorApi.ActorTerminating {
  constructor() {
    super();
    this.socketConnection = null;
    this.networkType = StackApi.NetworkType.TCP;
    this.waitForSetup = [];
    this.waitForClose = [];
    this.messages = [];
  }
  
  *data() {
    this.networkType = this.getTestDataNumber('transport', 'tcp', {
      tcp: StackApi.NetworkType.TCP,
      udp: StackApi.NetworkType.UDP,
      mc: StackApi.NetworkType.MC
    });
    this.waitForSetup = this.getTestDataStrings('wait-for-setup', this.waitForSetup);
    this.waitForClose = this.getTestDataStrings('wait-for-close', this.waitForClose);
    this.messages = this.getTestDataStrings('messages', this.messages);
  }
    
  *initServer() {
    this.socketConnection = this.createServer('socket', {
      networkType: this.networkType,
      messageSelector: new GetImageMs()
    });
  }
  
  *run() {
    for(let i = 0; i < this.waitForSetup.length; ++i) {
      this.waitForSharedData(this.waitForSetup[i]);
    }
    this.setSharedData('IMAGE-ACCEPT');
    this.socketConnection.accept();
    
    for(let i = 0; i < this.messages.length; ++i) {
      if('Image' === this.messages[i]) {
        const msg = this.socketConnection.receiveObject();
      }
    }
  }
  
  *exit(interrupted) {
    if(!interrupted) {
      for(let i = 0; i < this.waitForClose.length; ++i) {
        this.waitForSharedData(this.waitForClose[i]);
      }
    }
    this.setSharedData('IMAGE-CLOSE');
    this.closeConnection(this.socketConnection);
  }
}

module.exports = GetImageTerm;
