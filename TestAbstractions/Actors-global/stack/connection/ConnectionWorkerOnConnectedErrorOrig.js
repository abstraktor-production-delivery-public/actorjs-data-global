
const ActorApi = require('actor-api');
const CwTestOnConnectedErrorClient = require('./cw/CwTestOnConnectedErrorClient');
const WebsocketStackApi = require('websocket-stack-api');


class ConnectionWorkerOnConnectedErrorOrig extends ActorApi.ActorOriginating {
  constructor() {
    super();
    this.wsConnection = null;
  }
  
  *data() {
  }
  
  *initClient() {
    this.wsConnection = this.createConnection('http', {
      connectionWorker: new CwTestOnConnectedErrorClient()
    });
  }
  
  *run() {
    this.wsConnection.send(new WebsocketStackApi.TextFrame('Hello World', false));
    
    this.wsConnection.send(new WebsocketStackApi.CloseFrame());
  }
  
  *exit(interrupted) {
    this.closeConnection(this.wsConnection);
  }
}

module.exports = ConnectionWorkerOnConnectedErrorOrig;
