
const ActorApi = require('actor-api');
const CwTestServer = require('./cw/CwTestServer');
const WebsocketStackApi = require('websocket-stack-api');


class ConnectionWorkerTerm extends ActorApi.ActorTerminating {
  constructor() {
    super();
    this.httpConnection = null;
    this.wsConnection = null;
  }
  
  *data() { 
  }
  
  *initServer() {
    this.httpConnection = this.createServer('http');
  }
  
  *run() {
    this.wsConnection = this.httpConnection.accept(new CwTestServer());
    
    const textFrame = this.wsConnection.receive();
    VERIFY_VALUE('Hello World', textFrame.payloadData);
    
    const closeFrame = this.wsConnection.receive();
  }
  
  *exit(interrupted) {
    this.closeConnection(this.wsConnection);
  }
}

module.exports = ConnectionWorkerTerm;
