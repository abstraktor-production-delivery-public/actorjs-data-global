
'use strict';

const HttpApi = require('http-stack-api');
const WebsocketApi = require('websocket-stack-api');


class HttpSwitchingProtocolsResponse extends HttpApi.Response {
  constructor(wsProtocol) {
    super(HttpApi.Version.HTTP_1_1, HttpApi.StatusCode.SwitchingProtocols, HttpApi.ReasonPhrase.SwitchingProtocols);
    this.addHeader(HttpApi.Header.UPGRADE, 'websocket');
    this.addHeader(HttpApi.Header.CONNECTION, 'Upgrade');
    this.addHeader(WebsocketApi.Header.SEC_WEBSOCKET_ACCEPT, 's3pPLMBiTxaQ9kYGzzhZRbK+xOo=');
    this.addHeader(WebsocketApi.Header.SEC_WEBSOCKET_PROTOCOL, wsProtocol);
  }
}

module.exports = HttpSwitchingProtocolsResponse;
