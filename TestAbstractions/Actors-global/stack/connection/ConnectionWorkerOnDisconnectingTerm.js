
const ActorApi = require('actor-api');
const WebsocketStackApi = require('websocket-stack-api');


class ConnectionWorkerOnDisconnectingTerm extends ActorApi.ActorTerminating {
  constructor() {
    super();
    this.httpConnection = null;
    this.wsConnection = null;
  }
  
  *data() { 
  }
  
  *initServer() {
    this.httpConnection = this.createServer('http');
  }
  
  *run() {
    this.wsConnection = this.httpConnection.accept(new WebsocketStackApi.ConnectionWorkerServer());
    
    const textFrame = this.wsConnection.receive();
    VERIFY_VALUE('Hello World', textFrame.payloadData);
  }
  
  *exit(interrupted) {
    this.closeConnection(this.wsConnection);
    this.closeConnection(this.httpConnection);
  }
}

module.exports = ConnectionWorkerOnDisconnectingTerm;
