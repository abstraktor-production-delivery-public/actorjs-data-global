
const StackApi = require('stack-api');
const HttpGetUpgradeRequest = require('../msg/HttpGetUpgradeRequest');


class CwTestOnConnectedErrorClient extends StackApi.ConnectionWorkerClient {
  constructor() {
    super();
    this.httpConnection = null;
  }
  
  *onConnected(connection) {
    yield connection.send(new HttpGetUpgradeRequest('http://example.com', 'demo'));
    this.noneExistingMethod();
    const respone = yield connection.receive();
    return this.switchProtocol('websocket', connection);
  }
}

module.exports = CwTestOnConnectedErrorClient;
