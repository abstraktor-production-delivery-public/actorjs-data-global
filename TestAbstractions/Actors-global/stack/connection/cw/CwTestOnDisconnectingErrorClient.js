
'use strict';

const StackApi = require('stack-api');
const HttpGetUpgradeRequest = require('../msg/HttpGetUpgradeRequest');
const WebsocketApi = require('websocket-stack-api');


class CwTestOnDisconnectingErrorClient extends StackApi.ConnectionWorkerClient {
  constructor() {
    super();
    this.httpConnection = null;
  }
    
  *onConnected(connection) {
    yield connection.send(new HttpGetUpgradeRequest('http://example.com', 'demo'));
    const respone = yield connection.receive();
    return this.switchProtocol('websocket', connection);
  }
  
  *onDisconnecting(connection) {
    this.noneExistingMethod();
    yield connection.send(new WebsocketApi.WebsocketMsgCloseFrame());
  }
}

module.exports = CwTestOnDisconnectingErrorClient;
