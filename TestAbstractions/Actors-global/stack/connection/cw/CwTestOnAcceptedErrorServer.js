
const StackApi = require('stack-api');
const HttpSwitchingProtocolsResponse = require('../msg/HttpSwitchingProtocolsResponse');


class CwTestOnAcceptedErrorServer extends StackApi.ConnectionWorkerServer {
  constructor() {
    super();
    this.httpConnection = null;
  }
  
  *onAccepted(connection) {
    const request = yield connection.receive();
    this.noneExistingMethod();
    yield connection.send(new HttpSwitchingProtocolsResponse('demo'));
    return this.switchProtocol('websocket', connection);
  }
}

module.exports = CwTestOnAcceptedErrorServer;
