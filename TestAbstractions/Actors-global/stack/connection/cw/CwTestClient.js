
const StackApi = require('stack-api');
const HttpGetUpgradeRequest = require('../msg/HttpGetUpgradeRequest');


class CwTestClient extends StackApi.ConnectionWorkerClient {
  constructor() {
    super();
    this.httpConnection = null;
  }
  
  *onConnected(connection) {
    yield connection.send(new HttpGetUpgradeRequest('http://example.com', 'demo'));
    const respone = yield connection.receive();
    return this.switchProtocol('websocket', connection);
  }
}

module.exports = CwTestClient;
