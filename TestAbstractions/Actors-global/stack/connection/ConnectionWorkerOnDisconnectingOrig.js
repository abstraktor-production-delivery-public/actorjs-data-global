
const ActorApi = require('actor-api');
const WebsocketStackApi = require('websocket-stack-api');


class ConnectionWorkerOnDisconnectingOrig extends ActorApi.ActorOriginating {
  constructor() {
    super();
    this.wsConnection = null;
  }
  
  *data() {
  }
  
  *initClient() {
    this.wsConnection = this.createConnection('http', {
      connectionWorker: new WebsocketStackApi.ConnectionWorkerClient()
    });
  }
  
  *run() {
    this.wsConnection.send(new WebsocketStackApi.TextFrame('Hello World', false));
  }
  
  *exit(interrupted) {
    this.closeConnection(this.wsConnection);
  }
}

module.exports = ConnectionWorkerOnDisconnectingOrig;
