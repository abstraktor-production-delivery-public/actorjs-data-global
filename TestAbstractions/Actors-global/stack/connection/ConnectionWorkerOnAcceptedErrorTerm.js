
const ActorApi = require('actor-api');
const CwTestOnAcceptedErrorServer = require('./cw/CwTestOnAcceptedErrorServer');
const WebsocketStackApi = require('websocket-stack-api');


class ConnectionWorkerOnAcceptedErrorTerm extends ActorApi.ActorTerminating {
  constructor() {
    super();
    this.httpConnection = null;
    this.wsConnection = null;
  }
  
  *data() { 
  }
  
  *initServer() {
    this.httpConnection = this.createServer('http');
  }
  
  *run() {
    this.wsConnection = this.httpConnection.accept(new CwTestOnAcceptedErrorServer());
    
    const textFrame = this.wsConnection.receive();
    VERIFY_VALUE('Hello World', textFrame.payloadData);
    
    const closeFrame = this.wsConnection.receive();
  }
  
  *exit(interrupted) {
    this.closeConnection(this.wsConnection);
  }
}

module.exports = ConnectionWorkerOnAcceptedErrorTerm;

