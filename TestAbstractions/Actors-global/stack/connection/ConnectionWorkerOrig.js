
const ActorApi = require('actor-api');
const CwTestClient = require('./cw/CwTestClient');
const WebsocketStackApi = require('websocket-stack-api');


class ConnectionWorkerOrig extends ActorApi.ActorOriginating {
  constructor() {
    super();
    this.wsConnection = null;
  }
  
  *data() {
  }
  
  *initClient() {
    this.wsConnection = this.createConnection('http', {
      connectionWorker: new CwTestClient()
    });
  }
  
  *run() {
    this.wsConnection.send(new WebsocketStackApi.TextFrame('Hello World', false));
    
    this.wsConnection.send(new WebsocketStackApi.CloseFrame());
  }
  
  *exit(interrupted) {
    this.closeConnection(this.wsConnection);
  }
}

module.exports = ConnectionWorkerOrig;
