
'use strict';

const ActorApi = require('actor-api');
const StackApi = require('stack-api');


class DefaultAddressMcOrig extends ActorApi.ActorOriginating {
  constructor() {
    super();
    this.socketConnection = null;
  }
  
  *data() {
  }
  
  *initClient() {
    this.socketConnection = this.createConnection('socket', {
      networkType: StackApi.NetworkType.MC
    });
  }
  
  *run() {
  }
  
  *exit(interrupted) {
    this.closeConnection(this.socketConnection);
  }
}

module.exports = DefaultAddressMcOrig;
