
'use strict';

const ActorApi = require('actor-api');


class DefaultAddressTcpProxy extends ActorApi.ActorProxy {
  constructor() {
    super();
    this.socketServerConnection = null;
    this.socketClientConnection = null;
  }
  
  *data() {
  }
    
  *initServer() {
    this.socketServerConnection = this.createServer('socket');
  }
  
  *initClient() {
    this.socketClientConnection = this.createConnection('socket');
  }
  
  *run() {
    this.socketServerConnection.accept();
  }
  
  *exit(interrupted) {
    this.closeConnection(this.socketServerConnection);
    this.closeConnection(this.socketClientConnection);
  }
}

module.exports = DefaultAddressTcpProxy;
