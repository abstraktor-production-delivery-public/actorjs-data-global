
'use strict';

const ActorApi = require('actor-api');
const StackApi = require('stack-api');


class DefaultAddressTlsTerm extends ActorApi.ActorTerminating {
  constructor() {
    super();
    this.socketConnection = null;
  }
  
  *data() {
  }
    
  *initServer() {
    this.socketConnection = this.createServer('socket', {
      networkType: StackApi.NetworkType.TLS
    });
  }
  
  *run() {
    this.socketConnection.accept();
  }
  
  *exit(interrupted) {
    this.closeConnection(this.socketConnection);
  }
}

module.exports = DefaultAddressTlsTerm;
