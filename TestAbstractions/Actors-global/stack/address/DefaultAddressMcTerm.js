
'use strict';

const ActorApi = require('actor-api');
const StackApi = require('stack-api');


class DefaultAddressMcTerm extends ActorApi.ActorTerminating {
  constructor() {
    super();
    this.socketConnection = null;
  }
  
  *data() {
  }
    
  *initServer() {
    this.socketConnection = this.createServer('socket', {
      networkType: StackApi.NetworkType.MC
    });
  }
  
  *run() {
    this.socketConnection.accept();
  }
  
  *exit(interrupted) {
    this.closeConnection(this.socketConnection);
  }
}

module.exports = DefaultAddressMcTerm;
