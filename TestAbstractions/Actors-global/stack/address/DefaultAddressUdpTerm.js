
'use strict';

const ActorApi = require('actor-api');
const StackApi = require('stack-api');


class DefaultAddressUdpTerm extends ActorApi.ActorTerminating {
  constructor() {
    super();
    this.socketConnection = null;
  }
  
  *data() {
  }
    
  *initServer() {
    this.socketConnection = this.createServer('socket', {
      networkType: StackApi.NetworkType.UDP
    });
  }
  
  *run() {
    this.socketConnection.accept();
    const req = this.socketConnection.receiveLine();
  }
  
  *exit(interrupted) {
    this.closeConnection(this.socketConnection);
  }
}

module.exports = DefaultAddressUdpTerm;
