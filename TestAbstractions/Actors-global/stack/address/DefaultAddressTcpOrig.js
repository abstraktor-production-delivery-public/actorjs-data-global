
'use strict';

const ActorApi = require('actor-api');


class DefaultAddressTcpOrig extends ActorApi.ActorOriginating {
  constructor() {
    super();
    this.socketConnection = null;
  }
    
  *initClient() {
    this.socketConnection = this.createConnection('socket');
  }
    
  *exit(interrupted) {
    this.closeConnection(this.socketConnection);
  }
}

module.exports = DefaultAddressTcpOrig;
