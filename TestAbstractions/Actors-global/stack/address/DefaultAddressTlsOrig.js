
'use strict';

const ActorApi = require('actor-api');
const StackApi = require('stack-api');


class DefaultAddressTlsOrig extends ActorApi.ActorOriginating {
  constructor() {
    super();
    this.socketConnection = null;
  }
  
  *data() {
  }
  
  *initClient() {
    this.socketConnection = this.createConnection('socket', {
      networkType: StackApi.NetworkType.TLS
    });
  }
  
  *run() {
  }
  
  *exit(interrupted) {
    this.closeConnection(this.socketConnection);
  }
}

module.exports = DefaultAddressTlsOrig;
