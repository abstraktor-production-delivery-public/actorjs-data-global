
'use strict';

const ActorApi = require('actor-api');
const StackApi = require('stack-api');
const HttpApi = require('http-stack-api');


class HttpGetRestOrig extends ActorApi.ActorOriginating {
  constructor() {
    super();
    this.httpConnection = null;
    this.requistTarget = 'www.example.com';
    this.httpHeaders = [];
    this.transportType = StackApi.NetworkType.TCP;
  }
  
  *data() {
    this.requistTarget = this.getTestDataString('request-target', this.requistTarget);
    this.httpHeaders = this.getTestDataArrayStrings('http-headers', this.httpHeaders);
    this.transportType = this.getTestDataNumber('transport-type', 'tcp', {
      tcp: StackApi.NetworkType.TCP,
      tls: StackApi.NetworkType.TLS
    });
  }
  
  *initClient() {
    this.httpConnection = this.createConnection('http', {
      networkType: this.transportType
    });
  }
  
  *run() {
    this.httpConnection.send(new HttpApi.GetHtmlReq(this.requistTarget, this.httpHeaders));
    const response = this.httpConnection.receive();
  }
  
  *exit(interrupted) {
    this.closeConnection(this.httpConnection);
  }
}

module.exports = HttpGetRestOrig;
