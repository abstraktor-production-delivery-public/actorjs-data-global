
'use strict';

const ActorApi = require('actor-api');
const StackApi = require('stack-api');


class PuppeteerPageOrig extends ActorApi.ActorOriginating {
  constructor() {
    super();
    this.puppeteerConnection = null;
  }
  
  *initClient() {
    this.puppeteerConnection = this.createConnection('puppeteer');
  }
  
  *run() {
    const page = this.puppeteerConnection.page;
    //this.delay(2345);
  }
  
  *exit(interrupted) {
    this.closeConnection(this.puppeteerConnection);
  }
}

module.exports = PuppeteerPageOrig;
