
'use strict';

const ActorApi = require('actor-api');
const StackApi = require('stack-api');


class BrowserLoginOrig extends ActorApi.ActorOriginating {
  constructor() {
    super();
    this.puppeteerConnection = null;
    this.transportType = StackApi.NetworkType.TCP;
  }
  
  *data() {
    this.transportType = this.getTestDataNumber('transport-type', 'tcp', {
      tcp: StackApi.NetworkType.TCP,
      tls: StackApi.NetworkType.TLS
    });
  }
  
  *initClient() {
    this.puppeteerConnection = this.createConnection('puppeteer');
  }
  
  *run() {
    const page = this.puppeteerConnection.page;
    
    const userName = 'username';
    
    // EXECUTE
    yield page.click('#from_start_to_login');
    
    // WAIT
    yield page.waitForSelector('#heading_login');
    
    // EXECUTE
    yield page.type('#actor_demo_username_input', userName);
    yield page.type('#actor_demo_password_input', 'password');
    yield page.click('#actor_demo_login_button');
    
    // WAIT
    const h4 = yield page.waitForFunction((selector, innerText) => {
      const button = document.getElementById(selector);
      return !!button ? button.innerText === innerText : false;
    }, {}, 'actor_demo_username_h4', `User: username`);
    
    // VERIFY
    const element = yield page.$('#actor_demo_username_h4');
    const value = yield page.evaluate(el => el.innerText, element);
  }
  
  *exit(interrupted) {
    this.closeConnection(this.puppeteerConnection);
  }
}

module.exports = BrowserLoginOrig;
