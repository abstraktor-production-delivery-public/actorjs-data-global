
'use strict';

const IcapRespmodResponse = require('../msg/IcapRespmodResponse');
const HttpResponse200Content = require('../msg/HttpResponse200Content');
const ActorApi = require('actor-api');
const IcapApi = require('icap-stack-api');


class AiServiceTerm extends ActorApi.ActorTerminating {
  constructor() {
    super();
    this.icapConnection = null;
  }
  
  *data() {
  }
    
  *initServer() {
    this.icapConnection = this.createServer('icap');
  }
  
  *run() {
    this.icapConnection.accept();
    
    while(true) {
      const request = this.icapConnection.receive();
      const parameters = request.requestTarget.split('/');
      
      VERIFY_VALUE('api', parameters[0]);
      VERIFY_VALUE('v1', parameters[1]);
      VERIFY_VALUE('image', parameters[2]);
      VERIFY_VALUE('ais', parameters[3]);
      if(parameters[4] && parameters[5]) {
        const splitted = parameters[5].split(',');
        if(splitted) {
          const joined = splitted.join('+');
          const contentName = `workshop_${parameters[4]}+${joined}`;
          const content = this.getContentByName(contentName);
          this.icapConnection.send(new IcapRespmodResponse(new HttpResponse200Content(content)));
          continue;
        }
      }
      // TODO: send error
    }
  }
  
  *exit(interrupted) {
    this.closeConnection(this.icapConnection);
  }
}

module.exports = AiServiceTerm;
