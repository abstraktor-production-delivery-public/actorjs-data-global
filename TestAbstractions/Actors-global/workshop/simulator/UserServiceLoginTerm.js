
const HttpResponse200DataJson = require('../msg/HttpResponse200DataJson');
const ActorApi = require('actor-api');


class UserServiceLoginTerm extends ActorApi.ActorTerminating {
  constructor() {
    super();
    this.httpConnection = null;
  }
  
  *data() {
  }
    
  *initServer() {
    this.httpConnection = this.createServer('http');
  }
  
  *run() {
    this.httpConnection.accept();
    
    while(true) {
      const request = this.httpConnection.receive();
      const parameters = request.requestTarget.split('/');
      
      VERIFY_VALUE('api', parameters[0]);
      VERIFY_VALUE('v1', parameters[1]);
      VERIFY_VALUE('user', parameters[2]);
      
      if('login' === parameters[3]) {
        this.httpConnection.send(new HttpResponse200DataJson({
          loggedIn: true,
          authorization: 'Guest'
        }));
      }
      else if('logout' === parameters[3]) {
        this.httpConnection.send(new HttpResponse200DataJson());
      }
    }
  }
  
  *exit(interrupted) {
    this.closeConnection(this.httpConnection);
  }
}

module.exports = UserServiceLoginTerm;
