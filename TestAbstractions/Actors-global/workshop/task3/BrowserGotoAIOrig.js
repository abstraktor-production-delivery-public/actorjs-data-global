
'use strict';

const ActorApi = require('actor-api');
const StackApi = require('stack-api');


class BrowserGotoAIOrig extends ActorApi.ActorOriginating {
  constructor() {
    super();
    this.puppeteerConnection = null;
    this.transportType = StackApi.NetworkType.TCP;
  }
  
  *data() {
    this.transportType = this.getTestDataNumber('transport-type', 'tcp', {
      tcp: StackApi.NetworkType.TCP,
      tls: StackApi.NetworkType.TLS
    });
  }
  
  *initClient() {
    this.puppeteerConnection = this.createConnection('puppeteer');
  }
  
  *run() {
    const page = this.puppeteerConnection.page;
    
    // EXECUTE
    yield page.click('#from_start_to_image_ai');
    
    // WAIT
    yield page.waitForSelector('#heading_image_ai');
  }
  
  *exit(interrupted) {
    this.closeConnection(this.puppeteerConnection);
  }
}

module.exports = BrowserGotoAIOrig;
