
const HttpResponse200DataJson = require('../msg/HttpResponse200DataJson');
const HttpResponse200Content = require('../msg/HttpResponse200Content');
const ActorApi = require('actor-api');


class ImageServiceTerm extends ActorApi.ActorTerminating {
  constructor() {
    super();
    this.httpConnection = null;
    this.content = null;
    this.aiImages = null;
  }
  
  *data() {
    this.aiImages = this.getTestDataObject('ai-images');
  }
  
  *initServer() {
    this.httpConnection = this.createServer('http');
  }
  
  *run() {
    this.httpConnection.accept();
    
    let request = this.httpConnection.receive();
    let parameters = request.requestTarget.split('/');
    VERIFY_VALUE('api', parameters[0]);
    VERIFY_VALUE('v1', parameters[1]);
    VERIFY_VALUE('image', parameters[2]);
    VERIFY_VALUE('get-images', parameters[3]);
      
    this.httpConnection.send(new HttpResponse200DataJson([
      this.aiImages
    ]));
    
    for(let i = 0; i < 2; ++i) {
      request = this.httpConnection.receive();
      parameters = request.requestTarget.split('/');
      VERIFY_VALUE('api', parameters[0]);
      VERIFY_VALUE('v1', parameters[1]);
      VERIFY_VALUE('image', parameters[2]);
      VERIFY_VALUE('get-image', parameters[3]);
    
      const content = this.getContentByName(`workshop_${parameters[4]}`, 'image');
      this.httpConnection.send(new HttpResponse200Content(content));
    }
  }
  
  *exit(interrupted) {
    this.closeConnection(this.httpConnection);
  }
}

module.exports = ImageServiceTerm;
