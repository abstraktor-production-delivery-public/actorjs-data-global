
'use strict';

const ActorApi = require('actor-api');
const StackApi = require('stack-api');


class BrowserAIOrig extends ActorApi.ActorOriginating {
  constructor() {
    super();
    this.puppeteerConnection = null;
    this.aiButtonId = '';
  }
  
  *data() {
    this.aiButtonId = this.getTestDataString('ai-button-id');
  }
  
  *initClient() {
    this.puppeteerConnection = this.createConnection('puppeteer');
  }
  
  *run() {
    const page = this.puppeteerConnection.page;
    
     // WAIT
    yield page.waitForFunction((selector) => {
      const button = document.getElementById(selector);
      return !!button && !button.classList.contains('disabled');
    }, {}, 'from_start_to_image_ai');
    
    // EXECUTE
    yield page.click('#from_start_to_image_ai');
    
    // WAIT
    yield page.waitForSelector('#heading_image_ai');
    
    // WAIT & CLICK
    page.waitForFunction((selector) => {
      const button = document.getElementById(selector);
      if(!!button && !button.classList.contains('disabled')) {
        button.click();
        return true;
      }
      else {
        return false;
      }
    }, {}, this.aiButtonId);
  }
  
  *exit(interrupted) {
    this.closeConnection(this.puppeteerConnection);
  }
}

module.exports = BrowserAIOrig;
