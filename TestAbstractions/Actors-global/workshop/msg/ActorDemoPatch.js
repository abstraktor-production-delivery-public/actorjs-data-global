
'use strict';

const HttpApi = require('http-stack-api');


class ActorDemoPatch extends HttpApi.Request {
  constructor(host, requestTarget, value, contentLength = value.length, contentType = 'text/plain') {
    super(HttpApi.Method.PATCH, `http://${host}/abs-api/${requestTarget}`, HttpApi.Version.HTTP_1_1);
    this.addHeader(HttpApi.Header.HOST, host);
    this.addHeader(HttpApi.Header.CONTENT_TYPE, contentType);
    this.addHeader(HttpApi.Header.CONTENT_LENGTH, contentLength);
    this.addBody(value);
  }
}

module.exports = ActorDemoPatch;
