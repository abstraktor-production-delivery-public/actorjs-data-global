
'use strict';

const HttpApi = require('http-stack-api');
const GuidGenerator = require('z-abs-corelayer-cs/clientServer/guid-generator');


class ActorDemoRestLogin extends HttpApi.Request {
  constructor(host, httpUserAgent, actorJsRequestName, ...actorJsRequestParams) {
    super(HttpApi.Method.POST, `http://${host}/abs-data/`, HttpApi.Version.HTTP_1_1);
    this.addHeader(HttpApi.Header.HOST, host);
    this.addHeader(HttpApi.Header.CONNECTION, 'keep-alive');
    this.addHeader(HttpApi.Header.ACCEPT, 'application/json');
    this.addHeader(HttpApi.Header.USER_AGENT, httpUserAgent);
    this.addHeader(HttpApi.Header.ACCEPT_ENCODING, 'gzip, deflate');
    this.addHeader(HttpApi.Header.ACCEPT_LANGUAGE, 'en-US,en;q=0.9');
    
    const body = this._getBody(actorJsRequestName, actorJsRequestParams );
    this.addHeader(HttpApi.Header.CONTENT_TYPE, 'application/json');
    this.addHeader(HttpApi.Header.CONTENT_LENGTH, body.length);
    this.addBody(body);
  }
  
  _getBody(actorJsRequestName, actorJsRequestParams ) {
    const bodyObject = {
      id: GuidGenerator.create(),
      requests: [{
        name: actorJsRequestName,
        index: 0,
        params: [...actorJsRequestParams]
      }]
    };
    return JSON.stringify(bodyObject);
  }
}

module.exports = ActorDemoRestLogin;
