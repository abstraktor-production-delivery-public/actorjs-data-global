
'use strict';

const HttpApi = require('http-stack-api');


class HttpResponse200Content extends HttpApi.Response {
  constructor(content) {
    super(HttpApi.Version.HTTP_1_1, HttpApi.StatusCode.OK, HttpApi.ReasonPhrase.OK);
    if(content) {
      this.addHeader(HttpApi.Header.CONTENT_TYPE, content.mime);
      this.addHeader(HttpApi.Header.CONTENT_LENGTH, content.size);
      this.addBody(content);
    }
  }
}

module.exports = HttpResponse200Content;

