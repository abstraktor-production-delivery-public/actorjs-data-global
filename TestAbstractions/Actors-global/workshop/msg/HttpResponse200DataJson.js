
'use strict';

const HttpApi = require('http-stack-api');


class HttpResponse200DataJson extends HttpApi.Response {
  constructor(data) {
    super(HttpApi.Version.HTTP_1_1, HttpApi.StatusCode.OK, HttpApi.ReasonPhrase.OK);
    if(data) {
      const content = JSON.stringify(data, null, 2);
      this.addHeader(HttpApi.Header.CONTENT_TYPE, 'application/json');
      this.addHeader(HttpApi.Header.CONTENT_LENGTH, content.length);
      this.addBody(content);
    }
  }
}

module.exports = HttpResponse200DataJson;

