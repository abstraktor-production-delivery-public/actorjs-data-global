
'use strict';

const HttpApi = require('http-stack-api');


class HttpResponse200Data extends HttpApi.Response {
  constructor(data, contentType) {
    super(HttpApi.Version.HTTP_1_1, HttpApi.StatusCode.OK, HttpApi.ReasonPhrase.OK);
    if(data) {
      this.addHeader(HttpApi.Header.CONTENT_TYPE, contentType);
      this.addHeader(HttpApi.Header.CONTENT_LENGTH, data.length);
      this.addBody(data);
    }
  }
}

module.exports = HttpResponse200Data;

