
'use strict';

const HttpApi = require('http-stack-api');
const ActorApi = require('actor-api');


class RestConfigAuthorizationCond extends ActorApi.ActorCondition {
  constructor() {
    super();
    this.httpConnection = null;
    this.host = '';
    this.requstUri = '';
    this.patchSetValue = '';
    this.patchResetValue = '';
  }
  
  *data() {
    this.host = this.getTestDataString('request-host');
    this.requstUri = this.getTestDataString('request-uri');
    this.patchSetValue = this.getTestDataString('patch-set-value');
    this.patchResetValue = this.getTestDataString('patch-reset-value');
  }
  
  *initClientPre() {
    this.httpConnection = this.createConnection('http');
  }
  
  *runPre() {
    this.httpConnection.send(new HttpApi.req.HttpRestPatchReq(this.host, this.requstUri, this.patchSetValue));
    
    const response = this.httpConnection.receive();
    VERIFY_VALUE(HttpApi.StatusCode.NoContent, response.statusCode);
  }
  
  *exitPre() {
    this.closeConnection(this.httpConnection);
  }
  
  *initClientPost() {
    this.httpConnection = this.createConnection('http');
  }
  
  *runPost() {
    this.httpConnection.send(new HttpApi.req.HttpRestPatchReq(this.host, this.requstUri, this.patchResetValue));
    
    const response = this.httpConnection.receive();
    VERIFY_VALUE(HttpApi.StatusCode.NoContent, response.statusCode);
  }
  
  *exitPost() {
    this.closeConnection(this.httpConnection);
  }
}

module.exports = RestConfigAuthorizationCond;
