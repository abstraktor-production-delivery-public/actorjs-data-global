
'use strict';

const ActorApi = require('actor-api');
const StackApi = require('stack-api');


class BrowserStartAthourizedOrig extends ActorApi.ActorOriginating {
  constructor() {
    super();
    this.puppeteerConnection = null;
    this.transportType = StackApi.NetworkType.TCP;
  }
  
  *data() {
    this.transportType = this.getTestDataNumber('transport-type', 'tcp', {
      tcp: StackApi.NetworkType.TCP,
      tls: StackApi.NetworkType.TLS
    });
  }
  
  *initClient() {
    this.puppeteerConnection = this.createConnection('puppeteer');
  }
  
  *run() {
    const page = this.puppeteerConnection.page;
    
    // WAIT
    yield page.waitForFunction((selector) => {
      return !!document.getElementById(selector);
    }, {}, 'from_start_to_login');
    
    // VERIFY
    const loginButton = yield page.$('#from_start_to_login');
    const loginButtonClassName = yield (yield loginButton.getProperty('className')).jsonValue();
    VERIFY_VALUE('btn btn-abs btn-default', loginButtonClassName);
    
    // WAIT
    yield page.waitForFunction((selector) => {
      return !!document.getElementById(selector);
    }, {}, 'from_start_to_image_ai');
    
    // VERIFY
    const imageAiButton = yield page.$('#from_start_to_image_ai');
    const imageAiButtonClassName = yield (yield imageAiButton.getProperty('className')).jsonValue();
    VERIFY_VALUE('btn btn-abs btn-default disabled', imageAiButtonClassName);
  }
  
  *exit(interrupted) {
    this.closeConnection(this.puppeteerConnection);
  }
}

module.exports = BrowserStartAthourizedOrig;
