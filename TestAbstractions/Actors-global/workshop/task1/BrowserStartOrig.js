
'use strict';

const ActorApi = require('actor-api');
const StackApi = require('stack-api');


class BrowserStartOrig extends ActorApi.ActorOriginating {
  constructor() {
    super();
    this.puppeteerConnection = null;
  }
    
  *initClient() {
    this.puppeteerConnection = this.createConnection('puppeteer');
  }
  
  *run() {
    const page = this.puppeteerConnection.page;
    
    // WAIT
    yield page.waitForFunction((selector) => {
      const button = document.getElementById(selector);
      if(!!button && !button.classList.contains('disabled')) {
        button.click();
        return true;
      }
      else {
        return false;
      }
    }, {}, 'button_bob_hat');
    
    // EXECUTE
 //   yield page.click('#button_bob_hat');
    
    // WAIT
    yield page.waitForFunction((selector) => {
      const image = document.getElementById(selector);
      if(image) {
        if('/abs-images/Bob+facepaint+hat+shirt.png' === image.getAttribute('src')) {
          if(image.complete) {
            return true;
          }
        }
      }
      return false;
    }, {}, 'image_bob_hat');
  }
  
  *exit(interrupted) {
    this.closeConnection(this.puppeteerConnection);
  }
}

module.exports = BrowserStartOrig;
