
const ActorApi = require('actor-api');
const WsSwitchingProtocolsWorker = require('./workers/WsSwitchingProtocolsWorker');


class WebsocketSwitchingProtocolsOrig extends ActorApi.ActorOriginating {
  constructor() {
    super();
    this.httpConnection = null;
  }
  
  *data() {
  }
  
  *initClient() {
    this.httpConnection = this.createConnection('http', {connectionWorker: new WsSwitchingProtocolsWorker()});
  }
  
  *run() {
  }
  
  *exit(interrupted) {
    this.closeConnection(this.httpConnection);
  }
}

module.exports = WebsocketSwitchingProtocolsOrig;
