
const ActorApi = require('actor-api');


class WebsocketSwitchingProtocolsTerm extends ActorApi.ActorTerminating {
  constructor() {
    super();
    this.httpConnection = null;
  }
  
  *data() {
  }
    
  *initServer() {
    this.httpConnection = this.createServer('http');
  }
  
  *run() {
    this.httpConnection.accept();
  }
  
  *exit(interrupted) {
    this.closeConnection(this.httpConnection);
  }
}

module.exports = WebsocketSwitchingProtocolsTerm;
