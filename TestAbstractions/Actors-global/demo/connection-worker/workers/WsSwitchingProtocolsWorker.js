

const StackApi = require('stack-api');
const HttpGetUpgradeRequest = require('../msg/HttpUpgradeRequest');


class WsSwitchingProtocolsWorker extends StackApi.ConnectionWorker {
  constructor() {
    super();
  }
  
  onConnected(done) {
    console.log('onConnected');
    
    return false;
  }
  
  onMessage(msg, done) {
    return false;
  }
  
  onDisconnected(done) {
    return false;
  }
}

module.exports = WsSwitchingProtocolsWorker;
