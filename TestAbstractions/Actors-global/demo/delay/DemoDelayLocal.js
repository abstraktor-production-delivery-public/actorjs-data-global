
const ActorApi = require('actor-api');


class DemoDelayLocal extends ActorApi.ActorLocal {
  constructor() {
    super();
    this.awaitMs = 0;
  }
  
  *data() {
    this.awaitMs = this.getTestDataNumber('await-ms', this.awaitMs);
  }
  
  *run() {
    this.delay(this.awaitMs);
  }
}

module.exports = DemoDelayLocal;
