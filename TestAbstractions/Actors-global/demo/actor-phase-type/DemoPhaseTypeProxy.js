
const ActorApi = require('actor-api');


class DemoPhaseTypeProxy extends ActorApi.ActorProxy {
  *data() {
  }
  
  *initServer() {
  }
  
  *initClient() {
  }
  
  *run() {
  }
  
  *exit(interrupted) {
  }
}

module.exports = DemoPhaseTypeProxy;
