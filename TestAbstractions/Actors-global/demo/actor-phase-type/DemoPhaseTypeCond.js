
const ActorApi = require('actor-api');


class DemoPhaseTypeCond extends ActorApi.ActorCondition {
  *data() {
  }
  
  *initClientPre() {
  }
  
  *runPre() {
  }
  
  *exitPre() {
  }
  
  *initClientPost() {
  }
  
  *runPost() {
  }
  
  *exitPost() {
  }
}

module.exports = DemoPhaseTypeCond;
