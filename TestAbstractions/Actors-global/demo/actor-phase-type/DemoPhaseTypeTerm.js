
const ActorApi = require('actor-api');


class DemoPhaseTypeTerm extends ActorApi.ActorTerminating {
  *data() {
  }
  
  *initServer() {
  }
  
  *run() {
  }
  
  *exit(interrupted) {
  }
}

module.exports = DemoPhaseTypeTerm;
