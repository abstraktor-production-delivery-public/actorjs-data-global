
const ActorApi = require('actor-api');


class DemoTestDataLocal extends ActorApi.ActorLocal {
  *data() {
    this.getTestDataString('DemoActor');
    this.getTestDataString('DemoActors');
    this.getTestDataString('DemoTestCase');
    this.getTestDataString('DemoGeneral');
    this.getTestDataString('DemoOutput');
    this.getTestDataString('DemoSystem');
    this.getTestDataString('DemoEnvironment');
    this.getTestDataString('DemoFormat');
    this.getTestDataNumber('DemoActorNumber');
    this.getTestDataNumber('DemoActorsNumber');
    this.getTestDataNumber('DemoTestCaseNumber');
    this.getTestDataNumber('DemoGeneralNumber');
    this.getTestDataNumber('DemoOutputNumber');
    this.getTestDataNumber('DemoSystemNumber');
    this.getTestDataNumber('DemoEnvironmentNumber');
  }
  
  *run() {
  }
}

module.exports = DemoTestDataLocal;
