
const ActorApi = require('actor-api');
const DiameterRequest = require('./DiameterRequest');


class HSSOrig extends ActorApi.ActorOriginating {
  constructor() {
    super();
    this.socketConnection = null;
  }
  
  *initClient() {
    this.socketConnection = this.createConnection('socket');
  }
  
  *run() {
    this.socketConnection.sendObject(new DiameterRequest('login'));
    
    const response = this.socketConnection.receiveObject();
    VERIFY_VALUE('login', response.reqType);
    VERIFY_VALUE('OK', response.result);
    this.setSharedData('LOGIN OK');
  }
  
  *exit(interrupted) {
    this.closeConnection(this.socketConnection);
  }
}

module.exports = HSSOrig;
