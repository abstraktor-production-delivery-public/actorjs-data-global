
const ActorApi = require('actor-api');
const HttpApi = require('http-stack-api');
const HttpGetImageReq = require('./msg/HttpGetImageReq');


class DemoMobileGetImageOrig extends ActorApi.ActorOriginating {
  constructor() {
    super();
    this.httpConnection = null;
    this.requistUri = '';
    this.acceptHeader = '';
  }
  
  *data() {
    this.requistUri = this.getTestDataString('request-uri');
    this.acceptHeader = this.getTestDataString('accept');
  }
  
  *initClient() {
    if(this.getSharedData('LOGIN-INFO', false)) {
      this.waitForSharedData('LOGIN OK');
    }
    this.httpConnection = this.createConnection('http');
  }
  
  *run() {
    this.httpConnection.send(new HttpGetImageReq(this.requistUri, this.acceptHeader));
    const response = this.httpConnection.receive();
  }
  
  *exit(interrupted) {
    this.closeConnection(this.httpConnection);
  }
}

module.exports = DemoMobileGetImageOrig;
