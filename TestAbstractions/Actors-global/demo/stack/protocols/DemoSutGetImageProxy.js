
const ActorApi = require('actor-api');


class DemoSutGetImageProxy extends ActorApi.ActorProxy {
  constructor() {
    super();
    this.httpMobileServerConnection = null;
    this.httpWwwClientConnection = null;
    
  }
 
  *initServer() {
    this.httpMobileServerConnection = this.createServer('http');
  }
  
  *run() {
    this.httpMobileServerConnection.accept();
    const httpRequest = this.httpMobileServerConnection.receive();
    
    this.httpWwwClientConnection = this.createConnection('http');
    this.httpWwwClientConnection.send(httpRequest);
    const httpResponse = this.httpWwwClientConnection.receive();
    this.httpMobileServerConnection.send(httpResponse);
  }
  
  *exit(interrupted) {
    this.closeConnection(this.httpWwwClientConnection);
    this.closeConnection(this.httpMobileServerConnection);
  }
}


module.exports = DemoSutGetImageProxy;
