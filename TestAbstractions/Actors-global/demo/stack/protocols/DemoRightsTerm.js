
const ActorApi = require('actor-api');


class DemoRightsTerm extends ActorApi.ActorTerminating {
  constructor() {
    super();
    this.socketConnection = null;
  }

  *data() {
  }
    
  *initServer() {
    this.socketConnection = this.createServer('socket');
  }
  
  *run() {
    this.socketConnection.accept();
	
    const request = this.socketConnection.receiveObject();
	  this.socketConnection.sendObject({result: 'ok'});
  }
  
  *exit(interrupted) {
    this.closeConnection(this.socketConnection);
  }
}

module.exports = DemoRightsTerm;
