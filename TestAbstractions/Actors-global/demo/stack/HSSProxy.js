
const ActorApi = require('actor-api');


class HSSProxy extends ActorApi.ActorProxy {
  constructor() {
    super();
    this.socketHssServerConnection = null;
    this.socketHssClientConnection = null;
  }
  
  *data() {
  }
  
  *initServer() {
    this.socketHssServerConnection = this.createServer('socket');
  }
  
  *initClient() {
    this.socketHssClientConnection = this.createConnection('socket')
  }
  
  *run() {
    this.socketHssServerConnection.accept();
    
    let diamRequest = this.socketHssServerConnection.receiveObject();
    this.socketHssClientConnection.sendObject(diamRequest);
    
    let diamResponse = this.socketHssClientConnection.receiveObject();
    this.socketHssServerConnection.sendObject(diamResponse);
  }
  
  *exit(interrupted) {
    this.closeConnection(this.socketHssServerConnection);
    this.closeConnection(this.socketHssClientConnection);
  }
}

module.exports = HSSProxy;
