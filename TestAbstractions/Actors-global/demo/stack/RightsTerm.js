
const ActorApi = require('actor-api');
const DiameterResponse = require('./DiameterResponse');


class RightsTerm extends ActorApi.ActorTerminating {
  constructor() {
    super();
    this.socketConnection = null;
  }

  *initServer() {
    this.socketConnection = this.createServer('socket');
  }
  
  *run() {
    this.socketConnection.accept();
	
    const request = this.socketConnection.receiveObject();
    VERIFY_VALUE('allowed', request.reqType);
    
	  this.socketConnection.sendObject(new DiameterResponse(request.reqType, 'OK'));
  }
  
  *exit(interrupted) {
    this.closeConnection(this.socketConnection);
  }
}

module.exports = RightsTerm;
