
const ActorApi = require('actor-api');
const ChildProcess = require('child_process');


class ProcessCond extends ActorApi.ActorCondition {
  constructor() {
    super();
    this.childProcess = null;
    this.processScript = '';
  }

  *data() {
    this.processScript = this.getTestDataString('process-script');
  }
  
  *runPre() {
    const address = this.getDstAddress();
    this.childProcess = yield this.forkChildProcess(`${this.processScript}.js`, [address.host, address.port], {
      cwd: `${__dirname}/processes`
    });
    this.childProcess.on('message', (msg) => {});
  }
  
  *runPost() {
    yield this.closeChildProcess(this.childProcess);
  }
}

module.exports = ProcessCond;
