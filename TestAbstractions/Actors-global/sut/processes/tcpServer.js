
'use strict';

const Net = require('net');


class TcpServer {
  constructor(host, port) {
    this.serverSocket = null;
    this.host = host;
    this.port = port;
    this.sockets = new Set();
  }
  
  start(done) {
    let cancel = false;
    this.serverSocket = Net.createServer((socket) => {
      this.sockets.add(socket);
      let buf = '';
      socket.on('data', (buffer) => {
        buf += buffer.toString();
        if(buf.endsWith('\r\n')) {
          const object = JSON.parse(buf);
          switch(object.cmd) {
            case 'login':
              socket.write(JSON.stringify({result:'ok'}));
              socket.write('\r\n');
              break;
            case 'msg':
            {
              const address = {
                host: object.host,
                port: object.port,
                localAddress: this.host,
                localPort: 9091
              };
              const outgoingSocket = Net.connect(address, () => {
                outgoingSocket.write(JSON.stringify({reqCmd:'name'}));
                outgoingSocket.write('\r\n');
              });
              outgoingSocket.on('data', (buf) => {
                socket.write(buf);
              });
              break;
            }
            default:
              console.error('NOT FOUND CMD:', object.cmd);
          };
        }
      });
    });
    this.serverSocket.on('error', (err) => {
      if(!cancel) {
        done(err);
        cancel = true;
      }
    });
    this.serverSocket.listen({
      host: this.host,
      port: this.port,
      exclusive: true,
      allowHalfOpen: true
    }, () => {
      if(!cancel) {
        done();
        cancel = true;
      }
    });
  }
  
  close(done) {
    this.serverSocket.close((err) => {
      done(err);
    });
  }
}


module.exports = TcpServer;
