
const ActorApi = require('actor-api');
const StackApi = require('stack-api');


class OneConnectionOneMessageToSutFailOrig extends ActorApi.ActorOriginating {
  constructor() {
    super();
    this.networkType = StackApi.NetworkType.TCP;
    this.socketConnection = null;
  }

  *data() {
    this.networkType = this.getTestDataNumber('transport', 'tcp', {
      tcp: StackApi.NetworkType.TCP,
      udp: StackApi.NetworkType.UDP
    });
  }
  
  *initClient() {
    let isCatched = false;
    try {
      this.socketConnection = this.createConnection('socket', {
        networkType: this.networkType
      });
    }
    catch(err) {
      isCatched = true;
      VERIFY_VALUE('ECONNREFUSED', err.code);
    }
    VERIFY_VALUE(true, isCatched);
  }
  
  *run() {
  }
  
  *exit(interrupted) {
    this.closeConnection(this.socketConnection);
  }
}

module.exports = OneConnectionOneMessageToSutFailOrig;
