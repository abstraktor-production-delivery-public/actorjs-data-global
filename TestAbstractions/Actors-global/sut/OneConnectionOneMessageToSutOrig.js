
const ActorApi = require('actor-api');
const StackApi = require('stack-api');


class OneConnectionOneMessageToSutOrig extends ActorApi.ActorOriginating {
  constructor() {
    super();
    this.networkType = StackApi.NetworkType.TCP;
    this.socketConnection = null;
  }

  *data() {
    this.networkType = this.getTestDataNumber('transport', 'tcp', {
      tcp: StackApi.NetworkType.TCP,
      udp: StackApi.NetworkType.UDP
    });
  }
  
  *initClient() {
    this.socketConnection = this.createConnection('socket', {
      networkType: this.networkType
    });
  }
  
  *run() {
    this.socketConnection.sendObject({cmd: 'login'});
    const response = this.socketConnection.receiveObject();
  }
  
  *exit(interrupted) {
    this.closeConnection(this.socketConnection);
  }
}

module.exports = OneConnectionOneMessageToSutOrig;
