
const ActorApi = require('actor-api');


class VerifyMandatoryTestDataValueArrayStringLocal extends ActorApi.ActorLocal {
  constructor() {
    super();
    this.testDataMandatory = [];
  }
  
  *data() {
    this.testDataMandatory = this.getTestDataStrings('mandatory');
  }
  
  *run() {
    VERIFY_MANDATORY('mandatory', this.testDataMandatory);
  }
}

module.exports = VerifyMandatoryTestDataValueArrayStringLocal;
