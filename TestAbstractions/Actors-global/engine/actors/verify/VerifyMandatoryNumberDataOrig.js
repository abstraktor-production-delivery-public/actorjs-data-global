
const ActorApi = require('actor-api');


class VerifyMandatoryNumberDataOrig extends ActorApi.ActorOriginating {
  constructor() {
    super();
    this.testDataMandatory = 0;
  }
  
  *data() {
    this.testDataMandatory = this.getTestDataNumber('mandatory');
  }
  
  *run() {
    VERIFY_MANDATORY('mandatory', this.testDataMandatory);
  }
}

module.exports = VerifyMandatoryNumberDataOrig;
