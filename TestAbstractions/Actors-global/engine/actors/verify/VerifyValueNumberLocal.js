
const ActorApi = require('actor-api');


class VerifyValueNumberLocal extends ActorApi.ActorLocal {
  constructor() {
    super();
    this.expected = 0;
    this.actual = 0;
  }
  
  *data() {
    this.expected = this.getTestDataNumber('expected');
    this.actual = this.getTestDataNumber('actual');
  }
  
  *run() {
    VERIFY_VALUE(this.expected, this.actual);
  }
}

module.exports = VerifyValueNumberLocal;
