
const ActorApi = require('actor-api');


class VerifyMandatoryTestDataValueArrayNumberLocal extends ActorApi.ActorLocal {
  constructor() {
    super();
    this.testDataMandatory = [];
  }
  
  *data() {
    this.testDataMandatory = this.getTestDataNumbers('mandatory');
  }
  
  *run() {
    VERIFY_MANDATORY('mandatory', this.testDataMandatory);
  }
}

module.exports = VerifyMandatoryTestDataValueArrayNumberLocal;
