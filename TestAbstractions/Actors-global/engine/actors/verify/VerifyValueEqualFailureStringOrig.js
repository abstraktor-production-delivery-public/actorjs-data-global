
const ActorApi = require('actor-api');


class VerifyValueEqualFailureStringOrig extends ActorApi.ActorOriginating {
  *data() {
  }
  
  *run() {
    VERIFY_VALUE('abcdef', 'abcdff');
  }
}

module.exports = VerifyValueEqualFailureStringOrig;
