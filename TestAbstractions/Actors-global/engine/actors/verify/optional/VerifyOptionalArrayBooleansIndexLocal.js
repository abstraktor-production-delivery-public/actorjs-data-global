
const ActorApi = require('actor-api');


class VerifyOptionalArrayBooleansIndexLocal extends ActorApi.ActorLocal {
  constructor() {
    super();
    this.testDataOptional = [[]];
  }
  
  *data() {
    this.testDataOptional = this.getTestDataArrayBooleans('optional');
  }
  
  *run() {
    this.testDataOptional.forEach((booleans, index1) => {
      VERIFY_OPTIONAL('optional', booleans, index1);
      booleans.forEach((boolean, index2)=> {
        VERIFY_OPTIONAL('optional', boolean, index1, index2);
      });
    });
  }
}

module.exports = VerifyOptionalArrayBooleansIndexLocal;
