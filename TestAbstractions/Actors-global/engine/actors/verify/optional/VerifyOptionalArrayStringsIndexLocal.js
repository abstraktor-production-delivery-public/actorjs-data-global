
const ActorApi = require('actor-api');


class VerifyOptionalArrayStringsIndexLocal extends ActorApi.ActorLocal {
  constructor() {
    super();
    this.testDataOptional = [[]];
  }
  
  *data() {
    this.testDataOptional = this.getTestDataArrayStrings('optional');
  }
  
  *run() {
    this.testDataOptional.forEach((strings, index1) => {
      VERIFY_OPTIONAL('optional', strings, index1);
      strings.forEach((string, index2)=> {
        VERIFY_OPTIONAL('optional', string, index1, index2);
      });
    });
  }
}

module.exports = VerifyOptionalArrayStringsIndexLocal;
