
const ActorApi = require('actor-api');


class LoadDataLocal extends ActorApi.ActorLocal {
  constructor() {
    super();
  }
  
  *data() {
  }
}

module.exports = LoadDataLocal;
