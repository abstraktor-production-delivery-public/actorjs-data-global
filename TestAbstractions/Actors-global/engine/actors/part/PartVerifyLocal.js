
const ActorApi = require('actor-api');


class PartVerifyLocal extends ActorApi.ActorLocal {
  constructor() {
    super();
  }
  
  *run() {
    VERIFY_MANDATORY('execution-order', this.getSharedData('execution-order'));
  }
}

module.exports = PartVerifyLocal;
