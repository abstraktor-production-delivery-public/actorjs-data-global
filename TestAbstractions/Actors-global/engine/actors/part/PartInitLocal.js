
const ActorApi = require('actor-api');


class PartInitLocal extends ActorApi.ActorLocal {
  constructor() {
    super();
  }
  
  *run() {
    this.setSharedData('execution-order', []);
  }
}

module.exports = PartInitLocal;
