
const ActorApi = require('actor-api');
const SuccessValuePre = require('./part/SuccessValuePre');
const SuccessValuePost = require('./part/SuccessValuePost');


class BothPartsSeveralSuccessOrig extends ActorApi.ActorOriginating {
  constructor() {
    super([SuccessValuePre, SuccessValuePre, SuccessValuePost, SuccessValuePost]);
  }

  *run() {
    this.getSharedData('execution-order').push(`${this.logName}`);
  }
}

module.exports = BothPartsSeveralSuccessOrig;
