
const ActorApi = require('actor-api');


class SuccessValuePre extends ActorApi.ActorPartPre {
  constructor() {
    super();
  }
  
  *run() {
    this.getSharedData('execution-order').push(`${this.name}[${this.index}]`);
  }
}

module.exports = SuccessValuePre;
