
const ActorApi = require('actor-api');
const SuccessValuePost = require('./part/SuccessValuePost');


class PostPartOneSuccessLocal extends ActorApi.ActorLocal {
  constructor() {
    super(SuccessValuePost);
  }
  
  *run() {
    this.getSharedData('execution-order').push(`${this.logName}`);
  }
}

module.exports = PostPartOneSuccessLocal;
