
const ActorApi = require('actor-api');
const FailureValuePre = require('./part/FailureValuePre');


class PrePartOneFailureOrig extends ActorApi.ActorOriginating {
  constructor() {
    super(FailureValuePre);
    this.failureIndex = 0;
  }

  *data() {
    this.failureIndex = this.getTestDataNumber('failure-index');
  }
  
  *run() {
    this.getSharedData('execution-order').push(`${this.logName}`);
    VERIFY_VALUE(1, (0 === this.failureIndex ? 2 : 1));
  }
}

module.exports = PrePartOneFailureOrig;
