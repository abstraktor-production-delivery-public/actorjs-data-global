
const ActorApi = require('actor-api');


class ThrowCond extends ActorApi.ActorCondition {
  constructor() {
    super();
    this.throwState = '';
  }
  
  *data() {
    this.throwState = this.getTestDataString('throwState');
    this.getSharedDataActor('state')[0] += 1;
    if('data' === this.throwState) {
      throw new Error('Simulated exception in data state.');
    }
  }
  
  *initClientPre() {
    this.getSharedDataActor('state')[2] += 1;
    if('initClientPre' === this.throwState) {
      throw new Error('Simulated exception in initClientPre state.');
    }
  }
  
  *runPre() {
    this.getSharedDataActor('state')[3] += 1;
    if('runPre' === this.throwState) {
      throw new Error('Simulated exception in runPre state.');
    }
  }
  
  *exitPre() {
    this.getSharedDataActor('state')[4] += 1;
    if('exitPre' === this.throwState) {
      throw new Error('Simulated exception in exitPre state.');
    }
  }
  
  *initClientPost() {
    this.getSharedDataActor('state')[6] += 1;
    if('initClientPost' === this.throwState) {
       throw new Error('Simulated exception in initClientPost state.');
    }
  }
  
  *runPost() {
    this.getSharedDataActor('state')[7] += 1;
    if('runPost' === this.throwState) {
      throw new Error('Simulated exception in runPost state.');
    }
  }
  
  *exitPost() {
    this.getSharedDataActor('state')[8] += 1;
    if('exitPost' === this.throwState) {
      throw new Error('Simulated exception in exitPost state.');
    }
  }
}

module.exports = ThrowCond;
