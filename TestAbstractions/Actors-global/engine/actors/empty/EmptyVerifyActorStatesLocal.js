
const ActorApi = require('actor-api');


class EmptyVerifyActorStatesLocal extends ActorApi.ActorLocal {
  constructor() {
    super();
    this.testDataActorNames = [];
    this.testDataActorInstanceIndices = [];
  }
  
  *data() {
    this.testDataActorNames = this.getTestDataStrings('execActorNames');
    this.testDataActorInstanceIndices = this.getTestDataStrings('execActorInstanceIndices');
  }
  
  *run() {
    const stateControls = [];
    for(let i = 0; i < this.testDataActorNames.length; ++i) {
      stateControls.push(this.getSharedDataActor('state', this.testDataActorNames[i], this.testDataActorInstanceIndices[i]));
    }
    VERIFY_MANDATORY('stateControl', stateControls);
  }
}

module.exports = EmptyVerifyActorStatesLocal;
