
const ActorApi = require('actor-api');


class EmptyVerifyCondActorStatesInitLocal extends ActorApi.ActorLocal {
  constructor() {
    super();
    this.testDataActorNames = [];
    this.testDataActorInstanceIndices = [];
  }
  
  *data() {
    this.testDataActorNames = this.getTestDataStrings('preActorNames');
    this.testDataActorInstanceIndices = this.getTestDataNumbers('preActorInstanceIndices');
    this.setSharedDataActorSync('state', [0, 0, 0, 0, 0], this.testDataActorNames[0], this.testDataActorInstanceIndices[0]);
    for(let i = 1; i < this.testDataActorNames.length; ++i) {
      this.setSharedDataActorSync('state', [0, 0, 0, 0, 0, 0, 0, 0, 0], this.testDataActorNames[i], this.testDataActorInstanceIndices[i]);
    }
  }
}

module.exports = EmptyVerifyCondActorStatesInitLocal;
