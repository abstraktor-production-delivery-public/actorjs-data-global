
const ActorApi = require('actor-api');


class NoneProxy extends ActorApi.ActorProxy {
  *data() {
  }
  
  *initServer() {
  }
  
  *initClient() {
  }
  
  *run() {
  }
  
  *exit(interrupted) {
  }
}

module.exports = NoneProxy;
