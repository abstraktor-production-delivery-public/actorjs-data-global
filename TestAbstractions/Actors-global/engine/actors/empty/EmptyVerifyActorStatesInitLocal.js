
const ActorApi = require('actor-api');


class EmptyVerifyActorStatesInitLocal extends ActorApi.ActorLocal {
  constructor() {
    super();
    this.testDataActorNames = [];
    this.testDataActorInstanceIndices = [];
  }
  
  *data() {
    this.testDataActorNames = this.getTestDataStrings('execActorNames');
    this.testDataActorInstanceIndices = this.getTestDataStrings('execActorInstanceIndices');
    for(let i = 0; i < this.testDataActorNames.length; ++i) {
      this.setSharedDataActor('state', [0, 0, 0, 0, 0], this.testDataActorNames[i], this.testDataActorInstanceIndices[i]);
    }
  }
}

module.exports = EmptyVerifyActorStatesInitLocal;
