
const ActorApi = require('actor-api');


class NoneOrig extends ActorApi.ActorOriginating {
  *data() {
  }
  
  *initClient() {
  }
  
  *run() {
  }
  
  *exit(interrupted) {
  }
}

module.exports = NoneOrig;
