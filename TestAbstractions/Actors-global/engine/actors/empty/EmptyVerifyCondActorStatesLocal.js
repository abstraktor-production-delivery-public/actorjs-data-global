
const ActorApi = require('actor-api');


class EmptyVerifyCondActorStatesLocal extends ActorApi.ActorLocal {
  constructor() {
    super();
    this.testDataActorNames = null;
    this.testDataActorInstanceIndices = null;
    this.orderIndices = null;
  }
  
  *data() {
    this.testDataActorNames = this.getTestDataStrings('preActorNames');
    this.testDataActorInstanceIndices = this.getTestDataNumbers('preActorInstanceIndices');
    this.orderIndices = this.getTestDataNumbers('stateResultIndex');
  }
  
  *run() {
    let stateControls = []
    for(let i = 0; i < this.testDataActorNames.length; ++i) {
      stateControls.push(this.getSharedDataActor('state', this.testDataActorNames[i], this.testDataActorInstanceIndices[i]));
    }
    VERIFY_MANDATORY('stateControl', stateControls);
    
    let orderIndex = 0;
    let actorResultsArray = [];
    this.nodes.forEach((node) => {
      const actor = node.actor;
      if(actor) {
        const actorOrderIndex = actor.getOrderIndexFromPhase(node.phaseId);
        if(this.orderIndices[orderIndex] === actorOrderIndex) {
          const index = actor.getIndexFromPhase(node.phaseId);
          actorResultsArray.push([]);
          for(let j = 0; j < 5; ++j) {
            actorResultsArray[orderIndex].push(actor.getStateResultName(j, index));
          }
          ++orderIndex;
        }
      }
    });
    VERIFY_MANDATORY('stateResult', actorResultsArray);
  }
}


module.exports = EmptyVerifyCondActorStatesLocal;
