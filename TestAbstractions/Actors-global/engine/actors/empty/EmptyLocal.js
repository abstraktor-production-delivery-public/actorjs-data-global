
const ActorApi = require('actor-api');


class EmptyLocal extends ActorApi.ActorLocal {
  *data() {
    this.getSharedDataActor('state')[0] += 1;
  }
  
  *run() {
    this.getSharedDataActor('state')[3] += 1;
  }
  
  *exit(interrupted) {
    this.getSharedDataActor('state')[4] += 1;
  }
}

module.exports = EmptyLocal;
