
const ActorApi = require('actor-api');


class NoneLocal extends ActorApi.ActorLocal {
  *data() {
  }
  
  *run() {
  }
  
  *exit(interrupted) {
  }
}

module.exports = NoneLocal;
