
const ActorApi = require('actor-api');


class ContentLocal extends ActorApi.ActorLocal {
  constructor() {
    super();
    this.content = null;
  }
  
  *data() {
    this.content = this.getContent('content-name', 'image');
  }
  
  *run() {
    VERIFY_VALUE(this.content.size, this.content.getLength());
  }
}

module.exports = ContentLocal;
