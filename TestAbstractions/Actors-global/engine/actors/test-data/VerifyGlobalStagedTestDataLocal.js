
const ActorApi = require('actor-api');


class VerifyGlobalStagedTestDataLocal extends ActorApi.ActorLocal {
  constructor() {
    super();
    this.testDataValue = '';
  }
  
  *data() {
    this.testDataValue = this.getTestDataString('global-staged-test-data');
  }
  
  *run() {
    VERIFY_MANDATORY('staged-test-data', this.testDataValue);
  }
}

module.exports = VerifyGlobalStagedTestDataLocal;
