
const ActorApi = require('actor-api');
const Button = require('../_helper-apis/button');
const Route = require('../_helper-apis/route');
const Modal = require('../_helper-apis/modal');


class TestSuitesModalOrig extends ActorApi.ActorOriginating {
  constructor() {
    super();
    this.puppeteerConnection = null;
    this.modalSelector = '';
    this.modalSelector = '';
    this.locateElementId = '';
    this.closeButtonId = '';
    this.puppeteerTestOptimization = false;
  }
  
  *data() {
    this.modalSelector = this.getTestDataString('modal-selector');
    this.locateElementId = this.getTestDataString('locate-element-id');
    this.closeButtonId = this.getTestDataString('close-button-id');
    this.puppeteerTestOptimization = this.getTestDataBoolean('puppeteer-test-optimization', this.puppeteerTestOptimization);
  }
  
  *initClient() {
    this.puppeteerConnection = this.createConnection('puppeteer');
    this.puppeteerConnection.addHelpers(Button, Modal, Route);
    yield* this.puppeteerConnection.page.route.waitForInitialUrl(this.puppeteerConnection);
  }
  
  *run() {
    const page = this.puppeteerConnection.page;
  
    yield* page.button.waitNotDisabledClassAndClickId(this.locateElementId);
    yield* page.modal.waitOpen(this.puppeteerTestOptimization, this.modalSelector);
    VERIFY_MANDATORY('verify-modal-heading', yield* page.modal.waitForTitle());
    
    yield* page.button.waitNotDisabledAttributeAndClickId(this.closeButtonId);
    yield* page.modal.waitClosed(this.puppeteerTestOptimization, this.modalSelector);
  }
  
  *exit(interrupted) {
    this.closeConnection(this.puppeteerConnection);
  }
}

module.exports = TestSuitesModalOrig;
