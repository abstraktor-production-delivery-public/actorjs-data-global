
const ActorApi = require('actor-api');
const Anchor = require('../_helper-apis/anchor');
const Route = require('../_helper-apis/route');
const Tab = require('../_helper-apis/tab');


class TestSuitesNavigationOrig extends ActorApi.ActorOriginating {
  constructor() {
    super();
    this.puppeteerConnection = null;
    this.locateAnchorType = '';
    this.locateAnchorTexts = [];
    this.locateAnchorLevels = [];
    this.locateTabName = '';
  }
  
  *data() {
    this.locateAnchorType = this.getTestDataString('locate-anchor-type');
    this.locateAnchorLevels = this.getTestDataStrings('locate-anchor-level');
    this.locateAnchorTexts = this.getTestDataStrings('locate-anchor-text');
    this.locateTabName = this.getTestDataString('locate-tab-name');
  }
  
  *initClient() {
    this.puppeteerConnection = this.createConnection('puppeteer');
    this.puppeteerConnection.addHelpers(Anchor, Route, Tab);
    yield* this.puppeteerConnection.page.route.waitForInitialUrl(this.puppeteerConnection);
  }
  
  *run() {
    const page = this.puppeteerConnection.page;
    
    let i = -1;
    let url = '/test-suites';
    let coreUrl = '';
    while(++i < this.locateAnchorTexts.length) {
      url += `/${this.locateAnchorTexts[i]}`;
      if(2 === i) {
        coreUrl = url;
        url += '/definition';
      }
      const anchorXpath = `//table[@id='${this.locateAnchorType}_${this.locateAnchorLevels[i]}_table']/tbody/tr/td/a[text()='${this.locateAnchorTexts[i]}']`;
      yield* page.anchor.waitLinkAndClick(anchorXpath, url);
    }
    url = `${coreUrl}/${this.locateTabName}`;
    if('definition' !== this.locateTabName) {
      
    }
    yield* page.tab.waitForActive(`test_suite_${this.locateTabName}`);
  }
  
  *exit(interrupted) {
    this.closeConnection(this.puppeteerConnection);
  }
}

module.exports = TestSuitesNavigationOrig;
