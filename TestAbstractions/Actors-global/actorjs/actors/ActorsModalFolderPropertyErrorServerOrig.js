
const ActorApi = require('actor-api');
const Button = require('../_helper-apis/button');
const Input = require('../_helper-apis/input');
const Modal = require('../_helper-apis/modal');
const Route = require('../_helper-apis/route');
const Tree = require('../_helper-apis/tree');


class ActorsModalFolderPropertyErrorServerOrig extends ActorApi.ActorOriginating {
  constructor() {
    super();
    this.puppeteerConnection = null;
    this.openFolderPath = '';
    this.clickFolder = '';
    this.newName = '';
    this.errorName = '';
  }
  
  *data() {
    this.openFolderPath = this.getTestDataString('open-folder-path');
    this.clickFolder = this.getTestDataString('click-folder');
    this.newName = this.getTestDataString('new-name');
    this.errorName = this.getTestDataString('error-name');
  }
  
  *initClient() {
    this.puppeteerConnection = this.createConnection('puppeteer');
    this.puppeteerConnection.addHelpers(Button, Input, Modal, Route, Tree);
    yield* this.puppeteerConnection.page.route.waitForInitialUrl(this.puppeteerConnection);
  }
  
  *run() {       
    const page = this.puppeteerConnection.page;
    page.tree.setTreeId('actor_editor_tree_code_global');
    
    yield* page.tree.expandFolder('Actors-global');
    yield* page.tree.expandFolder('Actors-global/actorjs-test');
    yield* page.tree.expandFolder(this.openFolderPath);
    yield* page.tree.selectFolder(`${this.openFolderPath}/${this.clickFolder}`, 'title', `/actor-editor/${this.openFolderPath}/${this.clickFolder}`);
    
    yield* page.button.waitNotDisabledClassAndClickId('actor_folder_properties');
    
    yield* page.modal.waitOpen(false, '#modal_folder_properties');
    VERIFY_MANDATORY('verify-modal-heading', yield* page.modal.waitForTitle());
    
    yield* page.input.waitSelectedTextById('modal_dialog_folder_properties_name', this.clickFolder);
    yield page.type('#modal_dialog_folder_properties_name', this.newName);
    
    yield* page.button.waitNotDisabledAttributeAndClickId('folder_properties_change_button');
    
    if('exists' === this.errorName) {
      VERIFY_VALUE(`Path './${this.openFolderPath}/${this.newName}' does already exist.`, yield* page.modal.waitForErrorMessge('folder_properties_modal_error_label'));
    }
    else {
      VERIFY_VALUE(true, false);
    }
    
    yield* page.button.waitNotDisabledAttributeAndClickId('folder_properties_close_button');
    yield* page.modal.waitClosed(this.puppeteerTestOptimization, '#modal_folder_properties');
  }
  
  *exit(interrupted) {
    this.closeConnection(this.puppeteerConnection);
  }
}

module.exports = ActorsModalFolderPropertyErrorServerOrig;
