
const ActorApi = require('actor-api');
const Route = require('../_helper-apis/route');
const Tab = require('../_helper-apis/tab');
const Tree = require('../_helper-apis/tree');


class ActorsLocateOrig extends ActorApi.ActorOriginating {
  constructor() {
    super();
    this.puppeteerConnection = null;
  }
  
  *initClient() {
    this.puppeteerConnection = this.createConnection('puppeteer');
    this.puppeteerConnection.addHelpers(Route, Tab, Tree);
    yield* this.puppeteerConnection.page.route.waitForInitialUrl(this.puppeteerConnection);
  }
  
  *run() {
    const page = this.puppeteerConnection.page;
    page.tree.setTreeId('actor_editor_tree_code_global');
    
    yield* page.tree.expandFolder('Actors-global');
    yield* page.tree.expandFolder('Actors-global/actorjs-test');
    yield* page.tree.selectFile('Actors-global/actorjs-test/TestLocal.js', 'title', '/actor-editor/Actors-global/actorjs-test/TestLocal.js');
    yield* page.tab.waitForActive('b27f2f47-fd3f-4930-867c-8729cd73ee13');
  }
  
  *exit(interrupted) {
    this.closeConnection(this.puppeteerConnection);
  }
}

module.exports = ActorsLocateOrig;
