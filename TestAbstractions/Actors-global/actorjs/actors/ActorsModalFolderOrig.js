
const ActorApi = require('actor-api');
const Button = require('../_helper-apis/button');
const Modal = require('../_helper-apis/modal');
const Route = require('../_helper-apis/route');
const Tree = require('../_helper-apis/tree');


class ActorsModalFolderOrig extends ActorApi.ActorOriginating {
  constructor() {
    super();
    this.puppeteerConnection = null;
    this.modalSelector = '';
    this.locateElementId = '';
    this.modalCloseButtonId = '';
    this.closeMethod = 'button';
  }
  
  *data() {
    this.modalSelector = this.getTestDataString('modal-selector');
    this.locateElementId = this.getTestDataString('locate-element-id');
    this.modalCloseButtonId = this.getTestDataString('modal-close-button-id');
    this.closeMethod = this.getTestDataString('close-method', this.closeMethod);
  }
  
  *initClient() {
    this.puppeteerConnection = this.createConnection('puppeteer');
    this.puppeteerConnection.addHelpers(Button, Modal, Route, Tree);
    yield* this.puppeteerConnection.page.route.waitForInitialUrl(this.puppeteerConnection);
  }
  
  *run() {
    const page = this.puppeteerConnection.page;
    page.tree.setTreeId('actor_editor_tree_code_global');
    
    yield* page.tree.expandFolder('Actors-global');
    
    yield* page.tree.selectFolder('Actors-global/actorjs-test', 'title', '/actor-editor/Actors-global/actorjs-test');
    
    yield* page.button.waitNotDisabledClassAndClickId(this.locateElementId);
    
    yield* page.modal.waitOpen(this.modalSelector);
    
    VERIFY_MANDATORY('verify-modal-heading', yield* page.modal.waitForTitle());
    
    if('button' === this.closeMethod) {
      yield* page.button.waitNotDisabledAttributeAndClickId(this.modalCloseButtonId);
    }
    else if('keyboard' === this.closeMethod) {
      yield page.keyboard.down('Control');
      yield page.keyboard.down('Shift');
      yield page.keyboard.down('C');
      yield page.keyboard.up('Shift');
      yield page.keyboard.up('Control');
      yield page.keyboard.down('C');
    }
    yield* page.modal.waitClosed(this.modalSelector);
  }
  
  *exit(interrupted) {
    this.closeConnection(this.puppeteerConnection);
  }
}


module.exports = ActorsModalFolderOrig;
