
const ActorApi = require('actor-api');
const Button = require('../_helper-apis/button');
const Input = require('../_helper-apis/input');
const Modal = require('../_helper-apis/modal');
const Route = require('../_helper-apis/route');
const Tree = require('../_helper-apis/tree');


class ActorsModalFileNewOrig extends ActorApi.ActorOriginating {
  constructor() {
    super();
    this.puppeteerConnection = null;
    this.modalSelector = '';
    this.locateElementId = '';
    this.newFileName = '';
    this.puppeteerTestOptimization = false;
  }
  
  *data() {
    this.modalSelector = this.getTestDataString('modal-selector');
    this.locateElementId = this.getTestDataString('locate-element-id');
    this.newFileName = this.getTestDataString('new-file-name');
    this.puppeteerTestOptimization = this.getTestDataBoolean('puppeteer-test-optimization', this.puppeteerTestOptimization);
  }
  
  *initClient() {
    this.puppeteerConnection = this.createConnection('puppeteer');
    this.puppeteerConnection.addHelpers(Button, Input, Modal, Route, Tree);
    yield* this.puppeteerConnection.page.route.waitForInitialUrl(this.puppeteerConnection);
  }
  
  *run() {
    const page = this.puppeteerConnection.page;
    page.tree.setTreeId('actor_editor_tree_code_global');
    
    yield* page.tree.expandFolder('Actors-global');
    yield* page.tree.selectFolder('Actors-global/actorjs-test', 'icon', '/actor-editor/Actors-global/actorjs-test');
   
    yield* page.button.waitNotDisabledClassAndClickId(this.locateElementId);
    
    yield* page.modal.waitOpen(this.puppeteerTestOptimization, this.modalSelector);
    VERIFY_MANDATORY('verify-modal-heading', yield* page.modal.waitForTitle());
    
    yield* page.input.waitSelectedTextById('modal_dialog_file_new_name', '');
    yield page.type('#modal_dialog_file_new_name', this.newFileName);
    
    yield* page.button.waitNotDisabledAttributeAndClickId('file_new_new_button');
    yield* page.modal.waitClosed(this.puppeteerTestOptimization, '#modal_file_new');
    
    VERIFY_VALUE(true, yield* page.tree.fileExist('Actors-global/actorjs-test', `${this.newFileName}Orig.js`), `Does file: 'Actors-global/actorjs-test/${this.newFileName}Orig.js' exist?`);
  }
  
  *exit(interrupted) {
    this.closeConnection(this.puppeteerConnection);
  }
}

module.exports = ActorsModalFileNewOrig;
