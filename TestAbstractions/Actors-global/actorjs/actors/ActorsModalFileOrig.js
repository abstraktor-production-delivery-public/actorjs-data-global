
const ActorApi = require('actor-api');
const Button = require('../_helper-apis/button');
const Modal = require('../_helper-apis/modal');
const Route = require('../_helper-apis/route');
const Tab = require('../_helper-apis/tab');
const Tree = require('../_helper-apis/tree');


class ActorsModalFileOrig extends ActorApi.ActorOriginating {
  constructor() {
    super();
    this.puppeteerConnection = null;
    this.modalSelector = '';
    this.locateElementId = '';
    this.modalCloseButtonId = '';
    this.closeMethod = 'button';
    this.puppeteerTestOptimization = false;
  }
  
  *data() {
    this.modalSelector = this.getTestDataString('modal-selector');
    this.locateElementId = this.getTestDataString('locate-element-id');
    this.modalCloseButtonId = this.getTestDataString('modal-close-button-id');
    this.closeMethod = this.getTestDataString('close-method', this.closeMethod);
    this.puppeteerTestOptimization = this.getTestDataBoolean('puppeteer-test-optimization', this.puppeteerTestOptimization);
  }
  
  *initClient() {
    this.puppeteerConnection = this.createConnection('puppeteer');
    this.puppeteerConnection.addHelpers(Button, Modal, Route, Tab, Tree);
    yield* this.puppeteerConnection.page.route.waitForInitialUrl(this.puppeteerConnection);
  }
  
  *run() {
    const page = this.puppeteerConnection.page;
    page.tree.setTreeId('actor_editor_tree_code_global');
    
    yield* page.tree.expandFolder('Actors-global');
    yield* page.tree.expandFolder('Actors-global/actorjs-test');
    yield* page.tree.selectFile('Actors-global/actorjs-test/TestLocal.js', 'title', '/actor-editor/Actors-global/actorjs-test/TestLocal.js');
    yield* page.tab.waitForActive('b27f2f47-fd3f-4930-867c-8729cd73ee13');
    
    yield* page.button.waitNotDisabledClassAndClickId(this.locateElementId);
    
    yield* page.modal.waitOpen(this.puppeteerTestOptimization, this.modalSelector);
    VERIFY_MANDATORY('verify-modal-heading', yield* page.modal.waitForTitle());
    
    if('button' === this.closeMethod) {
      yield* page.button.waitNotDisabledAttributeAndClickId(this.modalCloseButtonId);
    }
    else if('keyboard' === this.closeMethod) {
      yield page.keyboard.down('Control');
      yield page.keyboard.down('Shift');
      yield page.keyboard.down('C');
      yield page.keyboard.up('Shift');
      yield page.keyboard.up('Control');
    }
    yield* page.modal.waitClosed(this.puppeteerTestOptimization, this.modalSelector);
  }
  
  *exit(interrupted) {
    this.closeConnection(this.puppeteerConnection);
  }
}


module.exports = ActorsModalFileOrig;
