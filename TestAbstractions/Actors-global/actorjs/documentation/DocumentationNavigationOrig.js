
const ActorApi = require('actor-api');
const Documentation = require('../_helper-apis/documentation');
const Route = require('../_helper-apis/route');


class DocumentationNavigationOrig extends ActorApi.ActorOriginating {
  constructor() {
    super();
    this.puppeteerConnection = null;
    this.locateAnchorTexts = null;
  }
  
  *data() {
    this.locateAnchorTexts = this.getTestDataStrings('locate-anchor-text');
  }
  
  *initClient() {
    this.puppeteerConnection = this.createConnection('puppeteer');
    this.puppeteerConnection.addHelpers(Documentation, Route);
    yield* this.puppeteerConnection.page.route.waitForInitialUrl(this.puppeteerConnection);
  }
  
  *run() {
    const page = this.puppeteerConnection.page;
    
    yield* page.documentation.waitDocumentationLoaded();
    yield* page.documentation.waitHeadingAndOpen(this.locateAnchorTexts[0]);
    yield* page.documentation.waitLinkAndOpen(this.locateAnchorTexts[1]);
  }
  
  *exit(interrupted) {
    this.closeConnection(this.puppeteerConnection);
  }
}

module.exports = DocumentationNavigationOrig;
