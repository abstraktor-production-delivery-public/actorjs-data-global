
'use strict';

const HttpApi = require('http-stack-api');


class HttpPostRequest extends HttpApi.Request {
  constructor() {
    super(HttpApi.Method.POST, 'http://www.actorjs.com/abs-data/', HttpApi.Version.HTTP_1_1);
    this.addHeader(HttpApi.Header.HOST, 'actorjs.com');
    this.addHeader(HttpApi.Header.CONNECTION, 'keep-alive');
    this.addHeader(HttpApi.Header.ACCEPT, 'application/json, text/javascript, */*; q=0.01');
    this.addHeader(HttpApi.Header.USER_AGENT, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4298.0 Safari/537.36');
  }
}

module.exports = HttpPostRequest;
