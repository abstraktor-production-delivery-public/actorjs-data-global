
const ActorApi = require('actor-api');

const Fs = require('fs');
const Path = require('path');


class CleanLoginLocal extends ActorApi.ActorLocal {
  constructor() {
    super();
  }
  
  *data() {}
  
  *run() {
    const NodejsApiFs = this.require('nodejs-api-fs');
    yield* NodejsApiFs.rmdir(Path.resolve(`..${Path.sep}Test${Path.sep}ActorJs${Path.sep}Generated${Path.sep}Login`), { recursive: true });
  }
}

module.exports = CleanLoginLocal;
