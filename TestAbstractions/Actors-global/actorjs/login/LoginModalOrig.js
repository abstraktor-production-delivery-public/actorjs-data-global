
const ActorApi = require('actor-api');
const Route = require('../_helper-apis/route');


class LoginModalOrig extends ActorApi.ActorOriginating {
  constructor() {
    super();
    this.puppeteerConnection = null;
    this.locateButtonLogin = '';
    this.locateElementId = '';
    this.closeButton = '';
  }

  *data() {
    this.locateButtonLogin = this.getTestDataString('locate-buttonLogin');
    this.locateElementId = this.getTestDataString('locate-button-id');
    this.closeButton = this.getTestDataString('close-buttonId');
  }

  *initClient() {
    this.puppeteerConnection = this.createConnection('puppeteer');
    this.puppeteerConnection.addHelpers(Route);
    yield* this.puppeteerConnection.page.route.waitForInitialUrl(this.puppeteerConnection);
  }

  *run() {
    const page = this.puppeteerConnection.page;
    
    yield* page.button.waitNotDisabledClassAndClickId(this.locateElementId);
    
    yield* page.modal.waitOpen(this.puppeteerTestOptimization, this.modalSelector);
    VERIFY_MANDATORY('verify-modal-heading', yield* page.modal.waitForTitle());
    /*
    yield page.waitForFunction((selector) => {
      const button = document.getElementById(selector);
      return button ? !button.classList.contains('disabled') : false;
    }, {}, this.locateButtonLogin);
    
    yield page.click(`#${this.locateButtonLogin}`); 
    yield page.waitForFunction((selector) => {
      const button = document.getElementById(selector);
      return button ? !button.classList.contains('disabled') : false;
    }, {}, this.locateButton); 
    
    yield page.click(`#${this.locateButton}`);
    yield page.waitForFunction(() => { return document.querySelector('body').classList.contains('modal-open'); });

    yield page.waitForSelector('xpath///div[@style = "display: block;"]/div/div/div/h4');
    let [ModalH4] = yield page.$('xpath///div[@style = "display: block;"]/div/div/div/h4');
		let modalTitle = yield page.evaluate(h4 => h4.innerText, ModalH4);
    VERIFY_MANDATORY('verification-code', modalTitle);*/

    yield page.waitForFunction((selector) => {
      const button = document.getElementById(selector);
      return button ? !button.classList.contains('disabled') : false;
    }, {}, this.closeButton);
    
    yield page.click(`#${this.closeButton}`);
    yield page.waitForFunction(() => { return !document.querySelector('body').classList.contains('modal-open'); });
  }

  *exit(interrupted){
    this.closeConnection(this.puppeteerConnection);
  }
}

module.exports = LoginModalOrig;
