
const ActorApi = require('actor-api');
const GitSimple = require('simple-git');
const Fs = require('fs');
const Path = require('path');
const ChildProcess = require('child_process');


class InstallActorJsOrig extends ActorApi.ActorOriginating {
  constructor() {
    super();
    this.localPath = '../Test/ActorJs';
    this.localGitPath = Path.normalize(`${this.localPath}/actorjs`);
    this.repoPath = 'https://gitlab.com/abstraktor-development/actorjs.git';
    this.httpPort = 9020;
    this.httpHost = 'localhost';
    this.wsPort = 9021;
    this.wsHost = 'localhost';
    this.parentPort = 9022;
    this.parentHost = 'localhost';
    this.httpsPort = 9023;
    this.httpsHost = 'localhost';
  }
  
  *data() {
    const dstAddress = this.getDstAddress(0);
    this.httpPort = this.getTestDataNumber('http-port', dstAddress.port);
    this.httpHost = this.getTestDataString('http-host', dstAddress.host);
    this.wsPort = this.getTestDataNumber('ws-port', this.wsPort);
    this.wsHost = this.getTestDataString('ws-host', dstAddress.host);
    this.getTestDataNumber('actor-js-port', this.parentPort);
    this.getTestDataString('actor-js-host', this.parentHost);
    this.httpsPort = this.getTestDataNumber('https-port', this.httpsPort);
    this.httpsHost = this.getTestDataString('https-host', this.httpsHost);
  }
  
  *run() {
    let isRepo = false;
    const testGitDirectoryExists = this.callback((cb) => {
      Fs.access(this.localGitPath, Fs.constants.F_OK, (err) => {
        cb(undefined, err ? false : true);
      });
    });
    
    if(!testGitDirectoryExists) {
      const testDirectoryExists = this.callback((cb) => {
        Fs.access(this.localPath, Fs.constants.F_OK, (err) => {
          cb(undefined, err ? false : true);
        });
      });
      if(!testDirectoryExists) {
        this.logGui(`mkdir ${this.localPath}`);
        this.callback((cb) => {
          Fs.mkdir(this.localPath, {recursive: true}, (err) => {
            cb(err);
          });
        });
      }
    }
    else {
      isRepo = this.callback((cb) => {
        GitSimple(this.localGitPath).status((err, result) => {
          cb(null, err ? false :true);
        });
      }); 
    }
    
    if(isRepo) {
      this.logGui(`Repo '${this.repoPath}' exists on path '${this.localGitPath}'`);
    }
    else {
      this.logGui(`clone ${this.repoPath} to ${this.localGitPath}`);
      this.callback((cb) => {
        GitSimple().clone(this.repoPath, this.localGitPath, (err, result) => {
          cb(err);
        });
      });
      
      this.callback((cb) => {
        this.logGui('npm install');
        const childProcessNpmInstall = ChildProcess.exec('npm install', {cwd: this.localGitPath}, (err) => {
          if(err) {
            this.logError('npm install', err);
            cb(err);
          }
        });
        childProcessNpmInstall.stdout.on('data', (data) => {
          const logs = data.split('\n');
          logs.forEach((log) => {
            if('' !== log.trim()) {
              this.logIp('stdout:' +log);
            }
          });
        });
        childProcessNpmInstall.stderr.on('data', (data) => {
          const logs = data.split('\n');
          logs.forEach((log) => {
            if('' !== log.trim()) {
              this.logIp('stderr:' +log);
            }
          });
        });
        childProcessNpmInstall.on('exit', (number, signal) => {
          cb();
        });
      });
      
      this.callback((cb) => {
        this.logGui(this.localGitPath);
        this.logGui(`aj debug --build --port ${this.httpPort} --host ${this.httpHost} --sport ${this.httpsPort} --shost ${this.httpsHost} --wsp ${this.wsPort} --wsh ${this.wsHost} --pp ${this.parentPort} --ph ${this.parentHost}`);
        const childProcessActorJs = ChildProcess.exec(`aj debug --build --port ${this.httpPort} --host ${this.httpHost} --sport ${this.httpsPort} --shost ${this.httpsHost} --wsp ${this.wsPort} --wsh ${this.wsHost} --pp ${this.parentPort} --ph ${this.parentHost}`, {
          cwd: this.localGitPath
        }, (err) => {
          cb(err);
        });
        childProcessActorJs.stdout.on('data', (data) => {
          const logs = data.split('\n');
          logs.forEach((log) => {
            if('' !== log.trim()) {
              this.logIp('stdout:' + log);
            }
          });
        });
        childProcessActorJs.stderr.on('data', (data) => {
          const logs = data.split('\n');
          logs.forEach((log) => {
            if('' !== log.trim()) {
              this.logIp('stderr:' + log);
            }
          });
          childProcessActorJs.on('exit', (number, signal) => {
            cb();
          });
        });
      });
    }
  }
}

module.exports = InstallActorJsOrig;
