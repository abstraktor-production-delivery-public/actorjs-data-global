
const ActorApi = require('actor-api');
const Route = require('../_helper-apis/route');
const Tab = require('../_helper-apis/tab');
const Tree = require('../_helper-apis/tree');


class StacksLocateOrig extends ActorApi.ActorOriginating {
  constructor() {
    super();
    this.puppeteerConnection = null;
  }
    
  *initClient() {
    this.puppeteerConnection = this.createConnection('puppeteer');
    this.puppeteerConnection.addHelpers(Route, Tab, Tree);
    yield* this.puppeteerConnection.page.route.waitForInitialUrl(this.puppeteerConnection);
  }
  
  *run() {
    const page = this.puppeteerConnection.page;
    page.tree.setTreeId('stack_editor_tree_code_global');
    
    yield* page.tree.expandFolder('Stacks-global');
    yield* page.tree.expandFolder('Stacks-global/test');
    yield* page.tree.selectFile('Stacks-global/test/test-connection-client-options.js', 'title', '/stack-editor/Stacks-global/test/test-connection-client-options.js');
    yield* page.tab.waitForActive('29149f1a-7e2e-4492-9412-920ba6533d2b');
  }
  
  *exit(interrupted) {
    this.closeConnection(this.puppeteerConnection);
  }
}


module.exports = StacksLocateOrig;
