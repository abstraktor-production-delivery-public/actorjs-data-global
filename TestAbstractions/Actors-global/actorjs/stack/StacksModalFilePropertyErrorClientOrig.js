
const ActorApi = require('actor-api');
const Button = require('../_helper-apis/button');
const Input = require('../_helper-apis/input');
const Modal = require('../_helper-apis/modal');
const Route = require('../_helper-apis/route');
const Tab = require('../_helper-apis/tab');
const Tree = require('../_helper-apis/tree');


class StacksModalFilePropertyErrorClientOrig extends ActorApi.ActorOriginating {
  constructor() {
    super();
    this.puppeteerConnection = null;
    this.openFolderPath = '';
    this.clickFileName = '';
    this.clickFileId = '';
    this.newName = '';
    this.errorName = '';
  }
  
  *data() {
    this.openFolderPath = this.getTestDataString('open-folder-path');
    this.clickFileName = this.getTestDataString('click-file-name');
    this.clickFileId = this.getTestDataString('click-file-id');
    this.newName = this.getTestDataString('new-name');
    this.errorName = this.getTestDataString('error-name');
  }
  
  *initClient() {
    this.puppeteerConnection = this.createConnection('puppeteer');
    this.puppeteerConnection.addHelpers(Button, Input, Modal, Route, Tab, Tree);
    yield* this.puppeteerConnection.page.route.waitForInitialUrl(this.puppeteerConnection);
  }
  
  *run() {
    const page = this.puppeteerConnection.page;
    page.tree.setTreeId('stack_editor_tree_code_global');
    
    yield* page.tree.expandFolder('Stacks-global');
    yield* page.tree.expandFolder('Stacks-global/test');
    yield* page.tree.expandFolder(this.openFolderPath);
    yield* page.tree.selectFile(`${this.openFolderPath}/${this.clickFileName}`, 'title', `/stack-editor/${this.openFolderPath}/${this.clickFileName}`);
    yield* page.tab.waitForActive(this.clickFileId);
    
    yield* page.button.waitNotDisabledClassAndClickId('stack_file_properties');
    
    yield* page.modal.waitOpen(false, '#modal_file_properties');
    VERIFY_MANDATORY('verify-modal-heading', yield* page.modal.waitForTitle());
    
    const index = this.clickFileName.indexOf('.');
    const fileName = -1 === index ? this.clickFileName : this.clickFileName.substring(0, index);
    yield* page.input.waitSelectedTextById('modal_dialog_file_properties_name_input', fileName);
    yield page.type('#modal_dialog_file_properties_name_input', this.newName);
    
    if('characters' === this.errorName) {
      VERIFY_VALUE(`The name contains not allowed characters: '${this.getVerificationData('not-allowed-characters')}'`, yield* page.modal.waitForErrorMessge('file_properties_modal_error_label'));
    }
    else {
      VERIFY_VALUE(true, false);
    }
    
    yield* page.button.waitNotDisabledAttributeAndClickId('file_properties_close_button');
    yield* page.modal.waitClosed(this.puppeteerTestOptimization, '#modal_file_properties');
  }
  
  *exit(interrupted) {
    this.closeConnection(this.puppeteerConnection);
  }
}

module.exports = StacksModalFilePropertyErrorClientOrig;
