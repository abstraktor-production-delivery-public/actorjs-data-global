
'use strict';

const ActorApi = require('actor-api');
const ActorJsStackApi = require('actorjs-stack-api');
const HttpRestMs = require('./ms/HttpRestMs');


class StacksModalPropertyInter extends ActorApi.ActorIntercepting {
  constructor() {
    super();
    this.httpConnection = null;
    this.mockRequestName = '';
  }
  
  *data() {
    this.mockRequestName = this.getTestDataString('mock-request-name');
  }
  
  *initServer() {
    this.httpConnection = this.createServer('http', {
      messageSelector: new HttpRestMs(this.mockRequestName)
    });
  }
  
  *run() {
    this.httpConnection.accept();
    
    const request = this.httpConnection.receive();
    
    VERIFY_VALUE('POST', request.method);
    const actorJsRequest = JSON.parse(request.getBody().getBuffer().toString());
    
    this.httpConnection.send(new ActorJsStackApi.MsgResponseHttp(actorJsRequest.id, new ActorJsStackApi.PdResponseSuccess(this.mockRequestName)));
  }
  
  *exit(interrupted) {
    this.closeConnection(this.httpConnection);
  }
}

module.exports = StacksModalPropertyInter;
