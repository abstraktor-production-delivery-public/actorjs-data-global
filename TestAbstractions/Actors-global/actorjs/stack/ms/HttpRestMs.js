
'use strict';

const PuppeteerApi = require('puppeteer-stack-api');


class HttpRestMs extends PuppeteerApi.PuppeteerMessageSelector {
  constructor(requestName) {
    super();
    this.requestName = requestName;
  }
  
  onSelect(msg, HttpApi, WebsocketApi) {
    if(msg instanceof HttpApi.Request) {
      return msg.requestTarget.includes(this.requestName);
    }
    else {
      return false;
    }
  }
}

module.exports = HttpRestMs;
