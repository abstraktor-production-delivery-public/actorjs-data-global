
const ActorApi = require('actor-api');
const Button = require('../_helper-apis/button');
const Input = require('../_helper-apis/input');
const Modal = require('../_helper-apis/modal');
const Route = require('../_helper-apis/route');
const Tree = require('../_helper-apis/tree');


class StacksModalFolderPropertyOrig extends ActorApi.ActorOriginating {
  constructor() {
    super();
    this.puppeteerConnection = null;
    this.openFolderPath = '';
    this.clickFolder = '';
    this.newFolderName = '';
  }
  
  *data() {
    this.openFolderPath = this.getTestDataString('open-folder-path');
    this.clickFolder = this.getTestDataString('click-folder');
    this.newFolderName = this.getTestDataString('new-folder-name');
  }
  
  *initClient() {
    this.puppeteerConnection = this.createConnection('puppeteer');
    this.puppeteerConnection.addHelpers(Button, Input, Modal, Route, Tree);
    yield* this.puppeteerConnection.page.route.waitForInitialUrl(this.puppeteerConnection);
  }
  
  *run() {
    const page = this.puppeteerConnection.page;
    page.tree.setTreeId('stack_editor_tree_code_global');
    
    yield* page.tree.expandFolder('Stacks-global');
    yield* page.tree.expandFolder('Stacks-global/test');
    yield* page.tree.expandFolder(this.openFolderPath);
    yield* page.tree.selectFolder(`${this.openFolderPath}/${this.clickFolder}`, 'icon', `/stack-editor/${this.openFolderPath}/${this.clickFolder}`);
    
    yield* page.button.waitNotDisabledClassAndClickId('stack_folder_properties');
    
    yield* page.modal.waitOpen(false, '#modal_folder_properties');
    VERIFY_MANDATORY('verify-modal-heading', yield* page.modal.waitForTitle());
    
    yield* page.input.waitSelectedTextById('modal_dialog_folder_properties_name', this.clickFolder);
    yield page.type('#modal_dialog_folder_properties_name', this.newFolderName);
    
    yield* page.button.waitNotDisabledAttributeAndClickId('folder_properties_change_button');
    yield* page.modal.waitClosed(this.puppeteerTestOptimization, '#modal_folder_properties');
  }
  
  *exit(interrupted) {
    this.closeConnection(this.puppeteerConnection);
  }
}

module.exports = StacksModalFolderPropertyOrig;
