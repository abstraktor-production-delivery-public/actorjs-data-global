
const ActorApi = require('actor-api');
const ActorJsStackApi = require('actorjs-stack-api');
const HttpRestMs = require('./ms/HttpRestMs');


class StacksModalPropertyErrorInter extends ActorApi.ActorIntercepting {
  constructor() {
    super();
    this.httpConnection = null;
    this.mockRequestName = '';
    this.errorName = '';
  }
  
  *data() {
    this.mockRequestName = this.getTestDataString('mock-request-name');
    this.errorName = this.getTestDataString('error-name');
  }
  
  *initServer() {
    this.httpConnection = this.createServer('http', {
      messageSelector: new HttpRestMs(this.mockRequestName)
    });
  }
  
  *run() {
    this.httpConnection.accept();
    
    const request = this.httpConnection.receive();
    
    VERIFY_VALUE('POST', request.method);
    const actorJsRequest = JSON.parse(request.getBody().getBuffer().toString());
    const oldPath = actorJsRequest.requests[0].params[0];
    const newPath = actorJsRequest.requests[0].params[1];
    
    if('exists' === this.errorName) {
      this.httpConnection.send(new ActorJsStackApi.MsgResponseHttp(actorJsRequest.id, new ActorJsStackApi.PdResponseError(this.mockRequestName, null, `Path '${newPath}' does already exist.`)));
    }
    else {
      VERIFY_VALUE(true, false);
    }
  }
  
  *exit(interrupted) {
    this.closeConnection(this.httpConnection);
  }
}

module.exports = StacksModalPropertyErrorInter;
