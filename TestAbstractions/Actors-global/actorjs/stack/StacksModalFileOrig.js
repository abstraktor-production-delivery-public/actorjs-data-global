
const ActorApi = require('actor-api');
const Button = require('../_helper-apis/button');
const Modal = require('../_helper-apis/modal');
const Route = require('../_helper-apis/route');
const Tab = require('../_helper-apis/tab');
const Tree = require('../_helper-apis/tree');


class StacksModalFileOrig extends ActorApi.ActorOriginating {
  constructor() {
    super();
    this.puppeteerConnection = null;
    this.modalSelector = '';
    this.locateElementId = '';
    this.modalCloseButtonId = '';
    this.puppeteerTestOptimization = false;
  }
  
  *data() {
    this.modalSelector = this.getTestDataString('modal-selector');
    this.locateElementId = this.getTestDataString('locate-element-id');
    this.modalCloseButtonId = this.getTestDataString('modal-close-button-id');
    this.puppeteerTestOptimization = this.getTestDataBoolean('puppeteer-test-optimization', this.puppeteerTestOptimization);
  }
  
  *initClient() {
    this.puppeteerConnection = this.createConnection('puppeteer');
    this.puppeteerConnection.addHelpers(Button, Modal, Route, Tab, Tree);
    yield* this.puppeteerConnection.page.route.waitForInitialUrl(this.puppeteerConnection);
  }
  
  *run() {
    const page = this.puppeteerConnection.page;
    page.tree.setTreeId('stack_editor_tree_code_global');
    
    yield* page.tree.expandFolder('Stacks-global');
    yield* page.tree.expandFolder('Stacks-global/test');
    yield* page.tree.selectFile('Stacks-global/test/test-connection-client-options.js', 'title', '/stack-editor/Stacks-global/test/test-connection-client-options.js');
    yield* page.tab.waitForActive('29149f1a-7e2e-4492-9412-920ba6533d2b');
    
    yield* page.button.waitNotDisabledClassAndClickId(this.locateElementId);
    
    yield* page.modal.waitOpen(this.puppeteerTestOptimization, this.modalSelector);
    VERIFY_MANDATORY('verify-modal-heading', yield* page.modal.waitForTitle());
    
    yield* page.button.waitNotDisabledAttributeAndClickId(this.modalCloseButtonId);
    
    yield* page.modal.waitClosed(this.puppeteerTestOptimization, this.modalSelector);
  }
  
  *exit(interrupted) {
    this.closeConnection(this.puppeteerConnection);
  }
}


module.exports = StacksModalFileOrig;
