
const ActorApi = require('actor-api');
const Button = require('../_helper-apis/button');
const Modal = require('../_helper-apis/modal');
const Tree = require('../_helper-apis/tree');


class StacksModalWizardOrig extends ActorApi.ActorOriginating {
  constructor() {
    super();
    this.puppeteerConnection = null;
    this.modalSelector = '';
    this.locateElementId = '';
    this.modalCloseButtonId = '';
    this.puppeteerTestOptimization = false;
  }
  
  *data() {
    this.modalSelector = this.getTestDataString('modal-selector');
    this.locateElementId = this.getTestDataString('locate-element-id');
    this.modalCloseButtonId = this.getTestDataString('modal-close-button-id');
    this.puppeteerTestOptimization = this.getTestDataBoolean('puppeteer-test-optimization', this.puppeteerTestOptimization);
  }
  
  *initClient() {
    this.puppeteerConnection = this.createConnection('puppeteer');
    this.puppeteerConnection.addHelpers(Button, Modal, Route, Tree);
  }
  
  *run() {
    const page = this.puppeteerConnection.page;
    yield* page.tree.waitForSelectedFolder('Stacks-global');
    
    yield* page.tree.clickFolder('', 'Stacks-global');
    
    yield* page.button.waitNotDisabledClassAndClickId(this.locateElementId);
    
    yield* page.modal.waitOpen(this.puppeteerTestOptimization, this.modalSelector);
    VERIFY_MANDATORY('verify-modal-heading', yield* page.modal.waitForTitle());
    
    yield* page.button.waitNotDisabledAttributeAndClickId(this.modalCloseButtonId);
    
    yield* page.modal.waitClosed(this.puppeteerTestOptimization, this.modalSelector);
  }
  
  *exit(interrupted) {
    this.closeConnection(this.puppeteerConnection);
  }
}


module.exports = StacksModalWizardOrig;
