
'use strict';


class Input {
  constructor(connection, actionslowMo) {
    this.connection = connection;
    this.page = connection.page;
    this.actionslowMo = actionslowMo;
  }
  
  *waitSelectedTextById(id, text) {
    yield this.page.waitForFunction((id, text) => {
      const foundText = document.getElementById(id);
      const selectedText = foundText.value.substr(foundText.selectionStart, foundText.selectionEnd - foundText.selectionStart);
      return selectedText === text;
    }, {}, id, text);
  }
}

module.exports = Input;
