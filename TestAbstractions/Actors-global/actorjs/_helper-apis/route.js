
'use strict';

const Puppeteer = require('puppeteer');


class Route {
  constructor(connection, actionslowMo) {
    this.connection = connection;
    this.page = connection.page;
    this.actionslowMo = actionslowMo;
  }
  
  *waitForInitialUrl(connection) {
    const uri = connection.browserData.dstAddress.uri;
    this.connection.addErrorData(`route.waitForInitialUrl('${uri}')`);
    yield this.page.waitForFunction((uri) => {
      return uri === document.location.href;
    }, {}, uri);
  }
  
  *waitForUrl(url) {
    this.connection.addErrorData(`route.waitForUrl'(${url}')`);
    try {
      yield this.page.waitForFunction((url) => {
        //console.log(url === document.location.pathname, "'" + url + "'", "'" + document.location.pathname + "'");
        //console.log(url, document.location.pathname);
        return url === document.location.pathname;
      }, {
        timeout: 3000
      }, url);
      return true;
    }
    catch(e) {
      if(e instanceof Puppeteer.TimeoutError) {
        yield this.page.console.log('waitForUrl[' + this.id + '] - TIMEOUT', e.constructor.name, e);
        return false;
      }
      else {
        console.log('waitForUrl[' + this.id + '] - ERROR', e.constructor.name, e);
        throw e;
      }
    }
  }
}


module.exports = Route;
