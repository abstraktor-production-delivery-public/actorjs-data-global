
'use strict';

const StackApi = require('stack-api');


class Tab {
  constructor(connection, actionslowMo) {
    this.connection = connection;
    this.page = connection.page;
    this.actionslowMo = actionslowMo;
  }
  
  *waitAndClick(id, awaitUrl) {
    this.connection.addErrorData(`tab.waitAndClick('${id}${awaitUrl ? ', ' + awaitUrl : ''}')`);
    let isDone = false;
    do {
      isDone = yield* this._waitAndClick(id, awaitUrl);
    } while(!isDone);
  }
  
  *_waitAndClick(id, awaitUrl) {
    yield this.page.waitForFunction((id) => {
      const tab = document.getElementById(id);
      const found = !!tab;
      if(found) {
        tab.click();
      }
      return found;
    }, {}, id);
    if(awaitUrl) {
      yield* this.page.route.waitForUrl(awaitUrl);
    }
    this.connection.logGuiEvent(() => '... _Tab.waitAndClick', StackApi.LogDataGuiType.CLICK, {}, 'Puppeteer', '45bd4094-3cdd-4186-b2af-7b0417863d25');
    return true;
  }
    
  *waitForActive(id) {
    this.connection.addErrorData(`tab.waitForActive('${id}')`);
    yield this.page.waitForFunction((selector) => {
      const tab = document.getElementById(selector);
      const active = !!tab && tab.parentNode.classList.contains('active');
      return active;
    }, {}, id);
  }
}

module.exports = Tab;
