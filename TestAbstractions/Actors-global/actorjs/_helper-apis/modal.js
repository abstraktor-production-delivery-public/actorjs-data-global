
'use strict';


class Modal {
  constructor(connection, actionslowMo) {
    this.connection = connection;
    this.page = connection.page;
    this.actionslowMo = actionslowMo;
  }
  
  *waitOpen(modalSelector) {
    yield this.page.waitForFunction(() => {
      return document.querySelector('body').classList.contains('modal-open');
    });
  }
  
  *waitClosed(modalSelector) {
    yield this.page.waitForFunction(() => {
      return !document.querySelector('body').classList.contains('modal-open');
    });
  }
  
  *waitForTitle() {
    yield this.page.waitForSelector('xpath///div[@style = "display: block;"]/div/div/div/h4');
    const modalH4 = yield this.page.$('xpath///div[@style = "display: block;"]/div/div/div/h4');
		return yield this.page.evaluate(h4 => h4.innerText, modalH4);
  }
  
  *waitForErrorMessge(errorPId) {
    const labelElement = yield this.page.waitForSelector(`#${errorPId}`);
    return yield this.page.evaluate(label => label.textContent, labelElement);
  }
}

module.exports = Modal;
