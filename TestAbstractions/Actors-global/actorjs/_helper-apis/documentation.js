
'use strict';


class Documentation {
  constructor(connection, actionslowMo) {
    this.connection = connection;
    this.page = connection.page;
    this.actionslowMo = actionslowMo;
  }
  
  *waitDocumentationLoaded() {
    yield this.page.waitForFunction((selector, data) => {
      const heading = document.querySelector(selector);
      if(heading) {
        if(heading.previousSibling.classList.contains('doc_nav_heading_closed')) {
          return false;
        }
        else if(heading.previousSibling.classList.contains('doc_nav_heading_open')) {
          return true;
        }
      }
      return false;
    }, {}, `h3[data-name="Introduction"] `, {clicked:false});
  }
  
  *waitHeadingAndOpen(text) {
    yield this.page.waitForFunction((selector, data) => {
      const heading = document.querySelector(selector);
      if(heading) {
        if(heading.previousSibling.classList.contains('doc_nav_heading_closed')) {
          if(!data.clicked) {
            data.clicked = true;
            heading.click()
          }
          return false;
        }
        else if(heading.previousSibling.classList.contains('doc_nav_heading_open')) {
          return true;
        }
      }
      return false;
    }, {}, `h3[data-name="${text}"] `, {clicked:false});
  }
  
  *waitInnerHeadingAndOpen(text) {
    yield this.page.waitForFunction((selector, data) => {
      const heading = document.querySelector(selector);
      if(heading) {
        if(heading.previousSibling.classList.contains('doc_nav_inner_heading_closed')) {
          if(!data.clicked) {
            data.clicked = true;
            heading.click()
          }
          return false;
        }
        else if(heading.previousSibling.classList.contains('doc_nav_inner_heading_open')) {
          return true;
        }
      }
      return false;
    }, {}, `p[data-name="${text}"] `, {clicked:false});
  }
  
  *waitLinkAndOpen(text) {
    yield this.page.waitForFunction((selector, data) => {
      const heading = document.querySelector(selector);
      if(heading) {
        if(heading.previousSibling.classList.contains('doc_nav_inner')) {
          if(!data.clicked) {
            data.clicked = true;
            heading.click()
          }
          return false;
        }
        else if(heading.previousSibling.classList.contains('doc_nav_inner_chosen')) {
          return true;
        }
      }
      return false;
    }, {}, `a[data-name="${text}"] `, {clicked:false});
  }
}

module.exports = Documentation;
