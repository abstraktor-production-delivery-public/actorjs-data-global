
'use strict';


class Button {
  constructor(connection, actionslowMo) {
    this.connection = connection;
    this.page = connection.page;
    this.actionslowMo = actionslowMo;
  }
  
  *waitNotDisabledAttributeAndClickId(id) {
    yield this.page.waitForFunction((selector) => {
      const button = document.getElementById(selector);
      const found = !!button && !button.hasAttribute('disabled');
      if(found) {
        button.click();
      }
      return found;
    }, {}, id);
  }
  
  *waitNotDisabledClassAndClickId(id) {
    yield this.page.waitForFunction((selector) => {
      const button = document.getElementById(selector);
      const found = !!button && !button.classList.contains('disabled');
      if(found) {
        button.click();
      }
      return found;
    }, {}, id);
  }
  
  *waitDisabledClass(id) {
    yield this.page.waitForFunction((selector) => {
      const button = document.getElementById(selector);
      const disabled = !!button && button.classList.contains('disabled');
      return disabled;
    }, {}, id);
  }
}

module.exports = Button;
