
const ActorApi = require('actor-api');
const Route = require('../route');
const Tree = require('../tree');


class TreeOrig extends ActorApi.ActorOriginating {
  constructor() {
    super();
    this.puppeteerConnection = null;
  }
  
  *data() {
  }
  
  *initClient() {
    this.puppeteerConnection = this.createConnection('puppeteer');
    this.puppeteerConnection.addHelpers(Route, Tree);
    yield* this.puppeteerConnection.page.route.waitForInitialUrl(this.puppeteerConnection);
  }
  
  *run() {
    const page = this.puppeteerConnection.page;
    
    yield* page.tree.selectFolder('Actors-global/actorjs-test/test-folder');
    yield* page.tree.selectFile('Actors-global/actorjs-test/test-folder/actor-test-folder-rename/TestFile.js', '/actor-editor/Actors-global/actorjs-test/test-folder/actor-test-folder-rename/TestFile.js');
  }
  
  *exit(interrupted) {
    this.closeConnection(this.puppeteerConnection);
  }
}


module.exports = TreeOrig;
