
'use strict';


class Tree {
  constructor(connection, actionslowMo) {
    this.connection = connection;
    this.page = connection.page;
    this.actionslowMo = actionslowMo;
    this.treeId = '';
  }
  
  setTreeId(treeId) {
    this.treeId = treeId;
  }
  
  *expandFolder(folderPath) {
    this.connection.addErrorData(`tree.expandFolder('${folderPath}')`);
    const paths = folderPath.split('/');
    const topFolderNode = yield* this.getFolder(paths);
    return yield* this._clickFolderExpander(paths, topFolderNode, true);
  }
  
  *collapseFolder(folderPath) {
    const paths = folderPath.split('/');
    const topFolderNode = yield* this.getFolder(paths);
    return yield* this._clickFolderExpander(paths, topFolderNode, false);
  }
  
  *selectFolder(folderPath, type, awaitUrl) {
    this.connection.addErrorData(`tree.selectFile('${folderPath}')`);
    const paths = folderPath.split('/');
    const lastFolder = paths.pop();
    const folderNode = yield* this.getFolder(paths);
    const lastFolderNode = yield* this._getElement(lastFolder, folderNode);
    let selectXPath = '';
    if('title' === type) {
      selectXPath = 'xpath/./span[contains(@class, "tree_title")]';
    }
    else if('icon' === type) {
      selectXPath = 'xpath/./img[contains(@class, "tree_icon")]';
    }
    else {
      
    }
    const selectNode = yield lastFolderNode.$(selectXPath);
    if(selectNode) {
      const visible = yield selectNode.isIntersectingViewport();
      if(!visible) {
        yield this.page.scrollIntoView(selectNode);
      }
      yield selectNode.click();
      yield* this._waitForFolderSelected(lastFolderNode, true);
    }
    else {
      throw Error('Could not find any title or icon.');
    }
  }
  
  *selectFile(filePath, type, awaitUrl) {
    this.connection.addErrorData(`tree.selectFile('${filePath}')`);
    const paths = filePath.split('/');
    const file = paths.pop();
    const folderNode = yield* this.getFolder(paths);
    const fileNode = yield* this._getElement(file, folderNode);
    let selectXPath = '';
    if('title' === type) {
      selectXPath = 'xpath/./span[contains(@class, "tree_title")]';
    }
    else if('icon' === type) {
      selectXPath = 'xpath/./img[contains(@class, "tree_icon")]';
    }
    else {
      
    }
    const selectNode = yield fileNode.$(selectXPath);
    if(selectNode) {
      const visible = yield selectNode.isIntersectingViewport();
      if(!visible) {
        yield this.page.scrollIntoView(selectNode);
      }
      yield selectNode.click();
      yield* this._waitForFolderSelected(fileNode, true);
    }
    else {
      throw Error('Could not find any title or icon.');
    }
  }
  
  *fileExist(folderPath, fileName) {
    this.connection.addErrorData(`tree.fileExist('${folderPath}', '${fileName}')`);
    const parentFolderHandler = yield* this.expandFolder(folderPath);
    const fileHandle = yield parentFolderHandler.$(`xpath/./ul/li/span[text()="${fileName}"]`);
    return !!fileHandle;
  }
  
  /////
  
  *_waitForFolderSelected(folderNode, selected) {
    return yield this.page.waitForFunction((node, selected) => {
      return `${selected}` === node.getAttribute('aria-selected');
    }, {}, folderNode, selected);
  }
  
  *_waitForFolderExpanded(folderNode, expanded) {
    return yield this.page.waitForFunction((node, expanded) => {
      return `${expanded}` === node.getAttribute('aria-expanded');
    }, {}, folderNode, expanded);
  }
  
  *_isExpanded(node) {
    return yield this.page.evaluate((node) => {
      return 'true' === node.getAttribute("aria-expanded"); 
    }, node);
  }
  
  *_clickFolderExpander(paths, folderNode, expand) {
    let expandedFolderNode = null;
    do {
      expandedFolderNode = yield* this.__clickFolderExpander(paths, folderNode, expand);
    } while(!expandedFolderNode);
    return expandedFolderNode;
  }
  
  *__clickFolderExpander(paths, folderNode, expand) {
    const expanded = yield* this._isExpanded(folderNode);
    if(!expanded) {
      if(1 !== paths.length) {
        throw Error('All previous expanders are not expanded.');
      }
      const expandXpath = 'xpath/./span[contains(@class, "tree_expander")]';
      const expandNode = yield folderNode.$(expandXpath);
      if(expandNode) {
        const visible = yield expandNode.isIntersectingViewport();
        if(!visible) {
          yield this.page.scrollIntoView(expandNode);
        }
        yield expandNode.click();
        yield* this._waitForFolderExpanded(folderNode, expand);
      }
      else {
        throw Error('Could not find any expanders.');
      }
    }
    paths.shift();
    if(0 !== paths.length) {
      const folderName = paths[0];
      const nextFolderNode = yield* this._getElement(folderName, folderNode);
      return yield* this._clickFolderExpander(paths, nextFolderNode, expand);
    }
    return folderNode;
  }
  
  // --- INTERNAL - GET
  
  *getFolder(paths) {
    if(0 === paths.length) {
      throw new Error('tree.getFolder: There are no paths.');
    }
    const folderName = paths[0];
    const topFolderNode = yield* this._getTopFolder(folderName);
    if(1 === paths.length) {
      return topFolderNode;
    }
    else {
      let nextFolderNode = topFolderNode;
      do {
        paths.shift();
        const elementName = paths[0];
        nextFolderNode = yield* this._getElement(elementName, nextFolderNode);
      } while(1 !== paths.length);
      return nextFolderNode;
    }
  }
  
  *_getTopFolder(name) {
    const treeRootSelector = `#${this.treeId}`;
    let treeRootNode = yield this.page.$(treeRootSelector);
    if(!treeRootNode) {
      yield this.page.waitForSelector(treeRootSelector);
      treeRootNode = yield this.page.$(treeRootSelector);
      if(!treeRootNode) {
        throw new Error(`Tree selector '#${this.treeId}' not found.`);
      }
    }
    const topFolderXpath = `xpath/./ul/li/span[text()="${name}"]/..`;
    let topFolderNode = yield treeRootNode.$(topFolderXpath);
    if(topFolderNode) {
      yield treeRootNode.waitForSelector(topFolderXpath);
      topFolderNode = yield treeRootNode.$(topFolderXpath);
    }
    if(topFolderNode) {
      return topFolderNode;
    }
    else {
      throw new Error(`Tree top folder not found.`);
    }
  }
  
  *_getElement(name, parentNode) {
    const folderXpath = `xpath/./ul/li/span[text()="${name}"]/..`;
    let folderNode = yield parentNode.$(folderXpath);
    if(folderNode) {
      yield parentNode.waitForSelector(folderXpath);
      folderNode = yield parentNode.$(folderXpath);
    }
    if(folderNode) {
      return folderNode;
    }
    else {
      throw new Error(`Tree folder not found.`);
    }
  }
}


module.exports = Tree;
