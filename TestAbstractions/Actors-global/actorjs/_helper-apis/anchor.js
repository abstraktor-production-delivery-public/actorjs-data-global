
'use strict';


class Anchor {
  constructor(connection, actionslowMo) {
    this.connection = connection;
    this.page = connection.page;
    this.actionslowMo = actionslowMo;
  }
  
  *waitLinkAndClick(xPath, awaitUrl) {
    this.connection.addErrorData(`anchor.waitLinkAndClick('xpath === ${xPath}')`);
    yield this.page.xPathClick(xPath);
    return yield* this.page.route.waitForUrl(awaitUrl);
  }
}


module.exports = Anchor;
