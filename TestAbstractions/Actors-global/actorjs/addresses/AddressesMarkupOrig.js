  
const ActorApi = require('actor-api');
const Button = require('../_helper-apis/button');
const Route = require('../_helper-apis/route');
const Tab = require('../_helper-apis/tab');


class AddressesMarkupOrig extends ActorApi.ActorOriginating {
  constructor() {
    super();
    this.puppeteerConnection = null;
    this.locateElementId = '';
    this.tabId = '';
    this.closeButtonId = '';
  }
  
  *data() {
    this.locateElementId = this.getTestDataString('locate-element-id');
    this.tabId = this.getTestDataString('tab-id');
    this.closeButtonId = this.getTestDataString('close-button-id');
  }
  
  *initClient() {
    this.puppeteerConnection = this.createConnection('puppeteer');
    this.puppeteerConnection.addHelpers(Button, Route, Tab);
    yield* this.puppeteerConnection.page.route.waitForInitialUrl(this.puppeteerConnection);
  }
  
  *run() { 
    const page = this.puppeteerConnection.page;
    
    yield* page.tab.waitForActive(this.tabId);
    yield* page.button.waitNotDisabledClassAndClickId(this.locateElementId);

    yield* page.button.waitNotDisabledClassAndClickId(this.closeButtonId);
    yield* page.button.waitDisabledClass(this.closeButtonId);
  }
    
  *exit(interrupted){
    this.closeConnection(this.puppeteerConnection);
  }
}

module.exports = AddressesMarkupOrig;





