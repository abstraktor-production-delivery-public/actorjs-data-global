
const ActorApi = require('actor-api');
const Anchor = require('../_helper-apis/anchor');
const Route = require('../_helper-apis/route');
const Tab = require('../_helper-apis/tab');


class SystemsUnderTestLocateSutOrig extends ActorApi.ActorOriginating {
  constructor() {
    super();
    this.puppeteerConnection = null;
    this.anchorText = '';
    this.anchorHref = '';
    this.tabId = '';
  }
  
  *data() {
    this.anchorText = this.getTestDataString('anchor-text');
    this.anchorHref = this.getTestDataString('anchor-href');
    this.tabId = this.getTestDataString('tab-id');
  }
  
  *initClient() {
    this.puppeteerConnection = this.createConnection('puppeteer');
    this.puppeteerConnection.addHelpers(Anchor, Tab, Route);
    yield* this.puppeteerConnection.page.route.waitForInitialUrl(this.puppeteerConnection);
  }
  
  *run() {
    const page = this.puppeteerConnection.page;
    
    const anchorXpath = `//table[@id='system_under_test_table']/tbody/tr/td/a[text()='${this.anchorText}']`;
    yield* page.anchor.waitLinkAndClick(anchorXpath, `/systems-under-test/${this.anchorHref}`);
    yield* page.tab.waitForActive(this.tabId);
  }
  
  *exit(interrupted) {
    this.closeConnection(this.puppeteerConnection);
  }
}

module.exports = SystemsUnderTestLocateSutOrig;
