
const ActorApi = require('actor-api');
const Route = require('../_helper-apis/route');
const Tab = require('../_helper-apis/tab');


class SystemsUnderTestLocateOrig extends ActorApi.ActorOriginating {
  constructor() {
    super();
    this.puppeteerConnection = null;
    this.tabId = '';
    this.tabHref = '';
  }
  
  *data() {
    this.tabId = this.getTestDataString('tab-id');
    this.tabHref = this.getTestDataString('tab-href');
  }
  
  *initClient() {
    this.puppeteerConnection = this.createConnection('puppeteer');
    this.puppeteerConnection.addHelpers(Route, Tab);
    yield* this.puppeteerConnection.page.route.waitForInitialUrl(this.puppeteerConnection);
  }
  
  *run() {
    const page = this.puppeteerConnection.page;    

    yield* page.tab.waitAndClick(this.tabId, `/systems-under-test/ActorJsTest/${this.tabHref}`);
    yield* page.tab.waitForActive(this.tabId);
  }
  
  *exit(interrupted) {
    this.closeConnection(this.puppeteerConnection);
  }
}

module.exports = SystemsUnderTestLocateOrig;
