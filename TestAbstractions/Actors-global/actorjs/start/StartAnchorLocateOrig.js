
const ActorApi = require('actor-api');
const Route = require('../_helper-apis/route');


class StartAnchorLocateOrig extends ActorApi.ActorOriginating {
  constructor(){
    super();
    this.puppeteerConnection = null;
    this.locateAnchor = [];
    this.verificationCode = [];
    this.verificationId = [];
    this.verifyId = false;
    this.verifySpan = false;
    this.verifyCode = false;
  }
  
  *data(){
    this.locateAnchor = this.getTestDataString('locate-anchor');
    this.verificationCode = this.getTestDataString('verify-code');
    this.verificationId = this.getTestDataString('verify-id');
    this.verifyId = this.getTestDataBoolean('verifying-id', false);
    this.verifySpan = this.getTestDataBoolean('verifying-span', false);
    this.verifyCode = this.getTestDataBoolean('verifying-code', false);
  }
  
  *initClient(){
    this.puppeteerConnection = this.createConnection('puppeteer');
    this.puppeteerConnection.addHelpers(Route);
    yield* this.puppeteerConnection.page.route.waitForInitialUrl(this.puppeteerConnection);
  }
  
  *run() {
    const page = this.puppeteerConnection.page;
    
    yield page.waitForSelector("xpath///a[text()='" + this.locateAnchor + "']");
    const anchorElementHandler = yield page.$("xpath///a[text()='" + this.locateAnchor + "']");
    yield anchorElementHandler.click();
    
    if(this.verifyId){     
      yield page.waitForFunction((selector) => {
        const tab = document.getElementById(selector);
        return tab ? tab.parentNode.classList.contains('active') : false;
      }, {}, this.verificationId);        
      
      const tab =  yield page.$eval(`#${this.verificationId}`, (el) => {
        return el.parentNode.getAttribute('class');
      });
      const classValues = tab.split(' ');
      const index = classValues.indexOf('active');
      const value = -1 !== index ? 'active' : '';
      VERIFY_VALUE('active', value);
    }
    
    if(this.verifySpan){
      yield page.waitForSelector("xpath///span[text()='"+this.verificationCode+"']");
      const spanElementHandler = yield page.$("xpath///span[text()='" + this.verificationCode + "']");
		  const spanTitle = yield page.evaluate(span => span.innerText, spanElementHandler);
      VERIFY_MANDATORY('verification-code', spanTitle);
    }
    
    if(this.verifyCode){
      yield page.waitForSelector("xpath///a[text()='"+this.verificationCode+"']");
      const anchorElementHandler = yield page.$("xpath///a[text()='" + this.verificationCode + "']");
		  const aTitle = yield page.evaluate(a => a.innerText, anchorElementHandler);
      VERIFY_MANDATORY('verification-code', aTitle);
    }
  }
  
  *exit(interrupted){
    this.closeConnection(this.puppeteerConnection);
  }
}

module.exports = StartAnchorLocateOrig;
