
const ActorApi = require('actor-api');
const Route = require('../_helper-apis/route');


class StartLocateOrig extends ActorApi.ActorOriginating{
  constructor(){
    super();
    this.puppeteerConnection = null;
    this.locateElement = [];
    this.verificationId = [];
    this.verificationh3 = [];
    this.verifyId = false;
    this.verifyh3 = false;
  }

  *data(){
    this.locateElement = this.getTestDataString('locate-element');
    this.verificationId = this.getTestDataString('verify-id');
    this.verificationh3 = this.getTestDataString('verify-h3');
    this.verifyId = this.getTestDataBoolean('verifying-id', false);
    this.verifyh3 = this.getTestDataBoolean('verifying-h3', false);
  }
  
  *initClient(){
    this.puppeteerConnection = this.createConnection('puppeteer');
    this.puppeteerConnection.addHelpers(Route);
    yield* this.puppeteerConnection.page.route.waitForInitialUrl(this.puppeteerConnection);
  }
  
  *run(){
    const page = this.puppeteerConnection.page;
    yield page.waitForFunction((selector) => {return null !== document.getElementById(selector);}, {}, this.locateElement);    
    yield page.click(`#${this.locateElement}`);
    
    if(this.verifyId){
      yield page.waitForFunction((selector) => {
        const tab = document.getElementById(selector);
        return tab ? tab.parentNode.classList.contains('active') :false;
      }, {}, this.verificationId); 
      
      const tab =  yield page.$eval(`#${this.verificationId}`,(el) => {return el.parentNode.getAttribute('class');});
      const classValues = tab.split(' ');
      const index = classValues.indexOf('active');
      const value = -1 !== index ? 'active' : '';
      VERIFY_VALUE('active', value);
    }
    if(this.verifyh3){
      yield page.waitForSelector("xpath///h3[text()='"+this.verificationh3+"']");
      const h3ElementHandler = yield page.$("xpath///h3[text()='"+this.verificationh3+"']");
		  const h3Title = yield page.evaluate(h3 => h3.innerText, h3ElementHandler);
      VERIFY_MANDATORY('verification-h3', h3Title);      
    }
  }
  
  *exit(interrupted){
    this.closeConnection(this.puppeteerConnection);
  }
}

module.exports = StartLocateOrig;
