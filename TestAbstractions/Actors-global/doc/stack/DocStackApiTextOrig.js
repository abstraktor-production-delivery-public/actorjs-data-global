
const ActorApi = require('actor-api');
const DemoStackApi = require('demo-stack-api');


class DocStackApiTextOrig extends ActorApi.ActorOriginating {
  constructor() {
    super();
    this.demoConnection = null;
  }
  
  *data() {
  }
  
  *initClient() {
    this.demoConnection = this.createConnection('demo');
  }
  
  *run() {
    this.demoConnection.sendText(new DemoStackApi.DemoMsg('INVITE', 'Hello I am here!'));
    
    const msg = this.demoConnection.receiveText();
    VERIFY_VALUE('ACK', msg.msgId);
  }
  
  *exit(interrupted) {
    this.closeConnection(this.demoConnection);
  }
}

module.exports = DocStackApiTextOrig;
