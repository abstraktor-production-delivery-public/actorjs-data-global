
const ActorApi = require('actor-api');


class VerifyValueMSuccessLocal extends ActorApi.ActorLocal {
  constructor() {
    super();
  }
  
  *run() {
    VERIFY_VALUE(['1.1', '2.2'], [1.1, 2.2], '', '==');
    VERIFY_VALUE([1.0, 2.1], [1.1, 2.2], '', '<');
  }
}

module.exports = VerifyValueMSuccessLocal;
