
const ActorApi = require('actor-api');


class VerifyValueKSuccessLocal extends ActorApi.ActorLocal {
  constructor() {
    super();
  }
  
  *run() {
    VERIFY_VALUE([31.1, 32.2], [31.1, 32.2]);
  }
}

module.exports = VerifyValueKSuccessLocal;
