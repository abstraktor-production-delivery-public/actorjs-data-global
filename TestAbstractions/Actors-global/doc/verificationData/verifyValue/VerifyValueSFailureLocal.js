
const ActorApi = require('actor-api');


class VerifyValueSFailureLocal extends ActorApi.ActorLocal {
  constructor() {
    super();
  }
  
  *run() {
    VERIFY_VALUE([['Hi', 'Hi'], ['Bye', 'Later']], [['Hello', 'Hi'], ['Bye', 'Later']]);
  }
}

module.exports = VerifyValueSFailureLocal;
