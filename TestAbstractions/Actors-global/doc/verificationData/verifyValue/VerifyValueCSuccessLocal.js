
const ActorApi = require('actor-api');


class VerifyValueCSuccessLocal extends ActorApi.ActorLocal {
  constructor() {
    super();
  }
  
  *run() {
    VERIFY_VALUE(true, true);
    VERIFY_VALUE(2 > 1, true);
  }
}

module.exports = VerifyValueCSuccessLocal;
