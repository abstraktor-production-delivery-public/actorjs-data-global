
const ActorApi = require('actor-api');


class VerifyValueMFailureLocal extends ActorApi.ActorLocal {
  constructor() {
    super();
  }
  
  *run() {
    VERIFY_VALUE([1.1, 2.1], [1.1, 2.2], '', '<');
  }
}

module.exports = VerifyValueMFailureLocal;
