
const ActorApi = require('actor-api');


class VerifyValueLFailureLocal extends ActorApi.ActorLocal {
  constructor() {
    super();
  }
  
  *run() {
    VERIFY_VALUE([2 > 1, 3 > 2], [true, false]);
  }
}

module.exports = VerifyValueLFailureLocal;
