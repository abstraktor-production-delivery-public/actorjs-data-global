
const ActorApi = require('actor-api');


class VerifyValueKFailureLocal extends ActorApi.ActorLocal {
  constructor() {
    super();
  }
  
  *run() {
    VERIFY_VALUE([31.1, 32.0], [31.1, 32.2]);
  }
}

module.exports = VerifyValueKFailureLocal;
