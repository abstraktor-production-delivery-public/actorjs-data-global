
const ActorApi = require('actor-api');


class VerifyOptionalCFailureLocal extends ActorApi.ActorLocal {
  constructor() {
    super();
  }
  
  *run() { 
    VERIFY_OPTIONAL('demoKey3', 44.4);
  }
}

module.exports = VerifyOptionalCFailureLocal;
