
const ActorApi = require('actor-api');


class VerifyOptionalJFailureLocal extends ActorApi.ActorLocal {
  constructor() {
    super();
  }
  
  *run() {
    VERIFY_OPTIONAL('demoKey1', ['Hi', 'Hello']);
  }
}

module.exports = VerifyOptionalJFailureLocal;
