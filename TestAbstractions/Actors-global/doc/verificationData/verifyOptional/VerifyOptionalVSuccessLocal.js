
const ActorApi = require('actor-api');


class VerifyOptionalVSuccessLocal extends ActorApi.ActorLocal {
  constructor() {
    super();
  }
    
  *run() {
    VERIFY_OPTIONAL('demoKey1', ['Hello', 'Hi'], 0);
    VERIFY_OPTIONAL('demoKey2', 'Bye', 1, 0);
    VERIFY_OPTIONAL('demoKey3', [1, 2], 0);
    VERIFY_OPTIONAL('demoKey4', 6, 1, 1);
    VERIFY_OPTIONAL('demoKey7', [true, true], 0);
    VERIFY_OPTIONAL('demoKey8', true, 0, 1);
  }
}

module.exports = VerifyOptionalVSuccessLocal;
