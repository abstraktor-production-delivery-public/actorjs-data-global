
const ActorApi = require('actor-api');


class VerifyOptionalLSuccessLocal extends ActorApi.ActorLocal {
  constructor() {
    super();
  }
  
  *run() {
    VERIFY_OPTIONAL('demoKey7', [true, false]);
    VERIFY_OPTIONAL('demoKey8', [true, false]);
    VERIFY_OPTIONAL('demoKeyX', [true, false]);
  }
}

module.exports = VerifyOptionalLSuccessLocal;
