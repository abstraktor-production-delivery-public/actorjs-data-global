
const ActorApi = require('actor-api');


class VerifyOptionalJSuccessLocal extends ActorApi.ActorLocal {
  constructor() {
    super();
  }
  
  *run() {
    VERIFY_OPTIONAL('demoKey1', ['Hello', 'Hi']);
    VERIFY_OPTIONAL('demoKey2', ['Hello', 'Hi']);
    VERIFY_OPTIONAL('demoKeyX', ['Hello', 'Hi']);
  }
}

module.exports = VerifyOptionalJSuccessLocal;
