
const ActorApi = require('actor-api');


class VerifyOptionalLFailureLocal extends ActorApi.ActorLocal {
  constructor() {
    super();
  }
  
  *run() {
    VERIFY_OPTIONAL('demoKey8', [true, true]);
  }
}

module.exports = VerifyOptionalLFailureLocal;
