
const ActorApi = require('actor-api');


class VerifyOptionalDFailureLocal extends ActorApi.ActorLocal {
   constructor() {
    super();
  }
  
  *run() { 
    VERIFY_OPTIONAL('demoKey8', false);
  }
}

module.exports = VerifyOptionalDFailureLocal;
