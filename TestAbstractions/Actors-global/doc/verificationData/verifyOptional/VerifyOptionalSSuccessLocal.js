
const ActorApi = require('actor-api');


class VerifyOptionalSSuccessLocal extends ActorApi.ActorLocal {
  constructor() {
    super();
  }
  
  *run() {
    VERIFY_OPTIONAL('demoKey1', [['Hello', 'Hi'], ['Bye', 'Later']]);
    VERIFY_OPTIONAL('demoKey2', [['Hello', 'Hi'], ['Bye', 'Later']]);
    VERIFY_OPTIONAL('demoKeyX', [['Hello', 'Hi'], ['Bye', 'Later']]);
  }
}

module.exports = VerifyOptionalSSuccessLocal;
