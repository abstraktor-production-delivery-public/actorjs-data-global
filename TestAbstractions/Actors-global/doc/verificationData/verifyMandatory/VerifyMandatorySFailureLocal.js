
const ActorApi = require('actor-api');


class VerifyMandatorySFailureLocal extends ActorApi.ActorLocal {
  constructor() {
    super();
  }
  
  *run() {
    VERIFY_MANDATORY('demoKeyX', [['Hello', 'Hi'], ['Bye', 'Later']]);
  }
}

module.exports = VerifyMandatorySFailureLocal;
