
const ActorApi = require('actor-api');


class VerifyMandatoryLFailureLocal extends ActorApi.ActorLocal {
  constructor() {
    super();
  }
  
  *run() {
    VERIFY_MANDATORY('demoKey8', [true, true]);
  }
}

module.exports = VerifyMandatoryLFailureLocal;
