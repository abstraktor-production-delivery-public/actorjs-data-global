
const ActorApi = require('actor-api');


class VerifyMandatoryKSuccessLocal extends ActorApi.ActorLocal {
  constructor() {
    super();
  }
  
  *run() {
    VERIFY_MANDATORY('demoKey3', [1, 2]);
    VERIFY_MANDATORY('demoKey4', [1, 2]);
    VERIFY_MANDATORY('demoKey5', [1, 2]);
  }
}

module.exports = VerifyMandatoryKSuccessLocal;
