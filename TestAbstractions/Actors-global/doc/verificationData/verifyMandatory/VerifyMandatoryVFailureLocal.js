
const ActorApi = require('actor-api');


class VerifyMandatoryVFailureLocal extends ActorApi.ActorLocal {
  constructor() {
    super();
  }
  
  *run() {
    VERIFY_MANDATORY('demoKey2', 'Later', 2, 2);
  }
}

module.exports = VerifyMandatoryVFailureLocal;
