
const ActorApi = require('actor-api');


class VerifyMandatoryDSuccessLocal extends ActorApi.ActorLocal {
  constructor() {
    super();
  }
  
  *run() { 
    VERIFY_MANDATORY('demoKey7', false);
  }
}

module.exports = VerifyMandatoryDSuccessLocal;
