
const ActorApi = require('actor-api');


class VerifyMandatoryMSuccessLocal extends ActorApi.ActorLocal {
  constructor() {
    super();
  }
  
  *run() {
    VERIFY_MANDATORY('demoKey1', 'Hi', 1);
    VERIFY_MANDATORY('demoKey2', 'Hello', 0);
    VERIFY_MANDATORY('demoKey3', 2, 1);
    VERIFY_MANDATORY('demoKey4', 1, 0);
    VERIFY_MANDATORY('demoKey8', false, 1);
  }
}

module.exports = VerifyMandatoryMSuccessLocal;
