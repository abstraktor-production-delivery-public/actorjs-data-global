
const ActorApi = require('actor-api');


class VerifyMandatorySSuccessLocal extends ActorApi.ActorLocal {
  constructor() {
    super();
  }
  
  *run() {
    VERIFY_MANDATORY('demoKey1', [['Hello', 'Hi'], ['Bye', 'Later']]);
    VERIFY_MANDATORY('demoKey2', [['Hello', 'Hi'], ['Bye', 'Later']]);
  }
}

module.exports = VerifyMandatorySSuccessLocal;
