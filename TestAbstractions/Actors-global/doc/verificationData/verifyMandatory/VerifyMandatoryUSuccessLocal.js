
const ActorApi = require('actor-api');


class VerifyMandatoryUSuccessLocal extends ActorApi.ActorLocal {
  constructor() {
    super();
  }
  
  *run() {
    VERIFY_MANDATORY('demoKey7', [[true, true], [false, false]]);
    VERIFY_MANDATORY('demoKey8', [[true, true], [false, false]]);
  }
}

module.exports = VerifyMandatoryUSuccessLocal;
