
const ActorApi = require('actor-api');


class VerifyMandatoryKFailureLocal extends ActorApi.ActorLocal {
  constructor() {
    super();
  }
  
  *run() {
    VERIFY_MANDATORY('demoKey6', [1, 2]);
  }
}

module.exports = VerifyMandatoryKFailureLocal;
