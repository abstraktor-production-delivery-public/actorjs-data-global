
const ActorApi = require('actor-api');


class DocLogSizeOrig extends ActorApi.ActorOriginating {
  constructor() {
    super();
    this.socketConnection = null;
  }
  
  *initClient() {
    this.socketConnection = this.createConnection('socket');
  }
  
  *run() {
    this.socketConnection.send('HELLO');
  }

  *exit(interrupted) {
    this.closeConnection(this.socketConnection);
  }
}

module.exports = DocLogSizeOrig;
