
const ActorApi = require('actor-api');


class DocSharedDataGetLocal extends ActorApi.ActorLocal {
  constructor() {
    super();
    this.key = 'DemoKey';
    this.value = 'DemoValue';
    this.defaultValue = '';
    this.verifyValue = '';
  }
  
  *data() {
    this.key = this.getTestDataString('shared-data-get-key', this.key);
    this.value = this.getTestDataString('shared-data-get-value', this.value);
    this.verifyValue = this.getTestDataString('shared-data-get-verify-value', this.value);
    this.defaultValue = this.getTestDataString('shared-data-get-default-value', '___undefined_watch_dog');
    if('___undefined_watch_dog' === this.defaultValue) {
      this.defaultValue = undefined;
    }
  }
  
  *run() {
    VERIFY_VALUE(this.verifyValue, this.getSharedData(this.key, this.defaultValue));
  }
}

module.exports = DocSharedDataGetLocal;
