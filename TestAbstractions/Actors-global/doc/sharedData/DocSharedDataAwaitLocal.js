
const ActorApi = require('actor-api');


class DocSharedDataAwaitLocal extends ActorApi.ActorLocal {
  constructor() {
    super();
    this.key = 'DemoKey';
    this.value = 'DemoValue';
    this.defaultValue = '';
    this.expectedValue = '';
  }
  
  *data() {
    this.key = this.getTestDataString('shared-data-get-key', this.key);
    this.value = this.getTestDataString('shared-data-get-value', this.value);
    this.defaultValue = this.getTestDataString('shared-data-get-default-value', '___undefined_watch_dog');
    if('___undefined_watch_dog' === this.defaultValue) {
      this.expectedValue = undefined;
    }
  }
  
  *run() {
    VERIFY_VALUE(this.value, this.waitForSharedData(this.key));
  }
}

module.exports = DocSharedDataAwaitLocal;
