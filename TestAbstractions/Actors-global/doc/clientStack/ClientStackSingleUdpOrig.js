
const ActorApi = require('actor-api');


class ClientStackSingleUdpOrig extends ActorApi.ActorOriginating {
  constructor() {
    super();
    this.socketConnection = null;
  }
    
  *initClient() {
    this.socketConnection = this.createConnection('socket', {
      networkType: 1
    });
  }
  
  *run() {
    this.socketConnection.sendLine('HELLO SERVER');
    const response = this.socketConnection.receiveLine();
  }
  
  *exit(interrupted) {
    this.closeConnection(this.socketConnection);
  }
}

module.exports = ClientStackSingleUdpOrig;
