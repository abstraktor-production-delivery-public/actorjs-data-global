
const ActorApi = require('actor-api');


class ClientStackDoubleTerm extends ActorApi.ActorTerminating {
  constructor() {
    super();
    this.socketConnection1 = null;
    this.socketConnection2 = null;
  }
      
  *initServer() {
    this.socketConnection1 = this.createServer('socket');
    this.socketConnection2 = this.createServer('socket', {
      srvIndex: 1
    });
  }
  
  *run() {
    this.socketConnection1.accept();
    this.socketConnection2.accept();
  }
  
  *exit(interrupted) {
    this.closeConnection(this.socketConnection1);
    this.closeConnection(this.socketConnection2);
  }
}

module.exports = ClientStackDoubleTerm;
