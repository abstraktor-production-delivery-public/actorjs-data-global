
const ActorApi = require('actor-api');


class ClientStackSingleOrig extends ActorApi.ActorOriginating {
  constructor() {
    super();
    this.httpConnection = null;
  }
    
  *initClient() {
    this.httpConnection = this.createConnection('http');
  }
  
  *run() {
  }
  
  *exit(interrupted) {
    this.closeConnection(this.httpConnection);
  }
}

module.exports = ClientStackSingleOrig;
