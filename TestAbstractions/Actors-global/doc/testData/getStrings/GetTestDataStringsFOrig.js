
const ActorApi = require('actor-api');


class GetTestDataStringsFOrig extends ActorApi.ActorOriginating {
  constructor() {
    super();
    this.f = [];
  }
  
  *data() {
    this.f = this.getTestDataStrings('demoKeyObject', 'NIGHT', {
      DAY: ['Hello', 'day'],
      NIGHT: ['Hello', 'night']
    });
    this.logDebug(this.f);
  }
  
  *run() {
    VERIFY_VALUE(['Hello', 'day'], this.f);
  }
}

module.exports = GetTestDataStringsFOrig;
