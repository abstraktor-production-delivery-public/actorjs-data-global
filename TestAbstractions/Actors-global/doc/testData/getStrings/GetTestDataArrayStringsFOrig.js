
const ActorApi = require('actor-api');


class GetTestDataArrayStringsFOrig extends ActorApi.ActorOriginating {
  constructor() {
    super();
    this.f = [[]];
  }
  
  *data() {
    this.f = this.getTestDataArrayStrings('demoKeyObject', 'SIR', {
      LADY: [['Good', 'morning'], ['Dear', 'lady']],
      SIR: [['Good', 'evening'], ['Good', 'sir']]
    });
    this.logDebug(this.f);
  }
  
  *run() {
    VERIFY_VALUE([['Good', 'morning'], ['Dear', 'lady']], this.f);
  }
}

module.exports = GetTestDataArrayStringsFOrig;
