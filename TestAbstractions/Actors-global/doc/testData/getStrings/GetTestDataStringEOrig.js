
const ActorApi = require('actor-api');


class GetTestDataStringEOrig extends ActorApi.ActorOriginating {
  constructor() {
    super();
    this.e = '';
  }
  
  *data() {
    this.e = this.getTestDataString('demoKeyObject', 'UNKNOWN', {
      ASAP: 'As soon as Possible',
      FYI: 'For your information',
      AP: 'Action point'
    });
    this.logDebug(this.e);
  }
  
  *run() {
    VERIFY_VALUE('As soon as Possible', this.e);
  }
}

module.exports = GetTestDataStringEOrig;
