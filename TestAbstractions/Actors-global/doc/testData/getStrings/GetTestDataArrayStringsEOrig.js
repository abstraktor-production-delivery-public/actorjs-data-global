
const ActorApi = require('actor-api');


class GetTestDataArrayStringsEOrig extends ActorApi.ActorOriginating {
  constructor() {
    super();
    this.e = [[]];
    this.d = [[]];
  }
  
  *data() {
    this.d = this.getTestDataArrayStrings('demoKeyX', [['Fare', 'well'], ['Take', 'care']]);
    this.e = this.getTestDataArrayStrings('demoKeyX', () => {
      const x = this.d.slice();
      x.push(['My', 'friend']);
      return x;
    });
    this.logDebug(this.d);
    this.logDebug(this.e);
  }
  
  *run() {
    VERIFY_VALUE([['Fare', 'well'], ['Take', 'care']], this.d);
    VERIFY_VALUE([['Fare', 'well'], ['Take', 'care'], ['My', 'friend']], this.e);
  }
}

module.exports = GetTestDataArrayStringsEOrig;
