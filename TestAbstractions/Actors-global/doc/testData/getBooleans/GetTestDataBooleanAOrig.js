
const ActorApi = require('actor-api');


class GetTestDataBooleanAOrig extends ActorApi.ActorOriginating {
  constructor() {
    super();
    this.a = true;
  }
  
  *data() {
    this.a = this.getTestDataBoolean('demoKey1');
    this.logDebug(this.a);
  }
  
  *run() {
    VERIFY_VALUE(true, this.a);
  }
}

module.exports = GetTestDataBooleanAOrig;
