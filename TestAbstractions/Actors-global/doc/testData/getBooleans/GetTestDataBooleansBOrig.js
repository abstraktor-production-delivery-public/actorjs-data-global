
const ActorApi = require('actor-api');


class GetTestDataBooleansBOrig extends ActorApi.ActorOriginating {
  constructor() {
    super();
    this.b = [];
  }
  
  *data() {
    this.b = this.getTestDataBooleans('demoKey2');
    this.logDebug(this.b);
  }
  
  *run() {
    VERIFY_VALUE([false, true], this.b);
  }
}

module.exports = GetTestDataBooleansBOrig;
