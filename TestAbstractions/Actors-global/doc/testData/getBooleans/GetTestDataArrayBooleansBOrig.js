
const ActorApi = require('actor-api');


class GetTestDataArrayBooleansBOrig extends ActorApi.ActorOriginating {
  constructor() {
    super();
    this.b = [[]];
  }
  
  *data() {
    this.b = this.getTestDataArrayBooleans('demoKey2');
    this.logDebug(this.b);
  }
  
  *run() {
    VERIFY_VALUE([[true, false], [true, false]], this.b);
  }
}

module.exports = GetTestDataArrayBooleansBOrig;
