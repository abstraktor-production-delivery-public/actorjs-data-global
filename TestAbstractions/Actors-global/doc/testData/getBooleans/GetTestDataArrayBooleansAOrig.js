
const ActorApi = require('actor-api');


class GetTestDataArrayBooleansAOrig extends ActorApi.ActorOriginating {
  constructor() {
    super();
    this.a = [[]];
  }
  
  *data() {
    this.a = this.getTestDataArrayBooleans('demoKey1');
    this.logDebug(this.a);
  }
  
  *run() {
    VERIFY_VALUE([[true, true], [false, false]], this.a);
  }
}

module.exports = GetTestDataArrayBooleansAOrig;
