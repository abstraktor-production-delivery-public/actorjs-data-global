
const ActorApi = require('actor-api');


class GetTestDataBooleanGOrig extends ActorApi.ActorOriginating {
  constructor() {
    super();
    this.g = true;
  }
  
  *data() {
    this.g = this.getTestDataBoolean('demoKeyObject', 'UNKNOWN', {
      TRUTH: true,
      FALSITY: false
    });
    this.logDebug(this.g);
  }
  
  *run() {
    VERIFY_VALUE(true, this.g);
  }
}

module.exports = GetTestDataBooleanGOrig;
