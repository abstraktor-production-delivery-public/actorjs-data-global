
const ActorApi = require('actor-api');


class GetTestDataBooleanFOrig extends ActorApi.ActorOriginating {
  constructor() {
    super();
    this.a = true;
    this.f = true;
  }
  
  *data() {
    this.a = this.getTestDataBoolean('demoKey1');
    this.f = this.getTestDataBoolean('demoKeyX', () => {
      return !this.a;
    });
    this.logDebug(this.a);
    this.logDebug(this.f);
  }
  
  *run() {
    VERIFY_VALUE(true, this.a);
    VERIFY_VALUE(false, this.f);
  }
}

module.exports = GetTestDataBooleanFOrig;
