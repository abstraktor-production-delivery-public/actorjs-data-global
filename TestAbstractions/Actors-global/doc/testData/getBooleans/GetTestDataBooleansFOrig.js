
const ActorApi = require('actor-api');


class GetTestDataBooleansFOrig extends ActorApi.ActorOriginating {
  constructor() {
    super();
    this.a = [];
    this.f = [];
  }
  
  *data() {
    this.a = this.getTestDataBooleans('demoKey1');
    this.f = this.getTestDataBooleans('demoKeyX', () => {
      const x = this.a.slice();
      x.push(true);
      return x;
    });
    this.logDebug(this.a);
    this.logDebug(this.f);
  }
  
  *run() {
    VERIFY_VALUE([true, false], this.a);
    VERIFY_VALUE([true, false, true], this.f);
  }
}

module.exports = GetTestDataBooleansFOrig;
