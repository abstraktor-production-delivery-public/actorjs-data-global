
const ActorApi = require('actor-api');


class GetTestDataBooleansAOrig extends ActorApi.ActorOriginating {
  constructor() {
    super();
    this.a = [];
  }
  
  *data() {
    this.a = this.getTestDataBooleans('demoKey1');
    this.logDebug(this.a);
  }
  
  *run() {
    VERIFY_VALUE([true, false], this.a);
  }
}

module.exports = GetTestDataBooleansAOrig;
