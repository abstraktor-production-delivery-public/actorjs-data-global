
const ActorApi = require('actor-api');


class GetTestDataNumberCOrig extends ActorApi.ActorOriginating {
  constructor() {
    super();
    this.c = 0;
  }
  
  *data() {
    this.c = this.getTestDataNumber('demoKey1', 30);
    this.logDebug(this.c);
  }
  
  *run() {
    VERIFY_VALUE(25, this.c);
  }
}

module.exports = GetTestDataNumberCOrig;
