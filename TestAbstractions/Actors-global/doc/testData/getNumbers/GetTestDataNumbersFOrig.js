
const ActorApi = require('actor-api');


class GetTestDataNumbersFOrig extends ActorApi.ActorOriginating {
  constructor() {
    super();
    this.a = [];
    this.f = [];
  }
  
  *data() {
    this.a = this.getTestDataNumbers('demoKey1');
    this.f = this.getTestDataNumbers('demoKeyX', () => {
      const x = this.a.slice();
      x.push(10.0);
      return x;
    });
    this.logDebug(this.a);
    this.logDebug(this.f);
  }
  
  *run() {
    VERIFY_VALUE([56.833, 13.941, 10.0], this.f);
  }
}

module.exports = GetTestDataNumbersFOrig;
