
const ActorApi = require('actor-api');


class GetTestDataNumbersDOrig extends ActorApi.ActorOriginating {
   constructor() {
    super();
    this.d = [];
  }
  
  *data() {
    this.d = this.getTestDataNumbers('demoKeyX', [-32.859, -71.237]);
    this.logDebug(this.d);
  }
  
  *run() {
    VERIFY_VALUE([-32.859, -71.237], this.d);
  }
}

module.exports = GetTestDataNumbersDOrig;
