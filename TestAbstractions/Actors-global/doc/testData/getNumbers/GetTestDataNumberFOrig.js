
const ActorApi = require('actor-api');


class GetTestDataNumberFOrig extends ActorApi.ActorOriginating {
  constructor() {
    super();
    this.a = 0;
    this.f = 0;
  }
  
  *data() {
    this.a = this.getTestDataNumber('demoKey1');
    this.f = this.getTestDataNumber('demoKeyX', () => {
      return this.a + 75;
    });
    this.logDebug(this.a);
    this.logDebug(this.f);
  }
  
  *run() {
    VERIFY_VALUE(25, this.a);
    VERIFY_VALUE(100, this.f);
  }
}

module.exports = GetTestDataNumberFOrig;
