
const ActorApi = require('actor-api');


class GetTestDataNumberGOrig extends ActorApi.ActorOriginating {
  constructor() {
    super();
    this.g = 0.0;
  }
 
  *data() {
    this.g = this.getTestDataNumber('demoKeyObject', 'UNKNOWN', {
      e: 2.7182818284590452353602874713527,
      pi: 3.1415926535897932384626433832795
    });
    this.logDebug(this.g);
  }
  
  *run() {
    VERIFY_VALUE(2.7182818284590452353602874713527, this.g);
  }
}

module.exports = GetTestDataNumberGOrig;
