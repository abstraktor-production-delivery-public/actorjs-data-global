
const ActorApi = require('actor-api');


class GetTestDataNumbersGOrig extends ActorApi.ActorOriginating {
  constructor() {
    super();
    this.g = [];
  }
  
  *data() {
    this.g = this.getTestDataNumbers('demoKeyObject', 'UNKNOWN', {
      Lund: [55.711, 13.191],
      Adelaide: [-34.905, 138.601]
    });
    this.logDebug(this.g);
  }
  
  *run() {
    VERIFY_VALUE([-34.905, 138.601], this.g);
  }
}

module.exports = GetTestDataNumbersGOrig;
