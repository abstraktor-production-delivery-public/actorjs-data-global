
const ActorApi = require('actor-api');


class GetTestDataNumberAOrig extends ActorApi.ActorOriginating {
  constructor() {
    super();
    this.a = 0;
  }
  
  *data() {
    this.a = this.getTestDataNumber('demoKey1');
    this.logDebug(this.a);
  }
  
  *run() {
    VERIFY_VALUE(25, this.a);
  }
}

module.exports = GetTestDataNumberAOrig;
