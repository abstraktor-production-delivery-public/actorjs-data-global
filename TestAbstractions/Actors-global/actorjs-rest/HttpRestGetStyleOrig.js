
const ActorApi = require('actor-api');
const HttpGetRequestStyleGet = require('./msg/HttpGetRequestStyleGet');


class HttpRestGetStyleOrig extends ActorApi.ActorOriginating {
  constructor() {
    super();
    this.httpConnection = null;
  }
  
  *data() {
  }
  
  *initClient() {
    this.httpConnection = this.createConnection('http');
  }
  
  *run() {
    this.httpConnection.send(new HttpGetRequestStyleGet('actorjs:9020'));
 
    const response = this.httpConnection.receive();
  }
  
  *exit(interrupted) {
    this.closeConnection(this.httpConnection);
  }
}

module.exports = HttpRestGetStyleOrig;
