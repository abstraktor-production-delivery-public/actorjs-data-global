
const HttpApi = require('http-stack-api');
const GuidGenerator = require('z-abs-corelayer-cs/clientServer/guid-generator');


class HttpGetRequestStyleGet extends HttpApi.Request {
  constructor(host) {
    super(HttpApi.Method.POST, `http://${host}/abs-data/`, HttpApi.Version.HTTP_1_1);
    
    this.addHeader(HttpApi.Header.HOST, host);
    this.addHeader(HttpApi.Header.CONNECTION, 'keep-alive');
    this.addHeader(HttpApi.Header.ACCEPT, 'application/json');
    this.addHeader(HttpApi.Header.USER_AGENT, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4298.0 Safari/537.36');
    this.addHeader(HttpApi.Header.CONTENT_TYPE, 'application/json');
    this.addHeader(HttpApi.Header.ORIGIN, `http://${host}`);
    this.addHeader(HttpApi.Header.ACCEPT_ENCODING, 'gzip, deflate');
    this.addHeader(HttpApi.Header.ACCEPT_LANGUAGE, 'en-GB,en-US;q=0.9,en;q=0.8');
    
    const body = this._getBody();
    this.addHeader(HttpApi.Header.CONTENT_LENGTH, body.length);
    this.addBody(body);
  }
  
  _getBody() {
    const bodyObject = {
      id: GuidGenerator.create(),
      requests: [{
        name: 'StyleGet',
        index: 0,
        params: []
      }]
    };
    return JSON.stringify(bodyObject);
  }
}

module.exports = HttpGetRequestStyleGet;
