
const ActorApi = require('actor-api');
const StackApi = require('stack-api');
const WebsocketStackApi = require('websocket-stack-api');
const HttpGetUpgradeRequest = require('./msg/HttpGetUpgradeRequest');
const GuidGenerator = require('z-abs-corelayer-cs/clientServer/guid-generator');


class WebsocketRestGetStyleOrig extends ActorApi.ActorOriginating {
  constructor() {
    super();
    this.httpConnection = null;
    this.websocketConnection = null;
    this.requistUri = 'http://example.com';
    this.wsProtocol = 'demo';
  }
  
  *data() {
    this.requistUri = this.getTestDataString('request-uri', this.requistUri);
    this.wsProtocol = this.getTestDataString('ws-protocol', this.wsProtocol);
  }
  
  *initClient() {
    this.httpConnection = this.createConnection('http');
  }
  
  *run() {
    this.httpConnection.send(new HttpGetUpgradeRequest(this.requistUri, this.wsProtocol));
    const responseSwitchingProtocols = this.httpConnection.receive();
    VERIFY_VALUE(101, responseSwitchingProtocols.statusCode);
    this.websocketConnection = this.switchProtocol('websocket', this.httpConnection);
    
    this.websocketConnection.send(new WebsocketStackApi.BinaryFrame(this._requestJson(), true));
    const response = this.websocketConnection.receive();
    VERIFY_VALUE(2, response.opCode);
    
    this.websocketConnection.send(new WebsocketStackApi.CloseFrame(true));
  }
  
  *exit(interrupted) {
    this.closeConnection(this.websocketConnection);
    this.closeConnection(this.httpConnection);
  }
  
  _requestJson() {
    const bodyObject = {
      msgId: 2,
      id: StackApi.GuidGenerator.create(),
      requests: [{
        name: 'StyleGet',
        index: 0,
        params: []
      }]
    };
    const body = JSON.stringify(bodyObject);
    const buffer = Buffer.allocUnsafe(5 + body.length);
    buffer.writeUInt8(0, 0);
    buffer.writeUInt8(0, 1);
    buffer.writeUInt16BE(1, 2);
    buffer.writeUInt8(body.length, 4);
    buffer.write(body, 5, body.length);
    return buffer;
  }
}

module.exports = WebsocketRestGetStyleOrig;
