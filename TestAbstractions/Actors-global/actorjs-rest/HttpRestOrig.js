
const ActorApi = require('actor-api');
const ActorjsApi = require('actorjs-stack-api');
const GuidGenerator = require('z-abs-corelayer-cs/clientServer/guid-generator');


class HttpRestOrig extends ActorApi.ActorOriginating {
  constructor() {
    super();
    this.actorjsConnection = null;
    this.pdNames = [];
    this.parametersArray = [];
  }

  *data() {
    this.pdNames = this.getTestDataStrings('pd-names');
    this.parametersArray = this.getTestDataArrayStrings('pd-parameters-array');
  }
  
  *initClient() {
    this.actorjsConnection = this.createConnection('actorjs', {
      connectionOptions: { transportProtocol: 'http'}
    });
  }
  
  *run() {
    const dpRequests = [];
    for(let i = 0; i < this.pdNames.length; ++i) {
      const dpRequest = new ActorjsApi.PdRequest(this.pdNames[i], ...this.parametersArray[i]);
      dpRequests.push(dpRequest);
    }
    const id = GuidGenerator.create();
    this.actorjsConnection.sendRequest(new ActorjsApi.MsgRequestHttp('actorjs:9020', id, ...dpRequests));
    const response = this.actorjsConnection.receive();
    VERIFY_VALUE(id, response.id);
    VERIFY_VALUE(2, response.msgId);
    VERIFY_VALUE(1, response.responses.length, '', '<=');
    response.responses.forEach((resp) => {
      VERIFY_VALUE('success', resp.result.code);
    });  
  }
  
  *exit(interrupted) {
    this.closeConnection(this.actorjsConnection);
  }
}

module.exports = HttpRestOrig;
