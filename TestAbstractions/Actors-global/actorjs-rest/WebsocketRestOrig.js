
const ActorApi = require('actor-api');
const ActorjsApi = require('actorjs-stack-api');


class WebsocketRestOrig extends ActorApi.ActorOriginating {
  constructor() {
    super();
    this.actorjsConnection = null;
    this.pdNames = [];
    this.parametersArray = [];
    this.requistUri = 'http://actorjs:9020';
    this.wsProtocol = 'demo';
  }
  
  *data() {
    this.pdNames = this.getTestDataStrings('pd-names');
    this.parametersArray = this.getTestDataArrayStrings('pd-parameters-array');
    this.requistUri = this.getTestDataString('request-uri', this.requistUri);
    this.wsProtocol = this.getTestDataString('ws-protocol', this.wsProtocol);
  }
  
  *initClient() {
    this.actorjsConnection = this.createConnection('actorjs', {
      connectionOptions: { transportProtocol: 'websocket'}
    });
  }
  
  *run() {
    this.actorjsConnection.sendRequest(new ActorjsApi.HttpUpgradeRequest(this.requistUri, this.wsProtocol));
    const httpResponse = this.actorjsConnection.receive();
    VERIFY_VALUE(101, httpResponse.statusCode);
    VERIFY_VALUE('Switching Protocols', httpResponse.reasonPhrase);
        
    const dpRequests = [];
    for(let i = 0; i < this.pdNames.length; ++i) {
      const dpRequest = new ActorjsApi.PdRequest(this.pdNames[i], ...this.parametersArray[i]);
      dpRequests.push(dpRequest);
    }
    this.actorjsConnection.sendRequest(new ActorjsApi.MsgRequestWs(...dpRequests));
    const wsResponse = this.actorjsConnection.receive();
    
    console.log(wsResponse.payloadData.toString());
    const response = JSON.parse(wsResponse.payloadData);
    VERIFY_VALUE('DP_RESP', response.type);
    VERIFY_VALUE(1, response.responses.length, '', '<=');
    response.responses.forEach((resp) => {
      VERIFY_VALUE('success', resp.result.code);
    });  
  }
  
  *exit(interrupted) {
    this.closeConnection(this.actorjsConnection);
  }
}

module.exports = WebsocketRestOrig;
