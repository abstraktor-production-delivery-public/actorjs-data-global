
'use strict';

const HighResolutionTimestamp = require('z-abs-corelayer-server/server/high-resolution-timestamp');


class PuppeteerStateMachine {
  static stateId = -1;
  static STATE_NONE = ++PuppeteerStateMachine.stateId;
  static STATE_S_CONNECTED = ++PuppeteerStateMachine.stateId;
  static STATE_C_CONNECTING = ++PuppeteerStateMachine.stateId;
  static STATE_C_CONNECTED = ++PuppeteerStateMachine.stateId;
  static STATE_C_UPGRADING = ++PuppeteerStateMachine.stateId;
  static STATE_S_UPGRADING = ++PuppeteerStateMachine.stateId;
  static STATE_UPGRADED = ++PuppeteerStateMachine.stateId;
  static STATE_CLOSING = ++PuppeteerStateMachine.stateId;
  static STATE_CLOSED = ++PuppeteerStateMachine.stateId;

  static STATES = [
    'NONE',
    'S_CONNECTED',
    'C_CONNECTING',
    'C_CONNECTED',
    'C_UPGRADING',
    'S_UPGRADING',
    'UPGRADED',
    'CLOSING',
    'CLOSED'
  ];

  static actionId = -1;
  static ACTION_S_CONNECTION = ++PuppeteerStateMachine.actionId;
  static ACTION_S_UPGRADED = ++PuppeteerStateMachine.actionId;
  static ACTION_S_NOT_UPGRADED = ++PuppeteerStateMachine.actionId;
  static ACTION_S_END = ++PuppeteerStateMachine.actionId;
  static ACTION_S_CLOSE = ++PuppeteerStateMachine.actionId;
  static ACTION_S_ERROR = ++PuppeteerStateMachine.actionId;
  static ACTION_C_CONNECT = ++PuppeteerStateMachine.actionId;
  static ACTION_C_CONNECTED = ++PuppeteerStateMachine.actionId;
  static ACTION_C_DNS_FAILURE = ++PuppeteerStateMachine.actionId;
  static ACTION_C_NOT_CONNECTED = ++PuppeteerStateMachine.actionId;
  static ACTION_C_UPGRADE = ++PuppeteerStateMachine.actionId;
  static ACTION_C_UPGRADED = ++PuppeteerStateMachine.actionId;
  static ACTION_C_NOT_UPGRADED = ++PuppeteerStateMachine.actionId;
  static ACTION_C_END = ++PuppeteerStateMachine.actionId;
  static ACTION_C_CLOSE = ++PuppeteerStateMachine.actionId;
  static ACTION_C_ERROR = ++PuppeteerStateMachine.actionId;

  static ACTIONS = [
    'S_CONNECTION',
    'S_UPGRADED',
    'S_NOT_UPGRADED',
    'S_END',
    'S_CLOSE',
    'S_ERROR',
    'C_CONNECT',
    'C_CONNECTED',
    'C_DNS_FAILURE',
    'C_NOT_CONNECTED',
    'C_UPGRADE',
    'C_UPGRADED',
    'C_NOT_UPGRADED',
    'C_END',
    'C_CLOSE',
    'C_ERROR'
  ];

  constructor(id, name, stateImpl) {
    this.id = id;
    this.name = name;
    this.socketPair = null;
    this.stateImpl = stateImpl;
    this.cbDone = null;
    this.locked = false;
    this.lockQueue = [];
    this.state = PuppeteerStateMachine.STATE_NONE;
    this.sEndedDelayed = false;
    this.sEndedDelayedParams = null;
    this.sEnded = false;
    this.cEnded = false;
    this.sClosed = false;
    this.cClosed = false;
    this.dataReceived = false;
    this.record = [];
    this.cancelIn = null;
    this.cancelOut = null;
  }
  
  setTimeoutIn(cb, time, ...params) {
    this.cancelIn = setTimeout((...params) => {
      this.cancelIn = null;
      cb(...params);
    }, time, ...params);
  }
  
  clearTimeoutIn() {
    if(this.cancelIn) {
      clearTimeout(this.cancelIn);
      this.cancelIn = null;
    }
  }
  
  setTimeoutOut(cb, time, ...params) {
    this.cancelOut = setTimeout((...params) => {
      this.cancelOut = null;
      cb(...params);
    }, time, ...params);
  }
  
  clearTimeoutOut() {
    if(this.cancelOut) {
      clearTimeout(this.cancelOut);
      this.cancelOut = null;
    }
  }
  
  _log(text) {
    this.record.push({
      type: 'log',
      id: this.id,
      name: this.name,
      text: text
    });
  }
  
  _print() {
    let s = '******************* ' + this.name + ':' + this.id + '\r\n';
    this.record.forEach((rec) => {
      let a = '';
      rec.params?.forEach((param) => {  
        if(param instanceof Error) {
          console.log(param);
          a += ' code: ' + param.code + '; message: ' + param.message;
        }
      });
      if('state' === rec.type) {
        s += 'STATE:  ' + rec.name + ':' + rec.id + ' ' + PuppeteerStateMachine.STATES[rec.state] + a;
      }
      else if('action' === rec.type) {
        s += 'ACTION: ' + rec.name + ':' + rec.id + ' ' + PuppeteerStateMachine.ACTIONS[rec.action] + a;
      }
      else if('lock' === rec.type) {
        s += 'LOCK:   ' + rec.name + ':' + rec.id;
      }
      else if('unlock' === rec.type) {
        s += 'UNLOCK: ' + rec.name + ':' + rec.id;
      }
      else if('log' === rec.type) {
        s += 'LOG: ' + rec.name + ':' + rec.id + ' ' + rec.text;
      }
      s += '\r\n';
    });
    console.log(s);
  }
  
  _warning(action) {
    ddb.warning(`${this.id}:${this.name}`, `IN - WRONG ACTION[${this.name}:${this.id}]:`, PuppeteerStateMachine.ACTIONS[action], 'in state:', PuppeteerStateMachine.STATES[this.state]);
  }
  
  onAction(action, ...params) {
    if(this.locked) {
      this.lockQueue.push({
        action: action,
        params: params
      });
      return;
    }
    this.record.push({
      type: 'action',
      id: this.id,
      name: this.name,
      action: action,
      params: params
    });
    const space = 'handler' === this.name ? '                                                  ' : '';
    //console.log(space + 'ACTION[' + this.id + ']: ' + PuppeteerStateMachine.ACTIONS[action]);
    switch(this.state) {
      case PuppeteerStateMachine.STATE_NONE: {
        if(PuppeteerStateMachine.ACTION_S_CONNECTION === action) {
          this.setState(PuppeteerStateMachine.STATE_S_CONNECTED);
          this.stateImpl.onStateServerConnection(...params);
        }
        else if(PuppeteerStateMachine.ACTION_C_ERROR === action) {}
        else if(PuppeteerStateMachine.ACTION_S_ERROR === action) {}
        else {this._warning(action);this._print();}
        break;
      }
      case PuppeteerStateMachine.STATE_S_CONNECTED: {
        if(PuppeteerStateMachine.ACTION_C_CONNECT === action) {
          this.setState(PuppeteerStateMachine.STATE_C_CONNECTING);
          this.stateImpl.onStateClientConnecting(...params);
        }
        else if(PuppeteerStateMachine.ACTION_C_DNS_FAILURE === action) {
          this.cEnded = true;
          this.cClosed = true;
          this.setState(PuppeteerStateMachine.STATE_CLOSING);
          this.stateImpl.onStateClientDnsFailure(this.socketPair, ...params);
          this.clearTimeoutOut();
          this.socketPair.incommingClose();
        }
        else if(PuppeteerStateMachine.ACTION_S_END === action) {
          this.cEnded = true;
          this.cClosed = true;
          this.sEnded = true;
          this.setState(PuppeteerStateMachine.STATE_CLOSING);
          this.stateImpl.onStateServerEnded(this.socketPair);
          this.socketPair.incommingClose();
        }
        else if(PuppeteerStateMachine.ACTION_S_CLOSE === action) {
          this.cEnded = true;
          this.cClosed = true;
          this._severClosed();
          this.sClosed = true;
          this.clearTimeoutIn();
          this.stateImpl.onStateServerClosed(this.socketPair);
          this._closed();
        }
        else if(PuppeteerStateMachine.ACTION_C_ERROR === action) {}
        else if(PuppeteerStateMachine.ACTION_S_ERROR === action) {}
        else {this._warning(action);this._print();}
        break;
      }
      case PuppeteerStateMachine.STATE_C_CONNECTING: {
        if(PuppeteerStateMachine.ACTION_C_CONNECTED === action) {
          this.setState(PuppeteerStateMachine.STATE_C_CONNECTED);
          this.stateImpl.onStateClientConnected(this.socketPair, ...params);
          if(this.sEndedDelayed) {
            this.sEndedDelayed = false;
            this.socketPair.outgoingEnd();
          }
        }
        else if(PuppeteerStateMachine.ACTION_C_NOT_CONNECTED === action) {
          this.stateImpl.onStateClientNotConnected(this.socketPair, ...params);
          this.setState(PuppeteerStateMachine.STATE_CLOSING);
          this.clearTimeoutOut();
          this.socketPair.outgoingSocket.destroy();
          this.socketPair.incommingSocket.destroy();
        }
        else if(PuppeteerStateMachine.ACTION_S_END === action) {
          this.sEnded = true;
          this.sEndedDelayed = true;
          this.sEndedDelayedParams = params;
          this.stateImpl.onStateServerEnded(this.socketPair);
        }
        else if(PuppeteerStateMachine.ACTION_S_CLOSE === action) {
          this.sEnded = true;
          this.sClosed = true;
          this.setState(PuppeteerStateMachine.STATE_CLOSING);
          this.stateImpl.onStateServerClosed(this.socketPair);
          this.clearTimeoutIn();
          this.socketPair.outgoingClose();
        }
        else if(PuppeteerStateMachine.ACTION_C_ERROR === action) {
          this.clearTimeoutOut();
          this.stateImpl.onStateClientNotConnected(this.socketPair, ...params);
        }
        else if(PuppeteerStateMachine.ACTION_C_CLOSE === action) {
          this._clientClosed();
          this.cClosed = true;
          this.setState(PuppeteerStateMachine.STATE_CLOSING);
          this.stateImpl.onStateClientClosed(this.socketPair);
          this.clearTimeoutOut();
          this.socketPair.incommingClose();
        }
        else if(PuppeteerStateMachine.ACTION_S_ERROR === action) {}
        else {this._warning(action);this._print();}
        break;
      }
      case PuppeteerStateMachine.STATE_C_CONNECTED: {
        if(PuppeteerStateMachine.ACTION_C_UPGRADE === action) {
          this.setState(PuppeteerStateMachine.STATE_C_UPGRADING);
          this.stateImpl.onStateClientUpgrade(...params);
        }
        else if(PuppeteerStateMachine.ACTION_S_END === action) {
          this.sEnded = true;
          this.stateImpl.onStateServerEnded(this.socketPair);
          this.socketPair.outgoingEnd();
        }
        else if(PuppeteerStateMachine.ACTION_S_CLOSE === action) {
          this._severClosed();
          this.sClosed = true;
          this.setState(PuppeteerStateMachine.STATE_CLOSING);
          this.stateImpl.onStateServerClosed(this.socketPair);
          this.clearTimeoutIn();
          this.socketPair.outgoingClose();
        }
        else if(PuppeteerStateMachine.ACTION_C_END === action) {
          this.cEnded = true;
          this.stateImpl.onStateClientEnded(this.socketPair);
          this.socketPair.incommingEnd();
        }
        else if(PuppeteerStateMachine.ACTION_C_CLOSE === action) {
          this._clientClosed();
          this.cClosed = true;
          this.setState(PuppeteerStateMachine.STATE_CLOSING);
          this.stateImpl.onStateClientClosed(this.socketPair);
          this.clearTimeoutOut();
          this.socketPair.incommingClose();
        }
        else if(PuppeteerStateMachine.ACTION_C_ERROR === action) {}
        else if(PuppeteerStateMachine.ACTION_S_ERROR === action) {}
        else {this._warning(action);this._print();}
        break;
      }
      case PuppeteerStateMachine.STATE_C_UPGRADING: {
        if(PuppeteerStateMachine.ACTION_C_UPGRADED === action) {
          this.setState(PuppeteerStateMachine.STATE_S_UPGRADING);
          this.stateImpl.onStateClientUpgraded(...params);
        }
        else if(PuppeteerStateMachine.ACTION_C_ERROR === action) {
          this.clearTimeoutOut();
          this.stateImpl.onStateClientNotUpgraded();
        }
        else if(PuppeteerStateMachine.ACTION_S_ERROR === action) {}
        else if(PuppeteerStateMachine.ACTION_C_NOT_UPGRADED === action) {
          this.stateImpl.onStateClientNotUpgraded(...params);
          this.clearTimeoutOut();
          this.socketPair.incommingClose();
        }
        else if(PuppeteerStateMachine.ACTION_S_END === action) {
          this.sEnded = true;
          this.stateImpl.onStateServerEnded(this.socketPair);
          this.socketPair.outgoingEnd();
        }
        else if(PuppeteerStateMachine.ACTION_S_CLOSE === action) {
          this._severClosed();
          this.sClosed = true;
          this.setState(PuppeteerStateMachine.STATE_CLOSING);
          this.stateImpl.onStateServerClosed(this.socketPair);
          this.clearTimeoutIn();
          this.socketPair.outgoingClose();
        }
        else if(PuppeteerStateMachine.ACTION_C_END === action) {
          this.cEnded = true;
          this.stateImpl.onStateClientEnded(this.socketPair);
          this.socketPair.incommingEnd();
        }
        else if(PuppeteerStateMachine.ACTION_C_CLOSE === action) {
          this._clientClosed();
          this.cClosed = true;
          this.setState(PuppeteerStateMachine.STATE_CLOSING);
          this.stateImpl.onStateClientClosed(this.socketPair);
          this.clearTimeoutOut();
          this.socketPair.incommingClose();
        }
        else {this._warning(action);this._print();}
        break;
      }
      case PuppeteerStateMachine.STATE_S_UPGRADING: {
        if(PuppeteerStateMachine.ACTION_S_UPGRADED === action) {
          this.setState(PuppeteerStateMachine.STATE_UPGRADED);
          this.stateImpl.onStateServerUpgraded(...params);
        }
        else if(PuppeteerStateMachine.ACTION_S_ERROR === action) {
          this.clearTimeoutIn();
          this.stateImpl.onStateServerNotUpgraded();
        }
        else if(PuppeteerStateMachine.ACTION_C_ERROR === action) {}
        else if(PuppeteerStateMachine.ACTION_S_NOT_UPGRADED === action) {
          this.stateImpl.onStateServerNotUpgraded();
          this.clearTimeoutOut();
          this.socketPair.outgoingClose();
        } 
        else if(PuppeteerStateMachine.ACTION_S_END === action) {
          this.sEnded = true;
          this.stateImpl.onStateServerEnded(this.socketPair);
          this.socketPair.outgoingEnd();
        }
        else if(PuppeteerStateMachine.ACTION_S_CLOSE === action) {
          this._severClosed();
          this.sClosed = true;
          this.setState(PuppeteerStateMachine.STATE_CLOSING);
          this.stateImpl.onStateServerClosed(this.socketPair);
          this.clearTimeoutIn();
          this.socketPair.outgoingClose();
        }
        else if(PuppeteerStateMachine.ACTION_C_END === action) {
          this.cEnded = true;
          this.stateImpl.onStateClientEnded(this.socketPair);
          this.socketPair.incommingEnd();
        }
        else if(PuppeteerStateMachine.ACTION_C_CLOSE === action) {
          this._clientClosed();
          this.cClosed = true;
          this.setState(PuppeteerStateMachine.STATE_CLOSING);
          this.stateImpl.onStateClientClosed(this.socketPair);
          this.clearTimeoutOut();
          this.socketPair.incommingClose();
        }
        else {this._warning(action);this._print();}
        break;
      }
      case PuppeteerStateMachine.STATE_UPGRADED: {
        if(PuppeteerStateMachine.ACTION_S_END === action) {
          this.sEnded = true;
          this.stateImpl.onStateServerEnded(this.socketPair);
          this.socketPair.outgoingEnd();
        }
        else if(PuppeteerStateMachine.ACTION_S_CLOSE === action) {
          this._severClosed();
          this.sClosed = true;
          this.setState(PuppeteerStateMachine.STATE_CLOSING);
          this.stateImpl.onStateServerClosed(this.socketPair);
          this.clearTimeoutIn();
          this.socketPair.outgoingClose();
        }
        else if(PuppeteerStateMachine.ACTION_C_END === action) {
          this.cEnded = true;
          this.stateImpl.onStateClientEnded(this.socketPair);
          this.socketPair.incommingEnd();
        }
        else if(PuppeteerStateMachine.ACTION_C_CLOSE === action) {
          this._clientClosed();
          this.cClosed = true;
          this.setState(PuppeteerStateMachine.STATE_CLOSING);
          this.stateImpl.onStateClientClosed(this.socketPair);
          this.clearTimeoutOut();
          this.socketPair.incommingClose();
        }
        else if(PuppeteerStateMachine.ACTION_C_ERROR === action) {}
        else if(PuppeteerStateMachine.ACTION_S_ERROR === action) {}
        else {this._warning(action);this._print();}
        break;
      }
      case PuppeteerStateMachine.STATE_CLOSING: {
        if(PuppeteerStateMachine.ACTION_S_END === action) {
          this.sEnded = true;
          this.stateImpl.onStateServerEnded(this.socketPair);
          this.socketPair.outgoingEnd();
        }
        else if(PuppeteerStateMachine.ACTION_S_CLOSE === action) {
          this._severClosed();
          this.sClosed = true;
          this.clearTimeoutIn();
          this.stateImpl.onStateServerClosed(this.socketPair);
          if(!this.cClosed) {
            this.socketPair.outgoingClose();
          }
          else {
            this._closed();
          }
        }
        else if(PuppeteerStateMachine.ACTION_C_END === action) {
          this.cEnded = true;
          this.stateImpl.onStateClientEnded(this.socketPair);
          this.socketPair.incommingEnd();
        }
        else if(PuppeteerStateMachine.ACTION_C_CLOSE === action) {
          this._clientClosed();
          this.cClosed = true;
          this.clearTimeoutOut();
          this.stateImpl.onStateClientClosed(this.socketPair);
          if(!this.sClosed) {
            this.socketPair.incommingClose();
          }
          else {
            this._closed();
          }
        }
        else if(PuppeteerStateMachine.ACTION_C_CONNECT === action) {
          if(!this.sClosed) {
            params[3](new Error('To late to connect.'));
          }
        }
        else if(PuppeteerStateMachine.ACTION_C_NOT_CONNECTED === action) {}
        else if(PuppeteerStateMachine.ACTION_C_UPGRADE === action) {}
        else if(PuppeteerStateMachine.ACTION_C_NOT_UPGRADED === action) {}
        else if(PuppeteerStateMachine.ACTION_C_ERROR === action) {}
        else if(PuppeteerStateMachine.ACTION_S_ERROR === action) {}
        else {this._warning(action);this._print();}
        break;
      }
      case PuppeteerStateMachine.STATE_CLOSED: {
        if(PuppeteerStateMachine.ACTION_C_CONNECT === action) {}
        else if(PuppeteerStateMachine.ACTION_C_NOT_CONNECTED === action) {}
        else if(PuppeteerStateMachine.ACTION_C_UPGRADE === action) {}
        else if(PuppeteerStateMachine.ACTION_C_NOT_UPGRADED === action) {}
        else if(PuppeteerStateMachine.ACTION_C_ERROR === action) {}
        else if(PuppeteerStateMachine.ACTION_S_ERROR === action) {}
        else {this._warning(action);this._print();}
        break;
      }
      default: {this._warning(action);this._print();}
    }
  }
  
  kill() {
    // this._print();
    process.nextTick(() => {
      if(!this.cClosed) {
        this.clearTimeoutIn();
        this.socketPair.outgoingClose();
      }
      if(!this.sClosed) {
        this.clearTimeoutIn();
        this.socketPair.incommingClose();
      }
    });
  }
  
  lock() {
    this.locked = true;
    this.record.push({
      type: 'lock',
      id: this.id,
      name: this.name
    });
  }
  
  unlock() {
    this.locked = false;
    this.record.push({
      type: 'unlock',
      id: this.id,
      name: this.name
    });
    if(0 !== this.lockQueue.length) {
      while(0 !== this.lockQueue.length)  {
        const queued = this.lockQueue.shift();
        this.onAction(queued.action, ...queued.params);
      }
    }
  }
  
  setDataReceived() {
    this.dataReceived = true;
  }
  
  setCbDone(cbDone) {
    this.cbDone = cbDone;
  }
  
  _severClosed() {
    if(!this.sEnded) {
      this.sEnded = true;
      this.stateImpl.onStateServerEnded(this.socketPair);
    }
  }
  
  _clientClosed() {
    if(!this.cEnded) {
      this.cEnded = true;
      this.stateImpl.onStateClientEnded(this.socketPair);
    }
  }
  
  _closed() {
    this.setState(PuppeteerStateMachine.STATE_CLOSED);
    if(this.cbDone) {
      const cbDone = this.cbDone;
      this.cbDone = null;
      cbDone();
    }
  }
  
  isClosed() {
    return PuppeteerStateMachine.STATE_CLOSED === this.state;
  }
  
  isNone() {
    return PuppeteerStateMachine.STATE_NONE === this.state;
  }
  
  setState(state, ...params) {
    const previousState = this.state;
    this.state = state;
    const space = 'handler' === this.name ? '                                                  ' : '';
    //console.log(space + 'STATE[' + this.id + ']: ' + PuppeteerStateMachine.STATES[state]);
    this.record.push({
      type: 'state',
      id: this.id,
      name: this.name,
      state: this.state,
      params: params
    });
  }
}


module.exports = PuppeteerStateMachine;
