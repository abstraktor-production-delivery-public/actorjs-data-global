
'use strict';

const StackApi = require('stack-api');


class PuppeteerMockWorkerMain extends StackApi.WorkerMain {
  constructor() {
    super();
  }
  
  run(host, mockServer, srcAddress, mcPort, done) {
    this.channel.request('run', done, [mcPort], host, mockServer, srcAddress, mcPort);
  }
  
  addMessageChannel(mcPort, done) {
    this.channel.request('addMessageChannel', done, [mcPort], mcPort);
  }
}


module.exports = PuppeteerMockWorkerMain;
