
'use strict';

const StackApi = require('stack-api');
const HttpInnerLog = require('../http/http-inner-log');
const HttpInnerLog2 = require('../http/http-inner-log2');
const WebsocketInnerLog = require('../websocket/websocket-inner-log');
const WebsocketConst = require('../websocket/websocket-const');


class PuppeteerInnerLog {
  constructor(connection) {
    this.connection = connection;
    this.name = this.connection.name;
  }

  _getConnectionDataLocal(connectionObject) {
    if(connectionObject) {
      const clientConnectionDatas = connectionObject.getClientConnectionDatas();
      const connectionDataLocal = clientConnectionDatas.entries().next();
      if(!connectionDataLocal.done) {
        return connectionDataLocal.value[1];
      }
      else {
        return null;
      }
    }
    else {
      return null;
    }
  }

  _getConnectionDataRemote(connectionObject) {
    if(connectionObject) {
      const serverConnectionDatas = connectionObject.getServerConnectionDatas();
      const connectionDataRemote = serverConnectionDatas.entries().next();
      if(!connectionDataRemote.done) {
        return connectionDataRemote.value[1];
      }
      else {
        return null;
      }
    }
    else {
      return null;
    }
  }
  
  logConnecting(connectionObject) {
    const connectionDataLocal = this._getConnectionDataLocal(connectionObject);
    const connectionDataRemote = this._getConnectionDataRemote(connectionObject);
    this.connection.logIp(() => this.connection.createLogMessage(`${this.connection.name}.connection [${connectionDataLocal.getId()}] ==> [${connectionDataRemote.getId()}] CONNECTING`, undefined, new StackApi.LogMsgClient(StackApi.LogDataAction.CONNECTING, 'http', connectionDataLocal, connectionDataRemote), null), 'Puppeteer', 'b1bed13f-1469-4de5-ab3b-6d80696d4be3');
  }
  
  logDnsFailure(connectionObject, dstUrl, err) {
    const connectionDataLocal = this._getConnectionDataLocal(connectionObject);
    connectionObject.registerConnected(connectionDataLocal, false, (connectionDataRemote) => {
      this.connection.logIp(() => this.connection.createLogMessage(`${this.connection.name}.connection [${connectionDataLocal.getId()}] ==> [${dstUrl}] DNS FAILURE`, undefined, new StackApi.LogMsgClient(StackApi.LogDataAction.DNS_FAILURE, 'http', connectionDataLocal, connectionDataRemote), null), 'Puppeteer', 'b1bed13f-1469-4de5-ab3b-6d80696d4be3');
    });
  }
  
  logConnected(connectionObject) {
    const connectionDataLocal = this._getConnectionDataLocal(connectionObject);
    connectionObject.registerConnected(connectionDataLocal, true, (connectionDataRemote) => {
      this.connection.logIp(() => this.connection.createLogMessage(`${this.connection.name}.connection [${connectionDataLocal.getId()}] ==> [${connectionDataRemote.getId()}] CONNECTED`, undefined, new StackApi.LogMsgClient(StackApi.LogDataAction.CONNECTED, 'http', connectionDataLocal, connectionDataRemote), null), 'Puppeteer', '061b9b35-4087-44d5-bd01-6844b3431a9d');
    });
  }
  
  logNotConnected(connectionObject, err) {
    const connectionDataLocal = this._getConnectionDataLocal(connectionObject);
    connectionObject.registerConnected(connectionDataLocal, false, (connectionDataRemote) => {
      this.connection.logIp(() => this.connection.createLogMessage(`${this.connection.name}.connection [${connectionDataLocal.getId()}] ==> [${connectionDataRemote.getId()}] NOT CONNECTED - ${err.message}`, undefined, new StackApi.LogMsgClient(StackApi.LogDataAction.NOT_CONNECTED, 'http', connectionDataLocal, connectionDataRemote), null), 'Puppeteer', 'fcd47f83-f0de-45d8-9fbb-48951039f132');
    });
  }
  
  logUpgrading(connectionObject) {
    const connectionDataLocal = this._getConnectionDataLocal(connectionObject);
    const connectionDataRemote = this._getConnectionDataRemote(connectionObject);
    this.connection.logIp(() => this.connection.createLogMessage(`${this.connection.name}.connection [${connectionDataLocal.getId()}] ==> [${connectionDataRemote.getId()}] UPGRADING`, undefined, new StackApi.LogMsgClient(StackApi.LogDataAction.UPGRADING, 'http', connectionDataLocal, connectionDataRemote), null), 'Puppeteer', '93309923-ccce-4832-8a07-fb2588cfac5c');
  }
  
  logUpgraded(connectionObject) {
    const connectionDataLocal = this._getConnectionDataLocal(connectionObject);
    const connectionDataRemote = this._getConnectionDataRemote(connectionObject);
    connectionDataLocal.secure = true;
    connectionDataRemote.secure = true;
    this.connection.logIp(() => this.connection.createLogMessage(`${this.connection.name}.connection [${connectionDataLocal.getId()}] ==> [${connectionDataRemote.getId()}] UPGRADED`, undefined, new StackApi.LogMsgClient(StackApi.LogDataAction.UPGRADED, 'http', connectionDataLocal, connectionDataRemote), null), 'Puppeteer', '1bbf042e-9db3-4dd8-a9d8-1b0799e7510b');
  }
  
  logNotUpgraded(connectionObject, err) {
    const connectionDataLocal = this._getConnectionDataLocal(connectionObject);
    const connectionDataRemote = this._getConnectionDataRemote(connectionObject);
    this.connection.logIp(() => this.connection.createLogMessage(`${this.connection.name}.connection [${connectionDataLocal.getId()}] ==> [${connectionDataRemote.getId()}] NOT UPGRADED - ${err.message}`, undefined, new StackApi.LogMsgClient(StackApi.LogDataAction.NOT_UPGRADED, 'http', connectionDataLocal, connectionDataRemote), null), 'Puppeteer', '1974f652-b4af-42b7-8dcd-60c497763f81');
  }
  
  logClosing(connectionObject, isActor=false) {
    const connectionDataLocal = isActor ? this._getConnectionDataRemote(connectionObject) : this._getConnectionDataLocal(connectionObject);
    const connectionDataRemote = isActor ? this._getConnectionDataLocal(connectionObject) : this._getConnectionDataRemote(connectionObject);
    this.connection.logIp(() => this.connection.createLogMessage(`${this.connection.name}.connection [${connectionDataLocal.getId()}] ==> [${connectionDataRemote.getId()}] CLOSING`, undefined, new StackApi.LogMsgClient(StackApi.LogDataAction.CLOSING, 'http', connectionDataLocal, connectionDataRemote), null), 'Puppeteer', '6fe7c77f-59ae-4ca9-acac-a31ee0c034b1');
  }
  
  logClosed(connectionObject, isActor=false) {
    const connectionDataLocal = this._getConnectionDataLocal(connectionObject);
    if(connectionDataLocal) {
      connectionObject.registerClosed(connectionDataLocal, isActor, (connectionDataRemote) => {
        this.connection.logIp(() => this.connection.createLogMessage(`${this.connection.name}.connection [${connectionDataLocal.getId()}] ==> [${connectionDataRemote.getId()}] CLOSED`, undefined, new StackApi.LogMsgClient(StackApi.LogDataAction.CLOSED, 'http', connectionDataLocal, connectionDataRemote), null), 'Puppeteer', 'bb4a0e61-07f5-494e-93ca-b65e056a3268');
      });
    }
  }
  
  logMessageSend(msg, connectionObject) {
    if(this.connection.isLogIp()) {
      const ipLogs = [];
      let caption = 'UNKNOWN';
      const innerLogHttpRequest = HttpInnerLog.createRequestLine(msg);
      if(innerLogHttpRequest.logParts[0]) {
        const log = innerLogHttpRequest.logParts[0].text;
        caption = log.substring(0, log.lastIndexOf(' '));
      }
      ipLogs.push(innerLogHttpRequest);
      ipLogs.push(HttpInnerLog.createHeaders(msg));
      const connectionDataLocal = this._getConnectionDataLocal(connectionObject);
      const connectionDataRemote = this._getConnectionDataRemote(connectionObject);
      this.connection.logIp(() => this.connection.createLogMessage(`${this.name}.connection [${connectionDataLocal.getId()}] ==> [${connectionDataRemote.getId()}] SEND MSG`, ipLogs, new StackApi.LogMsgMessage(StackApi.LogDataAction.SEND, 'http', connectionDataLocal, connectionDataRemote, caption), null), 'Puppeteer', '15e6f51e-8dfb-4eb7-b926-56b42c82ff00');
    }
  }
  
  logMessageBodySend(connectionObject, contentType, contentEncoding, transferEncoding, rawBuffer) {
    if(this.connection.isLogIp()) {
      const ipLogs = [];
      const bodyInnerLog = HttpInnerLog.createBody(Buffer.from(rawBuffer), contentType, contentEncoding, transferEncoding);
      const caption = bodyInnerLog.caption;
      ipLogs.push(bodyInnerLog.log, bodyInnerLog.dataBuffers);
      const connectionDataLocal = this._getConnectionDataLocal(connectionObject);
      const connectionDataRemote = this._getConnectionDataRemote(connectionObject);
      this.connection.logIp(() => this.connection.createLogMessage(`${this.name}.connection [${connectionDataLocal.getId()}] ==> [${connectionDataRemote.getId()}] SEND MSG`, ipLogs, new StackApi.LogMsgMessage(StackApi.LogDataAction.SEND, 'http', connectionDataLocal, connectionDataRemote, caption), null), 'Puppeteer', 'f24a0b63-435c-42f4-977b-70fc526a59db');
    }
  }
  
  logMessageReceive(msg, connectionObject) {
    if(this.connection.isLogIp()) {
      const ipLogs = [];
      let caption = 'UNKNOWN';
      const innerLogHttpResponse = HttpInnerLog.createStatusLine(msg);
      if(innerLogHttpResponse.logParts[0]) {
        const log = innerLogHttpResponse.logParts[0].text;
        caption = log.substring(log.indexOf(' ') + 1);
      }
      ipLogs.push(innerLogHttpResponse);
      ipLogs.push(HttpInnerLog.createHeaders(msg));
      const connectionDataLocal = this._getConnectionDataLocal(connectionObject);
      const connectionDataRemote = this._getConnectionDataRemote(connectionObject);
      this.connection.logIp(() => this.connection.createLogMessage(`${this.name}.connection [${connectionDataLocal.getId()}] >== [${connectionDataRemote.getId()}] RECEIVE MSG`, ipLogs, new StackApi.LogMsgMessage(StackApi.LogDataAction.RECEIVE, 'http', connectionDataLocal, connectionDataRemote, caption), null), 'Puppeteer', '7eaf6f99-bac6-4504-abce-a8db804b2df2');
    }
  }
  
  logMessageBodyReceive(connectionObject, contentType, contentEncoding, transferEncoding, rawBuffer) {
    if(this.connection.isLogIp()) {
      const ipLogs = [];
      const bodyInnerLog = HttpInnerLog.createBody(Buffer.from(rawBuffer), contentType, contentEncoding, transferEncoding);
      const caption = bodyInnerLog.caption;
      ipLogs.push(bodyInnerLog.log, bodyInnerLog.dataBuffers);
      const connectionDataLocal = this._getConnectionDataLocal(connectionObject);
      const connectionDataRemote = this._getConnectionDataRemote(connectionObject);
      this.connection.logIp(() => this.connection.createLogMessage(`${this.name}.connection [${connectionDataLocal.getId()}] >== [${connectionDataRemote.getId()}] RECEIVE MSG`, ipLogs, new StackApi.LogMsgMessage(StackApi.LogDataAction.RECEIVE, 'http', connectionDataLocal, connectionDataRemote, caption), null), 'Puppeteer', '7eaf6f99-bac6-4504-abce-a8db804b2df2');
    }
  }
  
  logMessageSendWebSocket(msg, connectionObject) {
    if(this.connection.isLogIp()) {
      const ipLogs = [];
      const caption = WebsocketConst.OPCODES[msg.opCode];
      const websocketInnerLog = WebsocketInnerLog.createHeader(msg);
      ipLogs.push(websocketInnerLog);
      const payload = WebsocketInnerLog.createPayload(msg);
      if(payload) {
        ipLogs.push(payload);
      }
      const payloadUnmasked = WebsocketInnerLog.createPayloadUnmasked(msg);
      if(payloadUnmasked) {
        ipLogs.push(payloadUnmasked);
      }
      const connectionDataLocal = this._getConnectionDataLocal(connectionObject);
      const connectionDataRemote = this._getConnectionDataRemote(connectionObject);
      this.connection.logIp(() => this.connection.createLogMessage(`${this.name}.connection [${connectionDataLocal.getId()}] >== [${connectionDataRemote.getId()}] RECEIVE MSG`, ipLogs, new StackApi.LogMsgMessage(StackApi.LogDataAction.SEND, 'websocket', connectionDataLocal, connectionDataRemote, caption), null), 'Puppeteer', 'cdf8404c-548b-47e1-bc27-4c2495b9e0a4');
    }
  }
  
  logMessageReceiveWebSocket(msg, connectionObject) {
    if(this.connection.isLogIp()) {
      const ipLogs = [];
      const caption = WebsocketConst.OPCODES[msg.opCode];
      const websocketInnerLog = WebsocketInnerLog.createHeader(msg);
      ipLogs.push(websocketInnerLog);
      const payload = WebsocketInnerLog.createPayload(msg);
      if(payload) {
        ipLogs.push(payload);
      }
      const payloadUnmasked = WebsocketInnerLog.createPayloadUnmasked(msg);
      if(payloadUnmasked) {
        ipLogs.push(payloadUnmasked);
      }
      const connectionDataLocal = this._getConnectionDataLocal(connectionObject);
      const connectionDataRemote = this._getConnectionDataRemote(connectionObject);
      this.connection.logIp(() => this.connection.createLogMessage(`${this.name}.connection [${connectionDataLocal.getId()}] >== [${connectionDataRemote.getId()}] RECEIVE MSG`, ipLogs, new StackApi.LogMsgMessage(StackApi.LogDataAction.RECEIVE, 'websocket', connectionDataLocal, connectionDataRemote, caption), null), 'Puppeteer', 'f06adf40-33fd-4bf5-8852-3a1e7ee91128');
    }
  }
  
  logFrameSend(connectionObject, frame, dataBuffers) {
    if(this.connection.isLogIp()) {
      const result = this._handleFrame(frame, dataBuffers);
      const connectionDataLocal = this._getConnectionDataLocal(connectionObject);
      const connectionDataRemote = this._getConnectionDataRemote(connectionObject);
      this.connection.logIp(() => this.connection.createLogMessage(`${this.name}.connection [${connectionDataLocal.getId()}] ==> [${connectionDataRemote.getId()}] SEND FRAME`, result.ipLogs, new StackApi.LogMsgMessage(StackApi.LogDataAction.SEND, 'http', connectionDataLocal, connectionDataRemote, result.caption), dataBuffers), 'Puppeteer', 'a1e86b62-d69a-4051-8252-9d2e90725db6');
    }
  }
  
  logFrameReceive(connectionObject, frame, dataBuffers) {
    if(this.connection.isLogIp()) {
      const result = this._handleFrame(frame, dataBuffers);
      const connectionDataLocal = this._getConnectionDataLocal(connectionObject);
      const connectionDataRemote = this._getConnectionDataRemote(connectionObject);
      this.connection.logIp(() => this.connection.createLogMessage(`${this.name}.connection [${connectionDataLocal.getId()}] >== [${connectionDataRemote.getId()}] RECEIVE MSG`, result.ipLogs, new StackApi.LogMsgMessage(StackApi.LogDataAction.RECEIVE, 'http', connectionDataLocal, connectionDataRemote, result.caption), dataBuffers), 'Puppeteer', 'fa10c83b-983d-4457-a244-b089b27e8dd2');
    }
  }
  
  _handleFrame(frame, dataBuffers) {
    const result = {
      ipLogs: [],
      caption: HttpInnerLog2.FRAME_NAMES[frame.type]
    };
    //this.connection.stackContentCache
    const innerLogHttpFrame = HttpInnerLog2.createFrameHeader(frame);
    result.ipLogs.push(innerLogHttpFrame);
    switch(frame.type) {
      case HttpInnerLog2.FRAME_TYPE_DATA: {
        const framePayload = HttpInnerLog2.createFramePayloadData(frame);
        if(framePayload) {
          result.ipLogs.push(framePayload);
        }
        const dataLog = HttpInnerLog2.createData(frame);
        if(dataLog) {
          result.ipLogs.push(dataLog);
        }
        //result.caption += `[${frame.streamIdentifier}]`;
        break;
      }
      case HttpInnerLog2.FRAME_TYPE_HEADERS: {
        const framePayload = HttpInnerLog2.createFramePayloadHeaders(frame);
        if(framePayload) {
          result.ipLogs.push(framePayload);
        }
        const lineResult = HttpInnerLog2.createLine(frame);
        result.ipLogs.push(lineResult.log);
        const headers = HttpInnerLog2.createHeaders(frame);
        if(headers) {
          result.ipLogs.push(headers);
        }
        //result.caption += `[${frame.streamIdentifier}]`;
        //const pseudoHeader = frame.data.pseudoHeader;
        result.caption += ` - ${lineResult.caption}`;
        break;
      }
      case HttpInnerLog2.FRAME_TYPE_PRIORITY: {
        break;
      }
      case HttpInnerLog2.FRAME_TYPE_RST_STREAM: {
        const rstStream = HttpInnerLog2.createRstStream(frame);
        result.ipLogs.push(rstStream);
        break;
      }
      case HttpInnerLog2.FRAME_TYPE_SETTINGS: {
        const ackFlag = StackApi.BitByte.getBit(frame.flags, 7);
        if(ackFlag) {
          result.caption += ' - ACK';
        }
        const settings = HttpInnerLog2.createSettings(frame);
        if(settings) {
          result.ipLogs.push(settings);
        }
        break;
      }
      case HttpInnerLog2.FRAME_TYPE_PUSH_PROMISE: {
        break;
      }
      case HttpInnerLog2.FRAME_TYPE_PING: {
        const ackFlag = StackApi.BitByte.getBit(frame.flags, 7);
        if(ackFlag) {
          result.caption += ' - ACK';
        }
        const ping = HttpInnerLog2.createPing(frame, dataBuffers);
        result.ipLogs.push(ping);
        break;
      }
      case HttpInnerLog2.FRAME_TYPE_GOAWAY: {
        break;
      }
      case HttpInnerLog2.FRAME_TYPE_WINDOW_UPDATE: {
        const windowUpdate = HttpInnerLog2.createWindowUpdate(frame);
        result.ipLogs.push(windowUpdate);
        break;
      }
      case HttpInnerLog2.FRAME_TYPE_CONTINUATION: {
        break;
      }
      default: {
        ddb.warning('http2 frame dropped. type:', frame.type);
        break;
      }
    }
    return result;
  }
}


module.exports = PuppeteerInnerLog;
