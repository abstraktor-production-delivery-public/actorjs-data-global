
'use strict';

const PuppeteerConnectionWorkerMain = require('./puppeteer-connection-worker-main');
const PuppeteerSocketPair = require('./puppeteer-socket-pair');
const PuppeteerStateMachine = require('./puppeteer-state-machine');
const HttpInnerLog = require('../http/http-inner-log');
const StackApi = require('stack-api');
const Net = require('net');


class PuppeteerSocketProxy {
  constructor(id, connection, mockManager, sequrity, puppeteerLog) {
    this.id = id;
    this.connection = connection;
    this.mockManager = mockManager;
    this.sequrity = sequrity;
    this.puppeteerLog = puppeteerLog;
    this.connectionId = 0;
    this.mockServers = [];
    this.mockConnections = [];
    this.host = null;
    this.browserData = null;
    this.serverSocket = null;
    this.stateMachines = new Map();
    this.sockets = new Map();
    this.connections = new Map();
    this.mockState = PuppeteerSocketProxy.MOCK_STATE_NONE;
    this.mockStateInc = 0;
    this.mockStateQueue = [];
    this.cleanupObject = {
      done: null,
      ready: false
    };
    this.workerPool = new StackApi.WorkerPool();
//    this.gaugeIncommimgSockets = connection.createGauge('Puppeteer', `SocketProxy[${this.id}]`, 'IncommimgSockets', 0, 12);
//    this.gaugeOutgoingSockets = connection.createGauge('Puppeteer', `SocketProxy[${this.id}]`, 'OutgoingSockets', 0, 12);
//    this.gaugeConnections = connection.createGauge('Puppeteer', `SocketProxy[${this.id}]`, 'Connections', 0, 12);
  }
  
  start(host, srcAddress, done) {
    this.host = host;
    this.serverSocket = Net.createServer({allowHalfOpen: true}, (incommingSocket) => {
      const connectionId = ++this.connectionId;
      const stateMachine = new PuppeteerStateMachine(connectionId, 'proxy', this);
      const socketPair = new PuppeteerSocketPair(connectionId, 'proxy', incommingSocket, stateMachine);
      this.stateMachines.set(connectionId, stateMachine);
      stateMachine.onAction(PuppeteerStateMachine.ACTION_S_CONNECTION, connectionId, incommingSocket);
    });
    this.serverSocket.listen({
      host: this.host,
      port: 0
    }, (err) => {
      if(err) {
        this.connection.logError(err, '18f996bb-6b1e-4413-8423-87bc9442088d');
      }
      done(err, this.serverSocket.address().port);
    });
  }
  
  init(browserData, done) {
    const func = () => {
      this._init((err) => {
        done(err);
      });
    };
    this.browserData = browserData;
    let pendings = 2;
    this.connection.dnsUrlCache.resolveAddress(this.browserData.srcAddress, true, (err) => {
      if(0 === --pendings) {
        this.setMockState(func, PuppeteerSocketProxy.MOCK_STATE_LOCK);
      }
    });
    this.connection.dnsUrlCache.resolveAddress(this.browserData.dstAddress, true, (err) => {
      if(0 === --pendings) {
        this.setMockState(func, PuppeteerSocketProxy.MOCK_STATE_LOCK);
      }
    });
  }
  
  close(done) {
    const func = () => {
      this._close((err) => {
        done(err);
      });
    };
    this.setMockState(func, PuppeteerSocketProxy.MOCK_STATE_LOCK);
  }
  
  _cleanup() {
    if(0 === this.sockets.size && 0 === this.connections.size) {
      if(this.cleanupObject.done) {
        const cleanupDone = this.cleanupObject.done;
        this.cleanupObject.done = null;
        cleanupDone();
      }
      else {
        this.cleanupObject.ready = true;
      }
    }
  }
  
  _socketCleanup(incommingSocket) {
    this.sockets.delete(incommingSocket);
    this._cleanup();
  }
  
  _connectionCleanup(connectionId) {
    this.connections.delete(connectionId);
    // this.gaugeConnections.dec();
    this._cleanup();
  }
  
  _funcUnref(connectionData, incommingSocket) {
    this.detachMockServers(connectionData, (err) => {
      if(err) {
        console.log('detachMockServers, error:', err);
      }
      this.mockServersUnref(connectionData, () => {
        this.resetMockState();
        if(connectionData.connectionWorker) {
          connectionData.connectionWorker.stop(() => {
            this.stateMachines.delete(connectionData.connectionId);
          });
        }
        else {
          this._connectionCleanup(connectionData.connectionId);
        }
        this._socketCleanup(incommingSocket);
      });
    });
  }
  
  _stateMachineDone(connectionId, incommingSocket) {
    const connectionData = this.connections.get(connectionId);
    if(connectionData) {
      this.setMockState(this._funcUnref.bind(this, connectionData, incommingSocket), PuppeteerSocketProxy.MOCK_STATE_REF);  
    }
    else {
      this._socketCleanup(incommingSocket);
    }
  }
  
  stateIncommingListening() {}
  
  onStateServerConnection(connectionId, incommingSocket) {
    const stateMachine = this.stateMachines.get(connectionId);
    const socketPair = stateMachine.socketPair;//new PuppeteerSocketPair(connectionId, 'proxy', incommingSocket, stateMachine);
    stateMachine.setCbDone(this._stateMachineDone.bind(this, connectionId, incommingSocket));
    this.sockets.set(incommingSocket, socketPair);
    // this.gaugeIncommimgSockets.inc();
    incommingSocket.on('end', () => {
      stateMachine.onAction(PuppeteerStateMachine.ACTION_S_END, socketPair);
    });
    incommingSocket.on('close', () => {
      stateMachine.onAction(PuppeteerStateMachine.ACTION_S_CLOSE, socketPair, stateMachine.id);
      // this.gaugeIncommimgSockets.dec();
    });
    incommingSocket.on('error', (err) => {
    });
    const funcInitConnection = () => {
      stateMachine.onAction(PuppeteerStateMachine.ACTION_C_CONNECT, connectionId, socketPair, incommingSocket);
    };
    this.setMockState(funcInitConnection, PuppeteerSocketProxy.MOCK_STATE_REF);
  }
  
  onStateClientConnecting(connectionId, socketPair, incommingSocket) {
    this.initConnection(connectionId, socketPair, incommingSocket);
  }
    
  onStateClientConnected(socketPair) {
    // this.gaugeOutgoingSockets.inc();
    if(null !== socketPair.outgoingSocket && null !== socketPair.incommingSocket) {
      socketPair.incommingSocket.pipe(socketPair.outgoingSocket, {end: false});
      socketPair.outgoingSocket.pipe(socketPair.incommingSocket, {end: false});
    }
  }
  
  onStateClientNotConnected(socketPair) {}
  
  onStateServerEnded(socketPair) {
    if(socketPair.incommingSocket && socketPair.outgoingSocket) {
      socketPair.incommingSocket.unpipe(socketPair.outgoingSocket);
    }
  }
  
  onStateServerClosed(socketPair) {
    if(socketPair.incommingSocket && socketPair.outgoingSocket) {
      socketPair.incommingSocket.unpipe(socketPair.outgoingSocket);
    }
  }
  
  onStateClientEnded(socketPair) {
    if(socketPair.outgoingSocket && socketPair.incommingSocket) {
      socketPair.outgoingSocket.unpipe(socketPair.incommingSocket);
    }
  }
  
  onStateClientClosed(socketPair) {
    if(socketPair.outgoingSocket && socketPair.incommingSocket) {
      socketPair.outgoingSocket.unpipe(socketPair.incommingSocket);
    }
  }
    
  initConnection(connectionId, socketPair, incommingSocket) {
    const stateMachine = this.stateMachines.get(connectionId);
    stateMachine.lock();
    const connectionData = {
      connectionId: socketPair.id,
      connectionObject: null,
      connectionWorker: null,
      mockConnections: this.mockConnections,
      mockRefed: false,
      mockAttached: false
    };
    this.connections.set(socketPair.id, connectionData);
    this.mockServersRef(connectionData, (err, mockServerDatas) => {
      // this.gaugeConnections.inc();
      if(err) {
        stateMachine.unlock();
        stateMachine.onAction(PuppeteerStateMachine.ACTION_C_NOT_CONNECTED, err);
        return;
      }
      this.initConnectionHandler(socketPair, connectionData, mockServerDatas, (err, port) => { 
        if(err) {
          stateMachine.unlock();
          stateMachine.onAction(PuppeteerStateMachine.ACTION_C_NOT_CONNECTED, err);
          return;
        }
        stateMachine.unlock();
        const address = {
          host: this.host,
          port: port,
          localAddress: this.host,
          localPort: 0,
          allowHalfOpen: true
        };
        let cancelId = setTimeout((stateMachine, action, socketPair) => {
          cancelId = -1;
          stateMachine.onAction(action, socketPair, new Error('Connect Timeout.'));
        }, 3000, stateMachine, PuppeteerStateMachine.ACTION_C_NOT_CONNECTED);
        const outgoingSocket = Net.connect(address, () => {
          if(-1 !== cancelId) {
            clearTimeout(cancelId);
            cancelId = -1;
            stateMachine.onAction(PuppeteerStateMachine.ACTION_C_CONNECTED);
          }
        });
        socketPair.setOutgoingSocket(outgoingSocket);
        outgoingSocket.on('end', () => {
          stateMachine.onAction(PuppeteerStateMachine.ACTION_C_END, socketPair);
        });
        outgoingSocket.on('close', () => {
          if(-1 !== cancelId) {
            clearTimeout(cancelId);
            cancelId = -1;
          }
          stateMachine.onAction(PuppeteerStateMachine.ACTION_C_CLOSE);
        });
        outgoingSocket.on('error', (err) => {
          if(-1 !== cancelId) {
            clearTimeout(cancelId);
            cancelId = -1;
            stateMachine.onAction(PuppeteerStateMachine.ACTION_C_NOT_CONNECTED, err);
          }
        });
      });
    });
  }
  
  initConnectionHandler(socketPair, connectionData, mockServerDatas, done) {
    connectionData.connectionWorker = this.workerPool.getWorker('puppeteer-connection', PuppeteerConnectionWorkerMain, require.resolve('./puppeteer-connection-worker-thread'));
    connectionData.connectionWorker.start(connectionData.connectionId, this, (err) => {
      if(err) {
        this.resetMockState();
        done(err);
        return;
      }
      connectionData.connectionWorker.run(this.host, this.browserData, this.sequrity, (err, port) => {
        if(err || !port) {
          this.resetMockState();
          done(err);
          return;
        }
        this.attachMockServers(connectionData, mockServerDatas, (err) => {
          this.resetMockState();
          done(err, port);
        });
      });
    }, (exitCode) => {
      this._connectionCleanup(connectionData.connectionId);
    });
  }
  
  mockServersRef(connectionData, done) {
    if(0 !== this.mockServers.length && !connectionData.mockRefed) {
      this.mockManager.ref(this.mockServers, this.host, this.browserData.srcAddress, (err, mockServerDatas) => {
        if(!err) {
          connectionData.mockRefed = true;
        }
        done(err, mockServerDatas);
      });
    }
    else {
      process.nextTick(done);
    }
  }
  
  mockServersUnref(connectionData, done) {
    if(connectionData.mockRefed) {
      connectionData.mockRefed = false;
      this.mockManager.unref(done);
    }
    else {
      process.nextTick(done);
    }
  }
  
  attachMockServers(connectionData, mockServerDatas, done) {
    if(0 !== this.mockServers.length && connectionData.mockRefed && !connectionData.mockAttached) {
      connectionData.connectionWorker.attachMockServers(this.mockServers, mockServerDatas, (err) => {
        connectionData.mockAttached = true;
        const stop = process.hrtime.bigint();
        done(err);
      });
    }
    else {
      process.nextTick(done);
    }
  }
  
  detachMockServers(connectionData, done) {
    if(connectionData.mockRefed && connectionData.mockAttached) {
      connectionData.mockAttached = false;
      connectionData.connectionWorker.detachMockServers((err) => {
        done(err);
      });
    }
    else {
      process.nextTick(done);
    }
  }
  
  _init(done) {
    if(!this.connection.sharedManager.hasServer('puppeteer')) {
      return process.nextTick(() => {
        this.resetMockState();
        done();
      });
    }
    this.mockServers = this.connection.sharedManager.getServers('puppeteer');
    this.mockServers.forEach(() => {
      this.mockConnections.push({
        connectionDataLocal: null,
        connectionDataRemote: null
      });
    });
    if(0 === this.mockConnections.length || 0 === this.connections.size) {
      return process.nextTick(() => {
        this.resetMockState();
        done();
      });
    }
    let pendings = 0;
    let refs = 0;
    this.connections.forEach((connection) => {
      connection.mockConnections = this.mockConnections;
      ++pendings;
      const myRef = ++refs;
      this.mockServersRef(connection, (err, mockServerDatas) => {
        this.attachMockServers(connection, mockServerDatas, (err) => {
          if(0 === --pendings) {
            this.resetMockState();
            done();
          }
        });
      });
    }); 
  }
  
  __close(done) {
    this.resetMockState();
    done();
  }
  
  _close(done) {
    this.mockServers = [];
    this.mockConnections = [];
    if(0 !== this.connections.size) {
      let pendings = 0;
      this.connections.forEach((connectionData) => {
        ++pendings;
        this.detachMockServers(connectionData, (err) => {
          if(err) {
            console.log('detachMockServers, error:', err);
          }
          this.mockServersUnref(connectionData, () => {
            if(0 === --pendings) {
              this.__close(done);
            }
          });
        });
      });
      if(0 === pendings) {
        this.__close(done);
      }
    }
    else {
      this.__close(done);
    }
  }
  
  cleanup(done) {
    let pendings = 2;
    let cancelId = setTimeout(() => {
      this.stateMachines.forEach((stateMachine) => {
        stateMachine.kill();
      });
      setTimeout(() => {
        if(0 !== this.connections.size) {
          console.log('***************:' + this.id);
          console.log('*** SOCKETS:', this.sockets.size);
          console.log('*** CONNECTIONS', this.connections.size);
          this.connections.forEach((connectionData) => {
            ddb.warning('*****', connectionData.connectionId, 'NOT CLOSED');
          });
        }
      }, 3000);
    }, 2000);
    if(this.cleanupObject.ready) {
      this.cleanupObject.ready = false;
      if(0 === --pendings) {
        clearTimeout(cancelId);
        this.workerPool.release();
        done();
      }
    }
    else {
      this.cleanupObject.done = () => {
        if(0 === --pendings) {
          clearTimeout(cancelId);
          this.workerPool.release();
          this.cleanupObject.done = null;
          done();
        }
      };
    }
    this.serverSocket.close(() => {
      if(0 === --pendings) {
        clearTimeout(cancelId);
        this.workerPool.release();
        done();
      }
    });
  }
  
  onDnsGet(done, domain, family) {
    this.connection.dnsUrlCache.get(domain, family, (err, address) => {
      done(err, address);
    });
  }
  
  onLogConnecting(connectionId, mockId, srcAddress, dstAddress, transportType) {
    let connectionData = this.connections.get(connectionId);
    if(-1 !== mockId) {
      connectionData = connectionData.mockConnections[mockId];
    }
    if(connectionData) {
      connectionData.connectionObject = this.connection.registerClientConnectionData(connectionId, srcAddress, dstAddress, transportType);
      this.puppeteerLog.logConnecting(connectionData.connectionObject);
    }
    else {
      ddb.warning('dropped logConnecting');
    }
  }
  
  onLogConnected(connectionId, mockId, err, srcAddress, dstAddress) {
    let connectionData = this.connections.get(connectionId);
    if(-1 !== mockId) {
      connectionData = connectionData.mockConnections[mockId];
    }
    if(connectionData) {
      if(!err) {
        this.puppeteerLog.logConnected(connectionData.connectionObject);
      }
      else {
        this.puppeteerLog.logNotConnected(connectionData.connectionObject, err);
      }
    }
    else {
      ddb.warning('dropped logConnected');
    }
  }
  
  onLogDnsFailure(connectionId, mockId, srcAddress, dstUrl, err) {
    let connectionData = this.connections.get(connectionId);
    if(-1 !== mockId) {
      connectionData = connectionData.mockConnections[mockId];
    }
    if(connectionData) {
      //this.connection.updateConnectionData(connectionData.connectionDataLocal, srcAddress);
      this.puppeteerLog.logDnsFailure(connectionData.connectionObject, dstUrl, err);
    }
    else {
      ddb.warning('dropped logConnected');
    }
  }
  
  onLogUpgrading(connectionId, mockId) {
    let connectionData = this.connections.get(connectionId);
    if(-1 !== mockId) {
      connectionData = connectionData.mockConnections[mockId];
    }
    if(connectionData) {
      this.puppeteerLog.logUpgrading(connectionData.connectionObject);
    }
    else {
      ddb.warning('dropped logUpgraded');
    }
  }
  
  onLogUpgraded(connectionId, mockId, err) {
    let connectionData = this.connections.get(connectionId);
    if(-1 !== mockId) {
      connectionData = connectionData.mockConnections[mockId];
    }
    if(connectionData) {
      if(!err) {
        this.puppeteerLog.logUpgraded(connectionData.connectionObject);
      }
      else {
        this.puppeteerLog.logNotUpgraded(connectionData.connectionObject, err);
      }
    }
    else {
      ddb.warning('dropped logUpgraded');
    }
  }
  
  onLogClosing(connectionId, mockId, isActor) {
    let connectionData = this.connections.get(connectionId);
    if(-1 !== mockId) {
      connectionData = connectionData.mockConnections[mockId];
    }
    if(connectionData) {
      this.puppeteerLog.logClosing(connectionData.connectionObject, isActor);
    }
    else {
      ddb.warning('dropped logClosing');
    }
  }
  
  onLogClosed(connectionId, mockId, isActor) {
    let connectionData = this.connections.get(connectionId);
    if(-1 !== mockId) {
      connectionData = connectionData.mockConnections[mockId];
    }
    if(connectionData) {
      this.puppeteerLog.logClosed(connectionData.connectionObject, isActor);
    }
    else {
      ddb.warning('dropped logClosed');
    }
  }
  
  onLogMessageSend(connectionId, mockId, httpMessage) {
    let connectionData = this.connections.get(connectionId);
    if(-1 !== mockId) {
      connectionData = connectionData.mockConnections[mockId];
    }
    if(connectionData) {
      this.puppeteerLog.logMessageSend(httpMessage, connectionData.connectionObject);
    }
    else {
      ddb.warning('dropped logMessageSend');
    }
  }
  
  onLogMessageBodySend(connectionId, mockId, contentType, contentEncoding, transferEncoding, rawBuffer) {
    let connectionData = this.connections.get(connectionId);
    if(-1 !== mockId) {
      connectionData = connectionData.mockConnections[mockId];
    }
    if(connectionData) {
      this.puppeteerLog.logMessageBodySend(connectionData.connectionObject, contentType, contentEncoding, transferEncoding, rawBuffer);
    }
    else {
      ddb.warning('dropped logMessageBodySend');
    }
  }
  
  onLogMessageReceive(connectionId, mockId, httpMessage) {
    let connectionData = this.connections.get(connectionId);
    if(-1 !== mockId) {
      connectionData = connectionData.mockConnections[mockId];
    }
    if(connectionData) {
      this.puppeteerLog.logMessageReceive(httpMessage, connectionData.connectionObject);
    }
    else {
      ddb.warning('dropped logMessageReceive');
    }
  }
  
  onLogMessageBodyReceive(connectionId, mockId, contentType, contentEncoding, transferEncoding, rawBuffer) {
    let connectionData = this.connections.get(connectionId);
    if(-1 !== mockId) {
      connectionData = connectionData.mockConnections[mockId];
    }
    if(connectionData) {
      this.puppeteerLog.logMessageBodyReceive(connectionData.connectionObject, contentType, contentEncoding, transferEncoding, rawBuffer);
    }
    else {
      ddb.warning('dropped logMessageBodyReceive');
    }
  }
  
  onLogMessageSendWebSocket(connectionId, mockId, websocketMessage) {
    let connectionData = this.connections.get(connectionId);
    if(-1 !== mockId) {
      connectionData = connectionData.mockConnections[mockId];
    }
    if(connectionData) {
      this.puppeteerLog.logMessageSendWebSocket(websocketMessage, connectionData.connectionObject);
    }
    else {
      ddb.warning('dropped logMessageSendWebSocket');
    }
  }
  
  onLogMessageReceiveWebSocket(connectionId, mockId, websocketMessage) {
    let connectionData = this.connections.get(connectionId);
    if(-1 !== mockId) {
      connectionData = connectionData.mockConnections[mockId];
    }
    if(connectionData) {
      this.puppeteerLog.logMessageReceiveWebSocket(websocketMessage, connectionData.connectionObject);
    }
    else {
      ddb.warning('dropped logMessageReceiveWebSocket');
    }
  }
  
  onLogFrameSend(connectionId, mockId, frame, dataBuffers) {
    let connectionData = this.connections.get(connectionId);
    if(-1 !== mockId) {
      connectionData = connectionData.mockConnections[mockId];
    }
    if(connectionData) {
      this.puppeteerLog.logFrameSend(connectionData.connectionObject, frame, dataBuffers);
    }
    else {
      ddb.warning('dropped logFrameSend');
    }
  }
  
  onLogFrameReceive(connectionId, mockId, frame, dataBuffers) {
    let connectionData = this.connections.get(connectionId);
    if(-1 !== mockId) {
      connectionData = connectionData.mockConnections[mockId];
    }
    if(connectionData) {
      this.puppeteerLog.logFrameReceive(connectionData.connectionObject, frame, dataBuffers);
    }
    else {
      ddb.warning('dropped logFrameReceive');
    }
  }
  
  onMockSelected() {
    this.connection.mockSelected();
  }
  
  handleQueue() {
    const nextStates = [];
    const nextState = this.mockStateQueue[0].state;
    while(0 !== this.mockStateQueue.length && nextState === this.mockStateQueue[0].state) {
      nextStates.push(this.mockStateQueue.shift());
    }
    for(let i = 0; i < nextStates.length; ++i) {
      const nextState = nextStates[i];
      this.setMockState(nextState.func, nextState.state);
    }
  }
  
  setMockState(func, state) {
    const currentState = this.mockState;
    if(PuppeteerSocketProxy.MOCK_STATE_NONE === this.mockState) {
      this.updateMockState(state);
      if(PuppeteerSocketProxy.MOCK_STATE_REF === state) {
        ++this.mockStateInc;
      }
      func();
    }
    else if(PuppeteerSocketProxy.MOCK_STATE_REF === this.mockState) {
      if(PuppeteerSocketProxy.MOCK_STATE_REF === state) {
        ++this.mockStateInc;
        func();
      }
      else if(PuppeteerSocketProxy.MOCK_STATE_LOCK === state) {
        this.mockStateQueue.push({
          state,
          func
        });
      }
    }
    else if(PuppeteerSocketProxy.MOCK_STATE_LOCK === this.mockState) {
      if(PuppeteerSocketProxy.MOCK_STATE_REF === state) {
        this.mockStateQueue.push({
          state,
          func
        });
      }
      else {
        ddb.warning('setMockState, ERROR - state:', PuppeteerSocketProxy.MOCK_STATE[currentState], '=>', PuppeteerSocketProxy.MOCK_STATE[this.mockState], (PuppeteerSocketProxy.MOCK_STATE_REF === this.mockState) ? '' + this.mockStateInc : '');
      }
    }
  }
  
  resetMockState() {
    const currentState = this.mockState;
    if(PuppeteerSocketProxy.MOCK_STATE_REF === this.mockState) {
      if(0 === --this.mockStateInc) {
        this.updateMockState(PuppeteerSocketProxy.MOCK_STATE_NONE);
        if(0 !== this.mockStateQueue.length) {
          this.handleQueue();
        }
      }
    }
    else if(PuppeteerSocketProxy.MOCK_STATE_LOCK === this.mockState) {
      this.updateMockState(PuppeteerSocketProxy.MOCK_STATE_NONE);
      if(0 !== this.mockStateQueue.length) {
        this.handleQueue();
      }
    }
    else {
      ddb.warning('resetMockState, ERROR - state:', PuppeteerSocketProxy.MOCK_STATE[currentState], '=>', PuppeteerSocketProxy.MOCK_STATE[this.mockState], (PuppeteerSocketProxy.MOCK_STATE_REF === this.mockState) ? '' + this.mockStateInc : '', new Error(''));
    }
  }
  
  updateMockState(state) {
    const previousMockState = this.mockState;
    this.mockState = state;
  }
}


PuppeteerSocketProxy.MOCK_STATE_NONE = 0;
PuppeteerSocketProxy.MOCK_STATE_REF = 1;
PuppeteerSocketProxy.MOCK_STATE_LOCK = 2;
PuppeteerSocketProxy.MOCK_STATE = [
  'NONE',
  'REF',
  'LOCK'
];

PuppeteerSocketProxy.DISCONNECTED = 0;


module.exports = PuppeteerSocketProxy;
