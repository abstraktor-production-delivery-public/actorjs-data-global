
'use strict';

const StackApi = require('stack-api');
const PuppeteerPage = require('./templates-actors/puppeteer-page');
const PuppeteerPageActorPartPre = require('./templates-actors/puppeteer-page-actor-part-pre');
const PuppeteerPageActorPartPost = require('./templates-actors/puppeteer-page-actor-part-post');


class PuppeteerTemplatesActors extends StackApi.StackComponentsTemplateBaseActors {
  constructor() {
    super([
      PuppeteerPage,
      PuppeteerPageActorPartPre,
      PuppeteerPageActorPartPost
    ]);
  }
}


module.exports = PuppeteerTemplatesActors;
