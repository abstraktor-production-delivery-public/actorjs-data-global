# **Puppeteer**
[[API-STATUS=STABLE, ]]
[[DOC-STATUS=PARTLY, ]]

[[NOTE={"guid":"658a98f3-c209-4d75-b0c4-f9c598f1d406"}]]
[[ANCHOR={"id":"a9de84b1-d4f7-4bb1-aed7-24070e3430b4","visible":true}]]
## **Description**
The Puppeteer stack enables the [Puppeteer](https://pptr.dev/#?product=Puppeteer&version=v9.1.1&show=api-class-page) functionality and adds:
* **Debugging**
* **Logs**
* **Sequence Diagrams**
* **Structure**
  * **Actors**
  * **Test Cases**
  * **Test Suites**
* **Efficiency**
Combining the different reuse options can increase performance by several hundred percent!
  * **Reuse of Puppeteer Browsers**
  * **Reuse of Puppeteer BrowserContexts**
  * **Reuse of connections**
* **Interception**
Chosen HTTP requests and Websocket Frames can be intercepted.


[[NOTE={"guid":"63fc215c-df59-44af-80da-91559cfe478e"}]]
[[ANCHOR={"id":"77a704c3-254e-4a28-92c9-88772b6b3c30","visible":true}]]
### **Connection Options**

[[NOTE={"guid":"77c41eef-5cf9-41ab-a7d1-c60d8b40f3f0"}]]
[[ANCHOR={"id":"7c041897-ff08-41ad-b73a-ce3c0a26aeb8","visible":true}]]
```table
Config(classHeading: )

|Parameters                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             |
|Name                             |Type                |Default|Description                                                                                                                                                                                                                                                                                                                                                                                                                                             |
|puppeteer-reuse                  |[[REF=,MDN_boolean]]|false  |If set to ***true***, and there is an existing stored connection with the same properties, reuse the stored connection. This can significantly speed up execution of [[REF=, ABS_Test_Suite]]s.<br/>This also means that the connection will not be destroyed when the connection is closed. Instead, the connection is stored for reuse in the current [[REF=, ABS_Test_Case]] or other [[REF=, ABS_Test_Case]]s running in a [[REF=, ABS_Test_Suite]].|
|puppeteer-use-proxy              |[[REF=,MDN_boolean]]|true   |The built-in proxy enables logs and sequence diagrams.                                                                                                                                                                                                                                                                                                                                                                                                  |
|puppeteer-headless               |[[REF=,MDN_boolean]]|true   |Run Puppeteer headless, which means running Puppeteer without any visible browser. See [puppeteer.launch([options])](https://pptr.dev/#?product=Puppeteer&version=v9.1.0&show=api-puppeteerlaunchoptions).                                                                                                                                                                                                                                              |
|puppeteer-slow-mo                |[[REF=,MDN_boolean]]|false  |Slows down Puppeteer to allow visualization of what is happening. See [puppeteer.launch([options])](https://pptr.dev/#?product=Puppeteer&version=v9.1.0&show=api-puppeteerlaunchoptions).                                                                                                                                                                                                                                                               |
|puppeteer-navigation-optimization|[[REF=,MDN_boolean]]|false  |If **puppeteer-reuse** and **puppeteer-navigation-optimization** are set to ***true***, then ***Puppeteer.goto*** is replaced with client-side navigation. The connection will call ***history.pushState***, and then ***window.abstractorRerender***, if ***window.abstractorRerender*** is implemented. If needed, you can implement ***window.abstractorRerender*** to enable client-side navigation.                                                |
|puppeteer-test-optimization      |[[REF=,MDN_boolean]]|false  |Time-consuming tasks on your site can significantly slow down testing. If **puppeteer-test-optimization** is set to ***true***, the connection will add a member to the window object. The member specifies the site to optimize for Test. The member name is ***abstractorTestOptimization***.                                                                                                                                                         |
```

[[NOTE={"guid":"1573ba34-cf71-41f2-b855-c8fcad6ac0bb"}]]
[[ANCHOR={"id":"2d184cfe-39e3-44c4-b9be-ffcc3e9196b7","visible":true}]]
#### **When 'puppeteer-use-proxy' is set to *true***
The built-in proxy enables logs and sequence diagrams. To obtain the performance required to log everything, we need workers. There will be one worker for every connection to the web server.

[[NOTE={"guid":"8651585d-c4f0-45ff-9675-ef51f8220878"}]]
[[ANCHOR={"id":"e2547e53-719a-4f38-aabf-e8816e2e4835","visible":true}]]
```node
Config(nodeWidth: 70, nodeHeight: 70, nodeWidthBetween: 25, nodeHeightBetween: 25, widthBias: 30, heightBias: 30)

Nodes[, , , , , , Worker-1]
Nodes[Actor, Puppeteer, Chromium, , Proxy, , Worker-2, , web-server]
Nodes[, , , , , , Worker-3]
Nodes[, , , , , , Worker-n]


Actor => Puppeteer[]: api
Puppeteer => Chromium[]: ipc
Chromium => Proxy[http]: http
Chromium => Proxy[http]: http
Chromium => Proxy[websocket]: websocket
Chromium => Proxy[websocket]: websocket
Proxy => Worker-1[http]: http
Proxy => Worker-2[http]: http
Proxy => Worker-3[websocket]: websocket
Proxy => Worker-n[websocket]: websocket
Worker-1 => web-server[http]: http
Worker-2 => web-server[http]: http
Worker-3 => web-server[websocket]: websocket
Worker-n => web-server[websocket]: websocket
```

[[NOTE={"guid":"0cc61c11-50e4-4820-996b-4723bf8dfda9"}]]
## **Using Puppeteer**
A simple example using [Puppeteer](https://pptr.dev/#?product=Puppeteer&version=v9.1.1&show=api-class-page) would look like this:

```javascript

const puppeteer = require('puppeteer');

(async () => {
  const browser = await puppeteer.launch();
  const page = await browser.newPage();
  await page.goto('https://example.com');
  await page.screenshot({path: 'screenshot.png'});
  await browser.close();
})();

```

Doing the same in ActorJs would look like this:

```javascript

*initClient() {
  this.puppeteerConnection = this.createConnection('puppeteer');  
}

*run() {
  const page = this.puppeteerConnection.page;
  yield page.screenshot({path: 'screenshot.png'});
}

```

In the **this.createConnection('puppeteer');** method, the following happens: 
* [Puppeteer](https://pptr.dev/#?product=Puppeteer&version=v9.1.1&show=api-class-page) is launched.
* The browser and page are created.
* If needed, the **page.goto** is executed with help of [[REF=, ABS_Addressing]].


References in the [Puppeteer](https://pptr.dev/#?product=Puppeteer&version=v9.1.1&show=api-class-page) documentation to **await** should be replaced with **yield**. This will ensure that most of the functionality works in the same way.

[[NOTE={"guid":"8fa30348-0613-488a-aaf3-1ada0a3597e8"}]]
[[ANCHOR={"id":"98799a2d-533a-4f7c-8691-d7a0d24fac1c","visible":true}]]
#### **Test Cases using PuppeteerConnectionClient**
+ [ActorsModalFileAdd](/../test-cases/ActorJs/Actors/ActorsModalFileAdd)
+ [AddressesDstLocateSut](/../test-cases/ActorJs/Addresses/AddressesDstLocateSut)
