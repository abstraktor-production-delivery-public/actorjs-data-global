
'use strict';


const PuppeteerConnectionHandlerBinder = require('./puppeteer-connection-handler-binder');
const PuppeteerConnectionMock = require('./puppeteer-connection-mock');
const PuppeteerConnectionMocks = require('./puppeteer-connection-mocks');
const PuppeteerConnectionSocketHandler = require('./puppeteer-connection-socket-handler');
const PuppeteerConnectionSocket = require('./puppeteer-connection-socket');
const PuppeteerContentCache = require('./puppeteer-content-cache');
const PuppeteerParserHttp11 = require('./puppeteer-parser-http11');
const PuppeteerParserTls = require('./puppeteer-parser-tls');
const PuppeteerSocketPair = require('./puppeteer-socket-pair');
const PuppeteerStateMachine = require('./puppeteer-state-machine');
const StackApi = require('stack-api');
const HttpApi = require('http-stack-api');
const WebsocketApi = require('websocket-stack-api');
const Net = require('net');
const Tls = require('tls');


class PuppeteerConnectionHandler {
  constructor(id, cbMethod, cbMessage) {
    this.id = id;
    this.mocks = new PuppeteerConnectionMocks();
    this.replaceHeaderNames = new Map([['Proxy-Connection', 'Connection']]); // TODO: REMOVE
    this.serverSocket = null;
    this.socketPair = null;
    this.socketHandler = new PuppeteerConnectionSocketHandler(this.id, this, this.mocks);
    this.cbMethod = cbMethod;
    this.cbMessage = cbMessage;
    this.srcAddress = null;
    this.browserData = null;
    this.sequrity = null;
    this.host = '';
    this.connectDone = null;
    this.connectSrcAddress = null;
    this.connectDstAddress = null;
    this.beenConnected = false;
    this.puppeteerParserTls = new PuppeteerParserTls(id);
    this.contentCache = new PuppeteerContentCache();
    this.stateMachine = new PuppeteerStateMachine(this.id, 'handler', this);
    this.binder = new PuppeteerConnectionHandlerBinder(this);
  }
  
  run(host, browserData, sequrity, done) {
    this.host = host;
    this.srcAddress = StackApi.Address.from(browserData.srcAddress);
    this.browserData = browserData;
    this.socketHandler.setBrowserData(browserData);
    this.sequrity = sequrity;
    this.serverSocket = Net.createServer({allowHalfOpen: true}, (incommingSocket) => {
      this.socketPair = new PuppeteerSocketPair(this.id, 'handler', null, this.stateMachine);
      const incomingSut = new PuppeteerConnectionSocket(this.id, 'INCOMING-handler', 'TCP', new PuppeteerParserHttp11(this.id, PuppeteerParserHttp11.REQUEST, PuppeteerConnectionSocketHandler.replaceHeaderNames), PuppeteerConnectionSocketHandler.STATUS_HEADERS, PuppeteerConnectionSocketHandler.SOURCE_INCOMMING);
      incomingSut.setSocket(incommingSocket);
      this.socketPair.setIncommingSocket(incomingSut);
      incommingSocket.on('data', this.binder.inTcpData);
      incommingSocket.on('end', this.binder.inTcpEnd);
      incommingSocket.on('close', this.binder.inTcpClose);
      incommingSocket.on('error', this.binder.inTcpError);
      this.stateMachine.onAction(PuppeteerStateMachine.ACTION_S_CONNECTION, incommingSocket);
    });
    this.serverSocket.listen({
      host: this.host,
      port: 0
    }, (err) => {
      if(err) {
        done(err);
      }
      else {
        done(null, this.serverSocket.address().port);
      }
    });
  }
  
  _end(done) {
    if(this.serverSocket) {
      const serverSocket = this.serverSocket;
      this.serverSocket = null;
      let isDone = false;
      const cancel = setTimeout(() => {
        isDone = true;
        done();
      }, 1000);
      serverSocket.close(() => {
        if(!isDone) {
          clearTimeout(cancel);
          done();
        }
      });
    }
    else {
      process.nextTick((done) => {
        done();
      }, done);
    }
  }
  
  end(done) {
    if(this.stateMachine.isClosed() || this.stateMachine.isNone()) {
      this._end(done);
    }
    else {
      const cancel = setTimeout(() => {
        this.stateMachine.kill();
      }, 1000);
      this.stateMachine.setCbDone(() => {
        clearTimeout(cancel);
        this._end(done);
      });
    }
  }
      
  onStateServerConnection(incommingSocket) {}
  
  onStateClientConnecting(dstIpAddress, dstIpPort, protocol, done) {
    const srcAddress = this.srcAddress.clone();
    const address = StackApi.Address.netAddress(srcAddress, dstIpAddress, dstIpPort, protocol);
    const dstAddressLog = new StackApi.Address(`puppeteerDst_${this.id}`, address.host, srcAddress.family, address.port, 'sut');
    dstAddressLog.setLogName('Puppeteer-dst');
    const transportType = 'https' === protocol ? StackApi.NetworkType.TLS : StackApi.NetworkType.TCP;
    if(StackApi.NetworkType.TLS === transportType) {
      this.binder.setIcommingTls();
    }
    this.logConnecting(srcAddress, dstAddressLog, transportType);
    this.connectSrcAddress = srcAddress;
    this.connectDstcAddress = dstAddressLog;
    this.connectDone = done;
    const outgoingSutSocket = Net.connect(address, () => {
      if(this.stateMachine.cancelOut) {
        this.stateMachine.clearTimeoutOut();
        this.stateMachine.onAction(PuppeteerStateMachine.ACTION_C_CONNECTED);
      }
      else {
        outgoingSutSocket.destroy();
      }
    });
    this.stateMachine.setTimeoutOut((action) => {
      this.stateMachine.onAction(action, new Error('Connect Timeout.'));
    }, 3000, PuppeteerStateMachine.ACTION_C_NOT_CONNECTED);
    const outgoingSut = new PuppeteerConnectionSocket(this.id, 'OUTGOING-handler', 'TCP', new PuppeteerParserHttp11(this.id, PuppeteerParserHttp11.RESPONSE), PuppeteerConnectionSocketHandler.STATUS_HEADERS, PuppeteerConnectionSocketHandler.SOURCE_SUT);
    outgoingSut.setSocket(outgoingSutSocket);
    this.socketPair.setOutgoingSocket(outgoingSut);
    outgoingSutSocket.on('end', this.binder.outTcpEnd);
    outgoingSutSocket.on('close', this.binder.outTcpClose);
    outgoingSutSocket.on('error', this.binder.outTcpError);
  }
      
  onStateClientConnected(socketPair) {
    socketPair.outgoingSocket.on('data', this.binder.outTcpData);
    this.connectSrcAddress.port = socketPair.outgoingSocket.localPort;
    if(!this.connectSrcAddress.host) {
      this.connectSrcAddress.host = socketPair.outgoingSocket.localAddress;
    }
    this.logConnected(null, this.connectSrcAddress, this.connectDstcAddress);
    this.beenConnected = true;
    if(this.connectDone) {
      const connectDone = this.connectDone;
      this.connectDone = null;
      connectDone();
    }
  }
  
  onStateClientDnsFailure(socketPair, srcAddress, dstUrl, err) {
    this.logDnsFailure(srcAddress, dstUrl, err);
  }
  
  onStateClientNotConnected(socketPair, err) {
    this.logConnected(err, this.connectSrcAddress, this.connectDstcAddress);
    if(this.connectDone) {
      const connectDone = this.connectDone;
      this.connectDone = null;
      connectDone();
    }
  }
  
  onStateClientUpgrade(chunk) {
    this.logUpgrading();
    const msg = this.puppeteerParserTls.parse(chunk);
    const serverName = PuppeteerParserTls.getExtention(PuppeteerParserTls.EXTENTION_server_name, msg);
    const aLPNProtocols = PuppeteerParserTls.getExtention(PuppeteerParserTls.EXTENTION_application_layer_protocol_negotiation, msg);
    const previousoutgoingSocket = this.socketPair.outgoingSocket;
    previousoutgoingSocket.off('data', this.binder.outTcpData);
    previousoutgoingSocket.off('end', this.binder.outTcpEnd);
    previousoutgoingSocket.off('close', this.binder.outTcpClose);
    previousoutgoingSocket.off('error', this.binder.outTcpError);
    const outgoingSutSocket = Tls.connect({
      socket: previousoutgoingSocket.socket,
      allowHalfOpen: true,
      servername: serverName,
      ALPNProtocols: aLPNProtocols
    });
    this.stateMachine.setTimeoutOut((action, socketPair) => {
      this.stateMachine.onAction(action, socketPair, new Error('Timeout: upgrading tcp to tls.'));
    }, 3000, PuppeteerStateMachine.ACTION_C_NOT_UPGRADED, this.socketPair);
    outgoingSutSocket.on('secureConnect', () => {
      if(this.stateMachine.cancelOut) {
        this.stateMachine.clearTimeoutOut();
        if(outgoingSutSocket.authorized) {
          this.stateMachine.onAction(PuppeteerStateMachine.ACTION_C_UPGRADED, outgoingSutSocket, chunk);
        }
        else {
          this.stateMachine._log('CLIENT destroy after SECURE_CONNECT !authorized');
          outgoingSutSocket.destroy();
          this.stateMachine.onAction(PuppeteerStateMachine.ACTION_C_NOT_UPGRADED, this.socketPair, outgoingSutSocket.authorizationError);
        }
      }
      else {
        this.stateMachine._log('CLIENT destroy after SECURE_CONNECT');
        outgoingSutSocket.destroy();
      }
    });
    outgoingSutSocket.on('end', this.binder.outTlsEnd);
    outgoingSutSocket.on('close', this.binder.outTlsClose);
    outgoingSutSocket.on('error', this.binder.outTlsError);
    this.socketPair.setOutgoingSocket(outgoingSutSocket);
  }
  
  onStateClientUpgraded(outgoingSutSocket, chunk) {
    this.socketHandler.onClientUpgraded(outgoingSutSocket, outgoingSutSocket.alpnProtocol)
    outgoingSutSocket.on('data', this.binder.outTlsData);
    const previousIncommingSocket = this.socketPair.incommingSocket;
    previousIncommingSocket.off('end', this.binder.inTcpEnd);
    previousIncommingSocket.off('close', this.binder.inTcpClose);
    previousIncommingSocket.off('error', this.binder.inTcpError);
    const incommingSocket = new Tls.TLSSocket(previousIncommingSocket.socket, {
      allowHalfOpen: true,
      cert: this.sequrity.cert,
      key: this.sequrity.key,
      isServer: true,
      server: this.serverSocket,
      ALPNProtocols: outgoingSutSocket.alpnProtocol ? [outgoingSutSocket.alpnProtocol] : undefined
    });
    incommingSocket.on('error', this.binder.inTlsError);
    incommingSocket.on('end', this.binder.inTlsEnd);
    incommingSocket.on('close', this.binder.inTlsClose);
    this.stateMachine.setTimeoutIn((action, socketPair) => {
      this.stateMachine.onAction(action, socketPair, new Error('Timeout: upgrading tcp to tls.'));
    }, 3000, PuppeteerStateMachine.ACTION_S_NOT_UPGRADED, this.socketPair);
    incommingSocket.on('secure', (a, b) => {
      if(this.stateMachine.cancelIn) {
        this.stateMachine.clearTimeoutIn();
        this.stateMachine.onAction(PuppeteerStateMachine.ACTION_S_UPGRADED, incommingSocket, previousIncommingSocket, outgoingSutSocket, outgoingSutSocket.alpnProtocol);
      }
      else {
        this.stateMachine._log('SERVER destroy after SECURE');
        incommingSocket.destroy();
      }
    });
    this.socketPair.setIncommingSocket(incommingSocket);
    const ended = previousIncommingSocket.socket._readableState.ended;
    if(ended) {
      previousIncommingSocket.socket._readableState.ended = false;
      previousIncommingSocket.socket.push(chunk);
      previousIncommingSocket.socket._readableState.ended = ended;
    }
    else {
      previousIncommingSocket.socket.push(chunk);
    }
  }
  
  onStateClientNotUpgraded(socketPair, err) {
    this.logUpgraded(err);
  }
  
  onStateServerUpgraded(incommingSocket, previousIncommingSocket, outgoingSutSocket, alpn) {
    this.logUpgraded();
    incommingSocket.on('data', this.binder.inTlsData);
    this.socketHandler.onServerUpgraded(incommingSocket, alpn);
  }
  
  onStateServerNotUpgraded(socketPair, err) {
    this.logUpgraded(err);
  }
  
  onStateServerEnded(socketPair) {
    if(this.beenConnected) {
      this.logClosing(true);
    }
  }
  
  onStateServerClosed(socketPair) {
    this.logClosed(true);
  }
  
  onStateClientEnded(socketPair) {
    if(this.beenConnected) {
      this.logClosing(false);
    }
  }
  
  onStateClientClosed(socketPair) {
    if(this.beenConnected) {
      this.logClosed(false);
    }
  }
  
  attachMockServers(mockServers, mockServerDatas, done) {
    if(0 !== mockServers.length) {
      const evalMockServers = [];
      for(let i = 0; i < mockServers.length; ++i) {
        const mockServer = mockServers[i];
        const mockServerData = mockServerDatas[i];
        const start = mockServer.onSelectFunction.indexOf('{');
        const stop = mockServer.onSelectFunction.lastIndexOf('}');
        const messageSelector = {};
        Object.assign(messageSelector, JSON.parse(mockServer.members));
        messageSelector.onSelect = new Function('msg', 'HttpApi', 'WebsocketApi', mockServer.onSelectFunction.substring(start + 1, stop).trim());
        evalMockServers.push({
          err: mockServer.err,
          dstAddress: mockServer.dstAddress,
          host: mockServerData.host,
          port: mockServerData.port,
          mcPort: mockServerData.mcPort,
          messageSelector: messageSelector,
          connection: null,
          outgoing: null,
          connectionId: -1,
          mockId: i
        });
      }
      this.mocks.setMocks(evalMockServers);
    }
    process.nextTick(done);
  }
  
  detachMockServers(done) {
    let pendings = this.mocks.servers.length;
    this.mocks.servers.forEach((server) => {
      if(server.connectionId === this.id) {
        if(server.connection) {
          server.connection.close(() => {
            server.connection = null;
            server.outgoing = null;
            server.connectionId = -1;
            if(0 === --pendings) {
              this.mocks.clear();
              process.nextTick(done);
            }
          });
        }
        else if(0 === --pendings) {
          this.mocks.clear();
          process.nextTick(done);
        }
      }
      else if(0 === --pendings) {
        this.mocks.clear();
        process.nextTick(done);
      }
    });
  }
  
  resolveUrl(requestTarget, family, isConnect, done) {
    let domain = '';
    let protocol = 'http:';
    let port = 80;
    if(isConnect) {
      const delimiterIndex = requestTarget.indexOf(':');
      if(-1 === delimiterIndex) {
        domain = requestTarget;
      }
      else {
        domain = requestTarget.substring(0, delimiterIndex);
        port = Number.parseInt(requestTarget.substring(delimiterIndex + 1));
      }
    }
    else {
      const url = new URL(requestTarget);
      protocol = url.protocol;
      domain = url.hostname;
      if(url.port) {
        port = Number.parseInt(url.port);
      }
      else if('https:' === url.protocol) {
        port = 443;
      }
    }
    this.cbMethod((err, address) => {
      done(err, address, port);
    }, 'DnsGet', domain, family);
  }
  
  connectToMock(mockId, done) {
    const mockServer = this.mocks.servers[mockId]; // TODO: use the real address names.
    if(-1 !== mockServer.connectionId) {
      process.nextTick((done) => {
        done(new Error('Mock already exists.'));
      }, done);
      return;
    }
    const srcAddressLog = new StackApi.Address(`puppeteerSrc_${this.id}`, this.host, mockServer.dstAddress.family, 0, 'client');
    const dstAddressLog = mockServer.dstAddress;
    mockServer.connectionId = this.id;
    this.logConnecting(srcAddressLog, dstAddressLog, StackApi.NetworkType.TCP, mockServer.mockId);
    mockServer.mcPort.once('message', (message) => {
      if(message.err) {
        return done(message.err);
      }
      srcAddressLog.port = message.port;
      const connectionMock = new PuppeteerConnectionMock(this);
      connectionMock.connect(mockServer, message.host, message.port, srcAddressLog, dstAddressLog, (err, outgoingMockSocket) => {
        if(!err) {
          mockServer.connection = connectionMock;
          mockServer.outgoing = new PuppeteerConnectionSocket(this.id, `MOCK[${mockId}]`, 'TCP', new PuppeteerParserHttp11(this.id, PuppeteerParserHttp11.RESPONSE), PuppeteerConnectionSocketHandler.STATUS_HEADERS, mockId);
          mockServer.outgoing.setSocket(outgoingMockSocket);
          outgoingMockSocket.on('data', (chunk) => {
            this.socketHandler.onOutgoingData(chunk, mockServer.mockId);
          });
        }
        done(err);
      });
    });
    mockServer.mcPort.postMessage({cmd: 'connect'});
  }
  
  select(type, parser) {
    if('http-header' === type) {
      for(let i = 0; i < this.mocks.servers.length; ++i) { // TODO: check that it is not chosen!!!
        if((-1 === this.mocks.servers[i].connectionId || this.id === this.mocks.servers[i].connectionId)) {
          if(this.mocks.servers[i].messageSelector.onSelect(parser.message, HttpApi, WebsocketApi)) {
            this.sendMessage('MockSelected', []);
            return i;
          }
        }
      }
      return -1;
    }
    else if('websocket' === type) {
      return -1; // NOT IMPLEMENTED YET.
    }
    else {
      return -1;
    }
  }
  
  isConnected(mockId) {
    const outgoingSocket = -1 === mockId ? this.socketPair.outgoingSocket : this.mocks.servers[mockId].outgoing;
    return outgoingSocket && outgoingSocket.valid;
  }
  
  connect(dstIpAddress, dstIpPort, protocol, done) {
    this.stateMachine.onAction(PuppeteerStateMachine.ACTION_C_CONNECT, dstIpAddress, dstIpPort, protocol, done);
  }
  
  dnsFailure(requestTarget, err) {
    const srcAddress = this.srcAddress.clone();
    this.stateMachine.onAction(PuppeteerStateMachine.ACTION_C_DNS_FAILURE, srcAddress, requestTarget, err);
  }
  
  logConnecting(srcAddress, dstAddress, transportType, mockId = -1) {
    if(-1 === mockId) {
      this.sendMessage('LogConnecting', [], srcAddress, dstAddress, transportType);
    }
    else {
      this.sendMockMessage('LogConnecting', mockId, [], srcAddress, dstAddress, transportType);
    }
  }
  
  logDnsFailure(srcAddress, url, err, mockId = -1) {
    if(-1 === mockId) {
      this.sendMessage('LogDnsFailure', [], srcAddress, url, err);
    }
    else {
      this.sendMockMessage('LogDnsFailure', mockId, [], srcAddress, url, err);
    }
  }
  
  logConnected(err, srcAddress, dstAddress, mockId = -1) {
    if(-1 === mockId) {
      this.sendMessage('LogConnected', [], err, srcAddress, dstAddress);  
    }
    else {
      this.sendMockMessage('LogConnected', mockId, [], err, srcAddress, dstAddress);  
    }
  }
  
  logUpgrading(mockId = -1) {
    if(-1 === mockId) {
      this.sendMessage('LogUpgrading', []);
    }
    else {
      this.sendMockMessage('LogUpgrading', mockId, []);
    }
  }
  
  logUpgraded(err, mockId = -1) {
    if(-1 === mockId) {
      this.sendMessage('LogUpgraded', [], err);  
    }
    else {
      this.sendMockMessage('LogUpgraded', mockId, [], err);  
    }
  }
  
  logClosing(isActor, mockId = -1) {
    if(-1 === mockId) {
      this.sendMessage('LogClosing', [], isActor);
    }
    else {
      this.sendMockMessage('LogClosing', mockId, [], isActor);
    }
  }
  
  logClosed(isActor, mockId = -1) {
    if(-1 === mockId) {
      this.sendMessage('LogClosed', [], isActor);
    }
    else {
      this.sendMockMessage('LogClosed', mockId, [], isActor);
    }
  }
  
  logSendMessage(message, mockId = -1) {
    if(-1 === mockId) {
      this.sendMessage('LogMessageSend', [], message);
    }
    else {
      this.sendMockMessage('LogMessageSend', mockId, [], message);
    }
  }
  
  logMessageReceive(message, mockId = -1) {
    if(-1 === mockId) {
      this.sendMessage('LogMessageReceive', [], message);
    }
    else {
      this.sendMockMessage('LogMessageReceive', mockId, [], message);
    }
  }

  logMessageBodySend(chunk, contentType, contentEncoding, transferEncoding, mockId = -1) {
    if(-1 === mockId) {
      this.sendMessage('LogMessageBodySend', [chunk.buffer], contentType, contentEncoding, transferEncoding, chunk);
    }
    else {
      this.sendMockMessage('LogMessageBodySend', mockId, [chunk.buffer], contentType, contentEncoding, transferEncoding, chunk);
    }
  }

  logMessageBodyReceive(chunk, contentType, contentEncoding, transferEncoding, mockId = -1) {
    if(-1 === mockId) {
      this.sendMessage('LogMessageBodyReceive', [chunk.buffer], contentType, contentEncoding, transferEncoding, chunk);
    }
    else {
      this.sendMockMessage('LogMessageBodyReceive', mockId, [chunk.buffer], contentType, contentEncoding, transferEncoding, chunk);
    }
  }
  
  logMessageSendWebSocket(message, mockId = -1) {
    if(-1 === mockId) {
      this.sendMessage('LogMessageSendWebSocket', [], message);
    }
    else {
      this.sendMockMessage('LogMessageSendWebSocket', mockId, [], message);
    }
  }
  
  logMessageReceiveWebSocket(message, mockId = -1) {
    if(-1 === mockId) {
      this.sendMessage('LogMessageReceiveWebSocket', [], message);
    }
    else {
      this.sendMockMessage('LogMessageReceiveWebSocket', mockId, [], message);
    }
  }
  
  logSendFrame(frame, dataBuffers, mockId = -1) {
    if(0 === dataBuffers.length) {
      if(-1 === mockId) {
        this.sendMessage('LogFrameSend', undefined, frame, null);
      }
      else {
        this.sendMockMessage('LogFrameSend', mockId, undefined, frame, null);
      }
    }
    else {
      const transferlist = [];
      dataBuffers.forEach((dataBuffer) => {
        transferlist.push(dataBuffer.buffer);
      });
      if(-1 === mockId) {
        this.sendMessage('LogFrameSend', transferlist, frame, dataBuffers);
      }
      else {
        this.sendMockMessage('LogFrameSend', mockId, transferlist, frame, dataBuffers);
      }
    }
  }
  
  logReceiveFrame(frame, dataBuffers, mockId = -1) {
    if(0 === dataBuffers.length) {
      if(-1 === mockId) {
        this.sendMessage('LogFrameReceive', undefined, frame, null);
      }
      else {
        this.sendMockMessage('LogFrameReceive', mockId, undefined, frame, null);
      }
    }
    else {
      const transferlist = [];
      dataBuffers.forEach((dataBuffer) => {
        transferlist.push(dataBuffer.buffer);
      });
      if(-1 === mockId) {
        this.sendMessage('LogFrameReceive', transferlist, frame, dataBuffers);
      }
      else {
        this.sendMockMessage('LogFrameReceive', mockId, transferlist, frame, dataBuffers);
      }
    }
  }
  
  sendMessage(cmd, transferables, ...params) {
    this.cbMessage(cmd, -1, transferables, ...params);
  }
  
  sendMockMessage(cmd, mockId, transferables, ...params) {
    this.cbMessage(cmd, mockId, transferables, ...params);
  }
}

PuppeteerConnectionHandler.ADDRESS_NONE = 0;
PuppeteerConnectionHandler.ADDRESS_OK = 1;
PuppeteerConnectionHandler.ADDRESS_ERR = 2;


module.exports = PuppeteerConnectionHandler;
