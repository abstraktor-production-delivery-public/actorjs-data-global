
'use strict';


class PuppeteerContentCache {
  constructor() {
    this.idCache = new Map();
    this.metaCache = new Map();
  }
  
  setId(streamIdentifier, url) {
    if(!this.idCache.has(streamIdentifier)) {
      this.idCache.set(streamIdentifier, url);
    }
  }
  
  setMeta(streamIdentifier, contentType, contentEncoding, contentLength) {
    const url = this.idCache.get(streamIdentifier);
    if(url) {
      if(!this.metaCache.has(url)) {
        this.metaCache.set(url, {
          meta: {
            url: url,
            contentType: contentType,
            contentEncoding: contentEncoding,
            contentLength: contentLength
          }
        });
      }
    }
  }
  
  getMeta(streamIdentifier) {
    const url = this.idCache.get(streamIdentifier);
    if(url) {
      return this.metaCache.get(url);
    }
  }
}


module.exports = PuppeteerContentCache;
