
'use strict';

const PuppeteerConnectionHandler = require('./puppeteer-connection-handler');
const StackApi = require('stack-api');


class PuppeteerConnectionWorkerThread extends StackApi.WorkerThread {
  constructor() {
    super();
    this.connectionHandler = null;
  }
  
  onStart(id, done) {
    this.connectionHandler = new PuppeteerConnectionHandler(id, (done, msgId, ...params) => {
      if(0 !== params.length) {
        this.channel.method(msgId, done, undefined, ...params);
      }
      else {
        done(new Error('Wrong format'));
      }
    }, (msgId, mockId, transferables, ...params) => {
      this.channel.message(msgId, transferables, mockId, ...params);
    });
    process.nextTick(done);
  }
  
  onStop(done) {
    this.connectionHandler.end(() => {
      this.connectionHandler = null;
      done();
    });
  }
  
  run(host, browserData, sequrity, done) {
    this.connectionHandler.run(host, browserData, sequrity, done);
  }
  
  attachMockServers(mockServers, mockServerDatas, done) {
    this.connectionHandler.attachMockServers(mockServers, mockServerDatas, done);
  }
  
  detachMockServers(done) {
    this.connectionHandler.detachMockServers(done);
  }
}


module.exports = PuppeteerConnectionWorkerThread;
