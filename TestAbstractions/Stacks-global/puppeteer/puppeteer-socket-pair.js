
'use strict';


class PuppeteerSocketPair {
  constructor(id, name, incommingSocket, stateMachine) {
    this.id = id;
    this.name = name;
    this.incommingSocket = incommingSocket;
    this.stateMachine = stateMachine;
    this.outgoingSocket = null;
    stateMachine.socketPair = this;
  }
  
  setIncommingSocket(incommingSocket) {
    this.incommingSocket = incommingSocket;
  }
  
  setOutgoingSocket(outgoingSocket) {
    this.outgoingSocket = outgoingSocket;
  }
   
  incommingEnd() {
    this.incommingSocket.end();
  }
  
  incommingClose() {
    this.incommingSocket.destroySoon();
  }
  
  outgoingEnd() {
    if(this.outgoingSocket) {
      this.outgoingSocket.end();
    }
  }
  
  outgoingClose() {
    if(this.outgoingSocket) {
      this.outgoingSocket.destroySoon();
    }
  }
}


module.exports = PuppeteerSocketPair;
