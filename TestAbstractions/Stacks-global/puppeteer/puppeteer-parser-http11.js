
'use strict';

const StackApi = require('stack-api');
const HttpConst = require('../http/http-const');
const HttpConstHeader = require('../http/http-const-header'); 
const HttpMsgRequest = require('../http/http-msg-request');
const HttpMsgResponse = require('../http/http-msg-response');


class PuppeteerParserHttp11 {
  constructor(id, type, replaceHeaderNames = new Map()) {
    this.id = id;
    this.type = type;
    this.replaceHeaderNames = replaceHeaderNames;
    this.bufferManager = new StackApi.BufferManager(this.id, true);
    this.state = PuppeteerParserHttp11.FIRST_LINE;
    this.size = 0;
    this.sizeLeft = 0;
    this.chunks = null;
    this.message = null;
    this.rawBody = [];
  }
  
  parse(chunk) {
    this.bufferManager.addBuffer(chunk);
    switch(this.state) {
      case PuppeteerParserHttp11.FIRST_LINE: {
        if(!this.bufferManager.findLine()) {
          break;
        }
        else {
          this.size = 0;
          this.sizeLeft = 0;
          this.rawBody = [];
          this.chunks = null;
          const line = this.bufferManager.getLine();
          if(PuppeteerParserHttp11.REQUEST === this.type) {
            this.message = new HttpMsgRequest();
            this.message.addRequestLine(...line.split(' '));
          }
          else {
            this.message = new HttpMsgResponse();
            this.message.addStatusLine(...line.split(' '));
          }
          this.state = PuppeteerParserHttp11.HEADERS;
        }
      }
      case PuppeteerParserHttp11.HEADERS: {
        let allHeaders = false;
        while(this.bufferManager.findLine()) {
          const line = this.bufferManager.getLine();
          if(0 === line.length) {
            this.state = PuppeteerParserHttp11.BODY;
            allHeaders = true;
            break;
          }
          const delimiter = line.indexOf(':');
          if(-1 === delimiter) {
            console.log('parse Error:', delimiter, ', line:', line);
            break;
          }
          this.message.addHeader(line.substring(0, delimiter), line.substring(delimiter + 1).trimStart());
        }
        if(!allHeaders) {
          break;
        }
      }
      case PuppeteerParserHttp11.BODY: {
        const size = this.message.getHeader(HttpConstHeader.CONTENT_LENGTH);
        if(size) {
          this.size = this.sizeLeft = Number.parseInt(size);
          this.state = PuppeteerParserHttp11.BODY_CONTENT_LENGTH;
        }
        else {
          const transferEncoding = this.message.getHeader(HttpConstHeader.TRANSFER_ENCODING);
          if(transferEncoding) {
            this.state = PuppeteerParserHttp11.BODY_TRANSFER_ENCODING;
            return this._parseTransferEncoding(chunk);
          }
          else {
            this.state = PuppeteerParserHttp11.DONE;
            return true;
          }
        }
      }
      case PuppeteerParserHttp11.BODY_CONTENT_LENGTH: {
        if(0 === this.size) {
          this.state = PuppeteerParserHttp11.DONE;
          return true;
        }
        const buffer = this.bufferManager.receiveSize(this.size);
        if(undefined !== buffer) {
          this.sizeLeft -= buffer.length;
          this.rawBody.push(buffer);
          if(0 === this.sizeLeft) {
            this.state = PuppeteerParserHttp11.DONE;
            return true;
          }
        }
        break;
      }
      case PuppeteerParserHttp11.BODY_TRANSFER_ENCODING: {
        return this._parseTransferEncoding(chunk);
      }
      default: {
        
      }
    }
    return false;
  }
  
  _parseTransferEncoding(chunk) {
    if(null === this.chunks) {
      this.chunks = {
        state: PuppeteerParserHttp11.TE_SIZE,
        size: 0,
        chunkLines: [],
        chunks: [],
        trailers: []
      };
    }
    
    switch(this.chunks.state) {
      case PuppeteerParserHttp11.TE_SIZE:
      case PuppeteerParserHttp11.TE_DATA:
      case PuppeteerParserHttp11.TE_DATA_LINE: {
        // 1) Get Data
        while(true) {
          if(PuppeteerParserHttp11.TE_SIZE === this.chunks.state) {
            if(!this.bufferManager.findLine()) {
              return false;
            }
            this.chunks.state = PuppeteerParserHttp11.TE_DATA;
            const line = this.bufferManager.getLine();
            this.chunks.chunkLines.push(line);
            const chunkLineIndex = line.indexOf(HttpConst.COMMA_SP);
            if(-1 === chunkLineIndex) {
              this.chunks.size = Number.parseInt(line, 16);
            }
            else {
              this.chunks.size = Number.parseInt(line.substring(0, chunkLineIndex), 16);
            }
          }
          if(PuppeteerParserHttp11.TE_DATA === this.chunks.state) {
            if(0 !== this.chunks.size) {
              if(Number.isNaN(this.chunks.size)) {
                return true; // THROW ?
              }
              const buffer = this.bufferManager.receiveSize(this.chunks.size);
              if(undefined !== buffer) {
                this.chunks.chunks.push(buffer);
                this.chunks.size = 0;
                this.chunks.state = PuppeteerParserHttp11.TE_DATA_LINE;
              }
              else {
                return false;
              }
            }
            else {
              this.chunks.state = PuppeteerParserHttp11.TE_TRAILER;
              break;
            }
          }
          if(PuppeteerParserHttp11.TE_DATA_LINE === this.chunks.state) {
            if(!this.bufferManager.findLine()) {
              return false;
            }
            const line = this.bufferManager.getLine();
            this.chunks.state = PuppeteerParserHttp11.TE_SIZE;
          }
        }
        break;
      }
    }
    switch(this.chunks.state) {
      case PuppeteerParserHttp11.TE_TRAILER: {
        // 2) Get Trailers
        while(true) {
          if(!this.bufferManager.findLine()) {
            return false;
          }
          const line = this.bufferManager.getLine();
          if(0 === line.length) {
            this.chunks.state = PuppeteerParserHttp11.TE_DONE;
            break;
          }
          this.chunks.trailers.push(line);
        }
      }
    }
    // 3) Calculate size
    let size = 0;
    this.chunks.chunkLines.forEach((line) => {
      size += line.length + 2;
    });
    this.chunks.chunks.forEach((chunk) => {
      size += chunk.length + 2;
    });
    this.chunks.trailers.forEach((trailer) => {
      size += trailer.length + 2;
    });
    size += 2;
    
    // 4) Render chunk
    const buffer = Buffer.allocUnsafeSlow(size);
    let pos = 0;
    for(let i = 0; i < this.chunks.chunkLines.length - 1; ++i) {
      const line = this.chunks.chunkLines[i];
      pos += buffer.write(line, pos, line.length);
      pos += buffer.write(HttpConst.CR_CL, pos, HttpConst.CR_CL_LENGTH);
      pos += this.chunks.chunks[i].copy(buffer, pos);
      pos += buffer.write(HttpConst.CR_CL, pos, HttpConst.CR_CL_LENGTH);
    }
    const line = this.chunks.chunkLines[this.chunks.chunkLines.length - 1];
    pos += buffer.write(line, pos, line.length);
    pos += buffer.write(HttpConst.CR_CL, pos, HttpConst.CR_CL_LENGTH);
    this.chunks.trailers.forEach((trailer) => {
      pos += buffer.write(trailer, pos, trailer.length);
      pos += buffer.write(HttpConst.CR_CL, pos, HttpConst.CR_CL_LENGTH);
    });
    pos += buffer.write(HttpConst.CR_CL, pos, HttpConst.CR_CL_LENGTH);
    
    this.rawBody.push(buffer);
    
    this.state = PuppeteerParserHttp11.DONE;
    return true;
  }
  
  reset() {
    this.state = PuppeteerParserHttp11.FIRST_LINE;    
  }
  
  isHeadersReady() {
    return this.state > PuppeteerParserHttp11.HEADERS;
  }
}

PuppeteerParserHttp11.REQUEST = 0;
PuppeteerParserHttp11.RESPONSE = 1;

PuppeteerParserHttp11.FIRST_LINE = 0;
PuppeteerParserHttp11.HEADERS = 1;
PuppeteerParserHttp11.BODY = 2;
PuppeteerParserHttp11.BODY_CONTENT_LENGTH = 3;
PuppeteerParserHttp11.BODY_TRANSFER_ENCODING = 4;
PuppeteerParserHttp11.DONE = 5;

PuppeteerParserHttp11.STATUS = [
  'FIRST_LINE',
  'HEADERS',
  'BODY',
  'BODY_CONTENT_LENGTH',
  'BODY_TRANSFER_ENCODING',
  'DONE'
];

PuppeteerParserHttp11.TE_SIZE = 0;
PuppeteerParserHttp11.TE_DATA = 1;
PuppeteerParserHttp11.TE_DATA_LINE = 2;
PuppeteerParserHttp11.TE_TRAILER = 3;
PuppeteerParserHttp11.TE_DONE = 4;

PuppeteerParserHttp11.TE_STATUS = [
  'TE_SIZE',
  'TE_DATA',
  'TE_DATA_LINE',
  'TE_TRAILER',
  'TE_DONE'
];


module.exports = PuppeteerParserHttp11;
