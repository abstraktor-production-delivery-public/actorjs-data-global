
'use strict';

// https://www.rfc-editor.org/rfc/rfc9113.html
const StackApi = require('stack-api');
const hpack = require('hpack.js');


class PuppeteerParserHttp2 {
  constructor(id, type, contentCache) {
    this.id = id;
    this.type = type;
    this.contentCache = contentCache;
    this.logSettings = { // TODO: should come from log settings.
      isRawBuffer: true,
      isDataBuffer: true,
    };
    this.bufferManager = new StackApi.BufferManager(this.id, true);
    this.state = PuppeteerParserHttp2.FIRST_LINE;
    this.size = 0;
    this.chosenId = 1;
    this.frameData = this._createEmptyFrameData();
    this.resultFrameDatas = [];
    this.chosenType = PuppeteerParserHttp2.RESPONSE;
    this.parseFrame = [
      this._parseFrameData.bind(this),
      this._parseFrameHeaders.bind(this),
      this._parseFramePriority.bind(this),
      this._parseFrameRstStream.bind(this),
      this._parseFrameSettings.bind(this),
      this._parseFramePushPromise.bind(this),
      this._parseFramePing.bind(this),
      this._parseFrameGoAway.bind(this),
      this._parseFrameWindowUpdate.bind(this),
      this._parseFrameContinuation.bind(this)
    ];
    this.decompressor = hpack.decompressor.create({table: {maxSize: 4294967296}});
    this.decompressor.updateTableSize(4096);
    this.message = null;
  }
  
  _createEmptyFrameData() {
    return {
      frame: {
        length: -1,
        type: -1,
        flags: -1,
        r: -1,
        streamIdentifier: -1,
        data: undefined,
        meta: [],
      },
      dataBuffers: []
    };
  }
  
  log(...params) {
    if(this.chosenType === this.type && this.id === this.chosenId) {
   // if(this.chosenType === this.type) {
      console.log(this.id + ':', ...params);
    }
  }
  
  parse(chunk) {
    const pL = this.bufferManager.length;
    this.bufferManager.addBuffer(chunk);
    //console.log(this.id, PuppeteerParserHttp2.MSG_TYPES[this.type], '===>>> ¤¤¤¤¤¤¤¤¤¤ CHUNK.LENGTH - previous size:', pL, 'new size:', this.bufferManager.length, 'chunk', chunk.length);
    try {
      while(this._parse()) {}
    } catch(a) {
      console.log(this.id, PuppeteerParserHttp2.MSG_TYPES[this.type], 'ERROR HTTP2', a);
    }
    return 0 !== this.resultFrameDatas.length;
  }
  
  _parse() {
    const frame = this.frameData.frame;
    if(PuppeteerParserHttp2.FIRST_LINE === this.state) {
      if(PuppeteerParserHttp2.REQUEST === this.type) {
        if(this.bufferManager.length < 24) {
          return false;
        }
        const preface = this.bufferManager.receiveSize(24);
        this.log(this.id, 'REQUEST PREFACE', preface.toString());
      }
      this.state = PuppeteerParserHttp2.FRAMES_HEADER;
    }
    if(PuppeteerParserHttp2.FRAMES_HEADER === this.state) {
      if(this.bufferManager.length < 9) {
        return false;
      }
      const header = this.bufferManager.receiveSize(9);
      let offset = 0;
      frame.length = header.readUIntBE(offset, 3);
      offset += 3;
      frame.type = header.readUInt8(offset);
      offset += 1;
      frame.flags = header.readUInt8(offset);
      offset += 1;
      const buffer = header.readUInt32BE(offset);
      frame.r = 0b1 & ((buffer & 0x80000000) >> 31);
      frame.streamIdentifier = 0x7FFFFFFF & buffer;
      offset += 4;
      this.state = PuppeteerParserHttp2.FRAMES_PAYLOAD;
    }
    if(PuppeteerParserHttp2.FRAMES_PAYLOAD === this.state) {
      if(this.bufferManager.length < frame.length) {
        return false;
      }
      const payload = this.bufferManager.receiveSize(frame.length);
      this.parseFrame[frame.type](payload);
    }
    this.state = PuppeteerParserHttp2.FRAMES_HEADER;
    this.resultFrameDatas.push(this.frameData);
    this.frameData = this._createEmptyFrameData();
    return 0 !== this.bufferManager.length;
  }
  
  _parseFrameData(payload) {
    //this.log('* * * * * * * * * * * DATA');
    const frame = this.frameData.frame;
    let offset = {
      v: 0
    };
    let padLength = 0;
    frame.data = {
      padLength: undefined
    };
    const data = frame.data;
    const paddedFlag = StackApi.BitByte.getBit(frame.flags, 4);
    if(paddedFlag) {
      data.padLength = payload.readUInt8(offset.v);
      offset.v += 1;
      padLength = data.padLength;
    }
    const headersLength = frame.length - padLength;
    if(this.logSettings.isDataBuffer && 0 < headersLength) {
      const metaData = this.contentCache.getMeta(frame.streamIdentifier);
      if(metaData) {
        frame.meta.push(metaData.meta);
        if(0 === headersLength) {
          this.log('* * * * * * * * * * * DATA', 0);
        }
        this.frameData.dataBuffers.push(payload.subarray(offset.v, offset.v + headersLength));
      }
    }
  }
  
  _parseFrameHeaders(payload) {
    //this.log('* * * * * * * * * * * HEADERS');
    const frame = this.frameData.frame;
    let offset = {
      v: 0
    };
    const padLength = 0;
    frame.data = {
      padLength: undefined,
      exclusive: undefined,
      streamDependency: undefined,
      weight: undefined,
      pseudoHeader: null,
      headers: []
    };
    const data = frame.data;
    const paddedFlag = StackApi.BitByte.getBit(frame.flags, 4);
    if(paddedFlag) {
      data.padLength = payload.readUInt8(offset.v);
      offset.v += 1;
      padLength = data.padLength;
    }
    let exclusive = 0;
    let streamDependency = 0;
    let weight = 0;
    const priorityFlag = StackApi.BitByte.getBit(frame.flags, 2);
    if(priorityFlag) {
      const buffer = payload.readUInt32BE(offset.v);
      streamDependency = 0x7FFFFFFF & buffer;
      exclusive = 0b1 & ((buffer & 0x80000000) >> 31);
      offset.v += 4;
      weight = payload.readUInt8(offset.v);
      offset.v += 1;
      data.exclusive = exclusive;
      data.streamDependency = streamDependency;
      data.weight = weight;
    }
    const headersLength = frame.length - padLength;
    const result = this._parseHeaders(payload, offset, headersLength - offset.v - padLength);
    data.pseudoHeader = result.pseudoHeader;
    data.headers = result.headers;
    if(data.pseudoHeader.status) { // RESPONSE TODO: filter on status!!!
      let contentType = '';
      let contentEncoding = '';
      let contentLength = -1;
      for(let i = 0; i < data.headers.length; ++i) {
        const header = data.headers[i];
        const headerName = header.name.toLowerCase();
        if('content-type' === headerName) {
          contentType = header.value;
        }
        else if('content-encoding' === headerName) {
          contentEncoding = header.value;
        }
        else if('content-length' === headerName) {
          contentLength = Number.parseInt(header.value);
        }
      }
      if(contentType) {
        this.contentCache.setMeta(frame.streamIdentifier, contentType, contentEncoding, contentLength);
      }
    }
    else { // REQUEST
      this.contentCache.setId(frame.streamIdentifier, `${data.pseudoHeader.scheme}${PuppeteerParserHttp2.SCHEME_DIVIDER}${data.pseudoHeader.authority}${data.pseudoHeader.path}`);
    }
  }
  
  _parseFramePriority(payload) {
    this.log('* * * * * * * * * * * PRIORITY');
  }
  
  _parseFrameRstStream(payload) {
   // this.log('* * * * * * * * * * * RST_STREAM');
    const frame = this.frameData.frame;
    let offset = 0;
    const errorCode  = payload.readUInt32BE(offset);
    frame.data = {
      errorCode: errorCode
    };
  }
  
  _parseFrameSettings(payload) {
    const frame = this.frameData.frame;
    //this.log('* * * * * * * * * * * SETTINGS');
    let offset = 0;
    frame.data = [];
    const ackFlag = StackApi.BitByte.getBit(frame.flags, 0);
    while(offset < frame.length) {
      const identifier  = payload.readUInt16BE(offset);
      offset += 2;
      const value  = payload.readUInt32BE(offset);
      offset += 4;
      if(1 === identifier) {
        this.decompressor.updateTableSize(value);
      }
      frame.data.push({
        identifier: identifier,
        value: value
      });
    }
  }
  
  _parseFramePushPromise(payload) {
    this.log('* * * * * * * * * * * PUSH_PROMISE');
  }
  
  _parseFramePing(payload) {
    if(0 === this.frameData.frame.length) {
      this.log('* * * * * * * * * * * PING', 0);
    }
    this.frameData.dataBuffers.push(payload.subarray(0, this.frameData.frame.length));
  }
  
  _parseFrameGoAway(payload) {
    this.log('* * * * * * * * * * * GOAWAY');
  }
  
  _parseFrameWindowUpdate(payload) {
//    this.log('* * * * * * * * * * * WINDOW_UPDATE');
    const frame = this.frameData.frame;
    let offset = 0
    frame.data = {
      reserved: undefined,
      windowSizeIncrement: undefined
    };
    const data = frame.data;
    const buffer = payload.readUInt32BE(offset);
    offset += 4;
    data.windowSizeIncrement = 0x7FFFFFFF & buffer;
    data.reserved = 0b1 & ((buffer & 0x80000000) >> 31);
  }
  
  _parseFrameContinuation(payload) {
    this.log('* * * * * * * * * * * CONTINUATION');
  }
  
  _parseHeaders(payload, offset, headersLength) {
    this.decompressor.write(payload.slice(offset.v, offset.v + headersLength));
    this.decompressor.execute();
    const result = {
      pseudoHeader: null,
      headers:[]
    };
    let value  = null;
    while(value = this.decompressor.read()) {
      if(!value.name.startsWith(':')) {
        result.headers.push(value);
      }
      else {
        if(!result.pseudoHeader) {
          if(':status' !== value.name) {
            result.pseudoHeader = {
              method: undefined,
              scheme: undefined,
              authority: undefined,
              path: undefined
            };
          }
          else {
            result.pseudoHeader = {
              status: undefined
            };
          }
        }
        Reflect.set(result.pseudoHeader, value.name.substring(1), value.value);
      }
    }
    return result;
  }
}


PuppeteerParserHttp2.REQUEST = 0;
PuppeteerParserHttp2.RESPONSE = 1;

PuppeteerParserHttp2.MSG_TYPES = [
  'REQUEST',
  'RESPONSE'
];

PuppeteerParserHttp2.FIRST_LINE = 0;
PuppeteerParserHttp2.FRAMES_HEADER = 1;
PuppeteerParserHttp2.FRAMES_PAYLOAD = 2;
PuppeteerParserHttp2.FRAMES_PAYLOAD_HEADERS = 3;

PuppeteerParserHttp2.STATUS = [
  'FIRST_LINE',
  'FRAMES_HEADER',
  'FRAMES_PAYLOAD',
  'FRAMES_PAYLOAD_HEADERS'
];

PuppeteerParserHttp2.SCHEME_DIVIDER = '://';

module.exports = PuppeteerParserHttp2;
