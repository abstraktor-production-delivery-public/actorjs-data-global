
'use strict';

const StackApi = require('stack-api');


class PuppeteerMessageSelector extends StackApi.MessageSelector {
  constructor() {
    super('puppeteer', true);
  }
}


module.exports = PuppeteerMessageSelector;
