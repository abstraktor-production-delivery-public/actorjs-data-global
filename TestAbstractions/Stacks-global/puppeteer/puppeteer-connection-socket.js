
'use strict';


class PuppeteerConnectionSocket {
  constructor(id, name, transport, parser, state, srcId, dstId = null) {
    this.id = id;
    this.name = name;
    this.transport = transport;
    this.parser = parser;
    this.state = state;
    this.srcId = srcId;
    this.dstId = dstId;
    this.socket = null;
    this.valid = false;
  }
  
  setDestination(sourceId) {
    this.dstId = sourceId;
  }
  
  setSocket(socket) {
    this.socket = socket;
    this.valid = true;
  }
  
  end(cb) {
    this.valid = false;
    this.socket.end(cb);
  }
  
  get authorized() {
    return this.socket.authorized();
  }
  
  get connecting() {
    return this.socket.connecting();
  }
  
  get pending() {
    return this.socket.pending();
  }
  
  destroy() {
    this.valid = false;
    this.socket.destroy();
  }
  
  destroySoon() {
    this.valid = false;
    this.socket.destroySoon();
  }
  
  write(data, cb) {
    return this.socket.write(data, cb);
  }
  
  pause() {
    return this.socket.pause();
  }
  
  resume() {
    return this.socket.resume();
  }
  
  on(eventName, listener) {
    this.socket.on(eventName, listener);
  }
  
  once(eventName, listener) {
    this.socket.once(eventName, listener);
  }
  
  off(eventName, listener) {
    this.socket.off(eventName, listener);
  }
  
  get localAddress() {
    return this.socket.localAddress;
  }
  
  get localPort() {
    return this.socket.localPort;
  }
  
  get localFamily() {
    return this.socket.localFamily;
  }
  
  get remoteAddress() {
    return this.socket.remoteAddress;
  }
  
  get remotePort() {
    return this.socket.remotePort;
  }
  
  get remoteFamily() {
    return this.socket.remoteFamily;
  }
}


module.exports = PuppeteerConnectionSocket;
