
'use strict';

const StackApi = require('stack-api');
const Puppeteer = require('puppeteer');
const PuppeteerInnerLog = require('./puppeteer-inner-log');
const PuppeteerConnectionClientOptions = require('./puppeteer-connection-client-options');
const PuppeteerMockManager = require('./puppeteer-mock-manager');
const PuppeteerSocketProxy = require('./puppeteer-socket-proxy');
const Fs = require('fs');


class PuppeteerConnectionClient extends StackApi.ConnectionWeb {
  constructor(id, type, actor, options) {
    super(id, type, 'puppeteer', actor, StackApi.NetworkType.TCP, PuppeteerConnectionClientOptions, options);
    this.browser = null;
    this.browserContext = null;
    this.browserContextName = '';
    this.page = null;
    this.pageAsync = null;
    this.pageLoaded = false;
    this.proxyArgsGenerated = false;
    this.isMockSelected = false;
    this.isPreviousSuccess = true;
    this.puppeteerLog = new PuppeteerInnerLog(this);
    this.helpers = new Set();
    this.mutex = new StackApi.MutexLocalCallback();
    this.onConnect = this.mutex.add(this, this.onConnect);
    this.onReuse = this.mutex.add(this, this.onReuse);
    this.onClose = this.mutex.add(this, this.onClose);
    this.clear = this.mutex.add(this, this.clear);
//    this.gaugeSocketProxyInstances = this.createGauge('Puppeteer', `SocketProxy`, 'Instances', 0, 0, 12);
  }
  
  static initStack() {
    PuppeteerConnectionClient.sequrity.cert = StackApi.StackComponentsCerts.cert;
    PuppeteerConnectionClient.sequrity.key = StackApi.StackComponentsCerts.key;
  }
  
  static supportInterception(testData) {
    return PuppeteerConnectionClientOptions.supportInterception(testData);
  }
  
  onConnect(browserData, reuse, done) {
    this.mutex.setStartTime();
    if(!this.browser) {
      this._getBrowser(browserData, reuse, done);
    }
    else if(!this.browserContext && browserData.srcAddress.incognitoBrowser) {
      this._getBrowserContext(browserData, reuse, done);
    }
    else if(!this.page) {
      this._createNewPage(browserData, reuse, done);
    }
  }
  
  onReuse(browserData, done) {
    this.browser.proxy.init(browserData, (err) => {
      if(err) {
        done(false, err);
        return;
      }
      this.logGuiEvent(() => `using existing Browser['${browserData.name}']`, StackApi.LogDataGuiType.USE_BROWSER, {browserData: browserData}, 'Puppeteer', '82c555fb-fafe-44e7-b492-a660aaafbb03');
      this._gotoPage(browserData, true, done);
    });
  }
  
  async __close(id, browser, err, done) {
    this.logGui(() => `Browser['${browser.name}'].close`, 'Puppeteer', '67bcf70e-b031-494f-be7d-e50582198983', 1);
    const context = this._getContext();
    const page = context.pages.get(id);
    context.pages.delete(id);
    context.browsers.delete(browser.name);
    if(page) {
      await page.page.close();
    }
    await browser.target.close();
    if(browser.proxy) {
      this.logGui(() => `Browser.proxy['${browser.name}'].cleanup ${PuppeteerConnectionClient.SUCCESS}`, 'Puppeteer', '1161c7ec-71a0-4566-96ae-2e3ea5f7277f', 1);
      browser.proxy.cleanup(() => {
        process.nextTick(done, err);
      });
    }
    else {
      process.nextTick(done, err);
    }
  }
  
  async _close(id, browser, err, done) {
    try {
      await this.__close(id, browser, err, done);
    }
    catch(e) {
      process.nextTick(done, e);
    }
  }
  
  onClose(done) {
    const err = this.actor.isSuccess() ? null : new Error('Not Success.');
    if(this.browser) {
      if(!this.browser.reuse || err) {
        if(this.browser.proxy) {
          this.browser.proxy.close(() => {
            this._close(this.id, this.browser, err, done);
          });
        }
        else {
          this._close(this.id, this.browser, err, done);
        }
      }
      else if(this.isMockSelected && this.browser.proxy) {
        this.browser.proxy.close(done);
      }
      else {
        process.nextTick(done, err);
      }
    }
    else {
      process.nextTick(done, err);
    }
  }
  
  addHelpers(...helpers) {
    helpers.forEach((Helper) => {
      if(!this.helpers.has(Helper.name)) {
        const object = new Helper(this, this.connectionOptions.puppeteerActionSlowMo);
        const name = object.constructor.name.substring(0, 1).toLowerCase() + object.constructor.name.substring(1);
        const newObject = {};
        Reflect.set(newObject, name, object);
        Object.assign(this.page, newObject);
        this.helpers.add(Helper.name);
      }
    });
  }
    
  _getBrowser(browserData, reuse, done) {
    const browser = this._getContext().browsers.get(browserData.name);
    if(browser) {
      if(browser.ready) {
        this.logGuiEvent(() => `using existing Browser['${browserData.name}']`, StackApi.LogDataGuiType.USE_BROWSER, {browserData: browserData}, 'Puppeteer', 'fa1e823b-f15b-41aa-9780-b008d1ca4d30');
        this.browser = browser;
        this.browser.proxy.init(browserData, (err) => {
          if(err) {
            done(err);
          }
          this._getBrowserContext(browserData, reuse, done);
        });
      }
      else {
        browser.queue.push({
          browserData: browserData,
          done: done
        });
      }
    }
    else {
      this._createNewBrowser(browserData, reuse, done);
    }
  }
  
  _launchPuppeteer(browser, browserData, reuse, done) {
    const formattedArgs = this._stringifyArguments(this.connectionOptions.launchOptions, 3);
    this.logGuiEvent(() => `Puppeteer.launch(${formattedArgs}) ${PuppeteerConnectionClient.CALL}`, StackApi.LogDataGuiType.NEW_BROWSER, {browserData: browserData}, 'Puppeteer', '9c99c773-6750-4ff1-a920-b7e1bddd3768');
    Puppeteer.launch(this.connectionOptions.launchOptions).then((newBrowser) => {
      this.logGui(() => `Puppeteer.launch(${formattedArgs}) ${PuppeteerConnectionClient.SUCCESS}`, 'Puppeteer', '6c023b85-981a-4253-a998-bb3e46885554');
      browser.ready = true;
      browser.reuse = reuse;
      browser.target = newBrowser;
      browser.targetProxy = this._createNewObject(newBrowser, `('${browser.name}')`);
      this.browser = browser;
      this._getContext().browsers.set(browserData.name, browser);
      this._getBrowserContext(browserData, reuse, done);
      browser.queue.forEach((launch) => {
        this.logGui(() => `*********************************using existing Browser['${browser.name}']`, 'Puppeteer', 'c7b49fab-983c-4144-b9d2-e80e175dfe3a');
        launch.done(false);
      });
      browser.queue = [];
    }, (reason) => {
      this.logGui(() => `Puppeteer.launch(${formattedArgs}) ${PuppeteerConnectionClient.FAILURE}: ${reason}`, 'Puppeteer', '4d12c327-2c4b-48b7-83ce-fffc3d6d75b4');
      done(true, reason);
    }).catch((e) => {
      console.log('<<< LAUNCH exception', e.message);
      done(true, e);
    });
  }
  
  _createNewBrowser(browserData, reuse, done) {
    this.logGui(() => '... _createNewBrowser, srcAddress: ' + this._stringifyArguments(browserData.srcAddress, 2, this._srcAddressFilter), 'Puppeteer', '7c8322d6-ebba-4ba0-b701-020b063ccd90');
    const browser = {
      name: browserData.name,
      reuse: reuse,
      target: null,
      targetProxy: null,
      proxy: null,
      ready: false,
      queue: []
    };
    if(this.connectionOptions.proxy) {
      const proxy = new PuppeteerSocketProxy(PuppeteerConnectionClient.currentSocketProxyId++, this, this._getContext().mockManager, PuppeteerConnectionClient.sequrity, this.puppeteerLog);
      browser.proxy = proxy;
      proxy.start('127.0.0.1', browserData.srcAddress, (err, port) => {
//        this.gaugeSocketProxyInstances.inc();
        if(err) {
          done(true, err);
        }
        else {
          browser.proxy.init(browserData, (err) => {
            if(err) {
              return done(false, err);
            }
            this.connectionOptions.launchOptions.args.push(`--proxy-server=http://127.0.0.1:${port};`);
            this._launchPuppeteer(browser, browserData, reuse, done);
          });
        }
      });
    }
    else {
      const args = this.connectionOptions.launchOptions.args;
      this.dnsUrlCache.localDns.forEach((v, k) => {
        this.connectionOptions.addArg('--host-rules', `MAP ${k} ${v.addresses[0]}`, false);
      });
      this._launchPuppeteer(browser, browserData, reuse, done);
    }
  }
  
  _getBrowserContext(browserData, reuse, done) {
    if(browserData.srcAddress.incognitoBrowser) {
      const context = this._getContext().incognitoBrowserContexts.get(browserData.srcAddress.incognitoBrowser);
      if(context) {
        if(context.ready) {
          this.browserContext = context.target;
          this._createNewPage(browserData, reuse, done);
        }
        else {
          context.queue.push({
            browserData: browserData,
            done: done
          });
        }
      }
      else {
        this._launchContext(browserData, done);
      }
    }
    else {
      this._createNewPage(browserData, reuse, done);
    }
  }
  
  _launchContext(browserData, done) {
    const context = {
      ready: false,
      target: null,
      queue: []
    };
    this._getContext().incognitoBrowserContexts.set(browserData.srcAddress.incognitoBrowser, context);
  }
  
  _createIncognitoBrowserContext(context) {
    const self = this;
    return new Proxy(context, {
      get: (target, property, receiver) => {
        return function (...args) {
          const formattedArgs = self._stringifyArguments(args);
          self.logGui(() => `BrowserContext['${self.browserContextName}'].${property}(${formattedArgs})`, 'Puppeteer', '4411bebf-4c37-4ec1-a07f-fa56a4600e01');
          const result = target[property](...args);
          if(result instanceof Promise) {
            let cancel = false;
            const externalPendingId = self.actor.setPending(() => {
              self.logGui(() => `BrowserContext['${self.browserContextName}'].${property}(${formattedArgs}) --- cancel`, 'Puppeteer', '98999d14-c6d9-425a-9dcc-0bba7053ba44');
              cancel = true;
            });
            result.then((value) => {
              if(!cancel) {
                self.logGui(() => `BrowserContext['${self.browserContextName}'].${property}(${formattedArgs}) --- done`, 'Puppeteer', 'ddcd57a0-5a71-4eb4-a823-03f249629b35');
                if('newPage' === property) {
                  self.actor.pendingSuccess(externalPendingId, self._createNewObject(value));
                }
                else {
                  self.cbActorCallbacks.cbDone(externalPendingId, value);
                }
              }
            }, (reason) => {
              if(!cancel) {
                self.logGui(() => `BrowserContext['${self.browserContextName}'].${property}(${formattedArgs}) --- error: ${reason}`, 'Puppeteer', '666af965-9130-49c3-a365-5f70a6c1debd');
                self.actor.pendingError(externalPendingId, reason);
              }
            });
          }
          else {
            return result;
          }
        };
      }
    });
  }
  
  async _goto(browserData, currentUrl) {
    const response = await this.pageAsync.goto(browserData.dstAddress.uri);
    this.pageLoaded = true;
    if(this.connectionOptions.puppeteerTestOptimization) {
      await this.pageAsync.evaluate(() => {
        window.abstractorTestOptimization = true;
      });
    }
    return response;
  }
  
  async _gotoPage(browserData, reuse, done) {
    const currentUrl = this.pageAsync.url();
    try {
      if(!reuse || !this.pageLoaded) {
        this.logDebug('GOTO - goto !reuse || !loaded', 'Puppeteer', 'ac8d6f9c-192e-4de4-a77d-535e575ff052');
        this.logGui(`page.goto('${browserData.dstAddress.uri}') ${PuppeteerConnectionClient.CALL} --- Page[!reuse]['${currentUrl}']`, 'Puppeteer', '982196d4-60eb-437d-a00f-532ecd6e62fa');
        const response = await this._goto(browserData, currentUrl);
        this.logGui(() => `page.goto('${browserData.dstAddress.uri}')${response ? ' STATUS:' + response.status() + ' ' + response.statusText() : ' '}${PuppeteerConnectionClient.SUCCESS} --- Page[!reuse]['${currentUrl}']`, 'Puppeteer', 'a1455f35-40f6-4f57-8bd2-460796ace0d8');
        done(true);
      }
      else {
        if(currentUrl !== browserData.dstAddress.uri) {
          if(this.connectionOptions.puppeteerNavigationOptimization && !this.isMockSelected) {
            this.logDebug('GOTO - goto reuse', 'Puppeteer', 'a9c97b89-6091-4aab-8ba4-8a1b9c830d86');
            this.logGui(`page.goto('${browserData.dstAddress.uri}') ${PuppeteerConnectionClient.CALL} --- Page[reuse]['${currentUrl}']`, 'Puppeteer', '390e76b2-66e5-4288-b77c-87260ffa9407');
            await this.pageAsync.evaluate(async (uri) => {
              await window.abstractorRerender(uri);
            }, browserData.dstAddress.uri);
            this.logGui(() => `page.goto('${browserData.dstAddress.uri}')${PuppeteerConnectionClient.SUCCESS} --- Page[reuse]['${currentUrl}']`, 'Puppeteer', 'b6b422ae-c64b-4b0a-a9d5-5ca2947a67a2');
          }
          else {
            let reason = this.isMockSelected ? 'mock selected' : '';
            reason += this.connectionOptions.puppeteerNavigationOptimization ? '' : (reason ? ' & ' : '') + 'no navigation optimization.';
            this.isMockSelected = false;
            this.logDebug('GOTO - goto !reuse - ' + reason, 'Puppeteer', 'f41c6ff1-7bb1-47d9-bab1-ae24d8612c55');
            this.logGui(`page.goto('${browserData.dstAddress.uri}') ${PuppeteerConnectionClient.CALL} --- Page[reuse-denied(${reason})]['${currentUrl}']`, 'Puppeteer', 'cb825948-8046-4f8a-9e55-f5c760d3c87c');
            await this._goto(browserData, currentUrl);
            this.logGui(() => `page.goto('${browserData.dstAddress.uri}')${PuppeteerConnectionClient.SUCCESS} --- Page[reuse-denied(${reason})]['${currentUrl}']`, 'Puppeteer', 'f2df38f0-51b4-4ab8-a648-87542e085e79');
          }
          done(true);
        }
        else {
          if(!this.isMockSelected) {
            this.logGui(`page.goto('${browserData.dstAddress.uri}') ${PuppeteerConnectionClient.CONTINUE} --- Page[reuse][]['${currentUrl}']`, 'Puppeteer', '982196d4-60eb-437d-a00f-532ecd6e62fa');
            this.logDebug('GOTO - goto !navigation', 'Puppeteer', '252502a5-9684-4151-bfa3-1dd0c594aeaf');
            process.nextTick((done) => {
              done(true);
            }, done);
          }
          else {
            let reason = this.isMockSelected ? 'mock selected' : '';
            reason += this.connectionOptions.puppeteerNavigationOptimization ? '' : (reason ? ' & ' : '') + 'no navigation optimization.';
            this.isMockSelected = false;
            this.logDebug('GOTO - goto !reuse - ' + reason, 'Puppeteer', 'dd2f712b-6145-4152-a03b-9aac1b4f22ad');
            this.logGui(`page.goto('${browserData.dstAddress.uri}') ${PuppeteerConnectionClient.CALL} --- Page[reuse-denied(${reason})]['${currentUrl}']`, 'Puppeteer', 'cf84eec7-c96d-41eb-8221-4d2cf1eec19a');
            await this._goto(browserData, currentUrl);
            this.logGui(() => `page.goto('${browserData.dstAddress.uri}')${PuppeteerConnectionClient.SUCCESS} --- Page[reuse-denied(${reason})]['${currentUrl}']`, 'Puppeteer', '638ac5f2-a37c-4a72-bb5a-ff9364d08a7b');
            done(true);
          }
        }
      }
    }
    catch(err) {
      this.logGui(() => `Page['${currentUrl}']${reuse ? '[reuse]' : ''}.goto('${browserData.dstAddress.uri}')${PuppeteerConnectionClient.FAILURE}: ${err}`, 'Puppeteer', 'efcbe242-f2c1-4943-bc03-698ebce0ff16');
      return done(true, err);
    }
  }
  
  async _createNewPage(browserData, reuse, done) {
    try {
      const newPage = await this.browser.target.newPage();
      newPage.setDefaultNavigationTimeout(0);
      newPage.setDefaultTimeout(0);
      
      // TEST - FETCH - START
      /*const cdpSession = await newPage.target().createCDPSession();
      await cdpSession.send('Fetch.enable');
      //await cdpSession.send('Network.enable');
      
      //cdpSession.on('Network.requestWillBeSent', async (request) => {
      //  console.log(request);
      //});
      cdpSession.on('Fetch.requestPaused', async (request) => {
       // console.log(request);
        //await cdpSession.send('Fetch.continueRequest', {
        //  requestId: request.requestId
        //});
        if(!request.request.url.startsWith('https://')) {
          try {
            console.log('------------------------HTTPS', request.request.url);
            await cdpSession.send('Fetch.continueRequest', {
              requestId: request.requestId
            });
          }
          catch(err) {
            console.log(err);
          }
        }
        else {
          try {
            console.log('--------------------HTTP', request.request.url.replace('https://', 'http://'));
            await cdpSession.send('Fetch.continueRequest', {
              requestId: request.requestId,
              url: request.request.url.replace('https://', 'http://')
            });
          }
          catch(err) {
            console.log(err);
          }
        }
      });*/
      // TEST - FETCH - END
      
      newPage.on('error', (err) => {
        // WRONG FORMATTED ERROR message from Puppeteer.
        err.stack = err.message;
        const newRowIndex = err.message.indexOf('\n');
        if(-1 !== newRowIndex) {
          err.message = err.message.substring(0, newRowIndex);
        }
        this.logError(() => err, 'abe240b2-e317-464b-8969-e89ef5758128');
      });
      newPage.on('pageerror', (err) => {
        // WRONG FORMATTED ERROR message from Puppeteer.
        err.stack = err.message;
        const newRowIndex = err.message.indexOf('\n');
        if(-1 !== newRowIndex) {
          err.message = err.message.substring(0, newRowIndex);
        }
        this.logBrowserErr(() => err, 'dbd9561f-b570-4f10-ad43-0b726b0bd815');
      });
      newPage.on('console', (msg) => {
        const type = msg.type();
        if('log' === type) {
          this.logBrowserLog(() => msg.text(), 'Puppeteer', '0510ffcd-6e59-4991-aba6-9e8a66c4868b');
        }
        else if('error' === type) {
          this.logBrowserErr(() => msg.text(), 'Puppeteer', '5adbb772-3333-4068-a570-018c2787f712');
        }
        else {
          this.logDebug(() => `${msg.type()}: ${msg.text()}`, 'Puppeteer', 'fdb663d7-9080-428f-b340-eb5371a10602');
        }
      });
      newPage.waitForTransitionEnd = async (element) => {
        return await this.pageAsync.evaluate((element) => {
          return new Promise((resolve) => {
            const transition = document.querySelector(element);
            const onEnd = function () {
              transition.removeEventListener('transitionend', onEnd);
              resolve();
            };
            transition.addEventListener('transitionend', onEnd);
          });
        }, element);
      };
      newPage.console = this.__createNewObject({
        log: async (...params) => {
          try {
            await this.pageAsync.evaluate((...params) => {
              console.log(...params);
            }, ...params);
          } catch(a) {}
        },
        error: async (...params) => {
          try {
            await this.pageAsync.evaluate((...params) => {
              console.error(...params);
            }, ...params);
            } catch(a) {}
        }
      }, 'console');
      newPage.scrollIntoView = async (node) => {
        try {
          const result = await this.pageAsync.evaluate((node) => {
            node.scrollIntoView();
          }, node);
        } catch(a) {}
      };
      newPage.xPathClick = async (xPath) => {
        try {
          const result = await this.pageAsync.evaluate((xPath) => {
            return new Promise((resolve, reject) => {
              function xPathClick(timestamp) {
                const result = document.evaluate(xPath, document, null, 9, null);
                if(result && result.singleNodeValue) {
                  result.singleNodeValue.click();
                  resolve(true);
                }
                else {
                  window.requestAnimationFrame(xPathClick);
                }
              }
              window.requestAnimationFrame(xPathClick);
            });
          }, xPath);
          return result;
        }
        catch(e) {
          return false;
        }
      };
      this.page = this._createNewObject(newPage, `('${browserData.dstAddress.uri}')`);
      this.pageAsync = newPage;
      this._getContext().pages.set(this.id, {
        name: `Page['${browserData.dstAddress.uri}']`,
        page: this.pageAsync
      });
    }
    catch(err) {
      this.logError(err, '6fd2bd0d-520b-461a-921f-d2bbfe2d12a3');
      return done(true, err);
    }
    this.logGuiEvent(() => `... _createNewPage, srcAddress: ${this._stringifyArguments(browserData.srcAddress, 2, this._srcAddressFilter)}, dstAddress: ${this._stringifyArguments(browserData.dstAddress, 2, this._dstAddressFilter)}`, StackApi.LogDataGuiType.NEW_PAGE, {browserData: browserData}, 'Puppeteer', '06ae91b5-457a-4639-95b4-64d7ee7699ea');
    this._gotoPage(browserData, reuse, done);
  }
  
  __createNewObject(object, objectName, log, argumentDepth=1) {
    const self = this;
    self.logGui(() => `new ${objectName}${log ? log : ''}`, 'Puppeteer', 'ae546d6a-0921-466b-be65-ab209ec63b34', 1);
    return new Proxy(object, {
      get: (target, property, receiver) => {
        const withNavigation = 'string' === typeof property && 'undefined' === typeof target[property] && property.endsWith(PuppeteerConnectionClient.WITH_NAVIGATION);
        if('function' === typeof target[property] || withNavigation) {
          return function (...args) {
            const formattedArgs = self._stringifyArguments(args, argumentDepth);
            const currentUrl = self.pageAsync.url();
            const type = typeof target[property];
            const logGuidType = self._logDataGuiType(objectName, property, type, formattedArgs);
            self.logGuiEvent(() => `${objectName}['${currentUrl}'].${property}(${formattedArgs}) ${PuppeteerConnectionClient.CALL}`, logGuidType[0], logGuidType[1], 'Puppeteer', '055e6064-072d-4b7f-b31f-515664fcaf05');
            const result = !withNavigation ? target[property](...args) : Promise.all([self.pageAsync.waitForNavigation(), target[property.substring(0, property.length - PuppeteerConnectionClient.WITH_NAVIGATION_LENGTH)](...args)]);
            if(result instanceof Promise) {
              let cancel = false;
              const externalPendingId = self.actor.setPending(() => {
                self.logGui(() => `${objectName}['${currentUrl}'].${property}(${formattedArgs}) ${PuppeteerConnectionClient.CANCEL}`, 'Puppeteer', '24613f40-34e1-46d8-a904-b468aab20958', 1);
                cancel = true;
              });
//              const timeoutClearId = setTimeout((self, externalPendingId) => {
//                cancel = true;
//                self.actor.pendingError(externalPendingId, new Error('timeout__asdf1234'));
//                console.log('TIMEOUT - TIMEOUT - TIMEOUT - TIMEOUT - TIMEOUT');
//              }, 5000, self, externalPendingId);
              result.then((value) => {
                //clearTimeout(timeoutClearId);
                if(!cancel) {
                  self.logGui(() => `${objectName}['${currentUrl}'].${property}(${formattedArgs}) ${PuppeteerConnectionClient.SUCCESS}`, 'Puppeteer', '6860aee5-0f03-4d35-a42b-47f3f3e4bb0c', 1);
                  if(self.actor.config.breakOnGuiEvent || self.actor.config.slowOnGuiEvent) {
                    if(self._isGuiEvent(property)) {
                      setTimeout((self, externalPendingId, value) => {
                        if(self.actor.config.breakOnGuiEvent) {
                          debugger;
                        }
                        self.actor.pendingSuccess(externalPendingId, self._createNewObject(value, '', false));
                      }, self.actor.config.slowOnGuiEvent ? self.actor.slowAwaitTime.p : 1000, self, externalPendingId, value);
                      return;
                    }
                  }
                  self.actor.pendingSuccess(externalPendingId, self._createNewObject(value, '', false));
                }
              }, (reason) => {
                //clearTimeout(timeoutClearId);
                if(!cancel) {
                  self.logGui(() => `${objectName}['${currentUrl}'].${property}(${formattedArgs}) ${PuppeteerConnectionClient.FAILURE}: ${reason}`, 'Puppeteer', '7b4e047d-d7f8-451e-b97b-78007bc9095b', 1);
                  if(self.actor.config.breakOnGuiEvent || self.actor.config.slowOnGuiEvent) {
                    if(self._isGuiEvent(property)) {
                      setTimeout((self, externalPendingId, reason) => {
                        if(self.actor.config.breakOnGuiEvent) {
                          debugger;
                        }
                        self.actor.pendingError(externalPendingId, reason);
                      }, self.actor.config.slowOnGuiEvent ? self.actor.slowAwaitTime.p : 1000, self, externalPendingId, reason);
                      return;
                    }
                  }
                  self.actor.pendingError(externalPendingId, reason);
                }
              });
            }
            else {
              return result;
            }
          }
        }
        else if('object' === typeof target[property]) {
          return self._createNewObject(target[property], '', false);
        }
        else {
          return target[property];
        }
      }
    });
  }
  
  _createNewObject(object, log, throwBrowserAndPage=true) {
    if(!object) {
      return object; 
    }
    else if(Array.isArray(object)) {
      const newArray = [];
      object.forEach((a) => {
        newArray.push(this._createNewObject(a, '', false));
      });
      return newArray;
    }
    let objectName = object.constructor.name;
    let argumentDepth = 1;
    if(objectName.startsWith('Cdp') || 'Keyboard' === objectName) {
      switch(objectName) {
        case 'CdpBrowser': {
          if(throwBrowserAndPage) {
            if(this.browser) {
              throw new Error('Creating a new browser is not allowed. Create a new PuppeterConnection if you need a new browser.');
            }
            argumentDepth = 2;
          }
          else {
            return object;
          }
          objectName = 'Browser';
          break;
        }
        case 'CdpPage': {
          if(throwBrowserAndPage) {
            if(null !== this.pageAsync) {
              throw new Error('Creating a new page is not allowed. Create a new PuppeterConnection if you need a new page.');
            }
            argumentDepth = 2;
          }
          else {
            return object;
          }
          objectName = 'Page';
          break;
        }
      }
      return this.__createNewObject(object, objectName, log, argumentDepth);
    }
    else {
      return object;
    }
  }
    
  async clear(done) {
    try {
      const context = this._getContext();
      let pendings = context.browsers.size;
      if(0 !== pendings) {
        let i = 0;
        if(0 !== context.pages.size) {
          const promises = [];
          context.pages.forEach((page) => {
            try {
              promises.push(page.page.close({}));
            }
            catch(a) {}
          });
          await Promise.all(promises);
        }
        if(0 !== context.browsers.size) {
          const promises = [];
          context.browsers.forEach((browser) => {
            try {
              promises.push(browser.target.close());
            }
            catch(a) {}
          });
          await Promise.all(promises);
          context.browsers.forEach((browser) => {
          if(browser.proxy) {
              browser.proxy.cleanup(() => {
                if(0 === --pendings) {
                  context.pages.clear();
                  context.browsers.clear();
                  context.incognitoBrowserContexts.clear();
                  done();
                }
              });
            }
            else {
              if(0 === --pendings) {
                context.pages.clear();
                context.browsers.clear();
                context.incognitoBrowserContexts.clear();
                done();
              }
            }
          });
        }
      }
      else {
        process.nextTick(done);
      }
    } catch(a) {
      process.nextTick(done);
    }
  }
  
  mockSelected() {
    this.isMockSelected = true;
  }
  
  _stringifyArguments(args, maxDepth=2, cbObjectFilter=null, depth=0) {
    if(maxDepth === depth) {
      return '';
    }
    if(Array.isArray(args)) {
      const formattedArgs = [];
      args.forEach((arg) => {
        formattedArgs.push(this._stringifyArguments(arg, maxDepth, cbObjectFilter, depth));
      });
      if(0 === depth) {
        return formattedArgs.join(', ');
      }
      else {
        return ['[', formattedArgs.join(', '), ']'].join('');
      }
    }
    else {
      if('string' === typeof args) {
        return `'${args}'`;
      }
      else if('object' === typeof args) {
        const formattedArgs = [];
        let filtered = false;
        for(let property in args) {
          if(null !== cbObjectFilter) {
            if(!cbObjectFilter(property)) {
              filtered = true;
              continue;
            }
          }           
          formattedArgs.push(`${property}: ${this._stringifyArguments(args[property], maxDepth, cbObjectFilter, depth + 1)}`);
        }
        return ['{', formattedArgs.join(', '), filtered ? ', ...}' : '}'].join('');
      }
      else {
        return `${args}`;
      }
    }
  }
  
  _logDataGuiType(objectName, name, type, args) {
    const totalName = `${objectName}.${name}`;
    switch(name) {
      case StackApi.LogDataGuiType.TEXT_ACTION_CLICK:
        return [StackApi.LogDataGuiType.ACTION_CLICK, {name: totalName, args: args}];
      case StackApi.LogDataGuiType.TEXT_ACTION_TYPE:
        return [StackApi.LogDataGuiType.ACTION_TYPE, {name: totalName, args: args}];
      default:
        return [StackApi.LogDataGuiType.FUNCTION_CALL, {name: totalName, args: args}];
    }
  }
  
  _srcAddressFilter(name) {
    if('addressName' === name || 'browser' === name || 'incognitoBrowser' === name) {
      return true;
    }
    else {
      return false;
    }
  }
  
  _dstAddressFilter(name) {
    if('addressName' === name || 'host' === name || 'port' === name|| 'uri' === name) {
      return true;
    }
    else {
      return false;
    }
  }
  
  _getContext() {
    return PuppeteerConnectionClient.context.get(this.executionId);
  }
  
  static initExecution(executionId, debugDashboard) {
    if(!PuppeteerConnectionClient.context.has(executionId)) {
      PuppeteerConnectionClient.context.set(executionId, {
        browsers: new Map(),
        incognitoBrowserContexts: new Map(),
        pages: new Map(),
        mockManager: new PuppeteerMockManager(debugDashboard)
      });
    }
    else {
      this.logError('INIT - already set!', 'fc49a5c4-0c8c-4531-b551-ab2c6480f662');
    }
  }
  
  static exitExecution(executionId) {
    const context = PuppeteerConnectionClient.context.get(executionId);
    context.mockManager.release();
    PuppeteerConnectionClient.context.delete(executionId);
  }
  
  _isGuiEvent(name) {
    switch(name) {
      case StackApi.LogDataGuiType.TEXT_ACTION_CLICK:
      case StackApi.LogDataGuiType.TEXT_ACTION_TYPE:
        return true;
      default:
        return false;
    }
  }
}

PuppeteerConnectionClient.context = new Map();

PuppeteerConnectionClient.WITH_NAVIGATION = 'WithNavigation';
PuppeteerConnectionClient.WITH_NAVIGATION_LENGTH = PuppeteerConnectionClient.WITH_NAVIGATION.length;

PuppeteerConnectionClient.CALL = ' -- async call';
PuppeteerConnectionClient.CONTINUE = ' -- continue';
PuppeteerConnectionClient.SUCCESS = ' -- success';
PuppeteerConnectionClient.FAILURE = ' -- failure';
PuppeteerConnectionClient.CANCEL =  ' -- cancel';

PuppeteerConnectionClient.currentSocketProxyId = 0;

PuppeteerConnectionClient.sequrity = {
  cert: null,
  key: null
};


module.exports = PuppeteerConnectionClient;
