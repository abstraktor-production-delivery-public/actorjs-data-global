
'use strict';

const StackApi = require('stack-api');


class PuppeteerParserTls {
  constructor(id, type) {
    this.id = id;
    this.type = type;
    this.bufferManager = new StackApi.BufferManager(this.id, true);
  }
  
  parse(chunk) {
    this.bufferManager.addBuffer(chunk);
    const header = this.bufferManager.receiveSize(5);
    const messageHeader = {
      contentType: header.readUInt8(0),
      version: header.slice(1, 3),
      length: header.readUInt16BE(3),
      message: null
    };
    if(22 === messageHeader.contentType) {
      return this._parseHandshake(messageHeader);
    }
  }
  
  static getExtention(id, msg) {
    const extension = msg.message.extensions.get(id);
    if(extension) {
      const extensionData = PuppeteerParserTls.EXTENTIONS.get(id);
      if(extensionData) {
        const data = extensionData.method(extension.data, extension.length);
        return data.data;
      }
    }
    
    //let data = extensionData ? extensionData.method(extension.data, extension.length) : undefined;
  }
  
  log(msg) {
    console.log('TLS');
    console.log(`- Handshake Protocol: (${msg.contentType})`);
    console.log(`- Version: (0x0${msg.version[0]}0${msg.version[1]})`);
    console.log(`- Length: (${msg.length})`);
    if(22 === msg.contentType) {
      console.log(`  - Handshake Type: (${msg.message.handshakeType})`);
      console.log(`  - Length: (${msg.message.length})`);
      console.log(`  - Version: (0x0${msg.message.version[0]}0${msg.message.version[1]})`);
      console.log(`  - Random: ${msg.message.random.toString('hex')}`);
      console.log(`  - Session ID Length: ${msg.message.sessionIdLength}`);
      console.log(`  - Session ID: ${msg.message.sessionId.toString('hex')}`);
      console.log(`  - Cipher Suites Length: ${msg.message.cipherSuitesLength}`);
      console.log(`  - Cipher Suites: ${msg.message.cipherSuites.toString('hex')}`);
      console.log(`  - Compression Methods Length: ${msg.message.compressionMethodsLength}`);
      console.log(`  - Compression Methods: (${msg.message.compressionMethods})`);
      console.log(`  - Extensions Length: ${msg.message.extensionsLength}`);
      msg.message.extensions.forEach((extension) => {
        const extensionData = PuppeteerParserTls.EXTENTIONS.get(extension.id);
        const name = extensionData ? extensionData.name : 'UNKNOWN';
        const id = extension.id.toString().padEnd(5, ' ');
        const data = extensionData ? extensionData.method(extension.data, extension.length).log : 'UNKNOWN';
        const ext = `    - Extensions: ${id} ${name}`.padEnd(65, ' ');
        console.log(`${ext} - ${data}`);
      });
    }
    else {
      console.log('UNKNOWN contentType:', msg.version);
    }
  }
  
  _parseHandshake(messageHeader) {
    const payload = this.bufferManager.receiveSize(messageHeader.length);
    const handshakeType = payload.readUInt8(0);
    let offset = 1;
    if(1 === handshakeType) {
      const length = payload.readUIntBE(offset, 3);
      offset += 3;
      const version = payload.slice(offset, offset + 2);
      offset += 2;
      const random = payload.slice(offset, offset + 32);
      offset += 32;
      const sessionIdLength = payload.readUInt8(38);
      offset += 1;
      const sessionId = payload.slice(offset, offset + sessionIdLength);
      offset += sessionIdLength;
      const cipherSuitesLength = payload.readUInt16BE(offset);
      offset += 2;
      const cipherSuites = payload.slice(offset, offset + cipherSuitesLength);
      offset += cipherSuitesLength;
      const compressionMethodsLength = payload.readUInt8(offset);
      offset += compressionMethodsLength;
      const compressionMethods = payload.readUInt8(offset);
      offset += 1;
      const extensionsLength = payload.readUInt16BE(offset);
      offset += 2;
      const extensions = this._parseExtensions(extensionsLength, payload, offset);
      const clientHello = {
        handshakeType: handshakeType,
        length: length,
        version: version,
        random: random,
        sessionIdLength: sessionIdLength,
        sessionId: sessionId,
        cipherSuitesLength: cipherSuitesLength,
        cipherSuites: cipherSuites,
        compressionMethodsLength: compressionMethodsLength,
        compressionMethods: compressionMethods,
        extensionsLength: extensionsLength,
        extensions: extensions
      };
      messageHeader.message = clientHello;
      return messageHeader;
    }
    else {
      ddb.error('handshakeType:', handshakeType, 'is not implemented.');
    }
  }
  
  _parseExtensions(extensionsLength, payload, offset) {
    let length = 0;
    const extensions = new Map();
    while(length < extensionsLength) {
      const extensionId = payload.readUInt16BE(offset);
      offset += 2;
      const extensionLength = payload.readUInt16BE(offset);
      offset += 2;
      const extension = {
        id: extensionId,
        length: extensionLength,
        data: null
      };
      extensions.set(extensionId, extension);
      length += 4 + extensionLength;
      if(0 !== extensionLength) {
        const extensionData = payload.slice(offset, offset + extensionLength);
        offset += extensionLength;
        extension.data = extensionData;
      }
    }
    return extensions;
  }
  
  logBuffer(buffer) {
    
  }
  
  static _parseNotImplemented(buffer, size) {
    return {
      log: 'NOT_IMPLEMENTED'
    };
  }
  
  static _parseAlpn(buffer, size) {
    let offset = 0;
    const result = {
      log: '',
      data: []
    };
    const data = [];
    const aplnExteneionLength = buffer.readUInt16BE(offset);
    offset += 2;
    while(offset < size) {
      const length = buffer.readUInt8(offset);
      offset += 1;
      const alpn = buffer.slice(offset, offset + length);
      offset += length;
      if(!result.log) {
        result.log += alpn.toString();
      }
      else {
        result.log += `, ${alpn.toString()}`;
      }
      result.data.push(alpn.toString());
    }
    return result;
  }
  
  static _parseServerName(buffer, size) {
    let offset = 0;
    const result = {
      log: '',
      type: 0,
      data: ''
    };
    const ServerNameListLength = buffer.readUInt16BE(offset);
    offset += 2;
    while(offset < size) {
      result.type = buffer.readUInt8(offset);
      offset += 1;
      const length = buffer.readUInt16BE(offset);
      offset += 2;
      const name = buffer.slice(offset, offset + length);
      offset += length;
      if(!result.log) {
        result.log += `(${result.type}) ${name.toString()}`;
      }
      else {
        result.log += `, (${result.type}) ${name.toString()}`;
      }
      if(0 === result.type) {
        result.data = name.toString();
      }
    }
    return result;
  }
}

PuppeteerParserTls.EXTENTION_server_name = 0;
PuppeteerParserTls.EXTENTION_max_fragment_length = 1;
PuppeteerParserTls.EXTENTION_client_certificate_url = 2;
PuppeteerParserTls.EXTENTION_trusted_ca_keys = 3;
PuppeteerParserTls.EXTENTION_truncated_hmac = 4;
PuppeteerParserTls.EXTENTION_status_request = 5;
PuppeteerParserTls.EXTENTION_user_mapping = 6;
PuppeteerParserTls.EXTENTION_client_authz = 7;
PuppeteerParserTls.EXTENTION_server_authz = 8;
PuppeteerParserTls.EXTENTION_cert_type = 9;
PuppeteerParserTls.EXTENTION_supported_groups = 10;
PuppeteerParserTls.EXTENTION_ec_point_formats = 11;
PuppeteerParserTls.EXTENTION_srp = 12;
PuppeteerParserTls.EXTENTION_signature_algorithms = 13;
PuppeteerParserTls.EXTENTION_use_srtp = 14;
PuppeteerParserTls.EXTENTION_heartbeat = 15;
PuppeteerParserTls.EXTENTION_application_layer_protocol_negotiation = 16;
PuppeteerParserTls.EXTENTION_status_request_v2 = 17;
PuppeteerParserTls.EXTENTION_signed_certificate_timestamp = 18;
PuppeteerParserTls.EXTENTION_client_certificate_type = 19;
PuppeteerParserTls.EXTENTION_server_certificate_type = 20;
PuppeteerParserTls.EXTENTION_padding = 21;
PuppeteerParserTls.EXTENTION_encrypt_then_mac = 22;
PuppeteerParserTls.EXTENTION_extended_master_secret = 23;
PuppeteerParserTls.EXTENTION_token_binding = 24;
PuppeteerParserTls.EXTENTION_cached_info = 25;
PuppeteerParserTls.EXTENTION_tls_lts = 26;
PuppeteerParserTls.EXTENTION_compress_certificate = 27;
PuppeteerParserTls.EXTENTION_record_size_limit = 28;
PuppeteerParserTls.EXTENTION_pwd_protect = 29;
PuppeteerParserTls.EXTENTION_pwd_clear = 30;
PuppeteerParserTls.EXTENTION_password_salt = 31;
PuppeteerParserTls.EXTENTION_ticket_pinning = 32;
PuppeteerParserTls.EXTENTION_tls_cert_with_extern_psk = 33;
PuppeteerParserTls.EXTENTION_delegated_credentials = 34;
PuppeteerParserTls.EXTENTION_session_ticket = 35;
PuppeteerParserTls.EXTENTION_TLMSP = 36;
PuppeteerParserTls.EXTENTION_TLMSP_proxying = 37;
PuppeteerParserTls.TLMSP_delegate = 38;
PuppeteerParserTls.EXTENTION_supported_ekt_ciphers = 39;
PuppeteerParserTls.EXTENTION_Reserved = 40;
PuppeteerParserTls.EXTENTION_pre_shared_key = 41;
PuppeteerParserTls.EXTENTION_early_data = 42;
PuppeteerParserTls.EXTENTION_supported_versions = 43;
PuppeteerParserTls.EXTENTION_cookie = 44;
PuppeteerParserTls.EXTENTION_psk_key_exchange_modes = 45;
PuppeteerParserTls.EXTENTION_Reserved = 46;
PuppeteerParserTls.EXTENTION_certificate_authorities = 47;
PuppeteerParserTls.EXTENTION_oid_filters = 48;
PuppeteerParserTls.EXTENTION_post_handshake_auth = 49;
PuppeteerParserTls.EXTENTION_signature_algorithms_cert = 50;
PuppeteerParserTls.key_share = 51;
PuppeteerParserTls.EXTENTION_transparency_info = 52;
PuppeteerParserTls.EXTENTION_connection_id = 53;
PuppeteerParserTls.EXTENTION_connection_id = 54;
PuppeteerParserTls.EXTENTION_external_id_hash = 55;
PuppeteerParserTls.EXTENTION_external_session_id = 56;
PuppeteerParserTls.EXTENTION_quic_transport_parameters = 57;
PuppeteerParserTls.EXTENTION_ticket_request = 58;
PuppeteerParserTls.EXTENTION_dnssec_chain = 59;
PuppeteerParserTls.EXTENTION_application_settings = 17513;
PuppeteerParserTls.EXTENTION_renegotiation = 65281;

PuppeteerParserTls.EXTENTIONS = new Map([
  [0, {name:'server_name',method: PuppeteerParserTls._parseServerName}],
  [1, {name:'max_fragment_length',method: PuppeteerParserTls._parseNotImplemented}],
  [2, {name:'client_certificate_url',method: PuppeteerParserTls._parseNotImplemented}],
  [3, {name:'trusted_ca_keys',method: PuppeteerParserTls._parseNotImplemented}],
  [4, {name:'truncated_hmac',method: PuppeteerParserTls._parseNotImplemented}],
  [5, {name:'status_request',method: PuppeteerParserTls._parseNotImplemented}],
  [6, {name:'user_mapping',method: PuppeteerParserTls._parseNotImplemented}],
  [7, {name:'client_authz',method: PuppeteerParserTls._parseNotImplemented}],
  [8, {name:'server_authz',method: PuppeteerParserTls._parseNotImplemented}],
  [9, {name:'cert_type',method: PuppeteerParserTls._parseNotImplemented}],
  [10, {name:'supported_groups',method: PuppeteerParserTls._parseNotImplemented}],
  [11, {name:'ec_point_formats',method: PuppeteerParserTls._parseNotImplemented}],
  [12, {name:'srp',method: PuppeteerParserTls._parseNotImplemented}],
  [13, {name:'signature_algorithms',method: PuppeteerParserTls._parseNotImplemented}],
  [14, {name:'use_srtp',method: PuppeteerParserTls._parseNotImplemented}],
  [15, {name:'heartbeat',method: PuppeteerParserTls._parseNotImplemented}],
  [16, {name:'application_layer_protocol_negotiation',method: PuppeteerParserTls._parseAlpn}],
  [17, {name:'status_request_v2',method: PuppeteerParserTls._parseNotImplemented}],
  [18, {name:'signed_certificate_timestamp',method: PuppeteerParserTls._parseNotImplemented}],
  [19, {name:'client_certificate_type',method: PuppeteerParserTls._parseNotImplemented}],
  [20, {name:'server_certificate_type',method: PuppeteerParserTls._parseNotImplemented}],
  [21, {name:'padding',method: PuppeteerParserTls._parseNotImplemented}],
  [22, {name:'encrypt_then_mac',method: PuppeteerParserTls._parseNotImplemented}],
  [23, {name:'extended_master_secret',method: PuppeteerParserTls._parseNotImplemented}],
  [24, {name:'token_binding',method: PuppeteerParserTls._parseNotImplemented}],
  [25, {name:'cached_info',method: PuppeteerParserTls._parseNotImplemented}],
  [26, {name:'tls_lts',method: PuppeteerParserTls._parseNotImplemented}],
  [27, {name:'compress_certificate',method: PuppeteerParserTls._parseNotImplemented}],
  [28, {name:'record_size_limit',method: PuppeteerParserTls._parseNotImplemented}],
  [29, {name:'pwd_protect',method: PuppeteerParserTls._parseNotImplemented}],
  [30, {name:'pwd_clear',method: PuppeteerParserTls._parseNotImplemented}],
  [31, {name:'password_salt',method: PuppeteerParserTls._parseNotImplemented}],
  [32, {name:'ticket_pinning',method: PuppeteerParserTls._parseNotImplemented}],
  [33, {name:'tls_cert_with_extern_psk',method: PuppeteerParserTls._parseNotImplemented}],
  [34, {name:'delegated_credentials',method: PuppeteerParserTls._parseNotImplemented}],
  [35, {name:'session_ticket',method: PuppeteerParserTls._parseNotImplemented}],
  [36, {name:'TLMSP',method: PuppeteerParserTls._parseNotImplemented}],
  [37, {name:'TLMSP_proxying',method: PuppeteerParserTls._parseNotImplemented}],
  [38, {name:'TLMSP_delegate',method: PuppeteerParserTls._parseNotImplemented}],
  [39, {name:'supported_ekt_ciphers',method: PuppeteerParserTls._parseNotImplemented}],
  [40, {name:'Reserved',method: PuppeteerParserTls._parseNotImplemented}],
  [41, {name:'pre_shared_key',method: PuppeteerParserTls._parseNotImplemented}],
  [42, {name:'early_data',method: PuppeteerParserTls._parseNotImplemented}],
  [43, {name:'supported_versions',method: PuppeteerParserTls._parseNotImplemented}],
  [44, {name:'cookie',method: PuppeteerParserTls._parseNotImplemented}],
  [45, {name:'psk_key_exchange_modes',method: PuppeteerParserTls._parseNotImplemented}],
  [46, {name:'Reserved',method: PuppeteerParserTls._parseNotImplemented}],
  [47, {name:'certificate_authorities',method: PuppeteerParserTls._parseNotImplemented}],
  [48, {name:'oid_filters',method: PuppeteerParserTls._parseNotImplemented}],
  [49, {name:'post_handshake_auth',method: PuppeteerParserTls._parseNotImplemented}],
  [50, {name:'signature_algorithms_cert',method: PuppeteerParserTls._parseNotImplemented}],
  [51, {name:'key_share',method: PuppeteerParserTls._parseNotImplemented}],
  [52, {name:'transparency_info',method: PuppeteerParserTls._parseNotImplemented}],
  [53, {name:'connection_id',method: PuppeteerParserTls._parseNotImplemented}],
  [54, {name:'connection_id',method: PuppeteerParserTls._parseNotImplemented}],
  [55, {name:'external_id_hash',method: PuppeteerParserTls._parseNotImplemented}],
  [56, {name:'external_session_id',method: PuppeteerParserTls._parseNotImplemented}],
  [57, {name:'quic_transport_parameters',method: PuppeteerParserTls._parseNotImplemented}],
  [58, {name:'ticket_request',method: PuppeteerParserTls._parseNotImplemented}],
  [59, {name:'dnssec_chain',method: PuppeteerParserTls._parseNotImplemented}],
  
  [17513, {name:'application_settings',method: PuppeteerParserTls._parseNotImplemented}],
  [65281, {name:'renegotiation',method: PuppeteerParserTls._parseNotImplemented}]
]);

PuppeteerParserTls.REQUEST = 0;
PuppeteerParserTls.RESPONSE = 1;

module.exports = PuppeteerParserTls;
