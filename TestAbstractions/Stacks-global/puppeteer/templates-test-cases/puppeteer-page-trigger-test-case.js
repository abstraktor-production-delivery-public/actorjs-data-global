
'use strict';

const StackApi = require('stack-api');


class PuppeteerPageTriggerTestCase extends StackApi.StackComponentsTemplatesTestCases {

  static createTestCase(testCase, testData) {
    const actor = {
      name: 'Actors-global.wizard.puppeteer.PuppeteerPageTriggerTestCaseOrig',
      type: 'orig',
      phase: 'exec',
      execution: '',
      src: 'DEFAULT_SRC',
      dst: '',
      srv: '',
      testData: '',
      verification: ''
    };
    testCase.actors.push(actor);
    
    let dataAddressing = null;
    let dataHeaders = null;
    testData.forEach((testCaseDatas) => {
      if('single' === testCaseDatas.view) {
      }
      /*else if('optional' === testCaseDatas.view) {
        const optional = testCaseDatas.views[testCaseDatas.checkedOptionIndex];
        if('hardcoded' === optional.name) {
          dataAddressing = PuppeteerPageTriggerTestCase._addAddressHardcoded(optional.data, testCase);
        }
      }*/
    });
    /*
    if(dataAddressing.port) {
      actor.dst = `{"uri":"${dataAddressing.url}","port":"${dataAddressing.port}"}`;
    }
    else {
      if('http' === dataAddressing.protocol) {
        actor.dst = `{"uri":"${dataAddressing.url}","port":"80"}`;
      }
      else if('https' === dataAddressing.protocol) {
        actor.dst = `{"uri":"${dataAddressing.url}","port":"443"}`;
      }
    }*/
  }
  
  static _addAddressHardcoded(data, testCase) {
    const dataAddressing = {
      protocol: '',
      url: '',
      port: '',
      host: ''
    };
    data.forEach((testCaseData) => {
      if(testCaseData.include) {
        if('Protocol' === testCaseData.name) {
          dataAddressing.protocol = testCaseData.value;
          testCase.testDataTestCases.push({
            key: 'transport-type',
            value: 'tls',
            description: `Transport Type - tls`
          });
        }
        if('Url' === testCaseData.name) {
          dataAddressing.url = testCaseData.value;
        }
        else if('Request Target' === testCaseData.name) {
          testCase.testDataTestCases.push({
            key: 'request-target',
            value: testCaseData.value,
            description: `Http Status Line - ${testCaseData.name}`
          });
        }
        else if('Port' === testCaseData.name) {
          dataAddressing.port = testCaseData.value;
        }
        else if('Host' === testCaseData.name) {
          dataAddressing.host = testCaseData.value;
        }
      }
    });
    return dataAddressing;
  }
  
  static _addHeaders(data, testCase) {
    const dataHeaders = {
      host: ''
    };
    data.forEach((testCaseData) => {
      if(testCaseData.include) {
        const testData = {
          key: 'http-headers[]',
          value: `["${testCaseData.name}", "${testCaseData.value}"]`,
          description: `Http Header - ${testCaseData.name}`
        };
        testCase.testDataTestCases.push(testData);
        if('Host' === testCaseData.name) {
          dataHeaders.host = testData;
        }
      }
    });
    return dataHeaders;
  }
}

PuppeteerPageTriggerTestCase.displayName = 'page-trigger-test-case';
PuppeteerPageTriggerTestCase.regressionFriendly = 'yes';

PuppeteerPageTriggerTestCase.template = PuppeteerPageTriggerTestCase._template`

`;

PuppeteerPageTriggerTestCase.validateUrlFunctionString = `
let urlObject = null;
let formattedPreview = preview;
try {
  if('Request Target' === name) {
    formattedPreview = 'http://www.example.com' + preview;
  }
  else if('Host' === name) {
    formattedPreview = 'http://' + preview;
  }
  urlObject = new URL(formattedPreview);
  if('Url' === name) {
    if('/' === urlObject.pathname) {
      if(urlObject.href !== preview + '/') {
        return 3; // ERROR
      }
    }
    else {
      if(urlObject.href !== preview) {
        return 3; // ERROR
      }
    }
  }
  else if('Host' === name && urlObject.host !== preview) {
    return 3; // ERROR
  }
}
catch(err) {
  return 3; // ERROR
}
const protocol = urlObject.protocol;
const protocolPosition = formattedPreview.lastIndexOf(protocol);
const domainExtensionPosition = formattedPreview.lastIndexOf( '.' );
if(0 === protocolPosition && - 1 !== ['http:', 'https:'].indexOf(protocol) && domainExtensionPosition > 2 && formattedPreview.length - domainExtensionPosition > 2) {
  return 1; // SUCCESS
}
else {
  return 3; // ERROR
}`;

PuppeteerPageTriggerTestCase.testDataViews = [
  {
    displayName: 'Puppeteer - Load Page',
    name: 'Puppeteer Page - Test Case',
    view: 'optional',
    views: [
      {
        name: 'hardcoded',
        heading: 'Hardcoded',
        data: [
          {
            name: 'Protocol',
            type: 'radio',
            placeHolder: 'request target',
            values: ['http', 'https'],
            value: 'http',
            valid: 0
          },
          {
            name: 'Domain',
            type: 'input',
            placeHolder: 'domain',
            value: '',
            valid: 0
          },
          {
            name: 'Port',
            type: 'input',
            placeHolder: 'port',
            value: '',
            include: 'optional:false',
            valid: 0
          },
          {
            name: 'Path',
            type: 'input',
            placeHolder: 'path',
            value: '',
            include: 'optional:false'
          },
          {
            name: 'Query',
            type: 'input',
            placeHolder: 'query',
            value: '',
            include: 'optional:false',
            valid: 0
          },
          {
            name: 'Url',
            type: 'preview',
            value: '${Protocol}://${Domain}${Port}${Path}${Query}',
            prefixes: [['Port', ':'], ['Path', '/'], ['Query', '?']],
            validate: PuppeteerPageTriggerTestCase.validateUrlFunctionString,
            valid: 1
          },
          {
            name: 'Request Target',
            type: 'preview',
            value: '/${Path}${Query}',
            prefixes: [['Path', '/'], ['Query', '?']],
            validate: PuppeteerPageTriggerTestCase.validateUrlFunctionString,
            valid: 1
          },
          {
            name: 'Host',
            type: 'preview',
            value: '${Domain}',
            prefixes: [],
            validate: PuppeteerPageTriggerTestCase.validateUrlFunctionString,
            valid: 1
          }
        ]
      },
      {
        name: 'generated',
        heading: 'Generated',
        data: [
          {
            name: 'Request Target',
            type: 'input',
            placeHolder: 'request target',
            value: ''
          },
          {
            name: 'Request Target Port',
            type: 'input',
            placeHolder: 'port',
            value: '80',
            include: 'optional:false'
          }
        ]
      }
    ]
  }
];

PuppeteerPageTriggerTestCase.markupNodes = 2;

PuppeteerPageTriggerTestCase.markup = `\`\`\`seq
Config(nodeWidth: 126, nodeMessageHeight: 17, nodeEventHeight: 10, widthBias: 23, heightBias: 20, lineNumbers: false, border: false, backgroundColor: transparent)

Title: Puppeteer
Nodes[orig, server]
orig -o server[http]: connect
orig => server[http]: GET
server => orig[http]: 200 OK
orig => server[websocket]: websocket frame
server => orig[websocket]: websocket frame
orig -x server[http]: disconnect
\`\`\`
`;


module.exports = PuppeteerPageTriggerTestCase;
