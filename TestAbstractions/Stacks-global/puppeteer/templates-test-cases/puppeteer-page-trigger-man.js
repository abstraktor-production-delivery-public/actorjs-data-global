
'use strict';

const StackApi = require('stack-api');
const GuidGenerator = require('z-abs-corelayer-cs/clientServer/guid-generator');


class PuppeteerPageTriggerMan extends StackApi.StackComponentsTemplatesTestCases {

  static createTestCase(testCase, testData, repoName, sutName, futName, tcName) {
    let name = `Inline${GuidGenerator.create()}`;
    name = name.replaceAll('-', '');
    const actor = {
      name: `Actors-inline.${repoName}.${sutName}.${futName}.${tcName}.${name}Orig`,
      type: 'orig',
      phase: 'exec',
      execution: '',
      src: 'DEFAULT_SRC',
      dst: '',
      srv: '',
      testData: '',
      verification: '',
      inlineCode: ''
    };
    testCase.actors.push(actor);
    const dataAddressing = PuppeteerPageTriggerMan._addAddressHardcoded(testData[0].data, testCase);
    
    if(dataAddressing.port) {
      actor.dst = `{"uri":"${dataAddressing.url}","port":"${dataAddressing.port}"}`;
    }
    else {
      if('http' === dataAddressing.protocol) {
        actor.dst = `{"uri":"${dataAddressing.url}"}`;
      }
      else if('https' === dataAddressing.protocol) {
        actor.dst = `{"uri":"${dataAddressing.url}"}`;
      }
    }
    actor.inlineCode = PuppeteerPageTriggerMan.template({
      actorEnding: 'Orig',
      name: name,
      actorType: 'Originating'
    });
  }
  
  static _addAddressHardcoded(data, testCase) {
    const dataAddressing = {
      protocol: '',
      url: '',
      port: '',
      host: ''
    };
    data.forEach((testCaseData) => {
      if(testCaseData.include) {
        if('Protocol' === testCaseData.name) {
          dataAddressing.protocol = testCaseData.value;
        }
        if('Url' === testCaseData.name) {
          dataAddressing.url = testCaseData.value;
        }
        else if('Port' === testCaseData.name) {
          dataAddressing.port = testCaseData.value;
        }
        else if('Host' === testCaseData.name) {
          dataAddressing.host = testCaseData.value;
        }
      }
    });
    return dataAddressing;
  }
}

PuppeteerPageTriggerMan.displayName = 'page-trigger-man';
PuppeteerPageTriggerMan.regressionFriendly = 'no';

PuppeteerPageTriggerMan.template = PuppeteerPageTriggerMan._template`
'use strict';

const ActorApi = require('actor-api');


class ${'name'}${'actorEnding'} extends ActorApi.Actor${'actorType'} {
  constructor() {
    super();
    this.puppeteerConnection = null;
  }
  
  *data() {
  }
  
  *initClient() {
    this.puppeteerConnection = this.createConnection('puppeteer');
  }
  
  *run() {
    const page = this.puppeteerConnection.page;
  }
  
  *exit(interrupted) {
    this.closeConnection(this.puppeteerConnection);
  }
}

module.exports = ${'name'}${'actorEnding'};
`;

PuppeteerPageTriggerMan.validateUrlFunctionString = `
let urlObject = null;
let formattedPreview = preview;
try {
  if('Request Target' === name) {
    formattedPreview = 'http://www.example.com' + preview;
  }
  else if('Host' === name) {
    formattedPreview = 'http://' + preview;
  }
  urlObject = new URL(formattedPreview);
  if('Url' === name) {
    if('/' === urlObject.pathname) {
      if(urlObject.href !== preview + '/') {
        return 3; // ERROR
      }
    }
    else {
      if(urlObject.href !== preview) {
        return 3; // ERROR
      }
    }
  }
  else if('Host' === name && urlObject.host !== preview) {
    return 3; // ERROR
  }
}
catch(err) {
  return 3; // ERROR
}
const protocol = urlObject.protocol;
const protocolPosition = formattedPreview.lastIndexOf(protocol);
const domainExtensionPosition = formattedPreview.lastIndexOf( '.' );
if(0 === protocolPosition && - 1 !== ['http:', 'https:'].indexOf(protocol) && domainExtensionPosition > 2 && formattedPreview.length - domainExtensionPosition > 2) {
  return 1; // SUCCESS
}
else {
  return 3; // ERROR
}`;

PuppeteerPageTriggerMan.testDataViews = [
  {
    displayName: 'Puppeteer - Load Page',
    name: 'Puppeteer Page - Trigger Man',
    view: 'single',
    data: [
      {
        name: 'Protocol',
        type: 'radio',
        placeHolder: 'request target',
        values: ['http', 'https'],
        value: 'http',
        valid: 0
      },
      {
        name: 'Domain',
        type: 'input',
        placeHolder: 'domain',
        value: '',
        valid: 0
      },
      {
        name: 'Port',
        type: 'input',
        placeHolder: 'port',
        value: '',
        include: 'optional:false',
        valid: 0
      },
      {
        name: 'Path',
        type: 'input',
        placeHolder: 'path',
        value: '',
        include: 'optional:false',
        val 0
      },
      {
        name: 'Query',
        type: 'input',
        placeHolder: 'query',
        value: '',
        include: 'optional:false',
        valid: 0
      },
      {
        name: 'Url',
        type: 'preview',
        value: '${Protocol}://${Domain}${Port}${Path}${Query}',
        prefixes: [['Port', ':'], ['Path', '/'], ['Query', '?']],
        validate: PuppeteerPageTriggerMan.validateUrlFunctionString,
        valid: 1
      },
      {
        name: 'Request Target',
        type: 'preview',
        value: '/${Path}${Query}',
        prefixes: [['Path', '/'], ['Query', '?']],
        validate: PuppeteerPageTriggerMan.validateUrlFunctionString,
        valid: 1
      },
      {
        name: 'Host',
        type: 'preview',
        value: '${Domain}',
        prefixes: [],
        validate: PuppeteerPageTriggerMan.validateUrlFunctionString,
        valid: 1
      }
    ]
  }
];

PuppeteerPageTriggerMan.markupNodes = 2;

PuppeteerPageTriggerMan.markup = `\`\`\`seq
Config(nodeWidth: 126, nodeMessageHeight: 17, nodeEventHeight: 10, widthBias: 23, heightBias: 20, lineNumbers: false, border: false, backgroundColor: transparent)

Title: Puppeteer
Nodes[orig, server]
orig -o server[http]: connect
orig => server[http]: GET
server => orig[http]: 200 OK
orig => server[websocket]: websocket frame
server => orig[websocket]: websocket frame
orig -x server[http]: disconnect
\`\`\`
`;


module.exports = PuppeteerPageTriggerMan;
