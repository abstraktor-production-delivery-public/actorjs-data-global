
'use strict';


class PuppeteerStyle {
  constructor(index) {
    this.index = index;
    this.textColor = 'black';
    this.textProtocolColor = 'aliceblue';
    this.protocolColor = 'ForestGreen';
    this.protocolBackgroundColor = 'rgba(34 139  34, 0.3)';
  }
}

module.exports = PuppeteerStyle;
