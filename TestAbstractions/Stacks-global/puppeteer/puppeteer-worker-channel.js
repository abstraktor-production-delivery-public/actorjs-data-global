
'use strict';


class PuppeteerWorkerChannel {
  constructor(channel) {
    this.responses = new Map();
    this.currentResponseId = 0;
    this.channel = channel;
    this.cbExit = null;
  }
  
  request(msgId, done, transferList, ...params) {
    const responseId = this.currentResponseId++;
    this.responses.set(responseId, {
      msgId,
      type: 'request',
      done
    });
    this.channel.postMessage({
      type: PuppeteerWorkerChannel.REQUEST,
      msgId: msgId,
      responseId: responseId,
      params: params
    }, transferList);
  }
  
  response(message, ...params) {
    this.channel.postMessage({
      type: PuppeteerWorkerChannel.RESPONSE,
      responseId: message.responseId,
      msgId: message.msgId,
      params: params
    });
  }
  
  method(msgId, done, transferList, ...params) {
    const responseId = this.currentResponseId++;
    this.responses.set(responseId, {
      msgId,
      type: 'method',
      done
    });
    this.channel.postMessage({
      type: PuppeteerWorkerChannel.METHOD,
      msgId: msgId,
      responseId: responseId,
      params: params
    }, transferList);
  }
  
  message(msgId, transferList, ...params) {
    this.channel.postMessage({
      type: PuppeteerWorkerChannel.MESSAGE,
      msgId: msgId,
      params: params
    }, transferList);
  }
  
  handleResponse(message, cb) {
    const cbResponse = this.responses.get(message.responseId);
    if(cb) {
      cb(message.msgId, ...message.params);
    }
    if(cbResponse) {
      this.responses.delete(message.responseId);
      cbResponse.done(...message.params);
      if(this.cbExit) {
        if(0 === this.responses.size) {
          this.cbExit();
        }    
      }
    }
  }
  
  exit(cbExit) {
    if(0 === this.responses.size) {
      cbExit();
    }
    else {
      this.cbExit = cbExit;
    }
  }
}

PuppeteerWorkerChannel.REQUEST = 0;
PuppeteerWorkerChannel.RESPONSE = 1;
PuppeteerWorkerChannel.METHOD = 2;
PuppeteerWorkerChannel.MESSAGE = 3;

PuppeteerWorkerChannel.TYPES = ['REQUEST', 'RESPONSE', 'METHOD', 'MESSAGE'];

module.exports = PuppeteerWorkerChannel;
