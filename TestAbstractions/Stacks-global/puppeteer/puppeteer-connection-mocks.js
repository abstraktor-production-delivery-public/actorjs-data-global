
'use strict';


class PuppeteerConnectionMocks {
  constructor() {
    this.servers = [];
  }
  
  setMocks(servers) {
    this.servers = servers;
  }
  
  clear() {
    this.servers = [];
  }
}


module.exports = PuppeteerConnectionMocks;
