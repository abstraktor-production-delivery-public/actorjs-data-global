
'use strict';


class PuppeteerConnectionClientOptions {
  constructor() {
    this.headless = false;
    this.slowMo = 0;
    this.puppeteerActionSlowMo = 0;
    this.launchOptions = {
      headless: this.headless,
      slowMo: this.slowMo,
      ignoreHTTPSErrors: true,
      protocolTimeout: 0,
      args: [
        '--proxy-bypass-list=<-loopback>;*.google.com,;*.googleapis.com;sdk.privacy-center.org',
        '--disable-dev-shm-usage',
        '--no-default-browser-check',
        '--browser-test',
        '--no-initial-navigation',
        '--disable-pushstate-throttle',
        '--window-position=720,0',
        '--window-size=1200,770',
        //'--auto-open-devtools-for-tabs',
        '--disable-web-security',
        '--ignore-certificate-errors'
      ],
      ignoreDefaultArgs: [
        "--enable-automation"
      ],
      defaultViewport: {
        width: 1200,
        height: 684
      }
    };
    this.proxy = false;
    this.puppeteerNavigationOptimization = false;
    this.puppeteerTestOptimization = false;
  }
  
  clone() {
    return new PuppeteerConnectionClientOptions();
  }
  
  addArg(name, value, isMono=true) {
    const args = this.launchOptions.args;
    for(let i = 0; i < args.length; ++i) {
      const arg = args[i];
      if(arg.startsWith(name)) {
        if(!isMono) {
          args[i] = `${arg},${value}`;
        }
        else {
          return false;
        }
      }
      else {
        args.push(`${name}=${value}`);
        return true;
      }
    }
  }
  
  getTestData(actor) {
    const headless = actor.getTestDataString('puppeteer-headless', this.headless);
    if('true' === headless) {
      this.launchOptions.headless = true;
    }
    else if('false' === headless) {
      this.launchOptions.headless = false;
    }
    else {
      this.launchOptions.headless = headless;
    }
    this.slowMo = actor.getTestDataNumber('puppeteer-slow-mo', this.slowMo);
    this.puppeteerActionSlowMo = actor.getTestDataNumber('puppeteer-action-slow-mo', this.puppeteerActionSlowMo);
    this.launchOptions.slowMo = this.slowMo;
    this.proxy = actor.getTestDataBoolean('puppeteer-use-proxy', this.proxy);
    this.puppeteerNavigationOptimization = actor.getTestDataBoolean('puppeteer-navigation-optimization', this.puppeteerNavigationOptimization);
    this.puppeteerTestOptimization = actor.getTestDataBoolean('puppeteer-test-optimization', this.puppeteerTestOptimization);
  }
  
  supportInterception(testData) {
    const data = testData.getTestData('puppeteer-use-proxy');
    if(data) {
      return 'true' === data.value;
    }
    else {
      return false;
    }
  }
}

module.exports = new PuppeteerConnectionClientOptions();
