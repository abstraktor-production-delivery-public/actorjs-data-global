
'use strict';

const PuppeteerParser = require('./puppeteer-parser');
const PuppeteerParserHttp11 = require('./puppeteer-parser-http11');
const PuppeteerParserHttp2 = require('./puppeteer-parser-http2');
const PuppeteerParserWebsocket = require('./puppeteer-parser-websocket');
const PuppeteerConnectionSocket = require('./puppeteer-connection-socket');
const HttpConst = require('../http/http-const');
const HttpConstMethod = require('../http/http-const-method');
const HttpConstStatusCode = require('../http/http-const-status-code');
const HttpConstHeader = require('../http/http-const-header');


class PuppeteerConnectionSocketHandler {
  constructor(id, connectionHandler, mocks) {
    this.id = id;
    this.connectionHandler = connectionHandler;
    this.mocks = mocks;
    this.browserData = null;
    this.incommingParser = new PuppeteerParser(id, PuppeteerParser.IN, this);
    this.incommingParseMethod = this._parseHttp.bind(this);
    this.outgoingParseMethod = this._parseHttp.bind(this);
    this.pendingRequests = [];
    this.firstMessageToClientReceived = false;
  }
  
  setBrowserData(browserData) {
    this.browserData = browserData;
  }
  
  onClientUpgraded(socket, alpn) {
    let outgoingSut = null;
    if('h2' === alpn) {
      outgoingSut = new PuppeteerConnectionSocket(this.id, 'OUTGOING-handler', 'TLS', new PuppeteerParserHttp2(this.id, PuppeteerParserHttp2.RESPONSE, this.connectionHandler.contentCache), PuppeteerConnectionSocketHandler.STATUS_HEADERS, PuppeteerConnectionSocketHandler.SOURCE_SUT, PuppeteerConnectionSocketHandler.SOURCE_INCOMMING);
      this.outgoingParseMethod = this._parseHttp2.bind(this);
    }
    else {
      outgoingSut = new PuppeteerConnectionSocket(this.id, 'OUTGOING-handler', 'TLS', new PuppeteerParserHttp11(this.id, PuppeteerParserHttp11.RESPONSE), PuppeteerConnectionSocketHandler.STATUS_HEADERS, PuppeteerConnectionSocketHandler.SOURCE_SUT);
    }
    outgoingSut.setSocket(socket);
    this.connectionHandler.socketPair.setOutgoingSocket(outgoingSut);
  }
  
  onServerUpgraded(socket, alpn) {
    let incomingSut = null;
    if('h2' === alpn) {
      incomingSut = new PuppeteerConnectionSocket(this.id, 'INCOMING-handler', 'TLS', new PuppeteerParserHttp2(this.id, PuppeteerParserHttp2.REQUEST, this.connectionHandler.contentCache), PuppeteerConnectionSocketHandler.STATUS_HEADERS, PuppeteerConnectionSocketHandler.SOURCE_INCOMMING, PuppeteerConnectionSocketHandler.SOURCE_SUT);
      this.incommingParseMethod = this._parseHttp2.bind(this);
    }
    else {
      incomingSut = new PuppeteerConnectionSocket(this.id, 'INCOMING-handler', 'TLS', new PuppeteerParserHttp11(this.id, PuppeteerParserHttp11.REQUEST, this.replaceHeaderNames), PuppeteerConnectionSocketHandler.STATUS_HEADERS, PuppeteerConnectionSocketHandler.SOURCE_INCOMMING, PuppeteerConnectionSocketHandler.SOURCE_SUT);
    }
    incomingSut.setSocket(socket);
    this.connectionHandler.socketPair.setIncommingSocket(incomingSut);
  }
  
  onIncommingData(chunk) {
    this.incommingParseMethod(PuppeteerConnectionSocketHandler.SOURCE_INCOMMING, chunk);
  }
  
  onOutgoingData(chunk, mockId) {
    this.outgoingParseMethod(mockId, chunk);
  }
  
  _writeHttpHeaders(directionFrom) {
    const parser = directionFrom.parser;
    const chunk = this._createHeader(parser);
    const directionTo = this._getConnectionSocket(directionFrom.dstId);
    if(directionTo.valid) {
      const sent = directionTo.write(chunk, () => {
        if(!sent) {
          directionFrom.resume();
        }
        if(PuppeteerParserHttp11.REQUEST === parser.type) {
		      this.connectionHandler.logSendMessage(parser.message, directionFrom.dstId);
        }
        else {
          this.connectionHandler.logMessageReceive(parser.message, directionFrom.srcId);
        }
      });
      if(!sent) {
        directionFrom.pause();
      }
    }
  }
  
  _writeHttpBodyBuffers(directionFrom, rawBody) {
    const parser = directionFrom.parser;
    const chunk = rawBody.shift();
    const contentType = parser.message.getHeader(HttpConstHeader.CONTENT_TYPE);
    const contentEncoding = parser.message.getHeader(HttpConstHeader.CONTENT_ENCODING);
    const transferEncoding = parser.message.getHeader(HttpConstHeader.TRANSFER_ENCODING);
    const directionTo = this._getConnectionSocket(directionFrom.dstId);
    if(directionTo.valid) {
      const sent = directionTo.write(chunk, () => {
        if(!sent) {
          if(0 !== rawBody.length) {
            this._writeHttpBodyBuffers(directionFrom, rawBody);
          }
          else {
            directionFrom.resume();
          }
        }
        if(PuppeteerParserHttp11.REQUEST === parser.type) {
          this.connectionHandler.logMessageBodySend(chunk, contentType, contentEncoding, transferEncoding, directionFrom.dstId);
        }
        else {
          this.connectionHandler.logMessageBodyReceive(chunk, contentType, contentEncoding, transferEncoding, directionFrom.srcId);
        }
      });
      if(!sent) {
        directionFrom.pause();
      }
      else {
        if(0 !== rawBody.length) {
          this._writeHttpBodyBuffers(directionFrom, rawBody);
        }
      }
    }
  }
  
  _writeHttpBody(directionFrom) {
    const parser = directionFrom.parser;
    if(0 !== parser.rawBody.length) {
      const rawBody = parser.rawBody;
      parser.rawBody = [];
      this._writeHttpBodyBuffers(directionFrom, rawBody);
    }
  }
  
  _writeHttpFirstPart(directionFrom) {
    this._writeHttpHeaders(directionFrom);
    this._writeHttpBody(directionFrom, directionFrom.parser);
  }
  
  _createHeader(parser) {
    let size = 0;
    const message = parser.message;
    if(PuppeteerParserHttp11.REQUEST === parser.type) {
      size += message.method.length + message.requestTarget.length + message.httpVersion.length + 4;
    }
    else {
      size += message.httpVersion.length + message.statusCodeString.length + message.reasonPhrase.length + 4;
    }
    
    const replacedHeaders = new Map();
    message.headers.forEach((values, name) => {
      const newName = parser.replaceHeaderNames.get(name);
      if(newName) {
        name = newName;
      }
      replacedHeaders.set(name, values);
      size += name.length + 2;
      values.forEach((value) => {
        size += value.length + 2;
      });
    });
    size += 2;
    const buffer = Buffer.allocUnsafeSlow(size);
    let pos = 0;
    if(PuppeteerParserHttp11.REQUEST === parser.type) {
      pos += buffer.write(message.method, pos, message.method.length);
      pos += buffer.write(HttpConst.SP, pos, HttpConst.SP_LENGTH);
      pos += buffer.write(message.requestTarget, pos, message.requestTarget.length);
      pos += buffer.write(HttpConst.SP, pos, HttpConst.SP_LENGTH);
      pos += buffer.write(message.httpVersion, pos, message.httpVersion.length);
    }
    else {
      pos += buffer.write(message.httpVersion, pos, message.httpVersion.length);
      pos += buffer.write(HttpConst.SP, pos, HttpConst.SP_LENGTH);
      pos += buffer.write(message.statusCodeString, pos, message.statusCodeString.length);
      pos += buffer.write(HttpConst.SP, pos, HttpConst.SP_LENGTH);
      pos += buffer.write(message.reasonPhrase, pos, message.reasonPhrase.length);
    }
    pos += buffer.write(HttpConst.CR_CL, pos, HttpConst.CR_CL_LENGTH);
    
    message.headers = replacedHeaders; 
    message.headers.forEach((values, name) => {
      pos += buffer.write(name, pos, name.length);
      pos += buffer.write(HttpConst.COLON_SP, pos, HttpConst.COLON_SP_LENGTH);
      for(let i = 0; i < values.length - 1; ++i) {
        pos += buffer.write(values[i], pos, values[i].length);
        pos += buffer.write(HttpConst.COMMA_SP, pos, HttpConst.COMMA_SP_LENGTH);
      }
      pos += buffer.write(values[values.length - 1], pos, values[values.length - 1].length);
      pos += buffer.write(HttpConst.CR_CL, pos, HttpConst.CR_CL_LENGTH);
    });
    pos += buffer.write(HttpConst.CR_CL, pos, HttpConst.CR_CL_LENGTH);
    return buffer;
  }
  
  _connectToSut(directionFrom, isConnect, done) {
     this.connectionHandler.resolveUrl(directionFrom.parser.message.requestTarget, directionFrom.remoteFamily, isConnect, (err, address, port) => {
       const isHttps = 443 === port;
       if(err) {
         this.connectionHandler.dnsFailure(directionFrom.parser.message.requestTarget, err);
         done(err);
       }
       else {
         this.connectionHandler.connect(address, port, 443 === port ? 'https' : 'http', done);
       }
    });
  }
  
  _handleConnectedHttpMessage(directionFrom) {
    if(PuppeteerParserHttp11.REQUEST === directionFrom.type) {
      this._writeHttpFirstPart(directionFrom);
    }
    else {
      const response = directionFrom.parser.message;
      if(HttpConstStatusCode.SwitchingProtocols === response.statusCode) {
        if('Upgrade' === response.getHeader(HttpConstHeader.CONNECTION) && 'websocket' === response.getHeader(HttpConstHeader.UPGRADE)) {
          this.incommingParseMethod = this._parseWebSocket.bind(this);
          this.outgoingParseMethod = this._parseWebSocket.bind(this);
          this._writeHttpFirstPart(directionFrom);
          const directionTo = this._getConnectionSocket(directionFrom.dstId);
          if(directionTo.valid) {
            directionTo.parser = new PuppeteerParserWebsocket(this.id, PuppeteerParserWebsocket.SERVER);
            directionFrom.parser = new PuppeteerParserWebsocket(this.id, PuppeteerParserWebsocket.CLIENT);
          }
        }
      }
      else {
        this._writeHttpFirstPart(directionFrom);
      }
    }
  }
  
  _parseHttp(sourceId, chunk) {
    const directionFrom = this._getConnectionSocket(sourceId);
    const parser = directionFrom.parser;
    const ready = parser.parse(chunk);
    if(PuppeteerConnectionSocketHandler.STATUS_HEADERS === directionFrom.state && parser.isHeadersReady()) {
      if(ready) {
        directionFrom.parser.reset();
      }
      else {
        directionFrom.state = PuppeteerConnectionSocketHandler.STATUS_BODY;
      }
      if(PuppeteerParserHttp11.REQUEST === parser.type) {
        if(PuppeteerConnectionSocketHandler.SOURCE_INCOMMING !== sourceId) {
          return ddb.error('Request from Server or Mock not allowed!, dropping request!');
        }
        const sutOrMockId = this.connectionHandler.select('http-header', parser);
        const connected = this.connectionHandler.isConnected(sutOrMockId);
        directionFrom.setDestination(sutOrMockId);
        this.pendingRequests.push({
          srcId: sourceId,
          dstId: sutOrMockId,
          responseParser: null
        });
        if(connected) {
          if(!this.firstMessageToClientReceived) {
            if(ready) {
              this.firstMessageToClientReceived = true;
              this.connectionHandler.stateMachine.setDataReceived()
            }
          }
          this._handleConnectedHttpMessage(directionFrom);
        }
        else {
          if(HttpConstMethod.CONNECT === parser.message.method) {
            this._connectToSut(directionFrom, true, (err) => {
              if(!err) {
                this.connectionHandler.binder.setConnected();
                if(directionFrom.valid) {
                  const dstAddress = this.browserData.dstAddress;
                  if(`${dstAddress.domain}:${dstAddress.port}` === directionFrom.parser.message.requestTarget) {
                    if(dstAddress.uri.startsWith('https://')) {
                      directionFrom.write('HTTP/1.1 200 Connection established\r\nProxy-agent: ActorJs-Proxy\r\nProxy-Connection: keep-alive\r\n\r\n');
                    }
                    else {
                      directionFrom.write('HTTP/1.1 502 Bad Gateway\r\nProxy-agent: ActorJs-Proxy\r\n\r\n');
                    }
                  }
                  else {
                    directionFrom.write('HTTP/1.1 200 Connection established\r\nProxy-agent: ActorJs-Proxy\r\nProxy-Connection: keep-alive\r\n\r\n');
                  }
                }
              }
              else {
                if(directionFrom.valid) {
                  directionFrom.write('HTTP/1.1 502 Bad Gateway\r\nProxy-agent: ActorJs-Proxy\r\n\r\n');
                }
              }
              parser.reset();
            });
          }
          else {
            if(-1 === sutOrMockId) {
              this._connectToSut(directionFrom, false, (err) => {
                if(!err) {
                  this._writeHttpFirstPart(directionFrom);
                }
                else {
                  console.log('ERROR: connecting to SUT. GET');
                }
              });
            }
            else {
              this.connectionHandler.connectToMock(sutOrMockId, (err) => {
                if(!err) {
                  this._writeHttpFirstPart(directionFrom);
                }
                else {
                  console.log('ERROR: connecting to MOCK.', sutOrMockId);
                }
              });
            }
          }
        }
      }
      else { // RESPONSE
        if(PuppeteerConnectionSocketHandler.SOURCE_INCOMMING === sourceId) {
          return ddb.error.log('Response from Client not allowed!, dropping response!');
        }
        if(0 === this.pendingRequests) {
          return ddb.error.log('Response without matching request!, dropping response!');
        }
        directionFrom.setDestination(PuppeteerConnectionSocketHandler.SOURCE_INCOMMING);
        if(sourceId === this.pendingRequests[0].dstId) {
          this.pendingRequests.shift();
          if(!this.firstMessageToClientReceived) {
            if(ready) {
              this.firstMessageToClientReceived = true;
              this.connectionHandler.stateMachine.setDataReceived()
            }
          }
          this._handleConnectedHttpMessage(directionFrom);
          for(let i = 1; i < this.pendingRequests; ++i) {
            const pendingRequest = this.pendingRequests[i];
            if(sourceId === pendingRequest.dstId && null !== pendingRequest.responseParser) {
            }
          }
        }
        else {
          for(let i = 1; i < this.pendingRequests; ++i) {
            const pendingRequest = this.pendingRequests[i];
            if(sourceId === pendingRequest.dstId && null === pendingRequest.responseParser) {
              pendingRequest.responseParser = parser;
            }
          }
        }
      }
    }
    else if(PuppeteerConnectionSocketHandler.STATUS_BODY === directionFrom.state) {
      if(!this.firstMessageToClientReceived) {
        if(ready) {
          this.firstMessageToClientReceived = true;
          this.connectionHandler.stateMachine.setDataReceived()
        }
      }
      this._writeHttpBody(directionFrom, parser);
      if(ready) {
        parser.reset();
        directionFrom.state = PuppeteerConnectionSocketHandler.STATUS_HEADERS;
      }
    }
  }
  
  _parseHttp2(sourceId, chunk) {
    const directionFrom = this._getConnectionSocket(sourceId);
    const parser = directionFrom.parser;
    const directionTo = this._getConnectionSocket(directionFrom.dstId);
    if(directionTo?.valid) {
      directionTo.write(chunk, () => {
        const ready = parser.parse(chunk);
        if(ready) {
          const resultFrameDatas = parser.resultFrameDatas;
          parser.resultFrameDatas = [];
          resultFrameDatas.forEach((frameData) => {
            if(PuppeteerParserHttp2.REQUEST === parser.type) {
              this.connectionHandler.logSendFrame(frameData.frame, frameData.dataBuffers, directionFrom.dstId);
            }
            else {
              this.connectionHandler.logReceiveFrame(frameData.frame, frameData.dataBuffers, directionFrom.srcId);
            }
          });
        }
      });
    }
  }
  
  _parseWebSocket(sourceId, chunk) {
    const directionFrom = this._getConnectionSocket(sourceId);
    const parser = directionFrom.parser; 
    const ready = parser.parse(chunk);
    if(ready) {
      const directionTo = this._getConnectionSocket(directionFrom.dstId);
      if(directionTo.valid) {
        const sent = directionTo.write(chunk, () => {
          if(!sent) {
            directionFrom.resume();
          }
          if(PuppeteerParserWebsocket.CLIENT === parser.type) {
            this.connectionHandler.logMessageReceiveWebSocket(parser.message, directionFrom.srcId);
          }
          else {
            this.connectionHandler.logMessageSendWebSocket(parser.message, directionFrom.dstId);
          }
        });
        if(!sent) {
          directionFrom.pause();
        }
      }
    }
  }
  
  _getConnectionSocket(sourceId) {
    if(PuppeteerConnectionSocketHandler.SOURCE_INCOMMING === sourceId) {
      return this.connectionHandler.socketPair.incommingSocket;
    }
    else if(PuppeteerConnectionSocketHandler.SOURCE_SUT === sourceId) {
      return this.connectionHandler.socketPair.outgoingSocket;
    }
    else {
      return this.mocks.servers[sourceId].outgoing;
    }
  }
}

PuppeteerConnectionSocketHandler.replaceHeaderNames = new Map([['Proxy-Connection', 'Connection']]);

PuppeteerConnectionSocketHandler.STATUS_HEADERS = 0;
PuppeteerConnectionSocketHandler.STATUS_BODY = 1;
PuppeteerConnectionSocketHandler.STATUS = ['STATUS_HEADERS', 'STATUS_BODY'];

PuppeteerConnectionSocketHandler.SOURCE_INCOMMING = -2;
PuppeteerConnectionSocketHandler.SOURCE_SUT = -1;


module.exports = PuppeteerConnectionSocketHandler;
