
'use strict';

const PuppeteerStateMachine = require('./puppeteer-state-machine');


class PuppeteerConnectionHandlerBinder {
  constructor(bindObject) {
    this.inTcpData = this._inTcpData.bind(bindObject);
    this.inTcpEnd = this._inTcpEnd.bind(bindObject);
    this.inTcpClose = this._inTcpClose.bind(bindObject);
    this.inTcpError = this._inTcpError.bind(bindObject);
    this.inTlsData = this._inTlsData.bind(bindObject);
    this.inTlsEnd = this._inTlsEnd.bind(bindObject);
    this.inTlsClose = this._inTlsClose.bind(bindObject);
    this.inTlsError = this._inTlsError.bind(bindObject);
    this.outTcpData = this._outTcpData.bind(bindObject);
    this.outTcpEnd = this._outTcpEnd.bind(bindObject);
    this.outTcpClose = this._outTcpClose.bind(bindObject);
    this.outTcpError = this._outTcpError.bind(bindObject);
    this.outTlsData = this._outTlsData.bind(bindObject);
    this.outTlsEnd = this._outTlsEnd.bind(bindObject);
    this.outTlsClose = this._outTlsClose.bind(bindObject);
    this.outTlsError = this._outTlsError.bind(bindObject);
    this.bindObject = bindObject;
    this.tls = false;
    this.connected = false;
  }
    
  setIcommingTls() {
    this.tls = true;
  }
  
  setConnected() {
    this.connected = true;
  }
  
  _inTcpData(chunk) {
    if(!this.binder.tls) {
      this.socketHandler.onIncommingData(chunk);
    }
    else {
      this.binder.tls = false;
      this.stateMachine.stateImpl.socketPair.incommingSocket.off('data', this.binder.inTcpData);
      this.stateMachine.onAction(PuppeteerStateMachine.ACTION_C_UPGRADE, chunk);
    }
  }
    
  _inTcpEnd() {
    this.stateMachine.onAction(PuppeteerStateMachine.ACTION_S_END, this.socketPair);
  }
  
  _inTcpClose(hadError) {
    this.stateMachine.onAction(PuppeteerStateMachine.ACTION_S_CLOSE, this.socketPair);
  }
  
  _inTcpError(err) {
    //ddb.error(`PUPPETEER.incommingSocket[${this.id}, tcp] state:`, PuppeteerStateMachine.STATES[this.stateMachine.state], ', Error:', err);
    this.stateMachine.onAction(PuppeteerStateMachine.ACTION_S_ERROR, err);    
  }
  
  _inTlsData(chunk) {
    this.socketHandler.onIncommingData(chunk);
  }
  
  _inTlsEnd() {
    this.stateMachine.onAction(PuppeteerStateMachine.ACTION_S_END, this.socketPair);
  }
  
  _inTlsClose(hadError) {
    this.stateMachine.onAction(PuppeteerStateMachine.ACTION_S_CLOSE, this.socketPair);
  }
  
  _inTlsError(err) {
    //ddb.error(`PUPPETEER.incommingSocket[${this.id}, tls] state:`, PuppeteerStateMachine.STATES[this.stateMachine.state], ', Error:', err);
    this.stateMachine.onAction(PuppeteerStateMachine.ACTION_S_ERROR, err);
  }
  
  _outTcpData(chunk) {
    this.socketHandler.onOutgoingData(chunk, -1);
  }
  
  _outTcpEnd() {
    this.stateMachine.onAction(PuppeteerStateMachine.ACTION_C_END, this.socketPair);
  }
  
  _outTcpClose(hadError) {
    this.stateMachine.onAction(PuppeteerStateMachine.ACTION_C_CLOSE);
  }
  
  _outTcpError(err) {
    //ddb.error(`PUPPETEER.outgoingSocket[${this.id}, tcp] state:`, PuppeteerStateMachine.STATES[this.stateMachine.state], ', Error:', err);
    this.stateMachine.onAction(PuppeteerStateMachine.ACTION_C_ERROR, err);
  }
  
  _outTlsData(chunk) {
    this.socketHandler.onOutgoingData(chunk, -1);
  }
  
  _outTlsEnd() {
    this.stateMachine.onAction(PuppeteerStateMachine.ACTION_C_END, this.socketPair);
  }
  
  _outTlsClose() {
    this.stateMachine.onAction(PuppeteerStateMachine.ACTION_C_CLOSE);
  }
  
  _outTlsError(err) {
    //ddb.error(`PUPPETEER.outgoingSocket[${this.id}, tls] state:`, PuppeteerStateMachine.STATES[this.stateMachine.state], ', Error:', err);
    this.stateMachine.onAction(PuppeteerStateMachine.ACTION_C_ERROR, err);
  }
}


module.exports = PuppeteerConnectionHandlerBinder;
