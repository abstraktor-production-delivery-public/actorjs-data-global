
'use strict';

const PuppeteerMockWorkerMain = require('./puppeteer-mock-worker-main');
const PuppeteerMockWorkerThreadPath = require.resolve('./puppeteer-mock-worker-thread');
const StackApi = require('stack-api');
const { MessageChannel } = require('worker_threads');


class PuppeteerMockManager {
  constructor(debugDashboard) {
    this.localMockServerDatas = [];
    this.nbrOfRefs = 0;
    this.lock = false;
    this.lockQueue = [];
    this.workerPool = new StackApi.WorkerPool();
//    this.gauge = debugDashboard.gaugeManager.create('Puppeteer', 'MockManager', 'Refs', 0, 12);
  }
  
  release() {
    this.workerPool.release();
  }
  
  handleLock() {
    const lockData = this.lockQueue.shift();
    this.lock = false;
    if('ref' === lockData.type) {
      this.ref(lockData.mockServers, lockData.host, lockData.srcAddress, lockData.done);
    }
    else if('unref' === lockData.type) {
      this.unref(lockData.done);
    }
  }
  
  ref(mockServers, host, srcAddress, done) {
    if(!this.lock) {
      this.lock = true;
      this._ref(mockServers, host, srcAddress, (err, mockServerDatas) => {
        done(err, mockServerDatas);
        if(0 === this.lockQueue.length) {
          this.lock = false;
        }
        else {
          this.handleLock();
        }
      });
    }
    else {
      this.lockQueue.push({
        type: 'ref',
        mockServers: mockServers,
        host: host,
        srcAddress: srcAddress,
        done: done
      });
    }
  }
  
  unref(done) {
    if(!this.lock) {
      this.lock = true;
      this._unref(() => {
        done();
        if(0 === this.lockQueue.length) {
          this.lock = false;
        }
        else {
          this.handleLock();
        }
      });
    }
    else {
      this.lockQueue.push({
        type: 'unref',
        done: done
      });
    }
  }
  
  _ref(mockServers, host, srcAddress, done) {
//    this.gauge.inc();
    const mockServerDatas = new Array(mockServers.length);
    if(1 === ++this.nbrOfRefs) {
      let pendings = 0;
      for(let i = 0; i < mockServers.length; ++i) {
        const mockServer = mockServers[i];
        ++pendings;
        const localMockServerData = {
          srvAddress: mockServer.srvAddress,
          host: host,
          port: -1,
          mockWorker: null
        };
        this.localMockServerDatas.push(localMockServerData);
        const { port1, port2 } = new MessageChannel();
        const index = i;
        localMockServerData.mockWorker = this.workerPool.getWorker('puppeteer-mock', PuppeteerMockWorkerMain, PuppeteerMockWorkerThreadPath);
        localMockServerData.mockWorker.start(index, this, (err) => {
          if(err) {
            done(err);
            return;
          }
          localMockServerData.mockWorker.run(host, mockServer, srcAddress, port2, (err, port) => {
            this.localMockServerDatas[index].port = port;
            mockServerDatas[index] = {
              mcPort: port1,
              host: host,
              port: port
            };
            if(0 === --pendings) {
              done(err, mockServerDatas);
            }
          });
        }, (exitCode) => {});
      }
    }
    else {
      let pendings = 0;
      for(let i = 0; i < mockServers.length; ++i) {
        ++pendings;
        const { port1, port2 } = new MessageChannel();
        const index = i;
        this.localMockServerDatas[index].mockWorker.addMessageChannel(port2, (err) => {
          mockServerDatas[index] = {
            mcPort: port1,
            host: this.localMockServerDatas[index].host,
            port: this.localMockServerDatas[index].port
          };
          if(0 === --pendings) {
            done(err, mockServerDatas);
          }
        });
      }
    }
  }
  
  _unref(done) {
//    this.gauge.dec();
    if(0 === --this.nbrOfRefs) {
      if(0 !== this.localMockServerDatas.length) {
        let pendings = 0;
        for(let i = 0; i < this.localMockServerDatas.length; ++i) {
          ++pendings;
          this.localMockServerDatas[i].mockWorker.stop(() => {
            if(0 === --pendings) {
              this.localMockServerDatas = [];
              return done();
            }
          });
        }
      }
      return;
    }
    process.nextTick(() => {
      done();
    });
  }
}

module.exports = PuppeteerMockManager;
