
'use strict';

const StackApi = require('stack-api');
const PuppeteerPageTriggerMan = require('./templates-test-cases/puppeteer-page-trigger-man');
const PuppeteerPageTriggerTestCase = require('./templates-test-cases/puppeteer-page-trigger-test-case');


class PuppeteerTemplatesTestCases extends StackApi.StackComponentsTemplateBaseTestCases {
  constructor() {
    super([
      PuppeteerPageTriggerMan/*,
      PuppeteerPageTriggerTestCase*/
    ]);
  }
}


module.exports = PuppeteerTemplatesTestCases;
