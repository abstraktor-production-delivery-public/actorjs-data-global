
'use strict';

const StackApi = require('stack-api');


class PuppeteerConnectionWorkerMain extends StackApi.WorkerMain {
  constructor() {
    super();
  }
  
  run(host, browserData, sequrity, done) {
    this.channel.request('run', done, undefined, host, browserData, sequrity);
  }
  
  attachMockServers(mockServers, mockServerDatas, done) {
    const transferList = mockServerDatas.map((mockServerData) => {
      return mockServerData.mcPort;
    });
    this.channel.request('attachMockServers', done, transferList, mockServers, mockServerDatas);
  }
  
  detachMockServers(done) {
    this.channel.request('detachMockServers', done);
  }
}


module.exports = PuppeteerConnectionWorkerMain;
