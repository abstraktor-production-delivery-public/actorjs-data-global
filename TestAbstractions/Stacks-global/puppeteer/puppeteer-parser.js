
'use strict';

const PuppeteerParserHttp2 = require('./puppeteer-parser-http2');
const PuppeteerParserHttp11 = require('./puppeteer-parser-http11');
const PuppeteerParserTls = require('./puppeteer-parser-tls');
const StackApi = require('stack-api');


class PuppeteerParser {
  constructor(id, type, socketHandler) {
    this.id = id;
    this.type = type;
    this.socketHandler = socketHandler;
    this.connected = false;
    this.state = PuppeteerParser.TLS;
    this.tlsParser = new PuppeteerParserTls(id, PuppeteerParser.IN ? PuppeteerParserTls.REQUEST : PuppeteerParserTls.RESPONSE);
    this.httpParser = new PuppeteerParserHttp11(this.id, PuppeteerParserHttp11.REQUEST, PuppeteerParser.replaceHeaderNamesHttpIn);
    this.bufferManager = new StackApi.BufferManager(id);
    this.currentParser = {isReady: () => {return true}};
  }
  
  setConnected() {
    this.connected = true;
  }
  
  parse(chunk, cb) {
    if(this.currentParser.isReady()) {
      if(!this.connected) {
        console.log(chunk.toString());
        console.log('***********************');
        const ready = this.httpParser.parse(chunk);
        cb(ready, this.httpParser);
      }
      else {
        console.log(chunk.toString());
        console.log('***********************');
        /*if(PuppeteerParser.TLS === this.state) {
          if(PuppeteerParser.IN === this.type) {
            const msg = this.tlsParser.parse(chunk);
            this.tlsParser.log(msg);
          }
          else {
            ddb.error('NOT IMPLEMENTED');
         }
        }
        else if(PuppeteerParser.HTTP === this.state) {
        }*/
      }
    }
  }
}

PuppeteerParser.replaceHeaderNamesHttpIn = new Map([['Proxy-Connection', 'Connection']]);

PuppeteerParser.IN = 0;
PuppeteerParser.OUT = 1;
PuppeteerParser.TLS = 0;
PuppeteerParser.HTTP = 1;
PuppeteerParser.HTTP2 = 1;

module.exports = PuppeteerParser;
