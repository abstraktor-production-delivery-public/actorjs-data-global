
'use strict';

const Net = require('net');


class PuppeteerConnectionMock {
  constructor(connectionHandler) {
    this.connectionHandler = connectionHandler;
    this.mockServer = null;
    this.closed = false;
    this.cbClosed = null;
  }
  
  connect(mockServer, host, port, srcAddressLog, dstAddressLog, done) {
    this.mockServer = mockServer;
    const address = {
      host: mockServer.host,
      port: mockServer.port,
      localAddress: host,
      localPort: 0,
      allowHalfOpen: true 
    };
    const outgoingMockSocket = Net.connect(address, () => {
      this.connectionHandler.logConnected(null, srcAddressLog, dstAddressLog, mockServer.mockId);
      done(null, outgoingMockSocket);
    });
    outgoingMockSocket.on('end', () => {
      this.connectionHandler.logClosing(false, mockServer.mockId);
      mockServer.outgoing.end(() => {
      });
    });
    outgoingMockSocket.on('close', (hadError) => {
      mockServer.mcPort.close();
      this.connectionHandler.logClosed(false, mockServer.mockId);
      this.closed = true;
      if(this.cbClosed) {
        const cbClosed = this.cbClosed;
        this.cbClosed = null;
        cbClosed();
      }
    });
    outgoingMockSocket.on('error', (err) => {
      if(outgoingMockSocket.pending) {
        this.connectionHandler.logConnected(err, srcAddressLog, dstAddressLog);
        done(err);
      }
      else {
        ddb.error('PUPPETEER Mock, error', err);
      }
    });
  }
  
  close(done) {
    if(this.closed) {
      process.nextTick(done);
    }
    else {
      this.cbClosed = done;
      if(this.mockServer) {
        this.mockServer.outgoing.destroySoon();
      }
    }
    this.mockServer = null;
  }
}


module.exports = PuppeteerConnectionMock;
