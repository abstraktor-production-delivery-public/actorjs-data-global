
'use strict';

const PuppeteerMockHandler = require('./puppeteer-mock-handler');
const StackApi = require('stack-api');


class PuppeteerMockWorkerThread extends StackApi.WorkerThread {
  constructor() {
    super();
    this.mockHandler = null;
  }
  
  onStart(id, done) {
    this.mockHandler = new PuppeteerMockHandler(id, (done, msgId, ...params) => {
      this.channel.method(msgId, done, undefined, ...params);
    }, (msgId, mockId, transferables, ...params) => {
      this.channel.message(msgId, undefined, mockId, ...params);
    });
    process.nextTick(done);
  }
  
  onStop(done) {
    this.mockHandler.stop(() => {
      this.mockHandler = null;
      done();
    });
  }
  
  run(host, mockServer, srcAddress, mcPort, done) {
    this.mockHandler.run(host, mockServer, srcAddress, mcPort, done);
  }
    
  addMessageChannel(mcPort, done) {
    this.mockHandler.attachChannel(mcPort);
    process.nextTick(done);
  }
}


module.exports = PuppeteerMockWorkerThread;
