
'use strict';

const PuppeteerStyle = require('./puppeteer-style');
const PuppeteerConnectionSocketHandler = require('./puppeteer-connection-socket-handler');
const PuppeteerMessageSelector = require('./puppeteer-message-selector');


const exportsObject = {
  Style: PuppeteerStyle,
  PuppeteerConnectionSocketHandler: PuppeteerConnectionSocketHandler,
  PuppeteerMessageSelector: PuppeteerMessageSelector
};


module.exports = exportsObject;
