
'use strict';

const StackApi = require('stack-api');
const Net = require('net');


class PuppeteerMockHandler {
  constructor(id, cbMethod, cbMessage) {
    this.id = id;
    this.cbMethod = cbMethod;
    this.cbMessage = cbMessage;
    this.serverSocket = null;
    this.srcAddress = null;
    this.incommingSocket = null;
    this.outgoingSocket = null;
    this.mcPorts = new Set();
    this.mockServer = null;
    //this.gauge = new StackApi.Gauge('Puppeteer', `MockHandler[${this.id}]`, 'Ports', 0, 12);
  }
  
  attachChannel(mcPort) {
    this.mcPorts.add(mcPort);
    //this.gauge.inc();
    mcPort.on('message', (message) => {
      if('connect' === message.cmd) {
        this.connect((err, host, port) => {
          mcPort.postMessage({
            cmd: 'connect',
            port,
            host,
            err: err
          });
        });
      }
      else if('disconnect' === message.cmd) {
        this.close((err) => {
          mcPort.postMessage({
            cmd: 'disconnect',
            err: err
          });
        });
      }
    });
    mcPort.on('close', () => {
      this.mcPorts.delete(mcPort);
      mcPort.unref(); // TODO: remove ???
      //this.gauge.dec();
    });
  }
  
  run(host, mockServer, srcAddress, mcPort, done) {
    this.srcAddress = srcAddress;
    this.mockServer = mockServer;
    this.listen(host, (err, host, port) => {
      this.attachChannel(mcPort);
      done(err, port);
    });
  } 
  
  stop(done) {
    this.mcPorts.forEach((mcPort) => {
      mcPort.close();
      this.mcPorts = new Set();
    });
    this.close((err) => {
      done();
    });
  }
  
  listen(host, done) {
    this.serverSocket = Net.createServer({allowHalfOpen: true}, (incommingSocket) => {
      this.incommingSocket = incommingSocket;
      incommingSocket.on('end', () => {
        if(null !== this.outgoingSocket) {
          this.outgoingSocket.end();
        }
      });
      incommingSocket.on('close', () => {
        if(null !== this.outgoingSocket) {
          this.outgoingSocket.destroySoon();
        }
        this.incommingSocket = null;
      });
      incommingSocket.on('error', (err) => {
        ddb.error('PUPPETEER MOCK - Incomming Socket ERROR', err);
      });
      if(null !== this.outgoingSocket) {
        this.incommingSocket.pipe(this.outgoingSocket);
        this.outgoingSocket.pipe(this.incommingSocket);
      }
      else {
        this.incommingSocket.destroySoon();
      }
    });
    this.serverSocket.listen({
      host: host,
      port: 0
    }, (err) => {
      done(err, this.serverSocket.address().address, this.serverSocket.address().port);
    });
  }
  
  connect(done) {
    const incommingSocket = null;
    const srcAddress = StackApi.Address.from(this.srcAddress);
    const address = StackApi.Address.netAddress(srcAddress, this.mockServer.dstAddress);
    let connected = false;
    const outgoingSocket = Net.connect(address, () => {
      connected = true;
      this.outgoingSocket = outgoingSocket;
      srcAddress.port = outgoingSocket.localPort;
      done(null, outgoingSocket.localAddress, outgoingSocket.localPort);    
    });
    outgoingSocket.on('end', () => {
      if(null !== this.incommingSocket) {
        this.incommingSocket.end();
      }     
    });
    outgoingSocket.on('close', () => {
      if(null !== this.incommingSocket) {
        this.incommingSocket.destroySoon();
      }
      this.outgoingSocket = null;
    });
    outgoingSocket.on('error', (err) => {
      ddb.error('PUPPETEER MOCK - outgoing Socket ERROR', err);
      if(!connected) {
        done(err);
      }
    });
  }
  
  close(done) {
    if(null !== this.serverSocket) {
      this.serverSocket.once('close', (hadError) => {
        done(hadError);
      });
      this.serverSocket.close();
    }
    else {
      process.nextTick(() => {
        done();
      });
    }
  }
}

module.exports = PuppeteerMockHandler;
