
'use strict';

const StackApi = require('stack-api');


class PuppeteerPage extends StackApi.StackComponentsTemplatesActors {}

PuppeteerPage.displayName = 'page';
PuppeteerPage.type = 'orig';
PuppeteerPage.template = PuppeteerPage._template`
'use strict';

const ActorApi = require('actor-api');
const StackApi = require('stack-api');


class ${'name'}${'actorEnding'} extends ActorApi.Actor${'actorType'} {
  constructor() {
    super();
    this.puppeteerConnection = null;
    this.transportType = StackApi.NetworkType.TCP;
  }
  
  *data() {
    this.transportType = this.getTestDataNumber('transport-type', 'tcp', {
      tcp: StackApi.NetworkType.TCP,
      tls: StackApi.NetworkType.TLS
    });
  }
  
  *initClient() {
    this.puppeteerConnection = this.createConnection('puppeteer');
  }
  
  *run() {
    const page = this.puppeteerConnection.page;
  }
  
  *exit(interrupted) {
    this.closeConnection(this.puppeteerConnection);
  }
}

module.exports = ${'name'}${'actorEnding'};
`;

PuppeteerPage.markupNodes = 2;
PuppeteerPage.markup = `\`\`\`seq
Config(nodeWidth: 126, nodeMessageHeight: 17, nodeEventHeight: 10, widthBias: 23, heightBias: 20, lineNumbers: false, border: false, backgroundColor: transparent)

Title: Page
Nodes[orig, server]
orig -o server[http]: connect
orig => server[http]: GET
server => orig[http]: 200 OK
orig -> server[http]: GET
orig -> server[websocket]: websocket frame
orig -x server[http]: disconnect
\`\`\`
`;

module.exports = PuppeteerPage;
