
'use strict';

const StackApi = require('stack-api');


class PuppeteerPageActorPartPre extends StackApi.StackComponentsTemplatesActors {}

PuppeteerPageActorPartPre.displayName = 'page';
PuppeteerPageActorPartPre.type = 'actorpartpre';
PuppeteerPageActorPartPre.template = PuppeteerPageActorPartPre._template`
const ActorApi = require('actor-api');


class ${'name'}${'actorEnding'} extends ActorApi.ActorPartPre {
  constructor() {
    super();
  }
  
  *run() {
    const page = this.puppeteerConnection.page;
  }
}

module.exports = ${'name'}${'actorEnding'};
`;

PuppeteerPageActorPartPre.markupNodes = 2;
PuppeteerPageActorPartPre.markup = `\`\`\`seq
Config(nodeWidth: 126, nodeMessageHeight: 17, nodeEventHeight: 10, widthBias: 23, heightBias: 20, lineNumbers: false, border: false, backgroundColor: transparent)

Title: Page
Nodes[orig, server]
orig -o server[http]: connect
orig => server[http]: GET
server => orig[http]: 200 OK
orig -> server[http]: GET
orig -> server[websocket]: websocket frame
orig -x server[http]: disconnect
\`\`\`
`;


module.exports = PuppeteerPageActorPartPre;
