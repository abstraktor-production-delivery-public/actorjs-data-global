
'use strict';

const StackApi = require('stack-api');
const WebsocketMsg = require('../websocket/websocket-msg');


class PuppeteerParserWebsocket {
  constructor(id, type) {
    this.id = id;
    this.type = type;
    this.bufferManager = new StackApi.BufferManager(this.id, true);
    this.state = PuppeteerParserWebsocket.HEADER_BYTES;
    this.message = null;
    this.pos = 0;
    this.fin = 0;
    this.rsv1 = 0;
    this.rsv2 = 0;
    this.rsv3 = 0;
    this.opCode = 0;
    this.mask = 0;
    this.payloadLength = 0;
    this.extendedPayloadLength = 0;
    this.choosenPayloadLength = 0;
    this.maskingKey = null;
  }
  
  parse(chunk) {
    this.bufferManager.addBuffer(chunk);
    switch(this.state) {
      case PuppeteerParserWebsocket.HEADER_BYTES: {
        this.fin = 0;
        this.rsv1 = 0;
        this.rsv2 = 0;
        this.rsv3 = 0;
        this.opCode = 0;
        this.secondByte = 0;
        this.mask = 0;
        this.payloadLength = 0;
        this.extendedPayloadLength = 0;
        this.choosenPayloadLength = 0;
        this.maskingKey = null;
        const buffer = this.bufferManager.receiveSize(2);
        if(undefined === buffer) {
          return false;
        }
        const firstByte = buffer.readUInt8(0);
        this.fin = StackApi.BitByte.getBit(firstByte, 0);
        this.rsv1 = StackApi.BitByte.getBit(firstByte, 1);
        this.rsv2 = StackApi.BitByte.getBit(firstByte, 2);
        this.rsv3 = StackApi.BitByte.getBit(firstByte, 3);
        this.opCode = StackApi.BitByte.getBits(firstByte, 4, 7);
        const secondByte = buffer.readUInt8(1);      
        this.mask = StackApi.BitByte.getBit(secondByte, 0);
        this.payloadLength = StackApi.BitByte.getBits(secondByte, 1, 7);
        this.state = PuppeteerParserWebsocket.EXTENDED_PAYLOAD_BYTES;
      }
      case PuppeteerParserWebsocket.EXTENDED_PAYLOAD_BYTES: {
        if(126 === this.payloadLength) {
          const buffer = this.bufferManager.receiveSize(2);
          if(undefined === buffer) {
            return false;
          }
          this.extendedPayloadLength = buffer.readUInt16BE(0);
          this.choosenPayloadLength = this.extendedPayloadLength;
        }
        else if(127 === this.payloadLength) {
          const buffer = this.bufferManager.receiveSize(8);
          if(undefined === buffer) {
            return false;
          }
          this.extendedPayloadLength = buffer.readBigUInt64BE(0);
          this.choosenPayloadLength = Number.parseInt(this.extendedPayloadLength); 
        }
        else {
          this.choosenPayloadLength = this.payloadLength;
        }
        this.state = PuppeteerParserWebsocket.MASK;
      }
      case PuppeteerParserWebsocket.MASK: {
        if(1 === this.mask) {
          const buffer = this.bufferManager.receiveSize(4);
          if(undefined === buffer) {
            return false;
          }
          this.maskingKey = buffer.slice(0, 4);
        }
        this.state = PuppeteerParserWebsocket.PAYLOAD;
      }
      case PuppeteerParserWebsocket.PAYLOAD: {
        let buffer = null;
        if(0 !== this.choosenPayloadLength) {
          buffer = this.bufferManager.receiveSize(this.choosenPayloadLength);
          if(!buffer) {
            return false;
          }
        }
        this.message = new WebsocketMsg(this.fin, this.rsv1, this.rsv2, this.rsv3, this.opCode, this.mask, buffer, this.maskingKey);
        this.message.payloadLength = this.payloadLength;
        this.message.extendedPayloadLength = this.extendedPayloadLength;
        if(1 === this.mask && null !== buffer) {
          const payloadDataUnmasked = Buffer.from(buffer);
          for(let i = 0; i < payloadDataUnmasked.length; ++i) {
            payloadDataUnmasked[i] = buffer[i] ^ this.maskingKey[i % 4];
          }
          this.message.payloadDataUnmasked = payloadDataUnmasked;
        }
        this.state = PuppeteerParserWebsocket.HEADER_BYTES;
        return true;
      }
    }
  }
}

PuppeteerParserWebsocket.CLIENT = 0;
PuppeteerParserWebsocket.SERVER = 1;

PuppeteerParserWebsocket.HEADER_BYTES = 0;
PuppeteerParserWebsocket.EXTENDED_PAYLOAD_BYTES = 1;
PuppeteerParserWebsocket.MASK = 2;
PuppeteerParserWebsocket.PAYLOAD = 3;

PuppeteerParserWebsocket.STATUS = [
  'HEADER_BYTES',
  'EXTENDED_PAYLOAD_BYTES',
  'MASK',
  'PAYLOAD'
];


module.exports = PuppeteerParserWebsocket;
