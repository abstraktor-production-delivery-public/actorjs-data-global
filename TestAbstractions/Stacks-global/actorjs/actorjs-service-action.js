
'use strict';


class ActorjsServiceAction {
  constructor(...params) {
    this.name = this.constructor.name.substring(13);
    this.index = -1;
    this.params = params;
  }
}


module.exports = ActorjsServiceAction;
