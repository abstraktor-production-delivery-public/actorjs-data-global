
'use strict';

const ActorjsMsgRequest = require('./actorjs-msg-request');
const ActorjsWebsocketTextFrame = require('./msg/actorjs-websocket-text-frame');


class ActorjsMsgRequestWs {
  constructor(...requests) {
    this.frame = new ActorjsWebsocketTextFrame(JSON.stringify(new ActorjsMsgRequest(...requests)), true);
  }
}


module.exports = ActorjsMsgRequestWs;
