
'use strict';

const StackApi = require('stack-api');
const ActorjsInnerLog = require('./actorjs-inner-log');


class ActorjsDecoderSocket extends StackApi.Decoder {
  constructor(msg) {
    super();
    this.msg = msg;
  }

  *decode() {
    const sizeString = yield* this.receiveLine();
    let size = Number.parseInt(sizeString, 16);
    const msg = yield* this.receiveSize(size);
    yield* this.receiveLine();
    const object = JSON.parse(msg);
    if(this.isLogIp) {
      this.setCaption(ActorjsInnerLog.generateLog(object, this.ipLog));
      this.logMessage();
    }
    if('DP_REQ' === object.type) {
      return object.data;
    }
    else if('DP_RESP' === object.type) {
      return object;
    }
    else if('DP_INIT' === object.type) {
      return object;
    }
  }
}

module.exports = ActorjsDecoderSocket;
