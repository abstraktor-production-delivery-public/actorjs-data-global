
'use strict';

const StackApi = require('stack-api');
const SocketLoggerObject = require('../socket/socket-logger-object');
const SocketConst = require('../socket/socket-const');
const ActorjsConst = require('./actorjs-const');


class ActorjsInnerLog {
  static generateLog(msg, ipLogs) {
    SocketLoggerObject.generateLog(msg, ipLog);
    let caption = '';
    if('DP_REQ' === msg.type) {
      if(1 === msg.data.requests.length) {
        caption = `REQ: ${msg.data.requests[0].name}(${ActorjsInnerLog._joinParams(msg.data.requests[0].params)})`;
      }
      else {
        console.log(ddb.yellow('ServiceAction requests with more than 1 request is not implemented in the decoder yet'));
      }
    }
    else if('DP_RESP' === msg.type) {
      if(1 === msg.responses.length) {
        const response = msg.responses[0];
        caption = `RESP - ${'success' === response.result.code ? 'OK' : 'NOK'}: ${response.name}(${ActorjsInnerLog._joinParams(response.data)})`;
      }
      else {
        console.log(ddb.yellow('ServiceAction responses with more than 1 response is not implemented in the decoder yet'));
      }
    }
    else if('DP_INIT' === msg.type) {
      if(undefined === msg.result) {
        caption = 'INIT - (' + (0 === msg.services.length ? ')' : `${msg.services.join(', ')})`);
      }
      else if(msg.result) {
        caption = 'INIT OK - (' + (0 === msg.services.length ? ')' : `${msg.services.join(', ')})`);
      }
      else {
        caption = 'INIT NOK - (' + (0 === msg.services.length ? ')' : `${msg.services.join(', ')})`);
      }
    }
    return caption.length <= SocketConst.MAX_CAPTION_SIZE ? caption : caption.substring(0, SocketConst.BREAK_CAPTION_SIZE) + '...';
  }
  
  static _joinParams(params) {
    if(Array.isArray(params)) {
      const fParams = [];
      params.forEach((param) => {
        if(Array.isArray(param)) {
          fParams.push(`[${param.join(', ')}]`);
        }
        else if('object' === typeof param) {
          fParams.push('{}');
        }
        else {
          fParams.push(param);
        }
      });
      return fParams.join(', ');
    }
    else {
      if('object' === typeof params) {
        return '{}';
      }
      if(undefined !== params) {
         return params;
      }
      else {
        return '';
      }
    }
  } 
}

module.exports = ActorjsInnerLog;
