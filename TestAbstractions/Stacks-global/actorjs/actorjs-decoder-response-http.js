
'use strict';

const StackApi = require('stack-api');
const HttpDecoderResponse = require('../http/http-decoder-response');
const ActorjsInnerLog = require('./actorjs-inner-log');


class ActorjsDecoderResponsetHttp extends StackApi.Decoder {
  constructor(msg) {
    super();
    this.msg = msg;
    this.httpDecoderResponse = new HttpDecoderResponse();
  }
  
  setConnection(connection, isLogIp, pendingContext) {
    super.setConnection(connection, isLogIp, pendingContext);
    this.httpDecoderResponse.setConnection(connection, isLogIp, pendingContext);
  }
  
  *decode() {
    const msg = yield* this.httpDecoderResponse.decode();
    const body = msg.getBody();
    const actorJsResponse = JSON.parse(body.getBuffer().toString());
    return actorJsResponse;
  }
}

module.exports = ActorjsDecoderResponsetHttp;
