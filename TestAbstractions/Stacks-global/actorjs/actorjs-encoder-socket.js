
'use strict';

const StackApi = require('stack-api');
const ActorjsInnerLog = require('./actorjs-inner-log');
const ActorjsMsgRequest = require('./actorjs-msg-request');
const ActorjsMsgResponse = require('./actorjs-msg-response');

class ActorjsEncoderSocket extends StackApi.Encoder {
  constructor(object, id, sessionId) {
    super();
    this.object = object;
    this.id = id;
    this.sessionId = sessionId;
  }
  
  *encode() {
    let object = null;
    if(this.object.constructor.name.startsWith('ServiceAction')) {
      object = new ActorjsMsgRequest(this.object);
    }
    else if(this.object.constructor.name.startsWith('ServiceResponse')) {
      object = new ActorjsMsgResponse(this.id , this.sessionId, this.object);
    }
    else {
      object = this.object;
    }
    
    const objectStringified = JSON.stringify(object);
    const length = objectStringified.length.toString(16);
    const buffer = Buffer.allocUnsafe(length.length + ActorjsEncoderSocket.CL_CR_LENGTH + objectStringified.length + ActorjsEncoderSocket.CL_CR_LENGTH);
    
    let offset = buffer.write(length);
    offset += buffer.write(ActorjsEncoderSocket.CL_CR, offset);
    offset += buffer.write(objectStringified, offset);
    buffer.write(ActorjsEncoderSocket.CL_CR, offset);
    
    yield* this.send(buffer);
    if(this.isLogIp) {
      this.setCaption(ActorjsInnerLog.generateLog(object, this.ipLog));
      this.logMessage();
    }
  }
}


ActorjsEncoderSocket.CL_CR = '\r\n';
ActorjsEncoderSocket.CL_CR_LENGTH = Buffer.byteLength(ActorjsEncoderSocket.CL_CR);


module.exports = ActorjsEncoderSocket;
