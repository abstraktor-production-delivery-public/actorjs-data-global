
'use strict';


class ActorjsStyle {
  constructor(index) {
    this.index = index;
    this.textColor= 'black';
    this.textProtocolColor = 'white';
    this.protocolColor = 'darkgrey';
    this.protocolBackgroundColor = 'lightgrey';
  }
}


module.exports = ActorjsStyle;
