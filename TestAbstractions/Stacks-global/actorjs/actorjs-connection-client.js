
'use strict';

const StackApi = require('stack-api');
const HttpEncoderRequest = require('../http/http-encoder-request');
const HttpDecoderResponse = require('../http/http-decoder-response');
const ActorjsConnectionClientOptions = require('./actorjs-connection-client-options');
const ActorjsEncoderRequestHttp = require('./actorjs-encoder-request-http');
const ActorjsDecoderResponsetHttp = require('./actorjs-decoder-response-http');
const ActorjsEncoderRequestWs = require('./actorjs-encoder-request-ws');
const ActorjsDecoderWs = require('./actorjs-decoder-ws');
const ActorjsEncoderSocket = require('./actorjs-encoder-socket');
const ActorjsDecoderSocket = require('./actorjs-decoder-socket');
const ActorjsConnectionJob = require('./actorjs-connection-worker');


class ActorjsConnectionClient extends StackApi.ConnectionClient {
  constructor(id, type, actor, options) {
    super(id, type, 'actorjs', actor, StackApi.NetworkType.TCP, ActorjsConnectionClientOptions, options);
    this.upgraded = false;
  }
  
  sendRequest(msg, sessionid = undefined) {
    if('tcp' === this.connectionOptions.transportProtocol) {
      this.sendMessage(new ActorjsEncoderSocket(msg, undefined, sessionid));
    }
    else if('http' === this.connectionOptions.transportProtocol) {
      this.sendMessage(new ActorjsEncoderRequestHttp(msg));
    }
    else if('websocket' === this.connectionOptions.transportProtocol) {
      if(!this.upgraded) {
        this.sendMessage(new HttpEncoderRequest(msg));
      }
      else {
        this.sendMessage(new ActorjsEncoderRequestWs(msg));
      }
    }
  }
  
  sendResponse(msg, id, sessionid = undefined) {
    this.sendMessage(new ActorjsEncoderSocket(msg, id, sessionid));
  }
  
  receive() {
    if('tcp' === this.connectionOptions.transportProtocol) {
      this.receiveMessage(new ActorjsDecoderSocket());
    }
    else if('http' === this.connectionOptions.transportProtocol) {
      this.receiveMessage(new ActorjsDecoderResponsetHttp());
    }
    else if('websocket' === this.connectionOptions.transportProtocol) {
      if(!this.upgraded) {
        this.receiveMessage(new HttpDecoderResponse());
        this.upgraded = true;
      }
      else {
        this.receiveMessage(new ActorjsDecoderWs());
      }
    }
  }
}


module.exports = ActorjsConnectionClient;
