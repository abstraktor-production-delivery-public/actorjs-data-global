
'use strict';


class ActorjsServiceResponse {
  constructor(resultCode, resultError, resultMsg, ...params) {
    this.name = this.constructor.name.substring(14);
    this.index = -1;
    this.result = {
      code: resultCode,
      error: resultError,
      msg: resultMsg
    };
    this.data = 2 <= params.length ? params : (1 === params.length ? params[0] : undefined);
  }
}

module.exports = ActorjsServiceResponse;
