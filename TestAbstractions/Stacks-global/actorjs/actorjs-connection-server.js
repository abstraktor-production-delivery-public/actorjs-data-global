
'use strict';

const StackApi = require('stack-api');
const ActorjsConnectionServerOptions = require('./actorjs-connection-server-options');
const ActorjsEncoderRequestHttp = require('./actorjs-encoder-request-http');
const ActorjsDecoderResponsetHttp = require('./actorjs-decoder-response-http');
const ActorjsEncoderSocket = require('./actorjs-encoder-socket');
const ActorjsDecoderSocket = require('./actorjs-decoder-socket');


class ActorjsConnectionServer extends StackApi.ConnectionServer {
  constructor(id, type, actor, cbActorCallbacks, options) {
    super(id, type, 'actorjs', actor, StackApi.NetworkType.TCP, ActorjsConnectionServerOptions, options);
  }

  receive(transport='tcp') {
    if('tcp' === transport) {
      this.receiveMessage(new ActorjsDecoderSocket());
    }
    else if('http' === transport) {
      this.receiveMessage(new ActorjsDecoderResponsetHttp());
    }
  }

  sendRequest(msg, sessionid = undefined) {
    this.sendMessage(new ActorjsEncoderSocket(msg, undefined, sessionid));
  }
  
  sendResponse(msg, id, sessionid = undefined) {
    this.sendMessage(new ActorjsEncoderSocket(msg, id, sessionid));
  }
}


module.exports = ActorjsConnectionServer;
