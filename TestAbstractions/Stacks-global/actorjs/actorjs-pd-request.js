
'use strict';


class ActorjsPdRequest {
  constructor(name, ...params) {
    this.name = name;
    this.index = -1;
    this.params = params;
  }
}

module.exports = ActorjsPdRequest;
