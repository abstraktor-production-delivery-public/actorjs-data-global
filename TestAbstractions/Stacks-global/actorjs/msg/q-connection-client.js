
'use strict';

const StackApi = require('stack-api');
const QConnectionClientOptions = require('./q-connection-client-options');
const QEncoder = require('./q-encoder');
const QDecoder = require('./q-decoder');


class QConnectionClient extends StackApi.ClientConnection {
  constructor(id, cbActorCallbacks, connectionOptions, name = 'q') {
    super(id, cbActorCallbacks, name, StackApi.NetworkType.TCP, connectionOptions, QConnectionClientOptions);
  }

  send(msg) {
    this.sendMessage(new QEncoder(msg));
  }
  
  receive(msg) {
    this.receiveMessage(new QDecoder(msg));
  }
}

module.exports = QConnectionClient;
