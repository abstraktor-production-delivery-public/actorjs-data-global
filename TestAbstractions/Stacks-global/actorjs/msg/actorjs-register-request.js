
'use strict';

const StackApi = require('stack-api');


class ActorjsRegisterRequest {
  constructor(name, services) {
    this.type = 'DP_INIT'
    this.name = name;
    this.services = services;
  }
}


module.exports = ActorjsRegisterRequest;
