
'use strict';

const StackApi = require('stack-api');
const WConnectionClientOptions = require('./w-connection-client-options');
const WEncoder = require('./w-encoder');
const WDecoder = require('./w-decoder');


class WConnectionClient extends StackApi.ClientConnection {
  constructor(id, cbActorCallbacks, connectionOptions, name = 'w') {
    super(id, cbActorCallbacks, name, StackApi.NetworkType.TCP, connectionOptions, WConnectionClientOptions);
  }

  send(msg) {
    this.sendMessage(new WEncoder(msg));
  }
  
  receive(msg) {
    this.receiveMessage(new WDecoder(msg));
  }
}

module.exports = WConnectionClient;
