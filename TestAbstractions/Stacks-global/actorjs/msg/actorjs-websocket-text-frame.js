
'use strict';

const WebsocketApi = require('websocket-stack-api');


class ActorjsWebsocketTextFrame extends WebsocketApi.TextFrame {
  constructor(text, mask) {
    super(text, mask);
  }
}

module.exports = ActorjsWebsocketTextFrame;
