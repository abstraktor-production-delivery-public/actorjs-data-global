
'use strict';

const StackApi = require('stack-api');


class ActorjsRegisterResponse {
  constructor(result, name, services) {
    this.type = 'DP_INIT'
    this.result = result;
    this.name = name;
    this.services = services;
  }
}


module.exports = ActorjsRegisterResponse;
