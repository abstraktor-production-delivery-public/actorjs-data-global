
'use strict';


class ActorjsConnectionClientOptions {
  constructor() {
    this.transportProtocol = 'tcp';
  }
  
  clone() {
    return new ActorjsConnectionClientOptions();
  }
}

module.exports = new ActorjsConnectionClientOptions();
