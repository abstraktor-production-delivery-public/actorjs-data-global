
'use strict';


class ActorjsResponse {
  constructor(id, ...pdResponses) {
    this.id = id;
    this.responses = pdResponses;
    let i = -1;
    this.responses.forEach((resp) => {
      resp.index = ++i;
    });
  }
}

module.exports = ActorjsResponse;
