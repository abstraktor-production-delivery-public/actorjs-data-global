
'use strict';

const StackApi = require('stack-api');
const WebsocketDecoder = require('../websocket/websocket-decoder');
const ActorjsInnerLog = require('./actorjs-inner-log');


class ActorjsDecoderWs extends StackApi.Decoder {
  constructor(msg) {
    super();
    this.msg = msg;
    this.websocketDecoder = new WebsocketDecoder();
  }
  
  setConnection(connection, isLogIp, pendingContext) {
    super.setConnection(connection, isLogIp, pendingContext);
    this.websocketDecoder.setConnection(connection, isLogIp, pendingContext);
  }
  
  *decode() {
    return yield* this.websocketDecoder.decode();
  }
}

module.exports = ActorjsDecoderWs;
