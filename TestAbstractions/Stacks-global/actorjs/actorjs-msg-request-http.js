
'use strict';

const ActorjsRequest = require('./actorjs-request');
const HttpApi = require('http-stack-api');
const GuidGenerator = require('z-abs-corelayer-cs/clientServer/guid-generator');


class ActorjsMsgRequestHttp {
  constructor(host, id, ...pdRequests) {
    const requestNames = pdRequests.map((pdRequest) => {
      return pdRequest.name;
    });
    const requestPath = requestNames.join('/');
    this.httpRequest = new HttpApi.Request(HttpApi.Method.POST, `http://${host}/abs-data/${requestPath}`, HttpApi.Version.HTTP_1_1);
    this.httpRequest.addHeader(HttpApi.Header.HOST, host);
    this.httpRequest.addHeader(HttpApi.Header.CONNECTION, 'keep-alive');
    this.httpRequest.addHeader(HttpApi.Header.ACCEPT, 'application/json');
    this.httpRequest.addHeader(HttpApi.Header.USER_AGENT, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4298.0 Safari/537.36');
    this.httpRequest.addHeader(HttpApi.Header.CONTENT_TYPE, 'application/json');
    this.httpRequest.addHeader(HttpApi.Header.ORIGIN, `http://${host}`);
    this.httpRequest.addHeader(HttpApi.Header.ACCEPT_ENCODING, 'gzip, deflate');
    this.httpRequest.addHeader(HttpApi.Header.ACCEPT_LANGUAGE, 'en-GB,en-US;q=0.9,en;q=0.8');
    
    const request = JSON.stringify(new ActorjsRequest(id, ...pdRequests));
    this.httpRequest.addHeader(HttpApi.Header.CONTENT_LENGTH, request.length);
    this.httpRequest.addBody(request);
  }
}


module.exports = ActorjsMsgRequestHttp;
