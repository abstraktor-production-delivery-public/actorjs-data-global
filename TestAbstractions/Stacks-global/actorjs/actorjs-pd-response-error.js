
'use strict';


class ActorjsPdResponseError {
  constructor(name, error, msg) {
    this.name = name;
    this.index = -1;
    this.result = {
      code: 'error',
      error: error,
      msg: msg
    };
  }
}

module.exports = ActorjsPdResponseError;
