
'use strict';

const StackApi = require('stack-api');
const WebsocketEncoder = require('../websocket/websocket-encoder');
const ActorjsInnerLog = require('./actorjs-inner-log');


class ActorjsEncoderRequestWsEncoder extends StackApi.Encoder {
  constructor(msg) {
    super();
    this.websocketEncoder = new WebsocketEncoder(msg.frame);
  }
  
  setConnection(connection, isLogIp, pendingContext) {
    super.setConnection(connection, isLogIp, pendingContext);
    this.websocketEncoder.setConnection(connection, isLogIp, pendingContext);
  }
  
  *encode() {
    yield* this.websocketEncoder.encode();
  }
}

module.exports = ActorjsEncoderRequestWsEncoder;
