
'use strict';


class ActorjsConst {
  constructor() {
    
  }
}

ActorjsConst.MAX_CAPTION_SIZE = 30;
ActorjsConst.BREAK_CAPTION_SIZE = ActorjsConst.MAX_CAPTION_SIZE - 3;


module.exports = ActorjsConst;
