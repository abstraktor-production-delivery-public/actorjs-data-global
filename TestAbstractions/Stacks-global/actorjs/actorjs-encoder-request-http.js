
'use strict';

const StackApi = require('stack-api');
const HttpEncoderRequest = require('../http/http-encoder-request');
const ActorjsInnerLog = require('./actorjs-inner-log');


class ActorjsEncoderRequestHttp extends StackApi.Encoder {
  constructor(msg) {
    super();
    this.httpEncoderRequest = new HttpEncoderRequest(msg.httpRequest);
  }
  
  setConnection(connection, isLogIp, pendingContext) {
    super.setConnection(connection, isLogIp, pendingContext);
    this.httpEncoderRequest.setConnection(connection, isLogIp, pendingContext);
  }
  
  *encode() {
    yield* this.httpEncoderRequest.encode();
  }
}

module.exports = ActorjsEncoderRequestHttp;
