
'use strict';

const GuidGenerator = require('z-abs-corelayer-cs/clientServer/guid-generator');


class ActorjsMsgRequest {
  constructor(...serviceActions) {
    this.type = 'DP_REQ';
    this.data = {
      id: GuidGenerator.create(),
      requests: serviceActions
    };
    let i = -1;
    this.data.requests.forEach((req) => {
      req.index = ++i;
    });
  }
}


module.exports = ActorjsMsgRequest;
