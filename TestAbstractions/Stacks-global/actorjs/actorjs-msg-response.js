
'use strict';


class ActorjsMsgResponse {
  constructor(id, sessionId, ...serviceResponses) {
    this.type = 'DP_RESP';
    this.id = id;
    this.sessionId = sessionId;
    this.responses = serviceResponses;
    let i = -1;
    this.responses.forEach((resp) => {
      resp.index = ++i;
    });
  }
}


module.exports = ActorjsMsgResponse;
