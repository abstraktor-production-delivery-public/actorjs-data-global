
'use strict';

const ActorjsResponse = require('./actorjs-response');
const HttpApi = require('http-stack-api');


class ActorjsMsgResponseHttp extends HttpApi.Response {
  constructor(id, ...pdResponses) {
    super(HttpApi.Version.HTTP_1_1, HttpApi.StatusCode.OK, HttpApi.ReasonPhrase.OK);
    this.addHeader(HttpApi.Header.CONTENT_TYPE, 'application/json');
    this.addHeader(HttpApi.Header.CONNECTION, 'keep-alive');
    
    const response = JSON.stringify(new ActorjsResponse(id, ...pdResponses));
    this.addHeader(HttpApi.Header.CONTENT_LENGTH, response.length);
    this.addBody(response);
  }
}

module.exports = ActorjsMsgResponseHttp;
