
'use strict';


class ActorjsDpRequest {
  constructor(name, ...params) {
    this.name = name;
    this.index = -1;
    this.params = params;
  }
}

module.exports = ActorjsDpRequest;
