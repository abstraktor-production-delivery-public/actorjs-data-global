
'use strict';


class ActorjsPdResponseSuccess {
  constructor(name, ...params) {
    this.name = name;
    this.index = -1;
    this.result = {
      code: 'success'
    };
    this.data = 2 <= params.length ? params : (1 === params.length ? params[0] : undefined);
  }
}

module.exports = ActorjsPdResponseSuccess;
