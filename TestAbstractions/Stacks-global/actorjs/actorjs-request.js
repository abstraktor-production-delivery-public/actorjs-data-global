
'use strict';


class ActorjsRequest {
  constructor(id, ...serviceActions) {
    this.id = id;
    this.requests = serviceActions;
    let i = -1;
    this.requests.forEach((req) => {
      req.index = ++i;
    });
  }
}

module.exports = ActorjsRequest;
