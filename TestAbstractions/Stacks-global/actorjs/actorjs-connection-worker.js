
'use strict';

const StackApi = require('stack-api');


class ActorjsConnectionWorker extends StackApi.ConnectionWorker {
  constructor() {
    super();
  }
  
  onConnected(done) {
    process.nextTick(() => {
      done();
    });
    return true;
  }
  
  onDisconnected(done) {
    return false;
  }
}

module.exports = ActorjsConnectionWorker;
