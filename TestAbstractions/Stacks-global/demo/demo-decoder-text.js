
'use strict';

const StackApi = require('stack-api');
const DemoMsg = require('./demo-msg');
const DemoInnerLog = require('./demo-inner-log');


class DemoDecoderText extends StackApi.Decoder {
  constructor(msg) {
    super();
    this.msg = msg;
  }
  
  *decode() {
    const msgId = yield* this.receiveLine();
    const msg = yield* this.receiveLine();
    const demoMsg = new DemoMsg(msgId, msg);
    if(this.isLogIp) {
      this.setCaption(DemoInnerLog.generateLog(demoMsg, this.ipLog));
      this.logMessage();
    }
    return demoMsg;
  }
}

module.exports = DemoDecoderText;
