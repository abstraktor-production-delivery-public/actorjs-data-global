
'use strict';

const StackApi = require('stack-api');
const DemoInnerLog = require('./demo-inner-log');


class DemoEncoderText extends StackApi.Encoder {
  constructor(msg) {
    super();
    this.msg = msg;
  }
  
  *encode() {
    const buffer = Buffer.allocUnsafe(this.msg.msgId.length + 2 + this.msg.msg.length + 2);
    let offset = 0;
    offset += buffer.write(this.msg.msgId, offset);
    offset += buffer.write(DemoEncoderText.CL_CR, offset);
    offset += buffer.write(this.msg.msg, offset);
    buffer.write(DemoEncoderText.CL_CR, offset);
    yield* this.send(buffer);
    if(this.isLogIp) {
      this.setCaption(DemoInnerLog.generateLog(this.msg, this.ipLog));
      this.logMessage();
    }
  }
}

DemoEncoderText.CL_CR = '\r\n';


module.exports = DemoEncoderText;
