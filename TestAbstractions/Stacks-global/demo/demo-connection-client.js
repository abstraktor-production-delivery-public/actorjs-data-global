
'use strict';

const StackApi = require('stack-api');
const DemoConnectionClientOptions = require('./demo-connection-client-options');
const DemoEncoderText = require('./demo-encoder-text');
const DemoDecoderText = require('./demo-decoder-text');


class DemoConnectionClient extends StackApi.ConnectionClient {
  constructor(id, type, actor, options) {
    super(id, type, 'demo', actor, StackApi.NetworkType.TCP, DemoConnectionClientOptions, options);
  }

  sendText(msg) {
    this.sendMessage(new DemoEncoderText(msg));
  }
  
  receiveText(msg) {
    this.receiveMessage(new DemoDecoderText(msg));
  }
}

module.exports = DemoConnectionClient;
