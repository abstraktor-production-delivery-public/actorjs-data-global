
'use strict';

const StackApi = require('stack-api');
const DemoConnectionServerOptions = require('./demo-connection-server-options');
const DemoEncoderText = require('./demo-encoder-text');
const DemoDecoderText = require('./demo-decoder-text');


class DemoConnectionServer extends StackApi.ConnectionServer {
  constructor(id, type, actor, options) {
    super(id, type, 'demo', actor, StackApi.NetworkType.TCP, DemoConnectionServerOptions, options);
  }

  receiveText(msg) {
    this.receiveMessage(new DemoDecoderText(msg));
  }

  sendText(msg) {
    this.sendMessage(new DemoEncoderText(msg));
  }
}

module.exports = DemoConnectionServer;
