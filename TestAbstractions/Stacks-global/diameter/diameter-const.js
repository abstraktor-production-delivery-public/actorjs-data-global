
'use strict';


class DiameterConst {}

DiameterConst.DOCUMENTATION_LINK_ROOT = 'https://tools.ietf.org/html/rfc6733/';

DiameterConst.DIAMETER_COMMON_MESSAGES = 0;
DiameterConst.NASREQ = 1;
DiameterConst.MOBILE_IP = 2;
DiameterConst.BASE_ACCOUNTING = 3;
DiameterConst.RELAY = 0xffffffff;


module.exports = DiameterConst;
