
'use strict';

const StackApi = require('stack-api');
const DiameterMsg = require('./diameter-msg');
const DiameterConstAvp = require('./diameter-const-avp');
const DiameterAvpFactory = require('./diameter-avp-factory');
const DiameterInnerLog = require('./diameter-inner-log');


class DiameterDecoder extends StackApi.Decoder {
  constructor(command = DiameterDecoder.RECEIVE_ALL, externalDecoding) {
    super();
    this.msg = new DiameterMsg();
    this.command = command;
    this.externalDecoding = externalDecoding;
  }
  
  *decode() {
    switch(this.command) {
      case DiameterDecoder.RECEIVE_ALL:
        yield* this._receiveAll();
    }
    return this.msg;
  }
  
  *_decodeHeader() {
    const header = yield* this.receiveSize(20);
    this.msg.version = header.readUInt8(0);
    this.msg.messageLength = header.readUIntBE(1, 3);
    this.msg.commandFlags = header.readUInt8(4);
    this.msg.commandCode = header.readUIntBE(5, 3);
    this.msg.applicationID = header.readUInt32BE(8);
    this.msg.hopByHopIdentifier = header.readUInt32BE(12);
    this.msg.endToEndIdentifier = header.readUInt32BE(16);
  }
  
  decodeAvp(avpData, offset, length, avps) {
    while(length > offset) {
      const avpCode = avpData.readUInt32BE(offset);
      const avpFlags = avpData.readUInt8(offset + 4);
      const avpLength = avpData.readUIntBE(offset + 5, 3);
      
      // ADD VendorId
      const headerLength = 8;
      offset += headerLength;
      
      let avpFactory = DiameterAvpFactory.create(avpCode);
      if(undefined === avpFactory && undefined !== this.externalDecoding) {
        avpFactory = this.externalDecoding.avpFactory.create(avpCode);
      }
      const avp = new avpFactory(0, (avpFlags >> 6) % 2 ? 1 : 0, (avpFlags >> 7) % 2 ? 1 : 0, (avpFlags >> 5) % 2 ? 1 : 0);
      
      if(DiameterConstAvp.GROUPED === avp.typeBasic) {
        this.decodeAvp(avpData, offset, avpLength - headerLength);
      }
      else {
        switch(avp.typeBasic) {
          case DiameterConstAvp.FLOAT_32:
            avp.setValue(avpData.readFloatBE(offset));
            offset += 4;
            break;
          case DiameterConstAvp.FLOAT_64:
            avp.setValue(avpData.readDoubleBE(offset));
            offset += 8;
            break;
          case DiameterConstAvp.INTEGER_32:
            avp.setValue(avpData.readInt32BE(offset));
            offset += 4;
            break;
          case DiameterConstAvp.INTEGER_64:
            avp.setValue(avpData.readBigInt64BE(offset));
            offset += 8;
            break;
          case DiameterConstAvp.OCTET_STRING:
            avp.setValue(avpData.slice(offset, offset + avpLength - headerLength));
            offset += avpLength - headerLength;
            break;
          case DiameterConstAvp.UNSIGNED_32:
            avp.setValue(avpData.readUInt32BE(offset));
            offset += 4;
            break;
          case DiameterConstAvp.UNSIGNED_64:
            avp.setValue(avpData.readBigUInt64BE(offset));
            offset += 8;
            break;
          default:
            break;
        };
        avps.addAvpObject(avp);
      }
    }
  }
  
  *_decodeAvps() {
    const avpData = yield* this.receiveSize(this.msg.messageLength - 20);
    this.decodeAvp(avpData, 0, this.msg.messageLength - 20, this.msg);
  }
  
  *_receiveAll() {
    yield* this._decodeHeader();
    yield* this._decodeAvps();
    if(this.isLogIp) {
      this.addLog(DiameterInnerLog.createHeader(this.msg));
      this.addLog(DiameterInnerLog.createAvps(this.msg.avps));
      this.setCaption(DiameterInnerLog.createCaption(this.msg, this.externalDecoding ? this.externalDecoding.commandCode : undefined));
      this.logMessage();
    }
    return this.msg;
  }
}

DiameterDecoder.RECEIVE_ALL = 0;


module.exports = DiameterDecoder;
