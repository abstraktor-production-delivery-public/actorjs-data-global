
'use strict';

const StackApi = require('stack-api');
const DiameterInnerLog = require('./diameter-inner-log');
const DiameterConstCommandCode = require('./diameter-const-command-code');
const DiameterConstAvp = require('./diameter-const-avp');
const Float32 = require('./avp-basic/float-32');
const Float64 = require('./avp-basic/float-64');
const Grouped = require('./avp-basic/grouped');
const Integer32 = require('./avp-basic/integer-32');
const Integer64 = require('./avp-basic/integer-64');
const OctetString = require('./avp-basic/octet-string');
const Unsigned2 = require('./avp-basic/unsigned-32');
const Unsigned64 = require('./avp-basic/unsigned-64');


class DiameterEncoder extends StackApi.Encoder {
  constructor(msg, command = DiameterEncoder.SEND_ALL, diameterExternalConstCommandCode) {
    super();
    this.msg = msg;
    this.command = command;
    this.diameterExternalConstCommandCode = diameterExternalConstCommandCode;
    this.offset = 0;
    this.buffer = null;
    this.buffers = [];
    this.size = 0;
  }
  
  *encode() {
    switch(this.command) {
      case DiameterEncoder.SEND_ALL:
        yield* this._sendAll();
        break;
      default:
        throw new Error('NOT IMPLEMENTD');
        break;
    };
  }
  
  _createBuffer(size) {
    if(0 !== size) {
      this.offset = 0;
      this.buffer = Buffer.allocUnsafe(size);
      this.buffers.push(this.buffer);
      return size;
    }
    else {
      return 0;
    }
  }
  
  encodeHeader() {
    const msg = this.msg;
    this.buffer.writeUInt8(msg.version, 0);
    this.buffer.writeUIntBE(msg.messageLength, 1, 3);
    this.buffer.writeUInt8(msg.commandFlags, 4);
    this.buffer.writeUIntBE(msg.commandCode, 5, 3);
    this.buffer.writeUInt32BE(msg.applicationID, 8);
    this.buffer.writeUInt32BE(msg.hopByHopIdentifier, 12);
    this.buffer.writeUInt32BE(msg.endToEndIdentifier, 16);
  }
  
  encodeAvps(avps, offset) {
    avps.forEach((avpArray) => {
      avpArray.forEach((avp) => {
        offset = this.buffer.writeUInt32BE(avp.code, offset);
        offset = this.buffer.writeUInt8(avp.flags, offset);
        offset = this.buffer.writeUIntBE(avp.length, offset, 3);
        if(0 !== avp.vendorId) {
          offset = this.buffer.writeUInt32BE(avp.vendorId, offset);
        }
        if(DiameterConstAvp.GROUPED === avp.typeBasic) {
          this.encodeAvps(avp.avps, offset);
        }
        else {
          switch(avp.typeBasic) {
            case DiameterConstAvp.FLOAT_32:
              offset = this.buffer.writeFloatBE(avp.value, offset);
              break;
            case DiameterConstAvp.FLOAT_64:
              offset = this.buffer.writeDoubleBE(avp.value, offset);
              break;
            case DiameterConstAvp.INTEGER_32:
              offset = this.buffer.writeInt32BE(avp.value, offset);
              break;
            case DiameterConstAvp.INTEGER_64:
              offset = this.buffer.writeBigInt64BE(avp.value, offset);
              break;
            case DiameterConstAvp.OCTET_STRING:
              offset += this.buffer.write(avp.value, offset, avp.value.length, 'ascii');
              const rest = avp.value.length % 4;
              if(0 !== rest) {
                this.buffer.writeUIntBE(0, offset, 4 - rest);
                offset += (4 - rest);
              }
              break;
            case DiameterConstAvp.UNSIGNED_32:
              offset = this.buffer.writeUInt32BE(avp.value, offset);
              break;
            case DiameterConstAvp.UNSIGNED_64:
              offset = this.buffer.writeBigUInt64BE(avp.value, offset);
              break;
            default:
              break;
          };
        }
      });
    });
  }
  
  *_sendAll() {
    this._createBuffer(this.msg.messageLength);
    this.encodeHeader();
    this.encodeAvps(this.msg.avps, 20);
    while(0 !== this.buffers.length) {
      yield* this.send(this.buffers.shift());
    }
    if(this.isLogIp) {
      this.addLog(DiameterInnerLog.createHeader(this.msg));
      this.addLog(DiameterInnerLog.createAvps(this.msg.avps));
      this.setCaption(DiameterInnerLog.createCaption(this.msg, this.diameterExternalConstCommandCode));
      this.logMessage();
    }
  }
}

DiameterEncoder.SEND_ALL = 0;


module.exports = DiameterEncoder;
