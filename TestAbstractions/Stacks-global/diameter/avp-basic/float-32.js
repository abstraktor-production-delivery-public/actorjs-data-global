
'use strict';

const AvpHeader = require('./avp-header');
const DiameterConstAvp = require('../diameter-const-avp');


class Float32 extends AvpHeader {
  constructor(code, value, mBit=0, vbit=0, pBit=0, vendorId=0) {
    super(code, 4, value, mBit, vbit, pBit, vendorId);
    this.typeBasic = DiameterConstAvp.FLOAT_32;
  }
  
  setValue(value) {
    this.value = value;
  }
}

module.exports = Float32;
