
'use strict';

const AvpHeader = require('./avp-header');
const DiameterConstAvp = require('../diameter-const-avp');


class Grouped extends AvpHeader {
  constructor(code, mBit=0, vbit=0, pBit=0, vendorId=0) {
    super(code, 0, undefined, mBit, vbit, pBit, vendorId);
    this.avps = new Map();
    this.typeBasic = DiameterConstAvp.GROUPED;
  }

  addAvp(avp) {
    this.avps.set(avp.constructor.name, avp);
    this.length += avp.length;
  }
}


module.exports = Grouped;
