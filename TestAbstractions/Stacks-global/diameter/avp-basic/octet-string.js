
'use strict';

const AvpHeader = require('./avp-header');
const DiameterConstAvp = require('../diameter-const-avp');


class OctetString extends AvpHeader {
  constructor(code, value, mBit = 0, vbit = 0, pBit = 0, vendorId = 0) {
    super(code, OctetString._size(value), value, mBit, vbit, pBit, vendorId);
    this.typeBasic = DiameterConstAvp.OCTET_STRING;
  }
  
  setValue(value) {
    this.value = value;
    this.changeLength(OctetString._size(value));
  }
  
  static _size(value) {
    const rest = value.length % 4;
    if(0 === rest) {
      return value.length;
    }
    else {
      return value.length + 4 - rest;
    }
  }
}


module.exports = OctetString;
