
'use strict';

const StackApi = require('stack-api');
const DiameterConst = require('./diameter-const');
const DiameterConstCommandCode  = require('./diameter-const-command-code');
const DiameterConstHeader = require('./diameter-const-header');
const DiameterConstAvp = require('./diameter-const-avp');


class DiameterInnerLog {
  static createHeader(msg) {
    const header = new StackApi.LogInner('Header', [], true);
    
    const version = [];
    version.push(new StackApi.LogPartRef('Version', `${DiameterConst.DOCUMENTATION_LINK_ROOT}${DiameterConstHeader.VERSION}`));
    version.push(new StackApi.LogPartText(`: ${msg.version}`));
    header.add(new StackApi.LogInner(version));
    
    const length = [];
    length.push(new StackApi.LogPartRef('Message Length', `${DiameterConst.DOCUMENTATION_LINK_ROOT}${DiameterConstHeader.MESSAGE_LENGTH}`));
    length.push(new StackApi.LogPartText(`: ${msg.messageLength}`));
    header.add(new StackApi.LogInner(length));
    
    const commandFlags = [];
    const commandFlag = new StackApi.LogInner(commandFlags, []);
    commandFlags.push(new StackApi.LogPartRef('Command Flags', `${DiameterConst.DOCUMENTATION_LINK_ROOT}${DiameterConstHeader.COMMAND_FLAGS}`));
    commandFlags.push(new StackApi.LogPartText(`: binary: ${msg.commandFlags.toString(2).padStart(8, '0')} - decimal: ${msg.commandFlags}`));
    header.add(commandFlag);
    
    const commandFlagsRbit = [];
    commandFlagsRbit.push(new StackApi.LogPartRef('R bit', `${DiameterConst.DOCUMENTATION_LINK_ROOT}${DiameterConstHeader.R_BIT}`));
    commandFlagsRbit.push(new StackApi.LogPartText(`: ${msg.getRBit()} - ${DiameterConstHeader.R_BIT_TEXT[msg.getRBit()]}`));
    commandFlag.add(new StackApi.LogInner(commandFlagsRbit));
    
    const commandFlagsPbit = [];
    commandFlagsPbit.push(new StackApi.LogPartRef('P bit', `${DiameterConst.DOCUMENTATION_LINK_ROOT}${DiameterConstHeader.P_BIT}`));
    commandFlagsPbit.push(new StackApi.LogPartText(`: ${msg.getPBit()} - ${DiameterConstHeader.P_BIT_TEXT[msg.getPBit()]}`));
    commandFlag.add(new StackApi.LogInner(commandFlagsPbit));
    
    const commandFlagsEbit = [];
    commandFlagsEbit.push(new StackApi.LogPartRef('E bit', `${DiameterConst.DOCUMENTATION_LINK_ROOT}${DiameterConstHeader.E_BIT}`));
    commandFlagsEbit.push(new StackApi.LogPartText(`: ${msg.getEBit()} - ${DiameterConstHeader.E_BIT_TEXT[msg.getEBit()]}`));
    commandFlag.add(new StackApi.LogInner(commandFlagsEbit));
    
    const commandFlagsTbit = [];
    commandFlagsTbit.push(new StackApi.LogPartRef('T bit', `${DiameterConst.DOCUMENTATION_LINK_ROOT}${DiameterConstHeader.T_BIT}`));
    commandFlagsTbit.push(new StackApi.LogPartText(`: ${msg.getTBit()} - ${DiameterConstHeader.T_BIT_TEXT[msg.getTBit()]}`));
    commandFlag.add(new StackApi.LogInner(commandFlagsTbit));
    
    const commandCode = [];
    commandCode.push(new StackApi.LogPartRef('Command Code', `${DiameterConst.DOCUMENTATION_LINK_ROOT}${DiameterConstHeader.COMMAND_CODE}`));
    commandCode.push(new StackApi.LogPartText(`: ${msg.commandCode} - `));
    const constCommandCode = DiameterConstCommandCode.get(msg.commandCode)[msg.getRBit()];
    commandCode.push(new StackApi.LogPartRef(`${constCommandCode.name}`, `${DiameterConst.DOCUMENTATION_LINK_ROOT}${constCommandCode.links[0]}`));
    header.add(new StackApi.LogInner(commandCode));
    
    const applicationId = [];
    applicationId.push(new StackApi.LogPartRef('Application-ID', `${DiameterConst.DOCUMENTATION_LINK_ROOT}${DiameterConstHeader.APPLICATION_ID}`));
    applicationId.push(new StackApi.LogPartText(`: ${msg.applicationID}`));
    header.add(new StackApi.LogInner(applicationId));
    
    const hopByHopIdentifier = [];
    hopByHopIdentifier.push(new StackApi.LogPartRef('Hop-by-Hop Identifier', `${DiameterConst.DOCUMENTATION_LINK_ROOT}${DiameterConstHeader.HOP_BY_HOP_IDENTIFIER}`));
    hopByHopIdentifier.push(new StackApi.LogPartText(`: ${msg.hopByHopIdentifier}`));
    header.add(new StackApi.LogInner(hopByHopIdentifier));
    
    const endToEndIdentifier = [];
    endToEndIdentifier.push(new StackApi.LogPartRef('End-to-End Identifier', `${DiameterConst.DOCUMENTATION_LINK_ROOT}${DiameterConstHeader.END_TO_END_IDENTIFIER}`));
    endToEndIdentifier.push(new StackApi.LogPartText(`: ${msg.endToEndIdentifier}`));
    header.add(new StackApi.LogInner(endToEndIdentifier));
    
    return header;
  }
  
  static createCaption(msg, externalCommandCode) {
    let constCommandCode = DiameterConstCommandCode.get(msg.commandCode)[msg.getRBit()];
    if(!constCommandCode.found && undefined !== externalCommandCode) {
      constCommandCode = externalCommandCode.get(msg.commandCode)[msg.getRBit()];
    }
    return `${constCommandCode.abbrev}`;
  }
  
  static createAvps(avps) {
    const avpLogs = new StackApi.LogInner('Avps', [], true);
    avps.forEach((avpArray, name) => {
      const avpLog = [];
      avpArray.forEach((avp) => {
        const avpData = DiameterConstAvp.get(avp.code);
        avpLog.push(new StackApi.LogPartRef(avpData.name, `${DiameterConst.DOCUMENTATION_LINK_ROOT}${avpData.links[0]}`));
        avpLog.push(new StackApi.LogPartText(`: ${avp.value}`));
        const avpInnerLog = new StackApi.LogInner(avpLog, []);
        avpLogs.add(avpInnerLog);
        
        const avpCode = [];
        avpCode.push(new StackApi.LogPartRef('AVP Code', `${DiameterConst.DOCUMENTATION_LINK_ROOT}${DiameterConstAvp.AVP_CODE}`));
        avpCode.push(new StackApi.LogPartText(`: ${avp.code} - `));
        avpCode.push(new StackApi.LogPartRef(avpData.type, `${DiameterConst.DOCUMENTATION_LINK_ROOT}${DiameterConstAvp.AVP_TYPE.get(avpData.type).links[0]}`));
        if(undefined !== avpData.typeBasic) {
          avpCode.push(new StackApi.LogPartText(` extends `));
          avpCode.push(new StackApi.LogPartRef(avpData.typeBasic, `${DiameterConst.DOCUMENTATION_LINK_ROOT}${DiameterConstAvp.AVP_TYPE.get(avpData.typeBasic).links[0]}`));
        }
        avpInnerLog.add(new StackApi.LogInner(avpCode));
        
        const avpFlags = [];
        const avpFlag = new StackApi.LogInner(avpFlags, []);
        avpFlags.push(new StackApi.LogPartRef('AVP Flags', `${DiameterConst.DOCUMENTATION_LINK_ROOT}${DiameterConstAvp.AVP_FLAGS}`));
        avpFlags.push(new StackApi.LogPartText(`: ${avp.flags}`));
        avpFlags.push(new StackApi.LogPartText(`: binary: ${avp.flags.toString(2).padStart(8, '0')} - decimal: ${avp.flags}`));
        avpInnerLog.add(avpFlag);
        
        const avpFlagsVbit = [];
        avpFlagsVbit.push(new StackApi.LogPartRef('V bit', `${DiameterConst.DOCUMENTATION_LINK_ROOT}${DiameterConstAvp.V_BIT}`));
        avpFlagsVbit.push(new StackApi.LogPartText(`: ${avp.vBit} - ${DiameterConstAvp.V_BIT_TEXT[avp.vBit]}`));
        avpFlag.add(new StackApi.LogInner(avpFlagsVbit));
        
        const avpFlagsMbit = [];
        avpFlagsMbit.push(new StackApi.LogPartRef('M bit', `${DiameterConst.DOCUMENTATION_LINK_ROOT}${DiameterConstAvp.M_BIT}`));
        avpFlagsMbit.push(new StackApi.LogPartText(`: ${avp.mBit} - ${DiameterConstAvp.M_BIT_TEXT[avp.mBit]}`));
        avpFlag.add(new StackApi.LogInner(avpFlagsMbit));
        
        const avpFlagsPbit = [];
        avpFlagsPbit.push(new StackApi.LogPartRef('P bit', `${DiameterConst.DOCUMENTATION_LINK_ROOT}${DiameterConstAvp.P_BIT}`));
        avpFlagsPbit.push(new StackApi.LogPartText(`: ${avp.pBit} - ${DiameterConstAvp.P_BIT_TEXT[avp.pBit]}`));
        avpFlag.add(new StackApi.LogInner(avpFlagsPbit));
        
        const avpLength = [];
        avpLength.push(new StackApi.LogPartRef( 'AVP Length', `${DiameterConst.DOCUMENTATION_LINK_ROOT}${DiameterConstAvp.AVP_LENGTH}`));
        avpLength.push(new StackApi.LogPartText(`: ${avp.length}`));
        avpInnerLog.add(new StackApi.LogInner(avpLength));
        
        if(avp.isVbit()) {
          const avpVendorId = [];
          avpVendorId.push(new StackApi.LogPartRef('Vendor-ID', `${DiameterConst.DOCUMENTATION_LINK_ROOT}${DiameterConstAvp.VENDER_ID}`));
          avpVendorId.push(new StackApi.LogPartText(`: ${avp.vendorId}`));
          avpInnerLog.add(new StackApi.LogInner(avpVendorId));
        }
      });
    });
    return avpLogs;
  }
}

module.exports = DiameterInnerLog;
