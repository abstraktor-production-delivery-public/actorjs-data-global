
'use strict';

const StackApi = require('stack-api');
const DiameterConnectionClientOptions = require('./diameter-connection-client-options');
const DiameterEncoder = require('./diameter-encoder');
const DiameterDecoder = require('./diameter-decoder');


class DiameterConnectionClient extends StackApi.ConnectionClient {
  constructor(id, type, actor, options) {
    super(id, type, 'diameter', actor, StackApi.NetworkType.TCP, DiameterConnectionClientOptions, options);
  }
  
  send(msg) {
    this.sendMessage(new DiameterEncoder(msg));
  }
  
  receive() {
    this.receiveMessage(new DiameterDecoder());
  }
}

module.exports = DiameterConnectionClient;
