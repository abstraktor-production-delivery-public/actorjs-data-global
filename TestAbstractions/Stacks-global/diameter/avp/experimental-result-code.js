
'use strict';

const Unsigned32 = require('../avp-basic/unsigned-32');


class ExperimentalResultCode extends Unsigned32 {
  constructor(value, mBit=1, vbit=0, pBit=0, vendorId=0) {
    super(298, value, mBit, vbit, pBit, vendorId);
  }
}

module.exports = ExperimentalResultCode;
