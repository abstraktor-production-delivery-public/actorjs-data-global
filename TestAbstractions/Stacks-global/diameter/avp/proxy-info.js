
'use strict';

const Grouped = require('../avp-basic/grouped');


class ProxyInfo extends Grouped     {
  constructor(value, mBit=1, vbit=0, pBit=0, vendorId=0) {
    super(284, value, mBit, vbit, pBit, vendorId);
  }
}

module.exports = ProxyInfo;
