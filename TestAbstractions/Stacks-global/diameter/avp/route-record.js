
'use strict';

const DiameterIdentity = require('../avp-derived/diameter-identity');


class RouteRecord extends DiameterIdentity {
  constructor(value, mBit=1, vbit=0, pBit=0, vendorId=0) {
    super(282, value, mBit, vbit, pBit, vendorId);
  }
}

module.exports = RouteRecord;
