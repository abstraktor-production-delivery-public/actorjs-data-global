
'use strict';

const DiameterIdentity = require('../avp-derived/diameter-identity');


class ErrorReportingHost extends DiameterIdentity {
  constructor(value, mBit=0, vbit=0, pBit=0, vendorId=0) {
    super(294, value, mBit, vbit, pBit, vendorId);
  }
}

module.exports = ErrorReportingHost;
