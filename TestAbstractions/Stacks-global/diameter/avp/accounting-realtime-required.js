
'use strict';

const Enumerated = require('../avp-derived/enumerated');


class AccountingRealtimeRequired extends Enumerated {
  constructor(value, mBit=1, vbit=0, pBit=0, vendorId=0) {
    super(483, value, mBit, vbit, pBit, vendorId);
  }
}

module.exports = AccountingRealtimeRequired;
