
'use strict';

const Unsigned32 = require('../avp-basic/unsigned-32');


class FirmwareRevision extends Unsigned32 {
  constructor(value, mBit=o, vbit=0, pBit=0, vendorId=0) {
    super(267, value, mBit, vbit, pBit, vendorId);
  }
}

module.exports = FirmwareRevision;
