
'use strict';

const DiameterURI = require('../avp-derived/diameter-uri');


class RedirectHost extends DiameterURI {
  constructor(value, mBit=1, vbit=0, pBit=0, vendorId=0) {
    super(292, value, mBit, vbit, pBit, vendorId);
  }
}

module.exports = RedirectHost;
