
'use strict';

const Address = require('../avp-derived/address');


class HostIPAddress extends Address {
  constructor(value, mBit=1, vbit=0, pBit=0, vendorId=0) {
    super(257, value, mBit, vbit, pBit, vendorId);
  }
}

module.exports = HostIPAddress;
