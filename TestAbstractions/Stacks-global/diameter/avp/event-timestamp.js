
'use strict';

const Time = require('../avp-derived/time');


class EventTimestamp extends Time {
  constructor(value, mBit=1, vbit=0, pBit=0, vendorId=0) {
    super(55, value, mBit, vbit, pBit, vendorId);
  }
}

module.exports = EventTimestamp;
