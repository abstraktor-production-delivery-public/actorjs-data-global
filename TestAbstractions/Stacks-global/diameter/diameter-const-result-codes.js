
'use strict';


class DiameterConstResultCodes {}

// -  1xxx (Informational)

DiameterConstResultCodes.DIAMETER_MULTI_ROUND_AUTH = 1001;

// -  2xxx (Success)

DiameterConstResultCodes.DIAMETER_SUCCESS = 2001;
DiameterConstResultCodes.DIAMETER_LIMITED_SUCCESS = 2002;

// -  3xxx (Protocol Errors)

DiameterConstResultCodes.DIAMETER_COMMAND_UNSUPPORTED = 3001;
DiameterConstResultCodes.DIAMETER_UNABLE_TO_DELIVER = 3002;
DiameterConstResultCodes.DIAMETER_REALM_NOT_SERVED = 3003;
DiameterConstResultCodes.DIAMETER_TOO_BUSY = 3004;
DiameterConstResultCodes.DIAMETER_LOOP_DETECTED = 3005;
DiameterConstResultCodes.DIAMETER_REDIRECT_INDICATION = 3006;
DiameterConstResultCodes.DIAMETER_APPLICATION_UNSUPPORTED = 3007;
DiameterConstResultCodes.DIAMETER_INVALID_HDR_BITS = 3008;
DiameterConstResultCodes.DIAMETER_INVALID_AVP_BITS = 3009;
DiameterConstResultCodes.DIAMETER_UNKNOWN_PEER = 3010;

// -  4xxx (Transient Failures)

DiameterConstResultCodes.DIAMETER_AUTHENTICATION_REJECTED = 4001;
DiameterConstResultCodes.DIAMETER_OUT_OF_SPACE = 4002;
DiameterConstResultCodes.ELECTION_LOST = 4003;

// -  5xxx (Permanent Failure)

DiameterConstResultCodes.DIAMETER_AVP_UNSUPPORTED = 5001;
DiameterConstResultCodes.DIAMETER_UNKNOWN_SESSION_ID = 5002;
DiameterConstResultCodes.DIAMETER_AUTHORIZATION_REJECTED = 5003;
DiameterConstResultCodes.DIAMETER_INVALID_AVP_VALUE = 5004;
DiameterConstResultCodes.DIAMETER_MISSING_AVP = 5005;
DiameterConstResultCodes.DIAMETER_RESOURCES_EXCEEDED = 5006;
DiameterConstResultCodes.DIAMETER_CONTRADICTING_AVPS = 5007;
DiameterConstResultCodes.DIAMETER_AVP_NOT_ALLOWED = 5008;
DiameterConstResultCodes.DIAMETER_AVP_OCCURS_TOO_MANY_TIMES = 5009;
DiameterConstResultCodes.DIAMETER_NO_COMMON_APPLICATION = 5010;
DiameterConstResultCodes.DIAMETER_UNSUPPORTED_VERSION = 5011;
DiameterConstResultCodes.DIAMETER_UNABLE_TO_COMPLY = 5012;
DiameterConstResultCodes.DIAMETER_INVALID_BIT_IN_HEADER = 5013;
DiameterConstResultCodes.DIAMETER_INVALID_AVP_LENGTH = 5014;
DiameterConstResultCodes.DIAMETER_INVALID_MESSAGE_LENGTH = 5015;
DiameterConstResultCodes.DIAMETER_INVALID_AVP_BIT_COMBO = 5016;
DiameterConstResultCodes.DIAMETER_NO_COMMON_SECURITY = 5017;

module.exports = DiameterConstResultCodes;
