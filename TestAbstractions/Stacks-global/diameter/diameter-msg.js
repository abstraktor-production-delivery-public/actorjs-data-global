
'use strict';

//       0                   1                   2                   3
//       0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
//      +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
//      |    Version    |                 Message Length                |
//      +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
//      | Command Flags |                  Command Code                 |
//      +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
//      |                         Application-ID                        |
//      +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
//      |                      Hop-by-Hop Identifier                    |
//      +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
//      |                      End-to-End Identifier                    |
//      +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
//      |  AVPs ...
//      +-+-+-+-+-+-+-+-+-+-+-+-+-

//       0 1 2 3 4 5 6 7
//      +-+-+-+-+-+-+-+-+
//      |R P E T r r r r|
//      +-+-+-+-+-+-+-+-+

class DiameterMsg {
  constructor(commandCode, applicationID, version, commandFlags, messageLength, hopByHopIdentifier, endToEndIdentifier) {
    this.version = version;
    this.messageLength = 20;
    this.fakeLength = messageLength;
    this.commandFlags = commandFlags;
    this.commandCode = commandCode;
    this.applicationID = applicationID;
    this.hopByHopIdentifier = hopByHopIdentifier;
    this.endToEndIdentifier = endToEndIdentifier;
    this.avps = new Map();
  }
  
  getRBit() {
    return this._getBit(this.commandFlags, 7);
  }
  
  getPBit() {
    return this._getBit(this.commandFlags, 6);
  }
  
  getEBit() {
    return this._getBit(this.commandFlags, 5);
  }
  
  getTBit() {
    return this._getBit(this.commandFlags, 4);
  }
  
  addAvp(avpFactory, value) {
    if(undefined !== value) {
      if(Array.isArray(value)) {
        value.forEach((val) => {
          this._addAvp(new avpFactory(val));
        });
      }
      else {
        this._addAvp(new avpFactory(value));
      }
    }
  }
  
  addAvpObject(avp) {
    this._addAvp(avp);
  }
  
  _getBit(num, bit) {
    return (num >> bit) % 2 ? 1 : 0;
  }
  
  _addAvp(avp) {
    const name = avp.constructor.name;
    if(this.avps.has(name)) {
      this.avps.get(name).push(avp);
    }
    else {
      this.avps.set(name, [avp]);    
    }
    this.messageLength += avp.length;
  }
}

module.exports = DiameterMsg;
