
'use strict';

const StackApi = require('stack-api');
const SomeipConnectionServerOptions = require('./someip-connection-server-options');
const SomeipEncoder = require('./someip-encoder');
const SomeipDecoder = require('./someip-decoder');


class SomeipConnectionServer extends StackApi.ConnectionServer {
  constructor(id, type, actor, options) {
    super(id, type, 'someip', actor, StackApi.NetworkType.TCP, SomeipConnectionServerOptions, options);
  }
  
  send(msg) {
    this.sendMessage(new SomeipEncoder(msg));
  }
  
  receive() {
    this.receiveMessage(new SomeipDecoder());
  }
}

module.exports = SomeipConnectionServer;
