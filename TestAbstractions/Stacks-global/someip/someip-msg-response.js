
'use strict';

const SomeipConst = require('./someip-const');
const SomeipMsg = require('./someip-msg');


class SomeipMsgResponse extends SomeipMsg {
  constructor(serviceId, methodIdEventId, clientId, sessionId, protocolVersion, interfaceVersion, payload) {
    super(serviceId, methodIdEventId, 8 + (undefined != payload ? Buffer.byteLength(payload) : 0), clientId, sessionId, protocolVersion, interfaceVersion, SomeipConst.MSG_TYPE__RESPONSE, SomeipConst.E_OK, payload);
  }
}

module.exports = SomeipMsgResponse;
