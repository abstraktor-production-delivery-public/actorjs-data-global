
'use strict';

const StackApi = require('stack-api');
const SomeipConnectionClientOptions = require('./someip-connection-client-options');
const SomeipEncoder = require('./someip-encoder');
const SomeipDecoder = require('./someip-decoder');


class SomeipConnectionClient extends StackApi.ConnectionClient {
  constructor(id, type, actor, options) {
    super(id, type, 'someip', actor, StackApi.NetworkType.TCP, SomeipConnectionClientOptions, options);
  }
  
  send(msg) {
    this.sendMessage(new SomeipEncoder(msg));
  }
  
  receive() {
    this.receiveMessage(new SomeipDecoder());
  }
}

module.exports = SomeipConnectionClient;
