
'use strict';

const StackApi = require('stack-api');
const RadiusConnectionServerOptions = require('./radius-connection-server-options');
const RadiusEncoder = require('./radius-encoder');
const RadiusDecoder = require('./radius-decoder');


class RadiusConnectionServer extends StackApi.ConnectionServer {
  constructor(id, type, actor, options) {
    super(id, type, 'radius', actor, StackApi.NetworkType.UDP, RadiusConnectionServerOptions, options);
  }
  
  receive() {
    this.receiveMessage(new RadiusDecoder());
  }
  
  send(msg) {
    this.sendMessage(new RadiusEncoder(msg));
  }
}

module.exports = RadiusConnectionServer;
