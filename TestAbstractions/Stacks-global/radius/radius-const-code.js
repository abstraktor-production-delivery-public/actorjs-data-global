
'use strict';


class RadiusConstCode {
  static get(code) {
    const codeData = RadiusConstCode.HEADER_DATA.get(code);
    if(undefined !== codeData) {
      return codeData;
    }
    else {
      return RadiusConstCode.UNKNOWN;
    }
  }
}

RadiusConstCode.ACCESS_REQUEST = 1;
RadiusConstCode.ACCESS_ACCEPT = 2;
RadiusConstCode.ACCESS_REJECT = 3;
RadiusConstCode.ACCOUNTING_REQUEST = 4;
RadiusConstCode.ACCOUNTING_RESPONSE = 5;
RadiusConstCode.ACCOUNTING_STATUS = 6;
RadiusConstCode.PASSWORD_REQUEST = 7;
RadiusConstCode.PASSWORD_ACK = 8;
RadiusConstCode.PASSWORD_REJECT = 9;
RadiusConstCode.ACCOUNTING_MESSAGE = 10;
RadiusConstCode.ACCESS_CHALLENGE = 11;
RadiusConstCode.STATUS_SERVER = 12;
RadiusConstCode.STATUS_CLIENT = 13;
/*   21       Resource-Free-Request        [RFC2882]
   22       Resource-Free-Response       [RFC2882]
   23       Resource-Query-Request       [RFC2882]
   24       Resource-Query-Response      [RFC2882]
   25       Alternate-Resource-
            Reclaim-Request              [RFC2882]
   26       NAS-Reboot-Request           [RFC2882]
   27       NAS-Reboot-Response          [RFC2882]
   28       Reserved
   29       Next-Passcode                [RFC2882]
   30       New-Pin                      [RFC2882]
   31       Terminate-Session            [RFC2882]
   32       Password-Expired             [RFC2882]
   33       Event-Request                [RFC2882]
   34       Event-Response               [RFC2882]
   40       Disconnect-Request           [DynAuth]
   41       Disconnect-ACK               [DynAuth]
   42       Disconnect-NAK               [DynAuth]
   43       CoA-Request                  [DynAuth]
   44       CoA-ACK                      [DynAuth]
   45       CoA-NAK                      [DynAuth]
   50       IP-Address-Allocate          [RFC2882]
   51       IP-Address-Release           [RFC2882]
   250-253  Experimental Use
   254      Reserved
   255      Reserved                     [RFC2865]
*/
RadiusConstCode.HEADER_DATA = new Map([
  [RadiusConstCode.ACCESS_REQUEST, {links: ['rfc2865#section-4', 'rfc2865#section-4.1'], abbrev: 'Access-Request', name: 'Access-Request'}],
  [RadiusConstCode.ACCESS_ACCEPT, {links: ['rfc2865#section-4', 'rfc2865#section-4.2'], abbrev: 'Access-Accept', name: 'Access-Accept'}],
  [RadiusConstCode.ACCESS_REJECT, {links: ['rfc2865#section-4', 'rfc2865#section-4.3'], abbrev: 'Access-Reject', name: 'Access-Reject'}],
  [RadiusConstCode.ACCOUNTING_REQUEST, {links: ['rfc2866#section-4', 'rfc2866#section-4.1'], abbrev: 'Accounting-Request', name: 'Accounting-Request'}],
  [RadiusConstCode.ACCOUNTING_RESPONSE, {links: ['rfc2866#section-4', 'rfc2866#section-4.2'], abbrev: 'Accounting-Response', name: 'Accounting-Response'}],
  [RadiusConstCode.ACCOUNTING_STATUS, {links: ['rfc2882', 'rfc2882'], abbrev: 'Accounting-Status', name: 'Accounting-Status'}],
  [RadiusConstCode.PASSWORD_REQUEST, {links: ['rfc2882', 'rfc2882'], abbrev: 'Password-Request', name: 'Password-Request'}],
  [RadiusConstCode.PASSWORD_ACK, {links: ['rfc2882', 'rfc2882'], abbrev: 'Password-Ack', name: 'Password-Ack'}],
  [RadiusConstCode.PASSWORD_REJECT, {links: ['rfc2882', 'rfc2882'], abbrev: 'Password-Reject', name: 'Password-Reject'}],
  [RadiusConstCode.ACCOUNTING_STATUS, {links: ['rfc2882', 'rfc2882'], abbrev: 'Accounting-Message', name: 'Accounting-Message'}],
  [RadiusConstCode.ACCESS_CHALLENGE, {links: ['rfc2865#section-4', 'rfc2865#section-4.4'], abbrev: 'Access-Challenge', name: 'Access-Challenge'}],
  [RadiusConstCode.STATUS_SERVER, {links: ['#section-4', 'xxxxx'], abbrev: 'Status-Server', name: 'Status-Server'}],
  [RadiusConstCode.STATUS_CLIENT, {links: ['#section-4', 'xxxxx'], abbrev: 'Status-Client', name: 'Status-Client'}]
]);

RadiusConstCode.UNKNOWN = {links: ['', ''], abbrev: 'UNKNOWN', name: 'UNKNOWN'};


module.exports = RadiusConstCode;
