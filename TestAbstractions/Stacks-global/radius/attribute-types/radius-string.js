
'use strict';

const RadiusAttributeHeader = require('../radius-attribute-header');


class RadiusString extends RadiusAttributeHeader {
  constructor(type, value) {
    super(type, value.length - 2, value);
  }
}

module.exports = RadiusString;
