
'use strict';

//  0                   1                   2
//  0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0
// +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-
// |     Type      |    Length     |  Value ...
// +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-
     
class RadiusAttributeHeader {
  constructor(type, length, value) {
    this.type = type;
    this.length = length;
    this.value = value;
  }
}

module.exports = RadiusAttributeHeader;
