
'use strict';

const StackApi = require('stack-api');
//const DiameterInnerLog = require('./diameter-inner-log');


class RadiusEncoder extends StackApi.Encoder {
  constructor(msg, command = RadiusEncoder.SEND_ALL) {
    super();
    this.msg = msg;
    this.command = command;
    this.offset = 0;
    this.buffer = null;
    this.buffers = [];
    this.size = 0;
  }
  
  *encode() {
    if(this.isLogIp) {
      this.setCaption(this.msg.msg);
      this.logMessage();
    }
    yield* this.send(`${this.msg.msg}\r\n`);
    
    
    /*
    switch(this.command) {
      case RadiusEncoder.SEND_ALL:
        yield* this._sendAll();
        break;
      default:
        throw new Error('NOT IMPLEMENTD');
        break;
    };*/
  }
  
  _createBuffer(size) {
    /*if(0 !== size) {
      this.offset = 0;
      this.buffer = Buffer.allocUnsafe(size);
      this.buffers.push(this.buffer);
      return size;
    }
    else {
      return 0;
    }*/
  }
  
  encodeHeader() {
    /*const msg = this.msg;
    this.buffer.writeUInt8(msg.version, 0);
    this.buffer.writeUIntBE(msg.messageLength, 1, 3);
    this.buffer.writeUInt8(msg.commandFlags, 4);
    this.buffer.writeUIntBE(msg.commandCode, 5, 3);
    this.buffer.writeUInt32BE(msg.applicationID, 8);
    this.buffer.writeUInt32BE(msg.hopByHopIdentifier, 12);
    this.buffer.writeUInt32BE(msg.endToEndIdentifier, 16);
  */}
  
  encodeAvps(avps, offset) {
    /*avps.forEach((avpArray) => {
      avpArray.forEach((avp) => {
        offset = this.buffer.writeUInt32BE(avp.code, offset);
        offset = this.buffer.writeUInt8(avp.flags, offset);
        offset = this.buffer.writeUIntBE(avp.length, offset, 3);
        if(0 !== avp.vendorId) {
          offset = this.buffer.writeUInt32BE(avp.vendorId, offset);
        }
        if(DiameterConstAvp.GROUPED === avp.typeBasic) {
          this.encodeAvps(avp.avps, offset);
        }
        else {
          switch(avp.typeBasic) {
            case DiameterConstAvp.FLOAT_32:
              offset = this.buffer.writeFloatBE(avp.value, offset);
              break;
            case DiameterConstAvp.FLOAT_64:
              offset = this.buffer.writeDoubleBE(avp.value, offset);
              break;
            case DiameterConstAvp.INTEGER_32:
              offset = this.buffer.writeInt32BE(avp.value, offset);
              break;
            case DiameterConstAvp.INTEGER_64:
              //offset = this.buffer.writeBigInt64BE(avp.value, offset);   // TODO: Use when v12.....
              offset = this.buffer.writeIntBE(avp.value, offset, 8);
              break;
            case DiameterConstAvp.OCTET_STRING:
              offset += this.buffer.write(avp.value, offset, avp.value.length, 'ascii');
              const rest = avp.value.length % 4;
              if(0 !== rest) {
                this.buffer.writeUIntBE(0, offset, 4 - rest);
                offset += (4 - rest);
              }
              break;
            case DiameterConstAvp.UNSIGNED_32:
              offset = this.buffer.writeUInt32BE(avp.value, offset);
              break;
            case DiameterConstAvp.UNSIGNED_64:
              //offset = this.buffer.writeBigUInt64BE(avp.value, offset);  // TODO: Use when v12.....
              offset = this.buffer.writeUIntBE(avp.value, offset, 8);
              break;
            default:
              break;
          };
        }
      });
    });*/
  }
  
  *_sendAll() {
    this._createBuffer(this.msg.messageLength);
    this.encodeHeader();
    this.encodeAvps(this.msg.avps, 20);
    while(0 !== this.buffers.length) {
      yield* this.send(this.buffers.shift());
    }
    if(this.isLogIp) {
      // this.addLog(DiameterInnerLog.createHeader(this.msg));
      // this.addLog(DiameterInnerLog.createAvps(this.msg.avps));
      // this.setCaption(DiameterInnerLog.createCaption(this.msg));
      this.logMessage();
    }
  }
}

RadiusEncoder.SEND_ALL = 0;


module.exports = RadiusEncoder;
