
'use strict';

const StackApi = require('stack-api');
const RadiusConnectionClientOptions = require('./radius-connection-client-options');
const RadiusEncoder = require('./radius-encoder');
const RadiusDecoder = require('./radius-decoder');


class RadiusConnectionClient extends StackApi.ConnectionClient {
  constructor(id, type, actor, options) {
    super(id, type, 'radius', actor, StackApi.NetworkType.UDP, RadiusConnectionClientOptions, options);
  }
  
  send(msg) {
    this.sendMessage(new RadiusEncoder(msg));
  }
  
  receive() {
    this.receiveMessage(new RadiusDecoder());
  }
}

module.exports = RadiusConnectionClient;
