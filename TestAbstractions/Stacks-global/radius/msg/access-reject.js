
'use strict';

const RadiusMsg = require('../radius-msg');
const RadiusConstCode = require('../radius-const-code');


class AccessReject extends RadiusMsg {
  constructor(identifier, authenticator, length=-1) {
    super(RadiusConstCode.ACCESS_REJECT, identifier, authenticator, length);
    this.msg = 'Access-Reject';
  }
}

module.exports = AccessReject;
