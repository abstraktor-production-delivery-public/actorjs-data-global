
'use strict';

const RadiusMsg = require('../radius-msg');
const RadiusConstCode = require('../radius-const-code');


class AccessRequest extends RadiusMsg {
  constructor(identifier, authenticator, length=-1) {
    super(RadiusConstCode.ACCESS_REQUEST, identifier, authenticator, length);
    this.msg = 'Access-Request';
  }
}

module.exports = AccessRequest;
