
'use strict';

const RadiusString = require('../attribute-types/radius-string');


class ChapPassword extends RadiusString {
  constructor(chapIdent, value) {
    super(19, value);
  }
}

module.exports = ChapPassword;
