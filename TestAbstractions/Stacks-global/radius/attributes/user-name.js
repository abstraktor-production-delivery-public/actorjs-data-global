
'use strict';

const RadiusString = require('../attribute-types/radius-string');


class UserName extends RadiusString {
  constructor(value) {
    super(1, value);
  }
}

module.exports = UserName;
