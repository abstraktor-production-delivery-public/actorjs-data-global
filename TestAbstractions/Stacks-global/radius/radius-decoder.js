
'use strict';

const StackApi = require('stack-api');


class RadiusDecoder extends StackApi.Decoder {
  constructor(isLogIp) {
    super(isLogIp);
  }
  
  *decode() {
    const msg = yield* this.receiveLine();
    if(this.isLogIp) {
      this.setCaption(msg);
      this.logMessage();
    }
    return {
      msg: msg
    };
  }
}

module.exports = RadiusDecoder;
