
'use strict';

const StackApi = require('stack-api');
const SocketLoggerObject = require('./socket-logger-object');


class SocketEncoderObject extends StackApi.Encoder {
  constructor(object) {
    super();
    this.object = object;
  }
  
  *encode() {
    const msgObject = JSON.stringify(this.object);
    const buffer = Buffer.allocUnsafe(Buffer.byteLength(msgObject) + SocketEncoderObject.CL_CR_LENGTH);
    let offset = 0;
    offset += buffer.write(msgObject, offset);
    buffer.write(SocketEncoderObject.CL_CR, offset);
    yield* this.send(buffer);
    if(this.isLogIp) {
      this.setCaption(SocketLoggerObject.generateLog(this.object, this.ipLog));
      this.logMessage();
    }
  }
}

SocketEncoderObject.CL_CR = '\r\n';
SocketEncoderObject.CL_CR_LENGTH = Buffer.byteLength(SocketEncoderObject.CL_CR);


module.exports = SocketEncoderObject;
