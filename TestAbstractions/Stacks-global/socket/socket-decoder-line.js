
'use strict';

const StackApi = require('stack-api');
const SocketLoggerLine = require('./socket-logger-line');


class SocketDecoderLine extends StackApi.Decoder {
  constructor() {
    super();
  }
  
  *decode() {
    const msg = yield* this.receiveLine();
    if(this.isLogIp) {
      this.setCaption(SocketLoggerLine.generateLog(msg, this.ipLog));
      this.logMessage();
    }
    return msg;
  }
}

module.exports = SocketDecoderLine;
