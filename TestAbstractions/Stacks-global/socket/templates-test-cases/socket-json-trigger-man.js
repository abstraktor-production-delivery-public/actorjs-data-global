
'use strict';

const StackApi = require('stack-api');


class SocketJsonTriggerMan extends StackApi.StackComponentsTemplatesTestCases {

  static createTestCase(testCase, testData, repoName, sutName, futName, tcName) {
    console.log('SocketJsonTriggerMan:', testCase, testData, repoName, sutName, futName, tcName);
  }
}

SocketJsonTriggerMan.displayName = 'json-trigger-man';
SocketJsonTriggerMan.regressionFriendly = 'no';

SocketJsonTriggerMan.template = SocketJsonTriggerMan._template`
'use strict';

const ActorApi = require('actor-api');


class ${'name'}${'actorEnding'} extends ActorApi.Actor${'actorType'} {
  constructor() {
    super();
    this.socketConnection = null;
    this.json = '';
    this.receiveResponse = false;
  }
  
  *data() {
    this.json = this.getTestDataString('trigger-man-socket-json');
    this.receiveResponse = this.getTestDataBoolean('receive-response', this.receiveResponse):
  }
  
  *initClient() {
    this.socketConnection = this.createConnection('socket');
  }
  
  *run() {
    this.socketConnection.send(this.text);
    if(this.receiveResponse) {
      const response = this.socketConnection.receive();
    }
  }
  
  *exit(interrupted) {
    this.closeConnection(this.socketConnection);
  }
}

module.exports = ${'name'}${'actorEnding'};
`;

SocketJsonTriggerMan.markupNodes = 2;

SocketJsonTriggerMan.markup = `\`\`\`seq
Config(nodeWidth: 126, nodeMessageHeight: 17, nodeEventHeight: 10, widthBias: 23, heightBias: 30, lineNumbers: false, border: false, backgroundColor: transparent)

Title: SOCKET
Nodes[orig, server]
orig -o server[http]: connect
orig => server[http]: json
server => orig[http]: [response text/json]
orig -x server[http]: disconnect
\`\`\`
`;


module.exports = SocketJsonTriggerMan;
