
'use strict';

const StackApi = require('stack-api');


class SocketBinTriggerMan extends StackApi.StackComponentsTemplatesTestCases {
  
  static createTestCase(testCase, testData, repoName, sutName, futName, tcName) {
    console.log('SocketBinTriggerMan:', testCase, testData, repoName, sutName, futName, tcName);
  }
}

SocketBinTriggerMan.displayName = 'bin-trigger-man';
SocketBinTriggerMan.regressionFriendly = 'no';


module.exports = SocketBinTriggerMan;
