
'use strict';

const StackApi = require('stack-api');


class SocketOrigTermConnectTcp extends StackApi.StackComponentsTemplatesTestCases {

  static createTestCase(testCase, testData) {
    console.log(testCase, testData);
  }
}

SocketOrigTermConnectTcp.displayName = 'orig-term-conncet-tcp';

SocketOrigTermConnectTcp.template = SocketOrigTermConnectTcp._template`

`;

SocketOrigTermConnectTcp.testDataViews = [
  {
    displayName: 'YABADO',
    name: 'yabado',
    view: 'single',
    data: [
      {
        name: 'transport',
        type: 'input',
        placeHolder: 'bla bla',
        value: 'tcp',
        description: 'Transport Layer: tcp | udp | mc | tls',
        valid: 1
      },
      {
        name: 'tctd2',
        type: 'input',
        placeHolder: 'yada yada',
        value: 'HelloS',
        valid: 1
      }
    ]
  }
];

SocketOrigTermConnectTcp.markupNodes = 2;

SocketOrigTermConnectTcp.markup = `\`\`\`seq
Config(nodeWidth: 126, nodeMessageHeight: 17, nodeEventHeight: 10, widthBias: 23, heightBias: 30, lineNumbers: false, border: false, backgroundColor: transparent)

Nodes[orig, server]
orig -o server[socket]: connect
orig -x server[socket]: disconnect
\`\`\`
`;

module.exports = SocketOrigTermConnectTcp;
