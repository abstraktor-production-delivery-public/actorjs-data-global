
'use strict';

const SocketValidator = require('./helper/socket-validator');
const StackApi = require('stack-api');
const GuidGenerator = require('z-abs-corelayer-cs/clientServer/guid-generator');


class SocketTextMockMan extends StackApi.StackComponentsTemplatesTestCases {
  
  static createTestCase(testCase, testData, repoName, sutName, futName, tcName) {
    let name = `Inline${GuidGenerator.create()}`;
    name = name.replaceAll('-', '');
    const actor = {
      name: `Actors-inline.${repoName}.${sutName}.${futName}.${tcName}.${name}Term`,
      type: 'term',
      phase: 'exec',
      execution: '',
      src: '',
      dst: '',
      srv: '',
      testData: '',
      verification: '',
      inlineCode: ''
    };
    testCase.actors.push(actor);
    actor.inlineCode = SocketTextMockMan.template({
      actorEnding: 'Term',
      name: name,
      actorType: 'Terminating'
    });
    
    const socketData = testData[0].data;
    const host = socketData[0].value;
    const port = socketData[1].value;
    actor.srv = `{"host":"${host}","port":"${port}"}`;
    
    const transportType = socketData[2].value;
    testCase.testDataTestCases.push({
      key: 'transport-type',
      value: transportType,
      description: `Transport Type - ${transportType}`
    });
    if('tcp' === transportType || 'tls' === transportType) {
      const allowHalfOpen = socketData[3].value;
      testCase.testDataTestCases.push({
        key: 'transport-properties[]',
        value: `"allowHalfOpen", ${allowHalfOpen}`,
        description: `allowHalfOpen socket`
      });
    }
    const textData = testData[1].data;
    const receiveResponse = textData[0].value;
    testCase.testDataTestCases.push({
      key: 'send-response',
      value: receiveResponse.toString(),
      description: ''
    });
    const text = textData[1].value;
    testCase.testDataTestCases.push({
      key: 'text',
      value: text,
      description: ''
    });
  }
}

SocketTextMockMan.displayName = 'text-mock-man';
SocketTextMockMan.regressionFriendly = 'no';

SocketTextMockMan.template = SocketTextMockMan._template`
'use strict';

const ActorApi = require('actor-api');


class ${'name'}${'actorEnding'} extends ActorApi.Actor${'actorType'} {
  constructor() {
    super();
    this.socketConnection = null;
    this.text = '';
    this.sendResponse = false;
  }
  
  *data() {
    this.text = this.getTestDataString('text');
    this.sendResponse = this.getTestDataBoolean('send-response', this.sendResponse);
  }
  
  *initServer() {
    this.socketConnection = this.createServer('socket');
  }
  
  *run() {
    this.socketConnection.accept();
  
    const response = this.socketConnection.receive();
    if(this.sendResponse) {
      this.socketConnection.send(this.text);
    }
  }
  
  *exit(interrupted) {
    this.closeConnection(this.socketConnection);
  }
}

module.exports = ${'name'}${'actorEnding'};
`;

SocketTextMockMan.validateHostFunctionString = ``;


SocketTextMockMan.testDataViews = [
  {
    displayName: 'Socket',
    name: 'socket',
    view: 'single',
    data: [
      {
        name: 'host',
        type: 'input',
        placeHolder: 'host',
        value: '127.0.0.1',
        description: 'Destination Host',
        valid: 1
      },
      {
        name: 'port',
        type: 'input',
        placeHolder: 'port',
        value: '8080',
        description: 'Destination Port',
        valid: 1,
        validate: SocketValidator.validatePortFunctionString
      },
      {
        name: 'transport',
        type: 'radio',
        values: ['tcp', 'tls', 'udp', 'mc'],
        value: 'tcp',
        description: 'Transport Layer: tcp | tls | udp | mc'
      },
      {
        name: 'allowHalfOpen',
        type: 'radio',
        values: ['true', 'false'],
        value: 'false',
        description: '',
        depends: 'transport,tcp|tls'
      }
    ]
  },
  {
    displayName: 'Text',
    name: 'text',
    view: 'single',
    data: [
      {
        name: 'Receive Response',
        type: 'checkbox',
        value: false,
        valid: 0
      },
      {
        name: 'text',
        type: 'textarea',
        rows: 5,
        guid: '84237a85-f599-4ea1-8bd1-600335faea13',
        value: '',
        valid: 0
      }
    ]
  }
];

SocketTextMockMan.markupNodes = 2;

SocketTextMockMan.markup = `\`\`\`seq
Config(nodeWidth: 126, nodeMessageHeight: 17, nodeEventHeight: 10, widthBias: 23, heightBias: 30, lineNumbers: false, border: false, backgroundColor: transparent)

Title: SOCKET
Nodes[client, term]
client -o term[http]: connect
client => term[http]: text
server => orig[http]: [response text/json]
client -x term[http]: disconnect
\`\`\`
`;


module.exports = SocketTextMockMan;
