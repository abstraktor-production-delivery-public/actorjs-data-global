
'use strict';


class SocketValidator {}

SocketValidator.validatePortFunctionString = `
const onlyDigits = new RegExp('^[0-9]+$');
if(!onlyDigits.test(preview)) {
  return 3; // ERROR
}
const port = Number.parseInt(preview);
if(!Number.isInteger(port)) {
  return 3; // ERROR
}
else if(port <= 1023 || port >= 49152) {
  return 2; // ERROR
}
else {
  return 0; // NONE
}
`;


module.exports = SocketValidator;
