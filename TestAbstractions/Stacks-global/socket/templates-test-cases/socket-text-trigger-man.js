
'use strict';

const SocketValidator = require('./helper/socket-validator');
const StackApi = require('stack-api');
const GuidGenerator = require('z-abs-corelayer-cs/clientServer/guid-generator');


class SocketTextTriggerMan extends StackApi.StackComponentsTemplatesTestCases {

  static createTestCase(testCase, testData, repoName, sutName, futName, tcName) {
    let name = `Inline${GuidGenerator.create()}`;
    name = name.replaceAll('-', '');
    const actor = {
      name: `Actors-inline.${repoName}.${sutName}.${futName}.${tcName}.${name}Orig`,
      type: 'orig',
      phase: 'exec',
      execution: '',
      src: 'DEFAULT_SRC',
      dst: '',
      srv: '',
      testData: '',
      verification: '',
      inlineCode: ''
    };
    testCase.actors.push(actor);
    actor.inlineCode = SocketTextTriggerMan.template({
      actorEnding: 'Orig',
      name: name,
      actorType: 'Originating'
    });
    
    const socketData = testData[0].data;
    const host = socketData[0].value;
    const port = socketData[1].value;
    actor.dst = `{"host":"${host}","port":"${port}"}`;
    
    const transportType = socketData[2].value;
    testCase.testDataTestCases.push({
      key: 'transport-type',
      value: transportType,
      description: `Transport Type - ${transportType}`
    });
    if('tcp' === transportType || 'tls' === transportType) {
      const allowHalfOpen = socketData[3].value;
      testCase.testDataTestCases.push({
        key: 'transport-properties[]',
        value: `["allowHalfOpen", ${allowHalfOpen}]`,
        description: `allowHalfOpen socket`
      });
    }
    const textData = testData[1].data;
    const text = textData[0].value;
    testCase.testDataTestCases.push({
      key: 'text',
      value: text,
      description: ''
    });
    const receiveResponse = textData[1].value;
    testCase.testDataTestCases.push({
      key: 'receive-response',
      value: receiveResponse.toString(),
      description: ''
    });
  }
}

SocketTextTriggerMan.displayName = 'text-trigger-man';
SocketTextTriggerMan.regressionFriendly = 'no';

SocketTextTriggerMan.template = SocketTextTriggerMan._template`
'use strict';

const ActorApi = require('actor-api');


class ${'name'}${'actorEnding'} extends ActorApi.Actor${'actorType'} {
  constructor() {
    super();
    this.socketConnection = null;
    this.text = '';
    this.receiveResponse = false;
  }
  
  *data() {
    this.text = this.getTestDataString('text');
    this.receiveResponse = this.getTestDataBoolean('receive-response', this.receiveResponse);
  }
  
  *initClient() {
    this.socketConnection = this.createConnection('socket');
  }
  
  *run() {
    this.socketConnection.send(this.text);
    if(this.receiveResponse) {
      const response = this.socketConnection.receive();
    }
  }
  
  *exit(interrupted) {
    this.closeConnection(this.socketConnection);
  }
}

module.exports = ${'name'}${'actorEnding'};
`;

SocketTextTriggerMan.validateHostFunctionString = ``;



SocketTextTriggerMan.validatePortFunctionString = `
const port = Number.parseInt(preview);
if(!Number.isInteger(port)) {
  return 3; // ERROR
}
else if(port <= 1023 || port >= 49152) {
  return 2; // ERROR
}
else {
  return 0; // NONE
}
`;


SocketTextTriggerMan.testDataViews = [
  {
    displayName: 'Socket',
    name: 'socket',
    view: 'single',
    data: [
      {
        name: 'host',
        type: 'input',
        placeHolder: 'host',
        value: '127.0.0.1',
        description: 'Destination Host',
        valid: 0
      },
      {
        name: 'port',
        type: 'input',
        placeHolder: 'port',
        value: '8080',
        description: 'Destination Port',
        valid: 0,
        validate: SocketValidator.validatePortFunctionString
      },
      {
        name: 'transport',
        type: 'radio',
        values: ['tcp', 'tls', 'udp', 'mc'],
        value: 'tcp',
        description: 'Transport Layer: tcp | tls | udp | mc'
      },
      {
        name: 'allowHalfOpen',
        type: 'radio',
        values: ['true', 'false'],
        value: 'false',
        description: '',
        depends: 'transport,tcp|tls'
      }
    ]
  },
  {
    displayName: 'Text',
    name: 'text',
    view: 'single',
    data: [
      {
        name: 'text',
        type: 'textarea',
        rows: 5,
        guid: '31063831-04de-4724-82cd-6a2e984b956b',
        value: '',
        valid: 0
      },
      {
        name: 'Receive Response',
        type: 'checkbox',
        value: false,
        valid: 0
      }
    ]
  }
];

SocketTextTriggerMan.markupNodes = 2;

SocketTextTriggerMan.markup = `\`\`\`seq
Config(nodeWidth: 126, nodeMessageHeight: 17, nodeEventHeight: 10, widthBias: 23, heightBias: 30, lineNumbers: false, border: false, backgroundColor: transparent)

Title: SOCKET
Nodes[orig, server]
orig -o server[http]: connect
orig => server[http]: text
server => orig[http]: [response text/json]
orig -x server[http]: disconnect
\`\`\`
`;


module.exports = SocketTextTriggerMan;
