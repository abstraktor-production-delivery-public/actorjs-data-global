
'use strict';

const StackApi = require('stack-api');


class SocketOrigProxyTermConnectTcp extends StackApi.StackComponentsTemplatesTestCases {

  static createTestCase(testCase, testData) {
    console.log(testCase, testData);
  }
}

SocketOrigProxyTermConnectTcp.displayName = 'orig-proxy-term-conncet-tcp';

SocketOrigProxyTermConnectTcp.template = SocketOrigProxyTermConnectTcp._template`

`;

SocketOrigProxyTermConnectTcp.testDataViews = [];

SocketOrigProxyTermConnectTcp.markupNodes = 3;

SocketOrigProxyTermConnectTcp.markup = `\`\`\`seq
Config(nodeWidth: 126, nodeMessageHeight: 17, nodeEventHeight: 10, widthBias: 23, heightBias: 30, lineNumbers: false, border: false, backgroundColor: transparent)

Title: Tcp
Nodes[client, proxy, server]
client -o proxy[socket]: connect
proxy -o server[socket]: connect
client -x proxy[socket]: disconnect
proxy -x server[socket]: disconnect
\`\`\`
`;

module.exports = SocketOrigProxyTermConnectTcp;
