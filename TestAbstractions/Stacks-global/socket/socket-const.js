
'use strict';


class SocketConst {
  constructor() {
    
  }
}

SocketConst.MAX_CAPTION_SIZE = 50;
SocketConst.BREAK_CAPTION_SIZE = SocketConst.MAX_CAPTION_SIZE - 3;

module.exports = SocketConst;
