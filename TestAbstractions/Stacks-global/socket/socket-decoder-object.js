
'use strict';

const StackApi = require('stack-api');
const SocketLoggerObject = require('./socket-logger-object');


class SocketDecoderObject extends StackApi.Decoder {
  constructor() {
    super();
  }
  
  *decode() {
    const line = yield* this.receiveLine();
    const object = JSON.parse(line);
    if(this.isLogIp) {
      this.setCaption(SocketLoggerObject.generateLog(object, this.ipLog));
      this.logMessage();
    }
    return object;
  }
}

module.exports = SocketDecoderObject;
