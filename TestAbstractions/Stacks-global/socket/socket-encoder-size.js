      
'use strict';

const StackApi = require('stack-api');
const SocketLoggerSize = require('./socket-logger-size');


class SocketEncoder extends StackApi.Encoder {
  constructor(msg) {
    super();
    this.msg = msg;
  }

  *encode() {
    if(this.msg instanceof StackApi.ContentBase) {
      let buffer = null;
      while(null !== (buffer = this.msg.getBuffer())) {
        yield* this.send(buffer);
        if(this.isLogIp) {
          this.setCaption(SocketLoggerSize.generateLog(buffer, this.ipLog));
          this.logMessage();
        }
      }
    }
    else {
      yield* this.send(this.msg);
      if(this.isLogIp) {
        this.setCaption(SocketLoggerSize.generateLog(this.msg, this.ipLog));
        this.logMessage();
      }
    }
  }
}


module.exports = SocketEncoder;
