
'use strict';

const StackApi = require('stack-api');
const SocketConst = require('./socket-const');
                            

class SocketLoggerSize {
  static generateLog(buffer, ipLog) {
    if(Buffer.isBuffer(buffer)) {
      return StackApi.BinaryLog.generateLog(buffer, ipLog);
    }
    else if(typeof buffer === 'string') {
      const asciiMsg = `${StackApi.AsciiDictionary.getSymbolString(buffer)}`;
      ipLog.addLog(new StackApi.LogInner(asciiMsg));
      return asciiMsg.length <= SocketConst.MAX_CAPTION_SIZE ? asciiMsg : asciiMsg.substring(0, SocketConst.BREAK_CAPTION_SIZE) + '...';
    }
  }
}


module.exports = SocketLoggerSize;
