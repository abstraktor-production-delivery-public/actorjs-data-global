
'use strict';

const StackApi = require('stack-api');
const SocketLoggerSize = require('./socket-logger-size');


class SocketDecoderAll extends StackApi.Decoder {
  constructor(msg) {
    super();
    this.msg = msg;
  }
  
  *decode() {
    const msg = yield* this.receive();
    if(this.isLogIp) {
      this.setCaption(SocketLoggerSize.generateLog(msg, this.ipLog));
      this.logMessage();
    }
    return msg;
  }
}


module.exports = SocketDecoderAll;
