
'use strict';

const SocketOrigConnectTcp = require('./templates-actors/socket-orig-connect-tcp');
const SocketOrigConnectUdp = require('./templates-actors/socket-orig-connect-udp');
const SocketOrigConnectMc = require('./templates-actors/socket-orig-connect-mc');
const SocketTermConnectTcp = require('./templates-actors/socket-term-connect-tcp');
const SocketTermConnectUdp = require('./templates-actors/socket-term-connect-udp');
const SocketTermConnectMc = require('./templates-actors/socket-term-connect-mc');
const SocketProxyConnectTcp = require('./templates-actors/socket-proxy-connect-tcp');
const SocketOrigHelloWorldTcp = require('./templates-actors/socket-orig-hello-world-tcp');
const SocketTermHelloWorldTcp = require('./templates-actors/socket-term-hello-world-tcp');
const StackApi = require('stack-api');


class SocketTemplatesActors extends StackApi.StackComponentsTemplateBaseActors {
  constructor() {
    super([
      SocketOrigConnectTcp,
      SocketOrigConnectUdp,
      SocketOrigConnectMc,
      SocketTermConnectTcp,
      SocketTermConnectUdp,
      SocketTermConnectMc,
      SocketProxyConnectTcp,
      SocketOrigHelloWorldTcp,
      SocketTermHelloWorldTcp
    ]);
  }
}


module.exports = SocketTemplatesActors;
