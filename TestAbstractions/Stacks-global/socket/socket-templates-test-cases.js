
'use strict';

//const SocketOrigTermConnectTcp = require('./templates-test-cases/socket-orig-term-connect-tcp');
//const SocketOrigProxyTermConnectTcp = require('./templates-test-cases/socket-orig-proxy-term-connect-tcp');
const SocketTextTriggerMan = require('./templates-test-cases/socket-text-trigger-man');
const SocketTextMockMan = require('./templates-test-cases/socket-text-mock-man');
//const SocketJsonTriggerMan = require('./templates-test-cases/socket-json-trigger-man');
const StackApi = require('stack-api');


class SocketTemplatesTestCases extends StackApi.StackComponentsTemplateBaseTestCases {
  constructor() {
    super([
      SocketTextTriggerMan,
      SocketTextMockMan,
      /*SocketJsonTriggerMan
      /*SocketOrigTermConnectTcp,
      SocketOrigProxyTermConnectTcp*/
    ]);
  }
}

module.exports = SocketTemplatesTestCases;
