
'use strict';

const StackApi = require('stack-api');


class SocketOrigConnectMc extends StackApi.StackComponentsTemplatesActors {}

SocketOrigConnectMc.displayName = 'connect-mc';
SocketOrigConnectMc.type = 'orig';
SocketOrigConnectMc.template = SocketOrigConnectMc._template`
'use strict';

const ActorApi = require('actor-api');
const StackApi = require('stack-api');


class ${'name'}${'actorEnding'} extends ActorApi.Actor${'actorType'} {
  constructor() {
    super();
    this.socketConnection = null;
  }
  
  *data() {
  }
  
  *initClient() {
    this.socketConnection = this.createConnection('socket', {
      networkType: StackApi.NetworkType.MC
    });
  }
  
  *run() {
  }
  
  *exit(interrupted) {
    this.closeConnection(this.socketConnection);
  }
}

module.exports = ${'name'}${'actorEnding'};
`;

SocketOrigConnectMc.markupNodes = 2;
SocketOrigConnectMc.markup = `\`\`\`seq
Config(nodeWidth: 126, nodeMessageHeight: 17, nodeEventHeight: 10, widthBias: 23, heightBias: 30, lineNumbers: false, border: false, backgroundColor: transparent)

Title: Multicast
Nodes[orig, server]
orig -o server[socket]: connect
orig -x server[socket]: disconnect
\`\`\`
`;

module.exports = SocketOrigConnectMc;
