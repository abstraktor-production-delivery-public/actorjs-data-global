
'use strict';

const StackApi = require('stack-api');


class SocketOrigConnectUdp extends StackApi.StackComponentsTemplatesActors {}

SocketOrigConnectUdp.displayName = 'connect-udp';
SocketOrigConnectUdp.type = 'orig';
SocketOrigConnectUdp.template = SocketOrigConnectUdp._template`
'use strict';

const ActorApi = require('actor-api');
const StackApi = require('stack-api');


class ${'name'}${'actorEnding'} extends ActorApi.Actor${'actorType'} {
  constructor() {
    super();
    this.socketConnection = null;
  }
  
  *data() {
  }
  
  *initClient() {
    this.socketConnection = this.createConnection('socket', {
      networkType: StackApi.NetworkType.UDP
    });
  }
  
  *run() {
  }
  
  *exit(interrupted) {
    this.closeConnection(this.socketConnection);
  }
}

module.exports = ${'name'}${'actorEnding'};
`;

SocketOrigConnectUdp.markupNodes = 2;
SocketOrigConnectUdp.markup = `\`\`\`seq
Config(nodeWidth: 126, nodeMessageHeight: 17, nodeEventHeight: 10, widthBias: 23, heightBias: 30, lineNumbers: false, border: false, backgroundColor: transparent)

Title: Udp
Nodes[orig, server]
orig -o server[socket]: connect
orig -x server[socket]: disconnect
\`\`\`
`;

module.exports = SocketOrigConnectUdp;
