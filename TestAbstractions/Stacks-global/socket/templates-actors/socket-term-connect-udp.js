
'use strict';

const StackApi = require('stack-api');


class SocketTermConnectUdp extends StackApi.StackComponentsTemplatesActors {}

SocketTermConnectUdp.displayName = 'connect-udp';
SocketTermConnectUdp.type = 'term';
SocketTermConnectUdp.template = SocketTermConnectUdp._template`
'use strict';

const ActorApi = require('actor-api');
const StackApi = require('stack-api');


class ${'name'}${'actorEnding'} extends ActorApi.Actor${'actorType'} {
  constructor() {
    super();
    this.socketConnection = null;
  }
  
  *data() {
  }
    
  *initServer() {
    this.socketConnection = this.createServer('socket', {
      networkType: StackApi.NetworkType.UDP
    });
  }
  
  *run() {
    this.socketConnection.accept();
  }
  
  *exit(interrupted) {
    this.closeConnection(this.socketConnection);
  }
}

module.exports = ${'name'}${'actorEnding'};
`;

SocketTermConnectUdp.markupNodes = 2;
SocketTermConnectUdp.markup = `\`\`\`seq
Config(nodeWidth: 126, nodeMessageHeight: 17, nodeEventHeight: 10, widthBias: 23, heightBias: 30, lineNumbers: false, border: false, backgroundColor: transparent)

Title: Udp
Nodes[client, term]
client -o term[socket]: connect
client -x term[socket]: disconnect
\`\`\`
`;

module.exports = SocketTermConnectUdp;
