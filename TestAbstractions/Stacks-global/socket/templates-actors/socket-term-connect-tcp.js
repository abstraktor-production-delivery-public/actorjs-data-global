
'use strict';

const StackApi = require('stack-api');


class SocketTermConnectTcp extends StackApi.StackComponentsTemplatesActors {}

SocketTermConnectTcp.displayName = 'connect-tcp';
SocketTermConnectTcp.type = 'term';
SocketTermConnectTcp.template = SocketTermConnectTcp._template`
'use strict';

const ActorApi = require('actor-api');


class ${'name'}${'actorEnding'} extends ActorApi.Actor${'actorType'} {
  constructor() {
    super();
    this.socketConnection = null;
  }
  
  *data() {
  }
    
  *initServer() {
    this.socketConnection = this.createServer('socket');
  }
  
  *run() {
    this.socketConnection.accept();
  }
  
  *exit(interrupted) {
    this.closeConnection(this.socketConnection);
  }
}

module.exports = ${'name'}${'actorEnding'};
`;

SocketTermConnectTcp.markupNodes = 2;
SocketTermConnectTcp.markup = `\`\`\`seq
Config(nodeWidth: 126, nodeMessageHeight: 17, nodeEventHeight: 10, widthBias: 23, heightBias: 30, lineNumbers: false, border: false, backgroundColor: transparent)

Title: Tcp
Nodes[client, term]
client -o term[socket]: connect
client -x term[socket]: disconnect
\`\`\`
`;

module.exports = SocketTermConnectTcp;
