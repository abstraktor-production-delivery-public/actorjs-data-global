
'use strict';

const StackApi = require('stack-api');


class SocketOrigConnectTcp extends StackApi.StackComponentsTemplatesActors {}

SocketOrigConnectTcp.displayName = 'connect-tcp';
SocketOrigConnectTcp.type = 'orig';
SocketOrigConnectTcp.template = SocketOrigConnectTcp._template`
'use strict';

const ActorApi = require('actor-api');


class ${'name'}${'actorEnding'} extends ActorApi.Actor${'actorType'} {
  constructor() {
    super();
    this.socketConnection = null;
  }
  
  *data() {
  }
  
  *initClient() {
    this.socketConnection = this.createConnection('socket');
  }
  
  *run() {
  }
  
  *exit(interrupted) {
    this.closeConnection(this.socketConnection);
  }
}

module.exports = ${'name'}${'actorEnding'};
`;

SocketOrigConnectTcp.markupNodes = 2;
SocketOrigConnectTcp.markup = `\`\`\`seq
Config(nodeWidth: 126, nodeMessageHeight: 17, nodeEventHeight: 10, widthBias: 23, heightBias: 30, lineNumbers: false, border: false, backgroundColor: transparent)

Title: Tcp
Nodes[orig, server]
orig -o server[socket]: connect
orig -x server[socket]: disconnect
\`\`\`
`;


module.exports = SocketOrigConnectTcp;
