
'use strict';

const StackApi = require('stack-api');


class SocketTermConnectMc extends StackApi.StackComponentsTemplatesActors {}

SocketTermConnectMc.displayName = 'connect-mc';
SocketTermConnectMc.type = 'term';
SocketTermConnectMc.template = SocketTermConnectMc._template`
'use strict';

const ActorApi = require('actor-api');
const StackApi = require('stack-api');


class ${'name'}${'actorEnding'} extends ActorApi.Actor${'actorType'} {
  constructor() {
    super();
    this.socketConnection = null;
  }
  
  *data() {
  }
    
  *initServer() {
    this.socketConnection = this.createServer('socket', {
      networkType: StackApi.NetworkType.MC
    });
  }
  
  *run() {
    this.socketConnection.accept();
  }
  
  *exit(interrupted) {
    this.closeConnection(this.socketConnection);
  }
}

module.exports = ${'name'}${'actorEnding'};
`;

SocketTermConnectMc.markupNodes = 2;
SocketTermConnectMc.markup = `\`\`\`seq
Config(nodeWidth: 126, nodeMessageHeight: 17, nodeEventHeight: 10, widthBias: 23, heightBias: 30, lineNumbers: false, border: false, backgroundColor: transparent)

Title: Multicast
Nodes[client, term]
client -o term[socket]: connect
client -x term[socket]: disconnect
\`\`\`
`;

module.exports = SocketTermConnectMc;
