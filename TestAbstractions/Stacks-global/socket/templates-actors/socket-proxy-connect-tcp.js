
'use strict';

const StackApi = require('stack-api');


class SocketProxyConnectTcp extends StackApi.StackComponentsTemplatesActors {}

SocketProxyConnectTcp.displayName = 'connect-tcp';
SocketProxyConnectTcp.type = 'proxy';
SocketProxyConnectTcp.template = SocketProxyConnectTcp._template`
'use strict';

const ActorApi = require('actor-api');


class ${'name'}${'actorEnding'} extends ActorApi.Actor${'actorType'} {
  constructor() {
    super();
    this.socketServerConnection = null;
    this.socketClientConnection = null;
  }
  
  *data() {
  }
    
  *initServer() {
    this.socketServerConnection = this.createServer('socket');
  }
  
  *initClient() {
    this.socketClientConnection = this.createConnection('socket');
  }
  
  *run() {
    this.socketServerConnection.accept();
  }
  
  *exit(interrupted) {
    this.closeConnection(this.socketServerConnection);
    this.closeConnection(this.socketClientConnection);
  }
}

module.exports = ${'name'}${'actorEnding'};
`;

SocketProxyConnectTcp.markupNodes = 3;
SocketProxyConnectTcp.markup = `\`\`\`seq
Config(nodeWidth: 126, nodeMessageHeight: 17, nodeEventHeight: 10, widthBias: 23, heightBias: 30, lineNumbers: false, border: false, backgroundColor: transparent)

Title: Tcp
Nodes[client, proxy, server]
client -o proxy[socket]: connect
proxy -o server[socket]: connect
client -x proxy[socket]: disconnect
proxy -x server[socket]: disconnect
\`\`\`
`;

module.exports = SocketProxyConnectTcp;
