
'use strict';

const StackApi = require('stack-api');


class SocketTermHelloWorldTcp extends StackApi.StackComponentsTemplatesActors {}

SocketTermHelloWorldTcp.displayName = 'hello-world-tcp';
SocketTermHelloWorldTcp.type = 'term';
SocketTermHelloWorldTcp.template = SocketTermHelloWorldTcp._template`
'use strict';

const ActorApi = require('actor-api');


class ${'name'}${'actorEnding'} extends ActorApi.Actor${'actorType'} {
  constructor() {
    super();
    this.socketConnection = null;
  }
  
  *data() {
  }
    
  *initServer() {
    this.socketConnection = this.createServer('socket');
  }
  
  *run() {
    this.socketConnection.accept();
    
    const message = this.socketConnection.receiveLine();
    VERIFY_VALUE('HELLO WORLD', message);
    
    this.socketConnection.sendLine('HELLO WORLD');
  }
  
  *exit(interrupted) {
    this.closeConnection(this.socketConnection);
  }
}

module.exports = ${'name'}${'actorEnding'};
`;

SocketTermHelloWorldTcp.markupNodes = 2;
SocketTermHelloWorldTcp.markup = `\`\`\`seq
Config(nodeWidth: 126, nodeMessageHeight: 17, nodeEventHeight: 10, widthBias: 23, heightBias: 30, lineNumbers: false, border: false, backgroundColor: transparent)

Title: Tcp
Nodes[client, term]
client -o term[socket]: connect
client => term[socket]: HELLO WORLD
term => client[socket]: HELLO WORLD
client -x term[socket]: disconnect
\`\`\`
`;

module.exports = SocketTermHelloWorldTcp;
