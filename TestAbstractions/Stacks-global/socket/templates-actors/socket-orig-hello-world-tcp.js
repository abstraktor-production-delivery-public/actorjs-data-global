
'use strict';

const StackApi = require('stack-api');


class SocketOrigHelloWorldTcp extends StackApi.StackComponentsTemplatesActors {}

SocketOrigHelloWorldTcp.displayName = 'hello-world-tcp';
SocketOrigHelloWorldTcp.type = 'orig';
SocketOrigHelloWorldTcp.template = SocketOrigHelloWorldTcp._template`
'use strict';

const ActorApi = require('actor-api');


class ${'name'}${'actorEnding'} extends ActorApi.Actor${'actorType'} {
  constructor() {
    super();
    this.socketConnection = null;
  }
  
  *data() {
  }
  
  *initClient() {
    this.socketConnection = this.createConnection('socket');
  }
  
  *run() {
    this.socketConnection.sendLine('HELLO WORLD');
    
    const answer = this.socketConnection.receiveLine();
    VERIFY_VALUE('HELLO WORLD', answer);
  }
  
  *exit(interrupted) {
    this.closeConnection(this.socketConnection);
  }
}

module.exports = ${'name'}${'actorEnding'};
`;

SocketOrigHelloWorldTcp.markupNodes = 2;
SocketOrigHelloWorldTcp.markup = `\`\`\`seq
Config(nodeWidth: 126, nodeMessageHeight: 17, nodeEventHeight: 10, widthBias: 23, heightBias: 30, lineNumbers: false, border: false, backgroundColor: transparent)

Title: Tcp
Nodes[orig, server]
orig -o server[socket]: connect
orig => server[socket]: HELLO WORLD
server => orig[socket]: HELLO WORLD
orig -x server[socket]: disconnect
\`\`\`
`;


module.exports = SocketOrigHelloWorldTcp;
