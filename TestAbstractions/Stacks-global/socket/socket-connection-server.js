
'use strict';

const StackApi = require('stack-api');
const SocketConnectionServerOptions = require('./socket-connection-server-options');
const SocketEncoderLine = require('./socket-encoder-line');
const SocketEncoderSize = require('./socket-encoder-size');
const SocketEncoderObject = require('./socket-encoder-object');
const SocketDecoderAll = require('./socket-decoder-all');
const SocketDecoderLine = require('./socket-decoder-line');
const SocketDecoderSize = require('./socket-decoder-size');
const SocketDecoderObject = require('./socket-decoder-object');


class SocketConnectionServer extends StackApi.ConnectionServer {
  constructor(id, type, actor, options) {
    super(id, type, 'socket', actor, StackApi.NetworkType.TCP, SocketConnectionServerOptions, options);
  }
  
  send(msg) {
    this.sendMessage(new SocketEncoderSize(msg));
  }
  
  sendLine(msg) {
    this.sendMessage(new SocketEncoderLine(msg));
  }
  
  sendObject(object) {
    this.sendMessage(new SocketEncoderObject(object));
  }
  
  receive() {
    this.receiveMessage(new SocketDecoderAll());
  }
  
  receiveLine() {
    this.receiveMessage(new SocketDecoderLine());
  }
  
  receiveSize(size) {
    this.receiveMessage(new SocketDecoderSize(size));
  }
  
  receiveObject() {
    this.receiveMessage(new SocketDecoderObject());
  }
}

module.exports = SocketConnectionServer;
