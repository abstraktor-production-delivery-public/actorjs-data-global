
'use strict';

const StackApi = require('stack-api');
const SocketLoggerSize = require('./socket-logger-size');


class SocketDecoderSize extends StackApi.Decoder {
  constructor(size) {
    super();
    this.size = size;
  }
  
  *decode() {
    const msg = yield* this.receiveSize(this.size);
    if(this.isLogIp) {
      this.setCaption(SocketLoggerSize.generateLog(msg, this.ipLog));
      this.logMessage();
    }
    return msg;
  }
}

module.exports = SocketDecoderSize;
