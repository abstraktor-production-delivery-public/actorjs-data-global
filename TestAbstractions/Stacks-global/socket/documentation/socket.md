# **Socket**
[[API-STATUS=STABLE, ]]
[[DOC-STATUS=PARTLY, ]]

[[NOTE={"guid":"27f67b7a-c2a9-4534-80e1-75bf65214072"}]]
[[ANCHOR={"id":"6d8cc427-1c92-4dcb-bd25-4f73ec780429","visible":true}]]
## **Description**
The Socket [[REF=, ABS_Stack]] acts like common sockets. 

[[NOTE={"guid":"481c5227-cd73-4bc8-bfc7-59ca3b294941"}]]
[[ANCHOR={"id":"ebdbc67e-f122-44bf-b26a-c5e42e7dd1c7","visible":true}]]
## **Objects**
* [SocketConnectionClient](#client-connection) & [SocketConnectionServer](#server-connection)
  * [send](#connection-send)
  * [sendLine](#connection-send-line)
  * [sendObject](#connection-send-object)
  * [receive](#connection-receive)
  * [receiveLine](#connection-receive-line)
  * [receiveSize](#connection-receive-size)
  * [receiveObject](#connection-receive-object)

***

[[NOTE={"guid":"79698c69-8117-438a-a0b7-f44a93130786"}]]
[[ANCHOR={"id":"8f80acd4-3e07-42f9-99f8-e7577e65d04b","visible":true}]]
## **Example**

[[NOTE={"guid":"15fbb45a-dcc0-4b3f-a975-6728f250d4f1"}]]
[[ANCHOR={"id":"de10e92e-a95c-4bef-a330-9f98b934bd7d","visible":true}]]
```seq
Config(nodeWidth: 150, nodeMessageHeight: 17, nodeEventHeight: 10, nodeCommentHeight: 26, widthBias: 30, heightBias: 30, lineNumbers: false, border: true, backgroundColor: default)

Nodes[orig, term]
orig => term[socket]: data
term => orig[socket]: data
```

[[NOTE={"guid":"c8b3735c-7cd7-417e-9427-0017023f1511"}]]
[[ANCHOR={"id":"64b767fc-5fad-4eaf-9931-c22baf9f962e","visible":true}]]
```javascript

const ActorApi = require('actor-api');
const StackApi = require('stack-api');


class SocketLogOrig extends ActorApi.ActorOriginating {
  constructor() {
    super();
    this.socketConnection = null;
  }
  
  *initClient() {
    this.socketConnection = this.createConnection('socket');
  }
  
  *run() {
    this.socketConnection.sendLine('HELLO WORLD');
  }
  
  *exit(interrupted) {
    this.closeConnection(this.socketConnection);
  }
}

module.exports = SocketLogOrig;

```

[[NOTE={"guid":"2e8afb89-ed81-4f55-8549-875575e43c49"}]]
[[ANCHOR={"id":"a86cef20-8a8e-47a6-a4b1-b8b2c193ed6d","visible":true}]]
```javascript

const ActorApi = require('actor-api');
const StackApi = require('stack-api');


class SocketLogTerm extends ActorApi.ActorTerminating {
  constructor() {
    super();
    this.socketConnection = null;
  }
  
  *initServer() {
    this.socketConnection = this.createServer('socket');
  }
  
  *run() {
    this.socketConnection.accept();
    
    const request = this.socketConnection.receiveLine();
  }
  
  *exit(interrupted) {
    this.closeConnection(this.socketConnection);
  }
}

module.exports = SocketLogTerm;

```

***
[[NOTE={"guid":"2c39e7d8-4886-4410-8ad4-d23535027610"}]]
[[ANCHOR={"id":"client-connection","visible":true}]]
## **SocketConnectionClient**
In the ***initClient()** and ***run()** methods, it is possible to make an [[REF=, ABS_Actor]] with client capacity connect to a socket. The implementation of the SocketConnectionClient API starts in [socket-connection-client.js](/stack-editor/Stacks-global/socket/socket-connection-client.js).

***

[[NOTE={"guid":"988233ea-ee9b-43aa-a648-d55823deb1eb"}]]
[[ANCHOR={"id":"server-connection","visible":true}]]
## **SocketConnectionServer**
In the ***initServer()** and ***run()** methods, it is possible to make an [[REF=, ABS_Actor]] with server capacity listen to a socket. The implementation of the SocketConnectionServer API starts in [socket-connection-server.js](/stack-editor/Stacks-global/socket/socket-connection-server.js).

***

[[NOTE={"guid":"1a5173d7-bf15-4db3-a477-3b4fe38f777a"}]]
[[ANCHOR={"id":"connection-send","visible":true}]]
### **SocketConnection[Client/Server].send**
```
connection.send(msg);
```

[[NOTE={"guid":"6f655109-f9d1-4176-a19c-c844a56633a3"}]]
[[ANCHOR={"id":"d5210055-6c04-4da4-9dc3-0fd700df3df7","visible":true}]]
Sends a message over a Socket.

#### **Method Description**
```table
Config(classHeading: )

|Parameters                                                                    |
|Name|Type                                                       |Description  |
|msg |[[REF=,MDN_string]]&#160;&#124;&#160;[[REF=,NODEJS_Buffer]]|Data to send.|
```

```table
Config(classHeading: )

|Async                                                                    |
|type |Description                                                        |
|yield|Generated if the name fulfills RegExp ***/this.(.\*)Connection/***.|
```

#### **Example**
```javascript

this.socketpConnection.send('HELLO WORLD');

```

[[NOTE={"guid":"d0b1a75f-ce3c-4f39-977b-4ef527adbaf7"}]]
[[ANCHOR={"id":"028c944a-532e-43c4-b5f9-b0d3a75f21c9","visible":true}]]
#### **Test Cases using SocketConnection[Client/Server].send**
+ [StackSocketLineTcpNormal](/../test-cases/Actor/StackSocket/StackSocketLineTcpNormal)
+ [StackSocketLineUdpNormal](/../test-cases/Actor/StackSocket/StackSocketLineUdpNormal)

***

[[NOTE={"guid":"8ae309e5-f43f-4490-85b0-9866482dda9d"}]]
[[ANCHOR={"id":"connection-send-line","visible":true}]]
### **SocketConnection[Client/Server].sendLine**
```
connection.sendLine(msg);
```

[[NOTE={"guid":"e22e392c-5eb1-4d79-ad43-00327b30c339"}]]
[[ANCHOR={"id":"3b53c208-bfb8-49e8-a57c-8a1945949e15","visible":true}]]
Sends a message, along with "\\r\\n", over a Socket.

#### **Method Description**
```table
Config(classHeading: )

|Parameters                                                                    |
|Name|Type                                                       |Description  |
|msg |[[REF=,MDN_string]]&#160;&#124;&#160;[[REF=,NODEJS_Buffer]]|Data to send.|
```

```table
Config(classHeading: )

|Async                                                                    |
|type |Description                                                        |
|yield|Generated if the name fulfills RegExp ***/this.(.\*)Connection/***.|
```

#### **Example**
```javascript

this.socketpConnection.sendLine('HELLO WORLD');

```

[[NOTE={"guid":"c5bee667-27ff-4d7b-adb0-dd57f96d13b3"}]]
[[ANCHOR={"id":"7df4e6ee-1172-4f46-871c-94748d753367","visible":true}]]
#### **Test Cases using SocketConnection[Client/Server].sendLine**
+ [LogSendLine](/../test-cases/Actor/StackSocket/LogSendLine)

***

[[NOTE={"guid":"f081e404-9916-40dd-bf55-8463417d7ce1"}]]
[[ANCHOR={"id":"connection-send-object","visible":true}]]
### **SocketConnection[Client/Server].sendObject**
```
connection.sendObject(msg);
```

[[NOTE={"guid":"a549b79b-ac42-4551-878d-96dfacdfad3e"}]]
[[ANCHOR={"id":"77cd3a5b-0377-4622-9158-99e58b0b2edc","visible":true}]]
JSON.stringify an [[REF=, MDN_Object]] and then send the [[REF=, MDN_Object]], along with "\\r\\n", over a Socket.

#### **Method Description**
```table
Config(classHeading: )

|Parameters                                           |
|Name|Type               |Description                 |
|msg |[[REF=,MDN_Object]]|[[REF=,MDN_Object]] to send.|
```

```table
Config(classHeading: )

|Async                                                                    |
|type |Description                                                        |
|yield|Generated if the name fulfills RegExp ***/this.(.\*)Connection/***.|
```

#### **Example**
```javascript

this.socketpConnection.sendObject({
  msg: 'HELLO WORLD',
  price: 35
});

```

[[NOTE={"guid":"a4a24dba-1302-4c22-9539-ff783f6bac0e"}]]
[[ANCHOR={"id":"78ca2cd9-d00f-4c12-8574-3c5f566671f6","visible":true}]]
#### **Test Cases using SocketConnection[Client/Server].sendObject**
+ [LogSendObject](/../test-cases/Actor/StackSocket/LogSendObject)

***

[[NOTE={"guid":"00b00f44-ff05-48eb-80eb-46e60a123952"}]]
[[ANCHOR={"id":"connection-receive","visible":true}]]
### **SocketConnection[Client/Server].receive**
```
connection.receive();
```

[[NOTE={"guid":"35db2889-3ee2-4732-aa2c-54d5ec1cfc43"}]]
[[ANCHOR={"id":"6c174741-854a-4da2-8937-96bb31d939a9","visible":true}]]
Receives all existing chunks or waits for the first chunk and receives it.

#### **Method Description**
```table
Config(classHeading: )

|Returns                               |
|Type               |Description       |
|[[REF=,NODEJS_Buffer]]|The received data.|
```
```table
Config(classHeading: )

|Async                                                                    |
|type |Description                                                        |
|yield|Generated if the name fulfills RegExp ***/this.(.\*)Connection/***.|
```
#### **Example**
```javascript

const response = this.socketpConnection.receive();

```

[[NOTE={"guid":"60fc0259-3550-4613-b945-f98726598fc4"}]]
[[ANCHOR={"id":"d03de529-f3a5-4455-8745-06aab0ecbb52","visible":true}]]
#### **Test Cases using SocketConnection[Client/Server].receive**

***

[[NOTE={"guid":"657f2d1a-a070-4a37-a8d9-84ae41536ee2"}]]
[[ANCHOR={"id":"connection-receive-line","visible":true}]]
### **SocketConnection[Client/Server].receiveLine**
```
connection.receiveLine();
```

[[NOTE={"guid":"16bba1f5-7bc7-4805-9a91-a043a1803e8b"}]]
[[ANCHOR={"id":"e18821f3-8b25-4ac7-afd3-66de4ec81336","visible":true}]]
Receives a text line and removes the "\\r\\n".

#### **Method Description**
```table
Config(classHeading: )

|Returns                                                   |
|Type               |Description                           |
|[[REF=,MDN_string]]|The received message without "\\r\\n".|
```
```table
Config(classHeading: )

|Async                                                                    |
|type |Description                                                        |
|yield|Generated if the name fulfills RegExp ***/this.(.\*)Connection/***.|
```
#### **Example**
```javascript

const response = this.socketpConnection.receiveLine();

```

[[NOTE={"guid":"97bc2ea1-9887-482e-ba97-d2a7bab52073"}]]
[[ANCHOR={"id":"83414734-e6cc-421b-a84e-811cb07d8e4d","visible":true}]]
#### **Test Cases using SocketConnection[Client/Server].receiveLine**
+ [StackSocketLineTcpNormal](/../test-cases/Actor/StackSocket/StackSocketLineTcpNormal)
+ [StackSocketLineUdpNormal](/../test-cases/Actor/StackSocket/StackSocketLineUdpNormal)

***

[[NOTE={"guid":"159949b5-eac8-47e1-8c86-da900a94e121"}]]
[[ANCHOR={"id":"connection-receive-size","visible":true}]]
### **SocketConnection[Client/Server].receiveSize**
```
connection.receiveSize(size);
```

[[NOTE={"guid":"e912a9a3-be07-4fae-9513-ea8fd9d7f739"}]]
[[ANCHOR={"id":"b95a1b48-6043-4b7c-8604-914afb2e8357","visible":true}]]
Receives a number of bytes determined by the ***size*** parameter.

#### **Method Description**
```table
Config(classHeading: )

|Parameters                                              |
|Name|Type               |Description                    |
|size|[[REF=,MDN_number]]|The number of bytes to receive.|
```

```table
Config(classHeading: )

|Returns                                                     |
|Type                  |Description                          |
|[[REF=,NODEJS_Buffer]]|The received data of size ***size***.|
```
```table
Config(classHeading: )

|Async                                                                    |
|type |Description                                                        |
|yield|Generated if the name fulfills RegExp ***/this.(.\*)Connection/***.|
```
#### **Example**
```javascript

const response = this.socketpConnection.receiveSize(11);

```

***

[[NOTE={"guid":"865434cd-d4ec-4eea-b848-6482c49ed7a9"}]]
[[ANCHOR={"id":"connection-receive-object","visible":true}]]
### **SocketConnection[Client/Server].receiveObject**
```
connection.receiveObject();
```

[[NOTE={"guid":"148a20d5-4abf-4e95-a563-adc28330a877"}]]
[[ANCHOR={"id":"4e637828-2902-4f23-a2ce-fca443ba1936","visible":true}]]
Receives a text line and removes "\\r\\n". Then it performs a JSON.parse on the data and returns an [[REF=, MDN_Object]].

#### **Method Description**
```table
Config(classHeading: )

|Returns                                                                 |
|Type                  |Description                                      |
|[[REF=,NODEJS_Buffer]]|JSON.parse of the received data without "\\r\\n".|
```
```table
Config(classHeading: )

|Async                                                                    |
|type |Description                                                        |
|yield|Generated if the name fulfills RegExp ***/this.(.\*)Connection/***.|
```
#### **Example**
```javascript

const response = this.socketpConnection.receiveObject();

```

[[NOTE={"guid":"df0157f8-b690-4243-a036-6771ebac9bea"}]]
[[ANCHOR={"id":"6052972e-5497-49c5-a5e0-afe5c02483e3","visible":true}]]
#### **Test Cases using SocketConnection[Client/Server].receiveObject**
+ [LogSendObject](/../test-cases/Actor/StackSocket/LogSendObject)
***
