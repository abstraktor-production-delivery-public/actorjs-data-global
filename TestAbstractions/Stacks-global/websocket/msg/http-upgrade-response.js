
'use strict';

const HttpApi = require('http-stack-api');
const WebsocketConstHeader = require('../websocket-const-header');


class HttpUpgradeResponse extends HttpApi.Response {
  constructor(wsProtocol) {
    super(HttpApi.Version.HTTP_1_1, HttpApi.StatusCode.SwitchingProtocols, HttpApi.ReasonPhrase.SwitchingProtocols);
    this.addHeader(HttpApi.Header.UPGRADE, 'websocket');
    this.addHeader(HttpApi.Header.CONNECTION, 'Upgrade');
    this.addHeader(WebsocketConstHeader.SEC_WEBSOCKET_ACCEPT, 's3pPLMBiTxaQ9kYGzzhZRbK+xOo=');
    this.addHeader(WebsocketConstHeader.SEC_WEBSOCKET_PROTOCOL, wsProtocol);
  }
}

module.exports = HttpUpgradeResponse;
