
'use strict';

const HttpApi = require('http-stack-api');
const WebsocketConstHeader = require('../websocket-const-header');


class HttpUpgradeRequest extends HttpApi.Request {
  constructor(requestUri, wsProtocol) {
    super(HttpApi.Method.GET, requestUri, HttpApi.Version.HTTP_1_1);
    this.addHeader(HttpApi.Header.UPGRADE, 'websocket');
    this.addHeader(HttpApi.Header.CONNECTION, 'Upgrade');
    this.addHeader(HttpApi.Header.ORIGIN, 'http://example.com');
    this.addHeader(WebsocketConstHeader.SEC_WEBSOCKET_KEY, 'dGhlIHNhbXBsZSBub25jZQ==');
    this.addHeader(WebsocketConstHeader.SEC_WEBSOCKET_PROTOCOL, wsProtocol);
    this.addHeader(WebsocketConstHeader.SEC_WEBSOCKET_VERSION, 13);
  }
}

module.exports = HttpUpgradeRequest;
