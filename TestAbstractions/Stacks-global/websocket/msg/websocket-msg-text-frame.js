
'use strict';

const WebsocketConst = require('../websocket-const');
const WebsocketMsg = require('../websocket-msg');


class WebsocketMsgTextFrame extends WebsocketMsg {
  constructor(text, mask) {
    super(WebsocketConst.FIN_ON, 0, 0, 0, WebsocketConst.OP_CODE_TEXT, mask ? WebsocketConst.MASK_ON : WebsocketConst.MASK_OFF, text);
  }
}


module.exports = WebsocketMsgTextFrame;
