
'use strict';

const WebsocketConst = require('../websocket-const');
const WebsocketMsg = require('../websocket-msg');


class WebsocketMsgBinaryFrame extends WebsocketMsg {
  constructor(data, mask) {
    super(WebsocketConst.FIN_ON, 0, 0, 0, WebsocketConst.OP_CODE_BINARY, mask ? WebsocketConst.MASK_ON : WebsocketConst.MASK_OFF, data);
  }
}

module.exports = WebsocketMsgBinaryFrame;
