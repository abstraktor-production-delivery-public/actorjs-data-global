
'use strict';

const WebsocketConst = require('../websocket-const');
const WebsocketMsg = require('../websocket-msg');


class WebsocketMsgCloseFrame extends WebsocketMsg {
  constructor(mask) {
    super(WebsocketConst.FIN_ON, 0, 0, 0, WebsocketConst.OP_CODE_CONNECTION_CLOSE, mask ? WebsocketConst.MASK_ON : WebsocketConst.MASK_OFF);
  }
}


module.exports = WebsocketMsgCloseFrame;
