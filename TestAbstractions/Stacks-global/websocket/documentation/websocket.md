# **Websocket**
[[API-STATUS=STABLE, Most functionallity verified]]
[[DOC-STATUS=MOSTLY, ]]

[[NOTE={"guid":"716e217e-16b4-4849-b354-700ddc0e3de1"}]]
[[ANCHOR={"id":"f81a54f9-cd4f-4b68-b8f4-f32588244a58","visible":true}]]
## **Description**
The websocket [[REF=, ABS_Stack]] implements the requirements in: [6455 - The WebSocket Protocol](https://tools.ietf.org/html/rfc6455).

[[NOTE={"guid":"aeedb955-456f-4bff-abf5-8241dca72959"}]]
[[ANCHOR={"id":"c501103c-8553-4c92-82ba-28eabe0bc7f6","visible":true}]]
## **Objects**
* [WebsocketConnectionClient](#client-connection)
* [WebsocketConnectionServer](#server-connection)
* [WebsocketMsg](#ws-message)
  * [WebsocketMsgTextFrame](#ws-message-text)
  * [WebsocketMsgBinaryFrame](#ws-message-binary)
  * [WebsocketMsgCloseFrame](#ws-message-close)

See the [[REF=, API_Actor_Client_Stack]] and [[REF=, API_Actor_Server_Stack]] for more information about how to create and close connections.

Further reading is available in [[REF=, ABS_Stack]] abstractions.

***

[[NOTE={"guid":"e292a4c2-611a-4420-8899-c9cbcbb46e39"}]]
[[ANCHOR={"id":"04f0050a-d58b-46b1-9189-5280dfcdb656","visible":true}]]
## **Example**
```seq
Config(nodeWidth: 150, nodeMessageHeight: 17, nodeEventHeight: 10, widthBias: 30, heightBias: 30, lineNumbers: false, border: true, backgroundColor: default)

Nodes[orig, term]
orig => term[http]: Upgrade
term => orig[http]: 101 Switching Protocols
orig => term[websocket]: 
term => orig[websocket]: 
```

[[NOTE={"guid":"45059261-c58c-4a23-be38-e3f30ff9d513"}]]
[[ANCHOR={"id":"561a5515-1505-4768-8eec-ee39d73e674a","visible":true}]]
```javascript





const ActorApi = require('actor-api');
const WebsocketStackApi = require('websocket-stack-api');
const HttpGetUpgradeRequest = require('./msg/HttpGetUpgradeRequest');


class WebsocketFrameTextOrig extends ActorApi.ActorOriginating {
  constructor() {
    super();
    this.httpConnection = null;
    this.websocketConnection = null;
    this.wsMask = false;
    this.wsText = 'Hello';
  }
  
  *data() {
    this.wsMask = this.getTestDataBoolean('ws-mask', this.wsMask);
    this.wsText = this.getTestDataString('ws-text', this.wsText);
  }
  
  *initClient() {
    this.httpConnection = this.createConnection('http');
  }
  
  *run() {
    this.httpConnection.send(new HttpGetUpgradeRequest());
 
    const response = this.httpConnection.receive();
    this.websocketConnection = this.switchProtocol('websocket', this.httpConnection);
    
    this.websocketConnection.send(new WebsocketStackApi.TextFrame(this.wsText, this.wsMask));
    this.websocketConnection.send(new WebsocketStackApi.CloseFrame());
  }
  
  *exit(interrupted) {
    this.closeConnection(this.websocketConnection);
  }
}

module.exports = WebsocketFrameTextOrig;
```

***
[[NOTE={"guid":"01f72bb9-4e85-4cdc-a488-76182dd4f510"}]]
[[ANCHOR={"id":"client-connection","visible":true}]]
## **WebsocketConnectionClient**
In the ***initClient()** and ***run()** methods, it is possible to make an [[REF=, ABS_Actor]] with client capacity connect to a socket. The implementation of the WebsocketConnectionClient API starts in [websocket-connection-client.js](/stack-editor/Stacks-global/websocket/websocket-connection-client.js).

[[NOTE={"guid":"3f46a256-52e5-49ea-a933-bea1d73b190d"}]]
[[ANCHOR={"id":"8a763848-3549-4149-9db6-485ecc4643b5","visible":true}]]
### **Methods**
* [WebsocketConnectionClient.send](#client-connection-send)
* [WebsocketConnectionClient.receive](#client-connection-receive)

***
[[NOTE={"guid":"bab47fdf-f683-45fc-83d3-0e23dad741a2"}]]
[[ANCHOR={"id":"client-connection-send","visible":true}]]
### **WebsocketConnectionClient.send**
```
websocketConnectionClient.send(msg);
```
Sends a WebSocket [WebsocketMsg](#ws-message) frame.

[[NOTE={"guid":"266d975a-1215-41f5-b7d4-ccadd18fbf00"}]]
[[ANCHOR={"id":"34b32147-1362-4db5-b93d-a004f675ce2d","visible":true}]]
#### **Method Description**

```table
Config(classHeading: )

|Parameters                                                                              |
|Name|Type               |Description                                                    |
|msg |[[REF=,MDN_Object]]|An [[REF=,MDN_Object]] inherited from &lt;**WebsocketMsg**&gt;.|
```

```table
Config(classHeading: )

|Async                                                                    |
|type |Description                                                        |
|yield|Generated if the name fulfills RegExp ***/this.(.\*)Connection/***.|
```

#### **Example**
```javascript





this.websocketConnection.send(new WebsocketStackApi.TextFrame('HELLO WORLD'));

```

[[NOTE={"guid":"3fa43ef1-c200-4cdd-a1e9-f725f31b1abc"}]]
[[ANCHOR={"id":"240fbdb5-2aa4-42a2-84ea-fec2b7f8d857","visible":true}]]
#### **Test Cases using WebsocketConnectionClient.send**
+ [TextFrame](/../test-cases/Actor/StackWebsocket/TextFrame)
+ [BinaryFrame](/../test-cases/Actor/StackWebsocket/BinaryFrame)

***
[[NOTE={"guid":"472b4a05-80bb-4eeb-815c-83854353ba7b"}]]
[[ANCHOR={"id":"client-connection-receive","visible":true}]]
### **WebsocketConnectionClient.receive**
```
websocketConnectionClient.receive();
```

[[NOTE={"guid":"b5f54def-2832-4725-88e5-f4b0aaaeea12"}]]
[[ANCHOR={"id":"2c4f0c7d-58a2-480f-b2ee-1b9c04ce2e00","visible":true}]]
Receives a WebSocket [WebsocketMsg](#ws-message) frame.
#### **Method Description**

```table
Config(classHeading: )

|Returns                                                           |
|Type               |Description                                   |
|[[REF=,MDN_Object]]|The received message &lt;**WebsocketMsg**&gt;.|
```
```table
Config(classHeading: )

|Async                                                                    |
|type |Description                                                        |
|yield|Generated if the name fulfills RegExp ***/this.(.\*)Connection/***.|
```
#### **Example**
```javascript





const response = this.websocketConnection.receive();

```

[[NOTE={"guid":"dd0c9d8b-09a5-4c2f-a506-43ef7270ceac"}]]
[[ANCHOR={"id":"3bf05263-eca4-485a-8df3-8fd72b4275f3","visible":true}]]
#### **Test Cases using WebsocketConnectionClient.receive**
+ [TextFrame](/../test-cases/Actor/StackWebsocket/TextFrame)
+ [BinaryFrame](/../test-cases/Actor/StackWebsocket/BinaryFrame)

***

[[NOTE={"guid":"1eb9de27-74fd-47b5-9076-4837cf247956"}]]
[[ANCHOR={"id":"server-connection","visible":true}]]
## **WebsocketConnectionServer**
In the ***initServer()** and ***run()** methods, it is possible to make an [[REF=, ABS_Actor]] with server capacity listen to a socket. The implementation of the WebsocketConnectionServer API starts in [websocket-connection-server.js](/stack-editor/Stacks-global/websocket/websocket-connection-server.js).

[[NOTE={"guid":"e1e38d25-5b89-4c50-be82-3fcedca067c6"}]]
[[ANCHOR={"id":"2e8bd082-2adb-4a77-bfd5-e61124c40deb","visible":true}]]
### **Methods**
* [WebsocketConnectionServer.send](#server-connection-send)
* [WebsocketConnectionServer.receive](#server-connection-receive)

***

[[NOTE={"guid":"3a495990-e007-4448-baa0-4ada0705e1fc"}]]
[[ANCHOR={"id":"server-connection-send","visible":true}]]
### **WebsocketConnectionServer.send**
```
websocketConnectionServer.send(msg);
```

[[NOTE={"guid":"3ce70576-58af-4326-9687-ce9a2a5cd499"}]]
[[ANCHOR={"id":"594989ab-3fdf-49f5-864c-9cc03fac59a9","visible":true}]]
Sends a WebSocket [WebsocketMsg](#ws-message) frame.

#### **Method Description**

```table
Config(classHeading: )

|Parameters                                                                         |
|Name|Type               |Description                                               |
|msg |[[REF=,MDN_Object]]|An [[REF=,MDN_Object]] inherited from &lt;**HttpMsg**&gt;.|
```

```table
Config(classHeading: )

|Async                                                                    |
|type |Description                                                        |
|yield|Generated if the name fulfills RegExp ***/this.(.\*)Connection/***.|
```

#### **Example**
```javascript





this.websocketConnection.send(new WebsocketStackApi.TextFrame('HELLO WORLD'));

```

[[NOTE={"guid":"2122b463-1e06-4149-ae4a-053335b19951"}]]
[[ANCHOR={"id":"db6b54e7-c62b-4caf-bf47-2fd08d90143f","visible":true}]]
#### **Test Cases using WebsocketConnectionServer.send**
+ [TextFrame](/../test-cases/Actor/StackWebsocket/TextFrame)
+ [BinaryFrame](/../test-cases/Actor/StackWebsocket/BinaryFrame)

***

[[NOTE={"guid":"50e3781f-96c0-40dc-9866-6f3a22a1bd42"}]]
[[ANCHOR={"id":"server-connection-receive","visible":true}]]
### **WebsocketConnectionServer.receive**
```
websocketConnectionServer.receive();
```

[[NOTE={"guid":"a028c95e-58d7-4ecb-b51e-c3b21ec6e45b"}]]
[[ANCHOR={"id":"1b05200f-e64d-45ae-8bdf-b96aee765336","visible":true}]]
Receives a WebSocket [WebsocketMsg](#ws-message) frame.
#### **Method Description**

```table
Config(classHeading: )

|Returns                                                           |
|Type               |Description                                   |
|[[REF=,MDN_Object]]|The received message &lt;**WebsocketMsg**&gt;.|
```
```table
Config(classHeading: )

|Async                                                                    |
|type |Description                                                        |
|yield|Generated if the name fulfills RegExp ***/this.(.\*)Connection/***.|
```
#### **Example**
```javascript





const response = this.websocketConnection.receive();

```

[[NOTE={"guid":"a2c34613-3ea3-42a1-a378-e88fc8e38666"}]]
[[ANCHOR={"id":"36338b79-1626-4e4d-afc3-0012cbfb4066","visible":true}]]
#### **Test Cases using WebsocketConnectionServer.receive**
+ [TextFrame](/../test-cases/Actor/StackWebsocket/TextFrame)
+ [BinaryFrame](/../test-cases/Actor/StackWebsocket/BinaryFrame)

***


[[NOTE={"guid":"36abb9ba-b00d-4f0b-b531-a41e9b4d5160"}]]
[[ANCHOR={"id":"ws-message","visible":true}]]
## **WebsocketMsg**
The WebsocketMsg contains either the data to send to a connection or the data received from a connection. The Websocket [[REF=, ABS_Stack_Encoder]] will read the data and the WebsocketMsg [[REF=, ABS_Stack_Decoder]] will write data. The WebsocketMsg must obey the rules of the [[REF=, ABS_Stack_Message]] abstraction. The implementation of the WebsocketMsg API starts in [websocket-msg.js](/stack-editor/Stacks-global/websocket/websocket-msg.js).

The WebsocketMsg has three subclasses:
* [WebsocketMsgTextFrame](#ws-message-text)
* [WebsocketMsgBinaryFrame](#ws-message-binary)
* [WebsocketMsgCloseFrame](#ws-message-close)

that should be inherited and specialized when a message is defined. These specialized classes are the ones to instantiate, not one of the four mentioned here.

* **
### **Example - create an Websocket message frame**

```javascript





class WebsocketMsgTextFrame extends WebsocketMsg {
  constructor(text, mask) {
    super(WebsocketConst.FIN_ON, 0, 0, 0, WebsocketConst.OP_CODE_TEXT, mask ? WebsocketConst.MASK_ON : WebsocketConst.MASK_OFF, text);
  }
}

```
We want the [[REF=, ABS_Actor]]s to be as compact as possible so we can send a message as a one-liner. Using the WebSocket message above could resemble the following.
```javascript





this.websocketConnection.send(new WebsocketMsgTextFrame('HELLO WORLD'));

```

Or it could look like the following, if we remove hard coding and use [[REF=, ABS_Test_Data]].
```javascript





this.websocketConnection.send(new WebsocketMsgTextFrame(this.text));

```

[[NOTE={"guid":"3804b4e2-8642-4487-b4af-9362d6525def"}]]
[[ANCHOR={"id":"3804b4e2-8642-4487-b4af-9362d6525def","visible":true}]]
### **Methods**
* [WebsocketMsg.constructor](#ws-message-constructor)

***
[[NOTE={"guid":"989151b0-085e-4583-84dc-d61aae54b92d"}]]
[[ANCHOR={"id":"ws-message-constructor","visible":true}]]
### **WebsocketMsg.constructor**
```
WebsocketMsg.constructor(fin, rsv1, rsv2, rsv3, opCode, mask, payloadData[, maskingKey]);
```

[[NOTE={"guid":"826e0a00-9475-4614-88a9-e78771003a20"}]]
[[ANCHOR={"id":"ad8302df-b136-4853-a8a6-13657d39681c","visible":true}]]
Creates the WebsocketMsg frame.
#### **Method Description**

```table
Config(classHeading: )

|Parameters                                                                                                                                                                                                                                                                                                       |
|Name       |Type                                                       |Default|Description                                                                                                                                                                                                                      |
|fin        |[[REF=,MDN_number]]                                        |       |**1 bit**: ***0*** - not the final frame, ***1*** - the final frame.                                                                                                                                                             |
|rsv1       |[[REF=,MDN_number]]                                        |       |**1 bit**: reserved                                                                                                                                                                                                              |
|rsv2       |[[REF=,MDN_number]]                                        |       |**1 bit**: reserved                                                                                                                                                                                                              |
|rsv3       |[[REF=,MDN_number]]                                        |       |**1 bit**: reserved                                                                                                                                                                                                              |
|opCode     |[[REF=,MDN_number]]                                        |       |**4 bits**:                                         <br />***0***: Continuation Frame<br />***1***: Text Frame<br />***2***: Binary Frame<br />***8***: Connection Close Frame<br />***9***: Ping Frame<br />***10***: Pong Frame|
|mask       |[[REF=,MDN_number]]                                        |       |**1 bit**: Defines whether the "Payload data" is masked. ***0*** not masked, ***1*** masked.                                                                                                                                     |
|payloadData|[[REF=,MDN_string]]&#160;&#124;&#160;[[REF=,NODEJS_Buffer]]|       |The payload data.                                                                                                                                                                                                                |
|maskingKey |[[REF=,MDN_string]]                                        |''     |If ***mask*** is set, ***maskingKey*** is 4 bytes. See [masking key in RFC 6455 - The WebSocket Protocol](https://datatracker.ietf.org/doc/html/rfc6455#section-5.3)                                                                                                                                                                              |
```

***

[[NOTE={"guid":"53b9461f-c95b-420a-a494-8c1899dce81c"}]]
[[ANCHOR={"id":"ws-message-text","visible":true}]]
## **WebsocketMsgTextFrame**
The WebsocketMsgTextFrame is a subclass of [WebsocketMsg](#ws-message). The implementation of the WebsocketMsgTextFrame starts in [websocket-msg-text-frame.js](/stack-editor/Stacks-global/websocket/msg/websocket-msg-text-frame.js).

[[NOTE={"guid":"fa998c9a-5abc-420f-93ee-1596919538b1"}]]
[[ANCHOR={"id":"24bd16cb-5d8b-49e2-a547-95916ca2317b","visible":true}]]
### **Methods**
* [WebsocketMsgTextFrame.constructor](#ws-message-text-constructor)

***
[[NOTE={"guid":"dd7394fd-c11b-4009-9278-bae7a0a9e1dd"}]]
[[ANCHOR={"id":"ws-message-text-constructor","visible":true}]]
### **WebsocketMsgTextFrame.constructor**
```
websocketMsgTextFrame.constructor(text, mask);
```

[[NOTE={"guid":"fbfc7bb4-ee26-4860-93bf-d1ec70d279a7"}]]
[[ANCHOR={"id":"a1adfd02-fcd7-44dd-b163-a455d99ddc57","visible":true}]]
Creates the WebsocketMsgTextFrame and adds the data for it.
#### **Method Description**

```table
Config(classHeading: )

|Parameters                                                                                                          |
|Name      |Type                                                       |Default|Description                          |
|text      |[[REF=,MDN_string]]&#160;&#124;&#160;[[REF=,NODEJS_Buffer]]|       |The payload data.                    |
|maskingKey|[[REF=,MDN_string]]                                        |''     |Masking key ***0*** or bytes ***4***.|
```

***

[[NOTE={"guid":"53b9461f-c95b-420a-a494-8c1899dce81c"}]]
[[ANCHOR={"id":"ws-message-binary","visible":true}]]
## **WebsocketMsgBinaryFrame**
The WebsocketMsgBinaryFrame is a subclass of [WebsocketMsg](#ws-message). The implementation of the WebsocketMsgBinaryFrame starts in [websocket-msg-binary-frame.js](/stack-editor/Stacks-global/websocket/msg/websocket-msg-binary-frame.js).

[[NOTE={"guid":"fa998c9a-5abc-420f-93ee-1596919538b1"}]]
[[ANCHOR={"id":"24bd16cb-5d8b-49e2-a547-95916ca2317b","visible":true}]]
### **Methods**
* [WebsocketMsgBinaryFrame.constructor](#ws-message-binary-constructor)

***
[[NOTE={"guid":"dd7394fd-c11b-4009-9278-bae7a0a9e1dd"}]]
[[ANCHOR={"id":"ws-message-binary-constructor","visible":true}]]
### **WebsocketMsgBinaryFrame.constructor**
```
websocketMsgBinaryFrame.constructor(data, mask);
```

[[NOTE={"guid":"b341db0e-cc1e-478e-a385-2254c3c9ff3a"}]]
[[ANCHOR={"id":"a1adfd02-fcd7-44dd-b163-a455d99ddc57","visible":true}]]
Creates the WebsocketMsgBinaryFrame and adds the data for it.
#### **Method Description**

```table
Config(classHeading: )

|Parameters                                                                                                          |
|Name      |Type                                                       |Default|Description                          |
|data      |[[REF=,MDN_string]]&#160;&#124;&#160;[[REF=,NODEJS_Buffer]]|       |The payload data.                    |
|maskingKey|[[REF=,MDN_string]]                                        |''     |Masking key ***0*** or bytes ***4***.|
```

***

[[NOTE={"guid":"cdeac4b8-21c6-4aad-b1f7-05327f013147"}]]
[[ANCHOR={"id":"ws-message-close","visible":true}]]
## **WebsocketMsgCloseFrame**
The WebsocketMsgCloseFrame is a subclass of [WebsocketMsg](#ws-message). The implementation of the WebsocketMsgCloseFrame starts in [websocket-msg-close-frame.js](/stack-editor/Stacks-global/websocket/msg/websocket-msg-close-frame.js).

[[NOTE={"guid":"1d03211a-8a1d-4f2e-8e1f-fc6e8559b127"}]]
[[ANCHOR={"id":"80f772a8-5306-4075-9160-1b9c2a049251","visible":true}]]
### **Methods**
* [WebsocketMsgCloseFrame.constructor](#ws-message-close-constructor)

***
[[NOTE={"guid":"99e82361-c481-4a19-b89c-2c4d7fdb2127"}]]
[[ANCHOR={"id":"ws-message-close-constructor","visible":true}]]
### **WebsocketMsgCloseFrame.constructor**
```
websocketMsgCloseFrame.constructor(mask);
```

[[NOTE={"guid":"bcf81e6eb-4c46-4fc6-9cc1-47e13d4801f1"}]]
[[ANCHOR={"id":"3d8aeb6d-b764-4d91-9068-525589e6fdb3","visible":true}]]
Creates the WebsocketMsgCloseFrame.
#### **Method Description**

```table
Config(classHeading: )

|Parameters                                                                  |
|Name      |Type               |Default|Description                          |
|maskingKey|[[REF=,MDN_string]]|''     |See [masking key in RFC 6455 - The WebSocket Protocol](https://datatracker.ietf.org/doc/html/rfc6455#section-5.3).|
```

***
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
