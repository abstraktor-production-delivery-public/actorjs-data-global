
'use strict';

const StackApi = require('stack-api');
const WebsocketConst = require('./websocket-const');


class WebsocketInnerLog {
  static createHeader(msg) {
    const header = new StackApi.LogInner('Header', [], true);
    
    const flags = [];
    const flagsHeader = new StackApi.LogInner(flags, []);
    flags.push(new StackApi.LogPartRef('Flags', `${WebsocketConst.DOCUMENTATION_LINK_ROOT}${WebsocketConst.FIN}`));
    flags.push(new StackApi.LogPartText(`: binary: ${msg.fin}${msg.rsv1}${msg.rsv2}${msg.rsv3}`));
    header.add(flagsHeader);
    
    const fin = [];
    fin.push(new StackApi.LogPartRef('FIN', `${WebsocketConst.DOCUMENTATION_LINK_ROOT}${WebsocketConst.FIN}`));
    fin.push(new StackApi.LogPartText(`: ${msg.fin}`));
    flagsHeader.add(new StackApi.LogInner(fin));
    
    const rsv1 = [];
    rsv1.push(new StackApi.LogPartRef('RSV1', `${WebsocketConst.DOCUMENTATION_LINK_ROOT}${WebsocketConst.RSV1}`));
    rsv1.push(new StackApi.LogPartText(`: ${msg.rsv1}`));
    flagsHeader.add(new StackApi.LogInner(rsv1));
    
    const rsv2 = [];
    rsv2.push(new StackApi.LogPartRef('RSV2', `${WebsocketConst.DOCUMENTATION_LINK_ROOT}${WebsocketConst.RSV2}`));
    rsv2.push(new StackApi.LogPartText(`: ${msg.rsv2}`));
    flagsHeader.add(new StackApi.LogInner(rsv2));
    
    const rsv3 = [];
    rsv3.push(new StackApi.LogPartRef('RSV3', `${WebsocketConst.DOCUMENTATION_LINK_ROOT}${WebsocketConst.RSV3}`));
    rsv3.push(new StackApi.LogPartText(`: ${msg.rsv3}`));
    flagsHeader.add(new StackApi.LogInner(rsv3));
    
    const opCode = [];
    opCode.push(new StackApi.LogPartRef('Opcode', `${WebsocketConst.DOCUMENTATION_LINK_ROOT}${WebsocketConst.OPCODE}`));
    opCode.push(new StackApi.LogPartText(`: ${msg.opCode} - ${WebsocketConst.OPCODES[msg.opCode]}`));
    header.add(new StackApi.LogInner(opCode));
    
    const mask = [];
    mask.push(new StackApi.LogPartRef('Mask', `${WebsocketConst.DOCUMENTATION_LINK_ROOT}${WebsocketConst.MASK}`));
    mask.push(new StackApi.LogPartText(`: ${msg.mask}`));
    header.add(new StackApi.LogInner(mask));
        
    const payloadLength = [];
    payloadLength.push(new StackApi.LogPartRef('Payload Length', `${WebsocketConst.DOCUMENTATION_LINK_ROOT}${WebsocketConst.PAYLOAD_LENGTH}`));
    payloadLength.push(new StackApi.LogPartText(`: ${msg.payloadLength}`));
    header.add(new StackApi.LogInner(payloadLength));
    
    if(0 !== msg.extendedPayloadLength) {
      const extendedPayloadLength = [];
      extendedPayloadLength.push(new StackApi.LogPartRef('Extended Payload Length', `${WebsocketConst.DOCUMENTATION_LINK_ROOT}${WebsocketConst.EXTENDED_PAYLOAD_LENGTH}`));
      extendedPayloadLength.push(new StackApi.LogPartText(`: ${msg.extendedPayloadLength}`));
      header.add(new StackApi.LogInner(extendedPayloadLength));
    }
    
    if(1 === msg.mask) {
      const maskingKey = msg.maskingKey;
      const maskingKeys = [];
      maskingKeys.push(new StackApi.LogPartRef('Masking-key', `${WebsocketConst.DOCUMENTATION_LINK_ROOT}${WebsocketConst.MASKING_KEY}`));
      maskingKeys.push(new StackApi.LogPartText(`: ${maskingKey[0].toString(16)}, ${maskingKey[1].toString(16)}, ${maskingKey[2].toString(16)}, ${maskingKey[3].toString(16)}`));
      header.add(new StackApi.LogInner(maskingKeys));
    }
    return header;
  }
  
  static createPayload(msg) {
    if(0 !== msg.payloadLength) {
      const payload = new StackApi.LogInner('Payload', [], true);
      if(2 === msg.opCode) {
        const binaries = [];
        StackApi.BinaryLog.generateLog(msg.payloadData, binaries);
        binaries.forEach((binary) => {
          payload.add(new StackApi.LogInner(binary));
        });
      }
      else {
        const payloadData = [];
        payloadData.push(new StackApi.LogPartText(`${Buffer.from(msg.payloadData).toString()}`));
        payload.add(new StackApi.LogInner(payloadData));
      }
      return payload;
    }
    else {
      return null;
    }
  }
  
  static createPayloadUnmasked(msg) {
    if(0 !== msg.payloadLength && 1 === msg.mask) {
      const payload = new StackApi.LogInner('Unmasked Payload', [], true);
      if(2 === msg.opCode) {
        const binaries = [];
        StackApi.BinaryLog.generateLog(msg.payloadDataUnmasked, binaries);
        binaries.forEach((binary) => {
          payload.add(new StackApi.LogInner(binary));
        });
      }
      else {
        const payloadData = [];
        payloadData.push(new StackApi.LogPartText(`${Buffer.from(msg.payloadDataUnmasked).toString()}`));
        payload.add(new StackApi.LogInner(payloadData));
      }
      return payload;
    }
    else {
      return null;
    }
  }
}

module.exports = WebsocketInnerLog;
