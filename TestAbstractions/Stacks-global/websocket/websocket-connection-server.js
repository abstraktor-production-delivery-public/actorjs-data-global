
'use strict';

const StackApi = require('stack-api');
const WebsocketConnectionServerOptions = require('./websocket-connection-server-options');
const WebsocketEncoder = require('./websocket-encoder');
const WebsocketDecoder = require('./websocket-decoder');


class WebsocketConnectionServer extends StackApi.ConnectionServer {
  constructor(id, type, actor, options) {
    super(id, type, 'websocket', actor, StackApi.NetworkType.TCP, WebsocketConnectionServerOptions, options);
  }

  receive() {
    this.receiveMessage(new WebsocketDecoder());
  }

  send(msg) {
    this.sendMessage(new WebsocketEncoder(msg));
  }
}

module.exports = WebsocketConnectionServer;
