
'use strict';

const {randomBytes} = require('crypto');

//       0                   1                   2                   3
//       0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
//      +-+-+-+-+-------+-+-------------+-------------------------------+
//      |F|R|R|R| opcode|M| Payload len |    Extended payload length    |
//      |I|S|S|S|  (4)  |A|     (7)     |             (16/64)           |
//      |N|V|V|V|       |S|             |   (if payload len==126/127)   |
//      | |1|2|3|       |K|             |                               |
//      +-+-+-+-+-------+-+-------------+ - - - - - - - - - - - - - - - +
//      |     Extended payload length continued, if payload len == 127  |
//      + - - - - - - - - - - - - - - - +-------------------------------+
//      |                               |Masking-key, if MASK set to 1  |
//      +-------------------------------+-------------------------------+
//      | Masking-key (continued)       |          Payload Data         |
//      +-------------------------------- - - - - - - - - - - - - - - - +
//      :                     Payload Data continued ...                :
//      + - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - +
//      |                     Payload Data continued ...                |
//      +---------------------------------------------------------------+

class WebsocketMsg {
  constructor(fin, rsv1, rsv2, rsv3, opCode, mask, payloadData, maskingKey) {
    this.fin = fin;
    this.rsv1 = rsv1;
    this.rsv2 = rsv2;
    this.rsv3 = rsv3;
    this.opCode = opCode;
    this.mask = mask;
    this.payloadLength = 0;
    this.extendedPayloadLength = 0;
    this.payloadData = payloadData;
    this.payloadDataUnmasked = null;
    this.maskingKey = !maskingKey && 1 === mask ? randomBytes(4) : maskingKey;
  }
}


module.exports = WebsocketMsg;
