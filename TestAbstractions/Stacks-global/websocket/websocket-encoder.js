
'use strict';

const StackApi = require('stack-api');
const WebsocketInnerLog = require('./websocket-inner-log');
const WebsocketConst = require('./websocket-const');
const util = require('util');


class WebsocketEncoder extends StackApi.Encoder {
  constructor(msg, command = WebsocketEncoder.SEND_ALL) {
    super();
    this.msg = msg;
    this.command = command;
    this.buffer = null;
  }
  
  *encode() {
    switch(this.command) {
      case WebsocketEncoder.SEND_ALL:
        yield* this._sendAll();
        break;
      default:
        throw new Error('NOT IMPLEMENTD');
        break;
    };
  }
  
  _createBuffer() {
    const msg = this.msg;
    let size = 2;
    if(undefined === msg.payloadData) {
      this.buffer = Buffer.allocUnsafe(size);

      return {
        type: WebsocketEncoder.PAYLOAD_7_BITS,
        length: 0
      };
    }
    const payloadDataLength = 'string' === msg.payloadData ? Buffer.byteLength(msg.payloadData) : msg.payloadData.length;
    let payloadDataType = WebsocketEncoder.PAYLOAD_7_BITS;
    size += payloadDataLength;
    if(payloadDataLength <= 125) {}
    else if(payloadDataLength >= 126 && payloadDataLength <= 65535) {
      payloadDataType = WebsocketEncoder.PAYLOAD_16_BITS;
      size += 2;
    }
    else {
      payloadDataType = WebsocketEncoder.PAYLOAD_64_BITS;
      size += 8; 
    }
    if(WebsocketConst.MASK_ON === msg.mask) {
      size += 4;
    }
    this.buffer = Buffer.allocUnsafe(size);
    return {
      type: payloadDataType,
      length: payloadDataLength
    };
  }
  
  *_sendAll() {
    const msg = this.msg;
    const payloadDataObject = this._createBuffer();
    let offset = 0;

    let firstByte = 0;
    firstByte = StackApi.BitByte.setBit(firstByte, 0, msg.fin);
    firstByte = StackApi.BitByte.setBit(firstByte, 1, msg.rsv1);
    firstByte = StackApi.BitByte.setBit(firstByte, 2, msg.rsv2);
    firstByte = StackApi.BitByte.setBit(firstByte, 3, msg.rsv3);
    firstByte = StackApi.BitByte.setBits(firstByte, 4, 7, msg.opCode);
    offset = this.buffer.writeUInt8(firstByte, offset);
    
    let secondByte = 0;
    secondByte = StackApi.BitByte.setBit(secondByte, 0, msg.mask);
    if(WebsocketEncoder.PAYLOAD_7_BITS === payloadDataObject.type) {
      secondByte = StackApi.BitByte.setBits(secondByte, 1, 7, payloadDataObject.length);
      msg.payloadLength = payloadDataObject.length;
      offset = this.buffer.writeUInt8(secondByte, offset);
    }
    else if(WebsocketEncoder.PAYLOAD_16_BITS === payloadDataObject.type) {
      secondByte = StackApi.BitByte.setBits(secondByte, 1, 7, 126);
      msg.payloadLength = 126;
      msg.extendedPayloadLength = payloadDataObject.length;
      offset = this.buffer.writeUInt8(secondByte, offset);
      offset = this.buffer.writeUInt16BE(payloadDataObject.length, offset);
    }
    else if(WebsocketEncoder.PAYLOAD_64_BITS === payloadDataObject.type) {
      secondByte = StackApi.BitByte.setBits(secondByte, 1, 7, 127);
      msg.payloadLength = 127;
      msg.extendedPayloadLength = BigInt(payloadDataObject.length);
      offset = this.buffer.writeUInt8(secondByte, offset);
      offset = this.buffer.writeBigUInt64BE(BigInt(payloadDataObject.length), offset);
    }
    
    if(0 !== payloadDataObject.length) {
      if(1 === msg.mask) {
        const payloadDataMasked = Buffer.from(msg.payloadData);
        msg.maskingKey.copy(this.buffer, offset, 0, 4);
        offset += 4;
        for(let i = 0; i < payloadDataMasked.length; ++i) {
          payloadDataMasked[i] = payloadDataMasked[i] ^ msg.maskingKey[i % 4];
        }
        payloadDataMasked.copy(this.buffer, offset, 0, payloadDataMasked.length);
        msg.payloadDataUnmasked = msg.payloadData;
        msg.payloadData = payloadDataMasked;
      }
      else {
        if(Buffer.isBuffer(msg.payloadData)) {
          msg.payloadData.copy(this.buffer, offset, 0, payloadDataObject.length);
        }
        else {
          this.buffer.write(msg.payloadData, offset);
        }
      }
    }
    yield* this.send(this.buffer);
    if(this.isLogIp) {
      this.addLog(WebsocketInnerLog.createHeader(msg));
      this.addLog(WebsocketInnerLog.createPayload(msg));
      this.addLog(WebsocketInnerLog.createPayloadUnmasked(msg));
      this.setCaption(WebsocketConst.OPCODES[msg.opCode]);
      this.logMessage();
    }
  }
}

WebsocketEncoder.SEND_ALL = 0;

WebsocketEncoder.PAYLOAD_7_BITS = 0;
WebsocketEncoder.PAYLOAD_16_BITS = 1;
WebsocketEncoder.PAYLOAD_64_BITS = 2;

module.exports = WebsocketEncoder;
