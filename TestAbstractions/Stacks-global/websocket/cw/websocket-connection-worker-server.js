
'use strict';

const StackApi = require('stack-api');
const HttpUpgradeResponse = require('../msg/http-upgrade-response');


class WebsocketConnectionWorkerServer extends StackApi.ConnectionWorkerServer {
  constructor() {
    super();
    this.httpConnection = null;
  }
  
  *onAccepted(connection) {
    const request = yield connection.receive();
    yield connection.send(new HttpUpgradeResponse('demo'));
    return this.switchProtocol('websocket', connection);
  }
  
  *onDisconnecting(connection) {
    const closeFrame = yield connection.receive();
  }
}

module.exports = WebsocketConnectionWorkerServer;
