
'use strict';

const StackApi = require('stack-api');
const HttpUpgradeRequest = require('../msg/http-upgrade-request');
const WebsocketMsgCloseFrame = require('../msg/websocket-msg-close-frame');


class WebSocketConnectionWorkerClient extends StackApi.ConnectionWorkerClient {
  constructor() {
    super();
    this.httpConnection = null;
  }
  
  *onConnected(connection) {
    yield connection.send(new HttpUpgradeRequest('http://example.com', 'demo'));
    const respone = yield connection.receive();
    return this.switchProtocol('websocket', connection);
  }
  
  *onDisconnecting(connection) {
    yield connection.send(new WebsocketMsgCloseFrame());
  }
}

module.exports = WebSocketConnectionWorkerClient;
