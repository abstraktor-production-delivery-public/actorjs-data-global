
'use strict';

const WebsocketConnectionClientOptions = require('./websocket-connection-client-options');
const WebsocketConnectionServerOptions = require('./websocket-connection-server-options');
const WebsocketConst = require('./websocket-const');
const WebsocketConstHeader = require('./websocket-const-header');
const WebsocketMsg = require('./websocket-msg');
const WebsocketMsgBinaryFrame = require('./msg/websocket-msg-binary-frame');
const WebsocketMsgTextFrame = require('./msg/websocket-msg-text-frame');
const WebsocketMsgCloseFrame = require('./msg/websocket-msg-close-frame');
const WebsocketConnectionWorkerClient = require('./cw/websocket-connection-worker-client');
const WebsocketConnectionWorkerServer = require('./cw/websocket-connection-worker-server');
const HttpUpgradeRequest = require('./msg/http-upgrade-request');
const HttpUpgradeResponse = require('./msg/http-upgrade-response');
const WebsocketStyle = require('./websocket-style');


const exportsObject = {
  ConnectionClientOptions: WebsocketConnectionClientOptions,
  ConnectionServerOptions: WebsocketConnectionServerOptions,
  Const: WebsocketConst,
  Header: WebsocketConstHeader,
  Msg: WebsocketMsg,
  BinaryFrame: WebsocketMsgBinaryFrame,
  TextFrame: WebsocketMsgTextFrame,
  CloseFrame: WebsocketMsgCloseFrame,
  ConnectionWorkerClient: WebsocketConnectionWorkerClient,
  ConnectionWorkerServer: WebsocketConnectionWorkerServer,
  HttpUpgradeRequest: HttpUpgradeRequest,
  HttpUpgradeResponse: HttpUpgradeResponse,
  Style: WebsocketStyle
}


module.exports = exportsObject;
