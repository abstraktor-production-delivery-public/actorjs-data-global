
'use strict';

const StackApi = require('stack-api');
const WebsocketConnectionClientOptions = require('./websocket-connection-client-options');
const WebsocketEncoder = require('./websocket-encoder');
const WebsocketDecoder = require('./websocket-decoder');


class WebsocketConnectionClient extends StackApi.ConnectionClient {
  constructor(id, type, actor, options) {
    super(id, type, 'websocket', actor, StackApi.NetworkType.TCP, WebsocketConnectionClientOptions, options);
  }

  send(msg) {
    this.sendMessage(new WebsocketEncoder(msg));
  }
  
  receive() {
    this.receiveMessage(new WebsocketDecoder());
  }
}

module.exports = WebsocketConnectionClient;
