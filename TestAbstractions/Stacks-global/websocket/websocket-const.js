
'use strict';


class WebsocketConst {}

WebsocketConst.FIN_OFF = 0;
WebsocketConst.FIN_ON = 1;

WebsocketConst.OP_CODE_CONTINUATION = 0;
WebsocketConst.OP_CODE_TEXT = 1;
WebsocketConst.OP_CODE_BINARY = 2;
WebsocketConst.OP_CODE_CONNECTION_CLOSE = 8;
WebsocketConst.OP_CODE_PING = 9;
WebsocketConst.OP_CODE_PONG = 10;

WebsocketConst.MASK_OFF = 0;
WebsocketConst.MASK_ON = 1;


WebsocketConst.DOCUMENTATION_LINK_ROOT = 'https://tools.ietf.org/html/rfc6455';

WebsocketConst.FIN = '#section-5.2';
WebsocketConst.RSV1 = '#section-5.2';
WebsocketConst.RSV2 = '#section-5.2';
WebsocketConst.RSV3 = '#section-5.2';
WebsocketConst.OPCODE = '#section-5.2';
WebsocketConst.MASK = '#section-5.2';
WebsocketConst.PAYLOAD_LENGTH = '#section-5.2';
WebsocketConst.EXTENDED_PAYLOAD_LENGTH = '#section-5.2';
WebsocketConst.MASKING_KEY = '#section-5.2';

WebsocketConst.OPCODES = [
  'Continuation Frame',
  'Text Frame',
  'Binary Frame',
  'RESERVED',
  'RESERVED',
  'RESERVED',
  'RESERVED',
  'RESERVED',
  'Connection Close',
  'Ping',
  'Pong',
  'RESERVED',
  'RESERVED',
  'RESERVED',
  'RESERVED',
  'RESERVED'
];

module.exports = WebsocketConst;
