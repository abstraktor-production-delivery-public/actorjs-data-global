
'use strict';


class WebsocketConstHeader {}

WebsocketConstHeader.SEC_WEBSOCKET_KEY = 'Sec-WebSocket-Key';
WebsocketConstHeader.SEC_WEBSOCKET_EXTENSIONS = 'Sec-WebSocket-Extensions';
WebsocketConstHeader.SEC_WEBSOCKET_ACCEPT = 'Sec-WebSocket-Accept';
WebsocketConstHeader.SEC_WEBSOCKET_PROTOCOL = 'Sec-WebSocket-Protocol';
WebsocketConstHeader.SEC_WEBSOCKET_VERSION = 'Sec-WebSocket-Version';

WebsocketConstHeader.HEADER_DATA = new Map([
  [WebsocketConstHeader.SEC_WEBSOCKET_KEY, {links: ['rfc6455#section-11.3.1', 'rfc6455#section-11.3']}],
  [WebsocketConstHeader.SEC_WEBSOCKET_EXTENSIONS, {links: ['rfc6455#section-11.3.2', 'rfc6455#section-11.3']}],
  [WebsocketConstHeader.SEC_WEBSOCKET_ACCEPT, {links: ['rfc6455#section-11.3.3', 'rfc6455#section-11.3']}],
  [WebsocketConstHeader.SEC_WEBSOCKET_PROTOCOL, {links: ['rfc6455#section-11.3.4', 'rfc6455#section-11.3']}],
  [WebsocketConstHeader.SEC_WEBSOCKET_VERSION, {links: ['rfc6455#section-11.3.5', 'rfc6455#section-11.3']}],
]);


module.exports = WebsocketConstHeader;
