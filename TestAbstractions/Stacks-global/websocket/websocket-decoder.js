
'use strict';

const StackApi = require('stack-api');
const WebsocketInnerLog = require('./websocket-inner-log');
const WebsocketConst = require('./websocket-const');
const WebsocketMsg = require('./websocket-msg');


class WebsocketDecoder extends StackApi.Decoder {
  constructor(command = WebsocketDecoder.RECEIVE_ALL) {
    super();
    this.msg = null;
    this.command = command;
  }
  
  *decode() {
    switch(this.command) {
      case WebsocketDecoder.RECEIVE_ALL:
        return yield* this._receiveAll();
    }
  }
  
  *_receiveAll() {
    const first2Bytes = yield* this.receiveSize(2);
    const firstByte = first2Bytes.readUInt8(0);
    const fin = StackApi.BitByte.getBit(firstByte, 0);
    const rsv1 = StackApi.BitByte.getBit(firstByte, 1);
    const rsv2 = StackApi.BitByte.getBit(firstByte, 2);
    const rsv3 = StackApi.BitByte.getBit(firstByte, 3);
    const opCode = StackApi.BitByte.getBits(firstByte, 4, 7);
    
    const secondByte = first2Bytes.readUInt8(1);
    const mask = StackApi.BitByte.getBit(secondByte, 0);
    const payloadLength = StackApi.BitByte.getBits(secondByte, 1, 7);
    
    let maskingKey = '';
    if(1 === mask) {
      maskingKey = yield* this.receiveSize(4);
    }
    
    let payloadDataLength = null;
    let isExtendedPayloadLength = false;
    if(payloadLength <= 125) {
      payloadDataLength = payloadLength;
    }
    else if(126 === payloadLength) {
      const extendedPayloadLength16Buffer = yield* this.receiveSize(2);
      payloadDataLength = extendedPayloadLength16Buffer.readUInt16BE();
      isExtendedPayloadLength = true;
    }
    else if(127 === payloadLength) {
      const extendedPayloadLength64Buffer = yield* this.receiveSize(8);
      payloadDataLength = extendedPayloadLength64Buffer.readBigUInt64BE();
      isExtendedPayloadLength = true;
    }
    
    let payloadData;
    let payloadDataUnmasked = null;
    if(0 !== payloadLength) {
      payloadData = yield* this.receiveSize(payloadDataLength);
      if(1 === mask) {
        const payloadDataMasked = Buffer.from(payloadData);
        payloadDataUnmasked = Buffer.from(payloadData);
        for(let i = 0; i < payloadDataUnmasked.length; ++i) {
          payloadDataUnmasked[i] = payloadDataUnmasked[i] ^ maskingKey[i % 4];
        }
      }
      if(WebsocketConst.OP_CODE_TEXT === opCode) {
        payloadData = payloadData.toString();
      }
    }
    const msg = new WebsocketMsg(fin, rsv1, rsv2, rsv3, opCode, mask, payloadData, maskingKey);
    msg.payloadLength = payloadLength;
    if(isExtendedPayloadLength) {
      msg.extendedPayloadLength = payloadDataLength;
    }
    msg.payloadDataUnmasked = payloadDataUnmasked;
    if(this.isLogIp) {
      this.addLog(WebsocketInnerLog.createHeader(msg));
      this.addLog(WebsocketInnerLog.createPayload(msg));
      this.addLog(WebsocketInnerLog.createPayloadUnmasked(msg));
      this.setCaption(WebsocketConst.OPCODES[msg.opCode]);
      this.logMessage();
    }
    
    return msg;
  }
}

WebsocketDecoder.RECEIVE_ALL = 0;


module.exports = WebsocketDecoder;
