
'use strict';

const StackApi = require('stack-api');
const TestInnerLog = require('./test-inner-log');


class TestDecoder extends StackApi.Decoder {
  constructor(msg) {
    super();
    this.msg = msg;
  }
}

module.exports = TestDecoder;
