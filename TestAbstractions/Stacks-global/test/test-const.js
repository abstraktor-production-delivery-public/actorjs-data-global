
'use strict';


class TestConst {
  constructor() {
    
  }
}

TestConst.MAX_CAPTION_SIZE = 30;
TestConst.BREAK_CAPTION_SIZE = TestConst.MAX_CAPTION_SIZE - 3;


module.exports = TestConst;
