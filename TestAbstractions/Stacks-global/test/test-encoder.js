
'use strict';

const StackApi = require('stack-api');
const TestInnerLog = require('./test-inner-log');


class TestEncoder extends StackApi.Encoder {
  constructor(msg) {
    super();
    this.msg = msg;
  }
  
  *encode() {

  }
}

module.exports = TestEncoder;
