
'use strict';

const StackApi = require('stack-api');
const TestConnectionClientOptions = require('./test-connection-client-options');
const TestEncoder = require('./test-encoder');
const TestDecoder = require('./test-decoder');


class TestConnectionClient extends StackApi.ConnectionClient {
  constructor(id, type, actor, options) {
    super(id, type, 'test', actor, StackApi.NetworkType.TCP, TestConnectionClientOptions, options);
  }

  send(msg) {
    this.sendMessage(new TestEncoder(msg));
  }
  
  receive(msg) {
    this.receiveMessage(new TestDecoder(msg));
  }
}

module.exports = TestConnectionClient;
