
'use strict';

const StackApi = require('stack-api');
const TestConnectionServerOptions = require('./test-connection-server-options');
const TestEncoder = require('./test-encoder');
const TestDecoder = require('./test-decoder');


class TestConnectionServer extends StackApi.ConnectionServer {
  constructor(id, type, actor, options) {
    super(id, type, 'test', actor, StackApi.NetworkType.TCP, TestConnectionServerOptions, options);
  }

  receive(msg) {
    this.receiveMessage(new TestDecoder(msg));
  }

  send(msg) {
    this.sendMessage(new TestEncoder(msg));
  }
}

module.exports = TestConnectionServer;
