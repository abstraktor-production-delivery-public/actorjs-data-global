
'use strict';

const StackApi = require('stack-api');
const TestConst = require('./test-const');


class TestInnerLog {
  static generateLog(msg, ipLogs) {
    const readableMsg = `${StackApi.AsciiDictionary.getSymbolString(msg)}`;
    const innerLog = new StackApi.LogInner(`'${readableMsg}'`);
    ipLogs.push(innerLog);
    return readableMsg.length <= TestConst.MAX_CAPTION_SIZE ? readableMsg : readableMsg.substring(0, TestConst.BREAK_CAPTION_SIZE) + '...';
  }
}

module.exports = TestInnerLog;
