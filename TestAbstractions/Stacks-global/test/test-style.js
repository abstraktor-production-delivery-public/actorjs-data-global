
'use strict';


class TestStyle {
  constructor(index) {
    this.index = index;
    this.textColor= 'black';
    this.textProtocolColor = 'black';
    this.protocolColor = '#90EE90';
    this.protocolBackgroundColor = 'rgba(144, 238, 144, 0.3)';
  }
}


module.exports = TestStyle;
