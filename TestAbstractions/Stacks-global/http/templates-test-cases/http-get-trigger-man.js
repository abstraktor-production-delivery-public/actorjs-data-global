
'use strict';

const StackApi = require('stack-api');
const GuidGenerator = require('z-abs-corelayer-cs/clientServer/guid-generator');


class HttpGetTriggerMan extends StackApi.StackComponentsTemplatesTestCases {
  
  static createTestCase(testCase, testData, repoName, sutName, futName, tcName) {
    let name = `Inline${GuidGenerator.create()}`;
    name = name.replaceAll('-', '');
    const actor = {
      name: `Actors-inline.${repoName}.${sutName}.${futName}.${tcName}.${name}Orig`,
      type: 'orig',
      phase: 'exec',
      execution: '',
      src: 'DEFAULT_SRC',
      dst: '',
      srv: '',
      testData: '',
      verification: '',
      inlineCode: ''
    };
    testCase.actors.push(actor);
    
    let dataAddressing = null;
    let dataHeaders = null;
    
    const addressing = testData[0];
    const addressingDatas = addressing.data;
    console.log(testData);
    dataAddressing = HttpGetTriggerMan._addAddressHardcoded(addressingDatas, testCase);
    
    const headers = testData[1];
    const headerDatas = headers.views[headers.checkedOptionIndex].data;
    dataHeaders = HttpGetTriggerMan._addHeaders(headerDatas, testCase);
    
    if('["Host", ""]' === dataHeaders.host.value) {
      dataHeaders.host.value = `["Host", "${dataAddressing.host}"]`;
    }
    if(dataAddressing.port) {
      actor.dst = `{"uri":"${dataAddressing.url}","port":"${dataAddressing.port}"}`;
    }
    else {
      if('http' === dataAddressing.protocol) {
        actor.dst = `{"uri":"${dataAddressing.url}","port":"80"}`;
      }
      else if('https' === dataAddressing.protocol) {
        actor.dst = `{"uri":"${dataAddressing.url}","port":"443"}`;
      }
    }
    actor.inlineCode = HttpGetTriggerMan.template({
      actorEnding: 'Orig',
      name: name,
      actorType: 'Originating'
    });
  }
  
  static _addAddressHardcoded(data, testCase) {
    const dataAddressing = {
      protocol: '',
      url: '',
      port: '',
      host: ''
    };
    data.forEach((testCaseData) => {
      if(testCaseData.include) {
        if('Protocol' === testCaseData.name) {
          dataAddressing.protocol = testCaseData.value;
          testCase.testDataTestCases.push({
            key: 'transport-type',
            value: 'tls',
            description: `Transport Type - tls`
          });
        }
        if('Url' === testCaseData.name) {
          dataAddressing.url = testCaseData.value;
        }
        else if('Request Target' === testCaseData.name) {
          testCase.testDataTestCases.push({
            key: 'request-target',
            value: testCaseData.value,
            description: `Http Status Line - ${testCaseData.name}`
          });
        }
        else if('Port' === testCaseData.name) {
          dataAddressing.port = testCaseData.value;
        }
        else if('Host' === testCaseData.name) {
          dataAddressing.host = testCaseData.value;
        }
      }
    });
    return dataAddressing;
  }
  
  static _addHeaders(data, testCase) {
    const dataHeaders = {
      host: ''
    };
    data.forEach((testCaseData) => {
      if(testCaseData.include) {
        const testData = {
          key: 'http-headers[]',
          value: `["${testCaseData.name}", "${testCaseData.value}"]`,
          description: `Http Header - ${testCaseData.name}`
        };
        testCase.testDataTestCases.push(testData);
        if('Host' === testCaseData.name) {
          dataHeaders.host = testData;
        }
      }
    });
    return dataHeaders;
  }
}

HttpGetTriggerMan.displayName = 'get-trigger-man';
HttpGetTriggerMan.regressionFriendly = 'no';

HttpGetTriggerMan.template = HttpGetTriggerMan._template`
'use strict';

const ActorApi = require('actor-api');
const StackApi = require('stack-api');
const HttpApi = require('http-stack-api');


class ${'name'}${'actorEnding'} extends ActorApi.Actor${'actorType'} {
  constructor() {
    super();
    this.httpConnection = null;
    this.requistTarget = 'www.example.com';
    this.httpHeaders = [];
  }
  
  *data() {
    this.requistTarget = this.getTestDataString('request-target', this.requistTarget);
    this.httpHeaders = this.getTestDataArrayStrings('http-headers', this.httpHeaders);
  }
  
  *initClient() {
    this.httpConnection = this.createConnection('http');
  }
  
  *run() {
    this.httpConnection.send(new HttpApi.req.GetHtmlReq(this.requistTarget, this.httpHeaders));
    const response = this.httpConnection.receive();
  }
  
  *exit(interrupted) {
    this.closeConnection(this.httpConnection);
  }
}

module.exports = ${'name'}${'actorEnding'};
`;

HttpGetTriggerMan.validateUrlFunctionString = `
let urlObject = null;
let formattedPreview = preview;
try {
  if('Request Target' === name) {
    formattedPreview = 'http://www.example.com' + preview;
  }
  else if('Host' === name) {
    formattedPreview = 'http://' + preview;
  }
  urlObject = new URL(formattedPreview);
  if('Url' === name) {
    if('/' === urlObject.pathname) {
      if(urlObject.href !== preview + '/') {
        return 3; // ERROR
      }
    }
    else {
      if(urlObject.href !== preview) {
        return 3; // ERROR
      }
    }
  }
  else if('Host' === name && urlObject.host !== preview) {
    return 3; // ERROR
  }
}
catch(err) {
  return 3; // ERROR
}
const protocol = urlObject.protocol;
const protocolPosition = formattedPreview.lastIndexOf(protocol);
const domainExtensionPosition = formattedPreview.lastIndexOf( '.' );
if(0 === protocolPosition && - 1 !== ['http:', 'https:'].indexOf(protocol) && domainExtensionPosition > 2 && formattedPreview.length - domainExtensionPosition > 2) {
  return 1; // SUCCESS
}
else {
  return 3; // ERROR
}`;

HttpGetTriggerMan.testDataViews = [
  {
    displayName: 'Addressing',
    name: 'addressing',
    view: 'optional',
    optionalName: 'httpVersion',
    views: [
      {
        name: 'HTTP/1.1',
        disabled: false,
        data: [
          {
            name: 'Protocol',
            type: 'radio',
            placeHolder: 'request target',
            values: ['https', 'http'],
            value: 'https',
            valid: 0
          },
          {
            name: 'Domain',
            type: 'input',
            placeHolder: 'domain',
            value: '',
            valid: 0
         },
         {
            name: 'Port',
            type: 'input',
            placeHolder: 'port',
            value: '',
            include: 'optional:false',
            valid: 0
         },
         {
            name: 'Path',
            type: 'input',
            placeHolder: 'path',
            value: '',
            include: 'optional:false',
            valid: 0
          },
          {
            name: 'Query',
            type: 'input',
            placeHolder: 'query',
            value: '',
            include: 'optional:false',
            valid: 0
          },
          {
            name: 'Url',
            type: 'preview',
            value: '${Protocol}://${Domain}${Port}${Path}${Query}',
            prefixes: [['Port', ':'], ['Path', '/'], ['Query', '?']],
            validate: HttpGetTriggerMan.validateUrlFunctionString,
            valid: 0
          },
          {
            name: 'Request Target',
            type: 'preview',
            value: '/${Path}${Query}',
            prefixes: [['Path', '/'], ['Query', '?']],
            validate: HttpGetTriggerMan.validateUrlFunctionString,
            valid: 0
          },
          {
            name: 'Host',
            type: 'preview',
            value: '${Domain}',
            prefixes: [],
            validate: HttpGetTriggerMan.validateUrlFunctionString,
            valid: 0
          }
        ]
      },
      {
        name: 'HTTP/2',
        disabled: false,
        data: []
      },
      {
        name: 'HTTP/3',
        disabled: true,
        data: []
      }
    ]
  },
  {
    displayName: 'Headers',
    name: 'headers',
    view: 'optional',
    optionalName: 'httpVersion',
    views: [
      {
        name: 'HTTP/1.1',
        disabled: false,
        data: [
          {
            name: 'Host',
            type: 'input',
            placeHolder: 'Generated if not set',
            value: '',
            include: 'optional:true',
            valid: 0
          },
          {
            name: 'User-Agent',
            type: 'input',
            placeHolder: 'User Agent header',
            value: 'ActorJs',
            include: 'optional:true',
            valid: 0
          },
          {
            name: 'Accept',
            type: 'input',
            placeHolder: 'Accept header',
            value: '*/*',
            include: 'optional:true',
            valid: 0
          },
          {
            name: 'Accept-Encoding',
            type: 'input',
            placeHolder: 'Accept-Encoding header',
            value: 'gzip, deflate, br',
            include: 'optional:true',
            valid: 0
          },
          {
            name: 'Connection',
            type: 'input',
            placeHolder: 'Connection header',
            value: 'keep-alive',
            include: 'optional:true',
            valid: 0
          }
        ]
      },
      {
        name: 'HTTP/2',
        disabled: false,
        data: [
          {
            name: 'Host',
            type: 'input',
            placeHolder: 'Generated if not set',
            value: '',
            include: 'optional:true',
            valid: 0
          }
        ]
      },
      {
        name: 'HTTP/3',
        disabled: true,
        data: []
      }
    ]
  }
];

HttpGetTriggerMan.markupNodes = 2;

HttpGetTriggerMan.markup = `\`\`\`seq
Config(nodeWidth: 126, nodeMessageHeight: 17, nodeEventHeight: 10, widthBias: 23, heightBias: 30, lineNumbers: false, border: false, backgroundColor: transparent)

Title: HTTP - GET
Nodes[orig, server]
orig -o server[http]: connect
orig => server[http]: GET
server => orig[http]: 200 OK
orig -x server[http]: disconnect
\`\`\`
`;


module.exports = HttpGetTriggerMan;
