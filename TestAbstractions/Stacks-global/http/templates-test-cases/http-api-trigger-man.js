
'use strict';

const StackApi = require('stack-api');
const GuidGenerator = require('z-abs-corelayer-cs/clientServer/guid-generator');


class HttpApiTriggerMan extends StackApi.StackComponentsTemplatesTestCases {
  
  static createTestCase(testCase, testData, repoName, sutName, futName, tcName) {
    let name = `Inline${GuidGenerator.create()}`;
    name = name.replaceAll('-', '');
    const actor = {
      name: `Actors-inline.${repoName}.${sutName}.${futName}.${tcName}.${name}Orig`,
      type: 'orig',
      phase: 'exec',
      execution: '',
      src: 'DEFAULT_SRC',
      dst: '',
      srv: '',
      testData: '',
      verification: '',
      inlineCode: ''
    };
    testCase.actors.push(actor);
  }
}

HttpApiTriggerMan.displayName = 'api-trigger-man';
HttpApiTriggerMan.regressionFriendly = 'no';

HttpApiTriggerMan.testDataViews = [
  {
    displayName: 'Public APIs',
    name: 'Public APIs',
    view: 'plugin',
    protocol: 'http',
    groups: [
      {
        name: 'Sports & Fitness',
        apis: [
          {
            name: 'NHL Records and Stats',
            doc: 'https://gitlab.com/dword4/nhlapi',
            groups: [
              {
                name: 'Seasons',
                doc: 'https://gitlab.com/dword4/nhlapi/-/blob/master/stats-api.md#seasons',
                viewName: 'http-get-trigger-man'
              }
            ]
          }
        ]
      }
    ]
  }
];


module.exports = HttpApiTriggerMan;
