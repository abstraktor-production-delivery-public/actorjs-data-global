
'use strict';

const StackApi = require('stack-api');
const HttpDecoder = require('./http-decoder');
const HttpConst = require('./http-const');
const HttpInnerLog = require('./http-inner-log');
const HttpMsgResponse = require('./http-msg-response');


class HttpDecoderResponse extends HttpDecoder {
  constructor(command = HttpDecoder.RECEIVE_ALL) {
    super(new HttpMsgResponse(), command);
  }
  
  *decodeFirstLine() {
    this._parseStatusLine(yield* this.receiveLine());
    if(this.isLogIp) {
      const innerLogHttpResponse = HttpInnerLog.createStatusLine(this.msg);
      if(innerLogHttpResponse.logParts[0]) {
        const log = innerLogHttpResponse.logParts[0].text;
        this.setCaption(log.substring(log.indexOf(' ') + 1));
      }
      this.addLog(innerLogHttpResponse);
    }
  }
  
  _parseStatusLine(statusLine) {
    const parameters = statusLine.split(HttpConst.SP);
    if(4 <= parameters.length) {
      parameters[2] = parameters.slice(2).join(' ');
    }
    this.msg.addStatusLine(...parameters);
  }
}

module.exports = HttpDecoderResponse;
