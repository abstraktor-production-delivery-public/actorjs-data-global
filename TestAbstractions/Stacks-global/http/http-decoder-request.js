
'use strict';

const StackApi = require('stack-api');
const HttpDecoder = require('./http-decoder');
const HttpConst = require('./http-const');
const HttpInnerLog = require('./http-inner-log');
const HttpMsgRequest = require('./http-msg-request');


class HttpDecoderRequest extends HttpDecoder {
  constructor(command = HttpDecoder.RECEIVE_ALL) {
    super(new HttpMsgRequest(), command);
  }
  
  *decodeFirstLine() {
    this._parseRequestLine(yield* this.receiveLine());
    if(this.isLogIp) {
      const innerLogHttpRequest = HttpInnerLog.createRequestLine(this.msg);
      if(innerLogHttpRequest.logParts[0]) {
        const log = innerLogHttpRequest.logParts[0].text;
        this.setCaption(log.substring(0, log.lastIndexOf(' ')));
      }
      this.addLog(innerLogHttpRequest);
    }
  }
  
  _parseRequestLine(requestLine) {
    let parameters = requestLine.split(HttpConst.SP);
    this.msg.addRequestLine(...parameters);
  }
}


module.exports = HttpDecoderRequest;
