
'use strict';

const StackApi = require('stack-api');
const HttpConst = require('./http-const');
const HttpConstMethod = require('./http-const-method');
const HttpConstHeader = require('./http-const-header');
const HttpConstVersion = require('./http-const-version');
const HttpConstReasonPhrase = require('./http-const-reason-phrase');
const HttpConstStatusCode = require('./http-const-status-code');


class HttpInnerLog {
  static createRequestLine(msg) {
    const methodLog = [];
    methodLog.push(new StackApi.LogPartRef('method', `${HttpConst.DOCUMENTATION_LINK_ROOT}${HttpConstMethod.METHOD.links[0]}`));
    const methodData = HttpConstMethod.METHOD_DATA.get(msg.method);
    if(methodData) {
      methodLog.push(new StackApi.LogPartText(': '));
      methodLog.push(new StackApi.LogPartRef(msg.method, `${HttpConst.DOCUMENTATION_LINK_ROOT}${methodData.links[0]}`));
    }
    else {
      methodLog.push(new StackApi.LogPartText(`: ${msg.method}`));
    }
    
    const requestTargetLog = [];
    requestTargetLog.push(new StackApi.LogPartRef('request-target', `${HttpConst.DOCUMENTATION_LINK_ROOT}${HttpConstMethod.REQUEST_TARGET.links[0]}`));
    requestTargetLog.push(new StackApi.LogPartText(`: ${msg.requestTarget}`));

    const httpVersionLog = [];
    httpVersionLog.push(new StackApi.LogPartRef('HTTP-version', `${HttpConst.DOCUMENTATION_LINK_ROOT}${HttpConstVersion.HTTP_VERSION_DATA.links[0]}`));
    httpVersionLog.push(new StackApi.LogPartText(`: ${msg.httpVersion}`));

    return new StackApi.LogInner(`${msg.method} ${msg.requestTarget} ${msg.httpVersion}`, [new StackApi.LogInner(methodLog), new StackApi.LogInner(requestTargetLog), new StackApi.LogInner(httpVersionLog)], true);
  }
  
  static createStatusLine(msg) {
    const httpVersionLog = [];
    httpVersionLog.push(new StackApi.LogPartRef('HTTP-version', `${HttpConst.DOCUMENTATION_LINK_ROOT}${HttpConstVersion.HTTP_VERSION_DATA.links[0]}`));
    httpVersionLog.push(new StackApi.LogPartText(`: ${msg.httpVersion}`));

    const statusCodeLog = [];
    statusCodeLog.push(new StackApi.LogPartRef('status-code', `${HttpConst.DOCUMENTATION_LINK_ROOT}${HttpConstStatusCode.DATA_STATUS_CODE.links[0]}`));
    const statusCodeData = HttpConstStatusCode.DATA.get(msg.statusCode);
    if(statusCodeData) {
      statusCodeLog.push(new StackApi.LogPartText(': '));
      statusCodeLog.push(new StackApi.LogPartRef(msg.statusCode, `${HttpConst.DOCUMENTATION_LINK_ROOT}${statusCodeData.links[0]}`));
    }
    else {
      statusCodeLog.push(new StackApi.LogPartText(`: ${msg.statusCode}`));
    }
    
    const reasonPhraseLog = [];
    reasonPhraseLog.push(new StackApi.LogPartRef('reason-phrase', `${HttpConst.DOCUMENTATION_LINK_ROOT}${HttpConstReasonPhrase.DATA_REASON_PHRASE.links[0]}`));
    const reasonPhraseData = HttpConstReasonPhrase.DATA.get(msg.statusCode);
    if(reasonPhraseData) {
      reasonPhraseLog.push(new StackApi.LogPartText(': '));
      reasonPhraseLog.push(new StackApi.LogPartRef(msg.reasonPhrase, `${HttpConst.DOCUMENTATION_LINK_ROOT}${reasonPhraseData.links[0]}`));
    }
    else {
      reasonPhraseLog.push(new StackApi.LogPartText(`: ${msg.reasonPhrase}`));
    }

    return new StackApi.LogInner(`${msg.httpVersion} ${msg.statusCode} ${msg.reasonPhrase}`, [new StackApi.LogInner(httpVersionLog), new StackApi.LogInner(statusCodeLog), new StackApi.LogInner(reasonPhraseLog)], true);
  }
  
  static createHeaders(msg) {
    const headersInnerLog = new StackApi.LogInner('[headers]', [], true);
    msg.headers.forEach((values, name) => {
      values.forEach((value) => {
        const header = `${name}: ${value}`;
        const headerData = HttpConstHeader.HEADER_DATA.get(name.toLowerCase());
        if(headerData) {
          const headerLog = [];
          headersInnerLog.add(new StackApi.LogInner(headerLog));
          if(headerData.links[0]) {
            headerLog.push(new StackApi.LogPartRef(name, `${HttpConst.DOCUMENTATION_LINK_ROOT}${headerData.links[0]}`));
          }
          else {
            headerLog.push(new StackApi.LogPartText(name));
          }
          headerLog.push(new StackApi.LogPartText(`: ${value}`));
        }
        else {
          headersInnerLog.add(new StackApi.LogInner(header));
        }
      });
    });
    return headersInnerLog;
  }
  
  static createBody(body, contentType, contentEncoding, transferEncoding) {
    if(null !== body) {
      const bodyLog = new StackApi.LogInner(`[body] - ${contentType}`);
      bodyLog.setType(StackApi.LogInner.TYPE_BUFFER);
      const bodyLogInners = [];
      const bodyLogInner = new StackApi.LogInner(bodyLogInners);
      bodyLogInner.setType(StackApi.LogInner.TYPE_BUFFER_INNER);
      bodyLog.add(bodyLogInner);
      bodyLogInners.push(new StackApi.LogPartBuffer(contentType, contentEncoding, transferEncoding));
      return {
        log: bodyLog,
        databuffers: body ? ['string' === typeof body ? Buffer.from(body) : body] : undefined,
        caption: `[${contentType}]`
      };
    }
    else {
      return null;
    }
  }
}

module.exports = HttpInnerLog;
