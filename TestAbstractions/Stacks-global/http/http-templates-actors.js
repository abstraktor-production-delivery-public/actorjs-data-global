
'use strict';

const HttpOrigConnect = require('./templates-actors/http-orig-connect');
const HttpOrigGetHtml = require('./templates-actors/http-orig-get-html');
const HttpTermConnect = require('./templates-actors/http-term-connect');
const HttpTermGetHtml = require('./templates-actors/http-term-get-html');
const HttpCondRestPatch = require('./templates-actors/http-cond-rest-patch');
const HttpRequestGet = require('./templates-actors/http-request-get');
const HttpRequestRestPatch = require('./templates-actors/http-request-rest-patch');
const HttpRequestPostActorjsSingle = require('./templates-actors/http-request-post-actorjs-single');
const HttpResponse200Content = require('./templates-actors/http-response-200-content');
const HttpResponse200Data = require('./templates-actors/http-response-200-data');
const StackApi = require('stack-api');


class HttpTemplatesActors extends StackApi.StackComponentsTemplateBaseActors {
  constructor() {
    super([
      HttpOrigConnect,
      HttpOrigGetHtml,
      HttpTermConnect,
      HttpTermGetHtml,
      HttpCondRestPatch,
      HttpRequestGet,
      HttpRequestRestPatch,
      HttpRequestPostActorjsSingle,
      HttpResponse200Content,
      HttpResponse200Data
    ]);
  }
}

module.exports = HttpTemplatesActors;
