
'use strict';

const Request = require('../http-msg-request');
const HttpConstMethod = require('../http-const-method');
const HttpConstHeader = require('../http-const-header');
const HttpConstVersion = require('../http-const-version');


class HttpRestPatchReq extends Request {
  constructor(host, requestTarget, value, ifMatch) {
    super(HttpConstMethod.PATCH, requestTarget, HttpConstVersion.HTTP_1_1);
    this.addHeader(HttpConstHeader.HOST, host);
    this.addHeader(HttpConstHeader.IF_MATCH, ifMatch);
    
    const stringifiedValue = 'string' === typeof value ? value : JSON.stringify(value, null, 2);
    this.addHeader(HttpConstHeader.CONTENT_TYPE, 'application/json');
    this.addHeader(HttpConstHeader.CONTENT_LENGTH, Buffer.byteLength(stringifiedValue));
    this.addBody(stringifiedValue);
  }
}

module.exports = HttpRestPatchReq;
