
'use strict';

const HttpApi = require('http-stack-api');


class HttpPostReq extends HttpApi.Request {
  constructor(requestUri, accept) {
    super(HttpApi.Method.POST, requestUri, HttpApi.Version.HTTP_1_1);
    this.addHeader(HttpApi.Header.HOST, 'www.example.com');
    this.addHeader(HttpApi.Header.CONNECTION, 'keep-alive');
    this.addHeader(HttpApi.Header.USER_AGENT, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) HeadlessChrome/92.0.4512.0 Safari/537.36');
    this.addHeader(HttpApi.Header.ACCEPT_ENCODING, '');
    this.addHeader(HttpApi.Header.ACCEPT_LANGUAGE, 'en-GB');
    this.addHeader(HttpApi.Header.ACCEPT, accept);
  }
}

module.exports = HttpPostReq;
