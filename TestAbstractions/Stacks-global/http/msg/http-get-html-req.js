
'use strict';

const Request = require('../http-msg-request');
const HttpConstMethod = require('../http-const-method');
const HttpConstHeader = require('../http-const-header');
const HttpConstVersion = require('../http-const-version');


class HttpGetHtmlReq extends Request {
  constructor(requestTarget, headers) {
    super(HttpConstMethod.GET, requestTarget, HttpConstVersion.HTTP_1_1);
    if(headers) {
      headers.forEach((header) => {
        this.addHeader(header[0], header[1]);
      });
    }
  }
}

module.exports = HttpGetHtmlReq;
