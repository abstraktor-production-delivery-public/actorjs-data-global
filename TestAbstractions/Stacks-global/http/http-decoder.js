
'use strict';

const StackApi = require('stack-api');
const HttpConst = require('./http-const');
const HttpConstHeader = require('./http-const-header');
const HttpInnerLog = require('./http-inner-log');


class HttpDecoder extends StackApi.Decoder {
  constructor(msg, command) {
    super();
    this.msg = msg;
    this.command = command;
    this.content = null;
    this.contentLength = -1;
    this.contentLengthToRead = -1;
  }
  
  *decode() {
    switch(this.command) {
      case HttpDecoder.RECEIVE_ALL:
        yield* this._receiveAll();
        break;
      case HttpDecoder.RECEIVE_FIRST_LINE:
        yield* this._receiveFirstLine();
        break;
      case HttpDecoder.RECEIVE_FIRST_LINE_AND_HEADERS:
        yield* this._receiveFirstLineAndHeaders();
        break;
      case HttpDecoder.RECEIVE_BODY_CHUNK:
        yield* this._receiveBodyChunk();
        break;
    }
    return this.msg;
  }
  
  *decodeHeaders() {
    while(this._parseHeaderLine(yield* this.receiveLine()));
    if(this.isLogIp) {
      this.addLog(HttpInnerLog.createHeaders(this.msg));
    }
  }
  
  *decodeBodyChunk() {
    const contentType = this.msg.getHeader(HttpConstHeader.CONTENT_TYPE);
    const contentEncoding = this.msg.getHeader(HttpConstHeader.CONTENT_ENCODING);
    const transferEncoding = this.msg.getHeader(HttpConstHeader.TRANSFER_ENCODING);
    while(true) {
      const line = yield* this.receiveLine();
      let size = 0;
      const chunkLineIndex = line.indexOf(HttpConst.COMMA_SP);
      if(-1 === chunkLineIndex) {
        size = Number.parseInt(line, 16);
      }
      else {
        size = Number.parseInt(line.substring(0, chunkLineIndex), 16);
      }
      if(0 !== size) {
        const buffer = yield* this.receiveSize(size);
        this.msg.addBody(buffer);
        const line = yield* this.receiveLine();
      }
      else {
        break;
      }
    }
    while(true) {
      const line = yield* this.receiveLine();
      if(0 === line.length) {
        break;
      }
      console.log('NOT IMPLEMENTED - add trailers to chunked.');
    }
    if(this.isLogIp) {
      const content = this.msg.getBody();
      if(content) {
        const buffer = Buffer.concat(content.buffers, content.getLength());
        const bodyLog = HttpInnerLog.createBody(buffer, contentType, contentEncoding, transferEncoding);
        if(null !== bodyLog) {
          this.setCaption(bodyLog.caption);
          this.addLog(bodyLog.log, bodyLog.databuffers);
        }
      }
    }
  }
  
  *decodeBody() {
    if(this.msg.hasHeader(HttpConstHeader.CONTENT_LENGTH)) { // ONE BODY, LENGTH KNOWN
      this._setContentLength(Number.parseInt(this.msg.getHeader(HttpConstHeader.CONTENT_LENGTH)));
      if(0 < this.contentLength) {
        this.content = new StackApi.ContentBinary();
        this.msg.addBody(this.content);
        while(this._parseBody(yield* this.receiveSize(this._getNextRead())));
        if(this.isLogIp) {
          const contentType = this.msg.getHeader(HttpConstHeader.CONTENT_TYPE);
          const contentEncoding = this.msg.getHeader(HttpConstHeader.CONTENT_ENCODING);
          const content = this.msg.getBody();
          const buffer = Buffer.concat(content.buffers, content.getLength());
          const bodyLog = HttpInnerLog.createBody(buffer, contentType, contentEncoding);
          if(null !== bodyLog) {
            this.setCaption(bodyLog.caption);
            this.addLog(bodyLog.log, bodyLog.databuffers);
          }
        }
      }
    }
    else if(this.msg.hasHeader(HttpConstHeader.TRANSFER_ENCODING)) { // ONE BODY, TRANSFER ENCODING
      if(-1 !== this.msg.getHeader(HttpConstHeader.TRANSFER_ENCODING).indexOf('chunked')) {
        yield* this.decodeBodyChunk();
      }
    }
  }
  
  _parseHeaderLine(headerLine) {
    if(0 !== headerLine.length) {
      const parameters = headerLine.split(HttpConst.COLON_SP);
      this.msg.addHeader(...parameters);
      return true;
    }
    else {
      return false;
    }
  }
  
  _setContentLength(contentLength) {
    this.contentLength = contentLength;
    this.contentLengthToRead = contentLength;
  }
  
  _getNextRead() {
    const nextRead = Math.min(this.contentLengthToRead, HttpConst.MAX_CONTENT_CHUNK_SIZE);
    this.contentLengthToRead -= nextRead;
    return nextRead;
  }
  
  _parseBody(buffer) {
    this.content.addBuffer(buffer);
    return this.contentLengthToRead > 0; 
  }
  
  *_receiveAll() {
    yield* this.decodeFirstLine();
    yield* this.decodeHeaders();
    yield* this.decodeBody();
    if(this.isLogIp) {
      this.logMessage();
    }
    return this.msg;
  }
  
  *_receiveFirstLine() {
    yield* this.decodeFirstLine();
    if(this.isLogIp) {
      this.logMessage();
    }
    return this.msg;
  }
  
  *_receiveFirstLineAndHeaders() {
    yield* this.decodeFirstLine();
    yield* this.decodeHeaders();
    if(this.isLogIp) {
      this.logMessage();
    }
    return this.msg;
  }
  
  *_receiveBodyChunk() {
    yield* this.decodeBodyChunk();
    if(this.isLogIp) {
      this.logMessage();
    }
    return this.msg;
  }
}

HttpDecoder.RECEIVE_ALL = 0;
HttpDecoder.RECEIVE_FIRST_LINE = 1;
HttpDecoder.RECEIVE_FIRST_LINE_AND_HEADERS = 2;
HttpDecoder.RECEIVE_HEADERS = 3;
HttpDecoder.RECEIVE_BODY = 4;
HttpDecoder.RECEIVE_BODY_CHUNK = 5;


module.exports = HttpDecoder;
