
'use strict';

const HttpConstHeader = require('./http-const-header');
const HttpConstMethod = require('./http-const-method');
const HttpConstVersion = require('./http-const-version');
const HttpConstStatusCode = require('./http-const-status-code');
const HttpConstReasonPhrase = require('./http-const-reason-phrase');
const HttpMsgRequest = require('./http-msg-request');
const HttpMsgResponse = require('./http-msg-response');
const HttpGetHtmlReq = require('./msg/http-get-html-req');
const HttpRestPatchReq = require('./msg/http-rest-patch-req');
const HttpStyle = require('./http-style');


const exportsObject = {
  Header: HttpConstHeader,
  Method: HttpConstMethod,
  Version: HttpConstVersion,
  StatusCode: HttpConstStatusCode,
  ReasonPhrase: HttpConstReasonPhrase,
  Request: HttpMsgRequest,
  Response: HttpMsgResponse,
  req: {
    GetHtmlReq: HttpGetHtmlReq,
    HttpRestPatchReq: HttpRestPatchReq
  },
  Style: HttpStyle
};

module.exports = exportsObject;
