
'use strict';

const StackApi = require('stack-api');


class HttpRequestGet extends StackApi.StackComponentsTemplatesActors {}

HttpRequestGet.displayName = 'get-request';
HttpRequestGet.type = 'msg';
HttpRequestGet.markupStyle = {
  fontSize: '10px',
  fontWeight: 'bold',
  overflow: 'hidden',
  whiteSpace: 'nowrap',
  padding: '2px'
};
HttpRequestGet.template = HttpRequestGet._template`
'use strict';

const HttpApi = require('http-stack-api');


class ${'name'}${'actorEnding'} extends HttpApi.Request {
  constructor(requestTarget, host) {
    super(HttpApi.Method.GET, requestTarget, HttpApi.Version.HTTP_1_1);
    this.addHeader(HttpApi.Header.HOST, host);
  }
}

module.exports = ${'name'}${'actorEnding'};
`;

HttpRequestGet.markupNodes = 2;
HttpRequestGet.markup = `

GET [requestTarget] HTTP/1.1
Host: [host]

`;

module.exports = HttpRequestGet;
