
'use strict';

const StackApi = require('stack-api');


class HttpOrigConnect extends StackApi.StackComponentsTemplatesActors {}

HttpOrigConnect.displayName = 'connect';
HttpOrigConnect.type = 'orig';
HttpOrigConnect.template = HttpOrigConnect._template`
'use strict';

const ActorApi = require('actor-api');


class ${'name'}${'actorEnding'} extends ActorApi.Actor${'actorType'} {
  constructor() {
    super();
    this.httpConnection = null;
  }
  
  *data() {
  }
  
  *initClient() {
    this.httpConnection = this.createConnection('http');
  }
  
  *run() {
  }
  
  *exit(interrupted) {
    this.closeConnection(this.httpConnection);
  }
}

module.exports = ${'name'}${'actorEnding'};
`;

HttpOrigConnect.markupNodes = 2;
HttpOrigConnect.markup = `\`\`\`seq
Config(nodeWidth: 126, nodeMessageHeight: 17, nodeEventHeight: 10, widthBias: 23, heightBias: 30, lineNumbers: false, border: false, backgroundColor: transparent)

Title: Connect
Nodes[orig, server]
orig -o server[http]: connect
orig -x server[http]: disconnect
\`\`\`
`;

module.exports = HttpOrigConnect;
