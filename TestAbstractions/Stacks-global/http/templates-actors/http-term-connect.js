
'use strict';

const StackApi = require('stack-api');


class HttpTermConnect extends StackApi.StackComponentsTemplatesActors {}

HttpTermConnect.displayName = 'connect';
HttpTermConnect.type = 'term';
HttpTermConnect.template = HttpTermConnect._template`
'use strict';

const ActorApi = require('actor-api');


class ${'name'}${'actorEnding'} extends ActorApi.Actor${'actorType'} {
  constructor() {
    super();
    this.httpConnection = null;
  }
  
  *data() {
  }
    
  *initServer() {
    this.httpConnection = this.createServer('http');
  }
  
  *run() {
    this.httpConnection.accept();
  }
  
  *exit(interrupted) {
    this.closeConnection(this.httpConnection);
  }
}

module.exports = ${'name'}${'actorEnding'};
`;

HttpTermConnect.markupNodes = 2;
HttpTermConnect.markup = `\`\`\`seq
Config(nodeWidth: 126, nodeMessageHeight: 17, nodeEventHeight: 10, widthBias: 23, heightBias: 30, lineNumbers: false, border: false, backgroundColor: transparent)

Title: Connect
Nodes[client, term]
client -o term[http]: connect
client -x term[http]: disconnect
\`\`\`
`;

module.exports = HttpTermConnect;
