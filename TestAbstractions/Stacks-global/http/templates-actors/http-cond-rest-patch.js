
'use strict';


const StackApi = require('stack-api');


class HttpCondHttRestPatch extends StackApi.StackComponentsTemplatesActors {}

HttpCondHttRestPatch.displayName = 'rest-patch';
HttpCondHttRestPatch.type = 'cond';
HttpCondHttRestPatch.template = HttpCondHttRestPatch._template`
'use strict';

const HttpApi = require('http-stack-api');
const ActorApi = require('actor-api');


class ${'name'}${'actorEnding'} extends ActorApi.Actor${'actorType'} {
  constructor() {
    super();
    this.httpConnection = null;
    this.host = '';
    this.requstUri = '';
    this.patchSetValue = '';
    this.patchResetValue = '';
  }
  
  *data() {
    this.host = this.getTestDataString('request-host');
    this.requstUri = this.getTestDataString('request-uri');
    this.patchSetValue = this.getTestDataString('patch-set-value');
    this.patchResetValue = this.getTestDataString('patch-reset-value');
  }
  
  *initClientPre() {
    this.httpConnection = this.createConnection('http');
  }
  
  *runPre() {
    this.httpConnection.send(new HttpApi.req.HttpRestPatchReq(this.host, this.requstUri, this.patchSetValue));
    
    const response = this.httpConnection.receive();
    VERIFY_VALUE(HttpApi.StatusCode.NoContent, response.statusCode);
  }
  
  *exitPre() {
    this.closeConnection(this.httpConnection);
  }
  
  *initClientPost() {
    this.httpConnection = this.createConnection('http');
  }
  
  *runPost() {
    this.httpConnection.send(new HttpApi.req.HttpRestPatchReq(this.host, this.requstUri, this.patchResetValue));
    
    const response = this.httpConnection.receive();
    VERIFY_VALUE(HttpApi.StatusCode.NoContent, response.statusCode);
  }
  
  *exitPost() {
    this.closeConnection(this.httpConnection);
  }
}

module.exports = ${'name'}${'actorEnding'};
`;

HttpCondHttRestPatch.markupNodes = 2;
HttpCondHttRestPatch.markup = `\`\`\`seq
Config(nodeWidth: 126, nodeMessageHeight: 17, nodeEventHeight: 10, widthBias: 23, heightBias: 30, lineNumbers: false, border: false, backgroundColor: transparent)

Title: Connect
Nodes[orig, server]
orig -o server[http]: connect
orig -x server[http]: disconnect
\`\`\`
`;

module.exports = HttpCondHttRestPatch;
