
'use strict';

const StackApi = require('stack-api');


class HttpTermGetHtml extends StackApi.StackComponentsTemplatesActors {}

HttpTermGetHtml.displayName = 'get-html';
HttpTermGetHtml.type = 'term';
HttpTermGetHtml.template = HttpTermGetHtml._template`
'use strict';

const ActorApi = require('actor-api');
const HttpApi = require('http-stack-api');


class ${'name'}${'actorEnding'} extends ActorApi.Actor${'actorType'} {
  constructor() {
    super();
    this.httpConnection = null;
  }
  
  *data() {
  }
    
  *initServer() {
    this.httpConnection = this.createServer('http');
  }
  
  *run() {
    this.httpConnection.accept();
    
    const request = this.httpConnection.receive();
  }
  
  *exit(interrupted) {
    this.closeConnection(this.httpConnection);
  }
}

module.exports = ${'name'}${'actorEnding'};
`;

HttpTermGetHtml.markupNodes = 2;
HttpTermGetHtml.markup = `\`\`\`seq
Config(nodeWidth: 126, nodeMessageHeight: 17, nodeEventHeight: 10, widthBias: 23, heightBias: 30, lineNumbers: false, border: false, backgroundColor: transparent)

Title: Get html
Nodes[client, term]
client -o term[http]: connect
client => term[http]: GET
term => client[http]: 200 OK
client -x term[http]: disconnect

\`\`\`
`;

module.exports = HttpTermGetHtml;
