
'use strict';

const StackApi = require('stack-api');


class HttpOrigGetHtml extends StackApi.StackComponentsTemplatesActors {}

HttpOrigGetHtml.displayName = 'get-html';
HttpOrigGetHtml.type = 'orig';
HttpOrigGetHtml.template = HttpOrigGetHtml._template`
'use strict';

const ActorApi = require('actor-api');
const HttpApi = require('http-stack-api');


class ${'name'}${'actorEnding'} extends ActorApi.Actor${'actorType'} {
  constructor() {
    super();
    this.httpConnection = null;
    this.requestUri = 'www.example.com';
  }
  
  *data() {
    this.requestUri = this.getTestDataString('request-uri', this.requestUri);
  }
  
  *initClient() {
    this.httpConnection = this.createConnection('http');
  }
  
  *run() {
    this.httpConnection.send(new HttpApi.GetHtmlReq(this.requestUri));
  }
  
  *exit(interrupted) {
    this.closeConnection(this.httpConnection);
  }
}

module.exports = ${'name'}${'actorEnding'};
`;

HttpOrigGetHtml.markupNodes = 2;
HttpOrigGetHtml.markup = `\`\`\`seq
Config(nodeWidth: 126, nodeMessageHeight: 17, nodeEventHeight: 10, widthBias: 23, heightBias: 30, lineNumbers: false, border: false, backgroundColor: transparent)

Title: Get html
Nodes[orig, server]
orig -o server[http]: connect
orig => server[http]: GET
server => orig[http]: 200 OK
orig -x server[http]: disconnect
\`\`\`
`;

module.exports = HttpOrigGetHtml;
