
'use strict';

const StackApi = require('stack-api');


class HttpResponse200Data extends StackApi.StackComponentsTemplatesActors {}

HttpResponse200Data.displayName = 'get-response-200-data';
HttpResponse200Data.type = 'msg';
HttpResponse200Data.markupStyle = {
  fontSize: '10px',
  fontWeight: 'bold',
  overflow: 'hidden',
  whiteSpace: 'nowrap',
  padding: '2px'
};
HttpResponse200Data.template = HttpResponse200Data._template`
'use strict';

const HttpApi = require('http-stack-api');


class ${'name'}${'actorEnding'} extends HttpApi.Response {
  constructor(data, contentType) {
    super(HttpApi.Version.HTTP_1_1, HttpApi.StatusCode.OK, HttpApi.ReasonPhrase.OK);
    if(data) {
      this.addHeader(HttpApi.Header.CONTENT_TYPE, contentType);
      this.addHeader(HttpApi.Header.CONTENT_LENGTH, data.length);
      this.addBody(data);
    }
  }
}

module.exports = ${'name'}${'actorEnding'};

`;

HttpResponse200Data.markupNodes = 2;
HttpResponse200Data.markup = `

HTTP/1.1 200 OK
Content-Type: [contentType]
Content-Size: [data.length]

[data .....]
`;

module.exports = HttpResponse200Data;
