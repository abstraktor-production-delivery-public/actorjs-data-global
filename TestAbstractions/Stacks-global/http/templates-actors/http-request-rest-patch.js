
'use strict';

const StackApi = require('stack-api');


class HttpRequestRestPatch extends StackApi.StackComponentsTemplatesActors {}

HttpRequestRestPatch.displayName = 'http-rest-patch';
HttpRequestRestPatch.type = 'msg';
HttpRequestRestPatch.markupStyle = {
  fontSize: '10px',
  fontWeight: 'bold',
  overflow: 'hidden',
  whiteSpace: 'nowrap',
  padding: '2px'
};
HttpRequestRestPatch.template = HttpRequestRestPatch._template`
'use strict';

const HttpApi = require('http-stack-api');


class ${'name'}${'actorEnding'} extends Request {
  constructor(host, requestTarget, value, ifMatch) {
    super(HttpApi.Method.PATCH, requestTarget, HttpApi.Version.HTTP_1_1);
    this.addHeader(HttpApi.Header.HOST, host);
    this.addHeader(HttpApi.Header.IF_MATCH, ifMatch);
    
    const stringifiedJson = JSON.stringify(value, null, 2);
    this.addHeader(HttpApi.Header.CONTENT_TYPE, 'application/json');
    this.addHeader(HttpApi.Header.CONTENT_LENGTH, Buffer.byteLength(stringifiedJson));
    this.addBody(stringifiedJson);
  }
}

module.exports = ${'name'}${'actorEnding'};
`;

HttpRequestRestPatch.markupNodes = 2;
HttpRequestRestPatch.markup = `

GET [requestTarget] HTTP/1.1
Host: [host]
Content-Type: 'text/plain'
Content-Length: [size of body]

[body]
`;

module.exports = HttpRequestRestPatch;
