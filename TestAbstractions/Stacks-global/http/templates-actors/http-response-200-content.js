
'use strict';

const StackApi = require('stack-api');


class HttpResponse200Content extends StackApi.StackComponentsTemplatesActors {}

HttpResponse200Content.displayName = 'get-response-200-content';
HttpResponse200Content.type = 'msg';
HttpResponse200Content.markupStyle = {
  fontSize: '10px',
  fontWeight: 'bold',
  overflow: 'hidden',
  whiteSpace: 'nowrap',
  padding: '2px'
};
HttpResponse200Content.template = HttpResponse200Content._template`
'use strict';

const HttpApi = require('http-stack-api');


class ${'name'}${'actorEnding'} extends HttpApi.Response {
  constructor(content) {
    super(HttpApi.Version.HTTP_1_1, HttpApi.StatusCode.OK, HttpApi.ReasonPhrase.OK);
    if(content) {
      this.addHeader(HttpApi.Header.CONTENT_TYPE, content.mime);
      this.addHeader(HttpApi.Header.CONTENT_LENGTH, content.size);
      this.addBody(content);
    }
  }
}

module.exports = ${'name'}${'actorEnding'};

`;

HttpResponse200Content.markupNodes = 2;
HttpResponse200Content.markup = `

HTTP/1.1 200 OK
Content-Type: [content.mime]
Content-Size: [content.size]

[content .....]
`;

module.exports = HttpResponse200Content;
