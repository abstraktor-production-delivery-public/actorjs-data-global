
'use strict';

const StackApi = require('stack-api');


class HttpRequestPostActorjsSingle extends StackApi.StackComponentsTemplatesActors {}

HttpRequestPostActorjsSingle.displayName = 'actorjs-request-single-rest';
HttpRequestPostActorjsSingle.type = 'msg';
HttpRequestPostActorjsSingle.markupStyle = {
  fontSize: '10px',
  fontWeight: 'bold',
  overflow: 'hidden',
  whiteSpace: 'nowrap',
  padding: '2px'
};
HttpRequestPostActorjsSingle.template = HttpRequestPostActorjsSingle._template`
'use strict';

const HttpApi = require('http-stack-api');
const GuidGenerator = require('z-abs-corelayer-cs/clientServer/guid-generator');


class ${'name'}${'actorEnding'} extends HttpApi.Request {
  constructor(host, httpUserAgent, actorJsRequestName, ...actorJsRequestParams) {
    super(HttpApi.Method.POST, \`http://\${host}/abs-data/\`, HttpApi.Version.HTTP_1_1);
    this.addHeader(HttpApi.Header.HOST, host);
    this.addHeader(HttpApi.Header.CONNECTION, 'keep-alive');
    this.addHeader(HttpApi.Header.ACCEPT, 'application/json');
    this.addHeader(HttpApi.Header.USER_AGENT, httpUserAgent);
    this.addHeader(HttpApi.Header.ACCEPT_ENCODING, 'gzip, deflate');
    this.addHeader(HttpApi.Header.ACCEPT_LANGUAGE, 'en-US,en;q=0.9');
    
    const body = this._getBody(actorJsRequestName, actorJsRequestParams );
    this.addHeader(HttpApi.Header.CONTENT_TYPE, 'application/json');
    this.addHeader(HttpApi.Header.CONTENT_LENGTH, body.length);
    this.addBody(body);
  }
  
  _getBody(actorJsRequestName, actorJsRequestParams) {
    const bodyObject = {
      id: GuidGenerator.create(),
      requests: [{
        name: actorJsRequestName,
        index: 0,
        params: [...actorJsRequestParams]
      }]
    };
    return JSON.stringify(bodyObject);
  }
}

module.exports = ${'name'}${'actorEnding'};
`;

HttpRequestPostActorjsSingle.markupNodes = 2;
HttpRequestPostActorjsSingle.markup = `

GET [requestTarget] HTTP/1.1
Host: [host]
Connection: keep-alive
Accept:application/json
UserAgent: [httpUserAgent]
Accept-Encoding: gzip, deflate
Accept-Language: en-US,en;q=0.9
Content-Type: 'application/json'
Content-Length: [size of body]

[body]
`;

module.exports = HttpRequestPostActorjsSingle;
