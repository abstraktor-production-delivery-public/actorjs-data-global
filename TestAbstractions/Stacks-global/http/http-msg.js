
'use strict';

const StackApi = require('stack-api');


class HttpMsg {
  constructor() {
    this.headers = new Map();
    this.currentBody = -1;
    this.contents = [];
  }
  
  addHeader(name, value) {
    if(undefined !== value) {
      let convertedValue = value;
      if(typeof value === 'string') {}
      else if(typeof value === 'number') {
        convertedValue = '' + value;
      }
      else if(typeof value === 'boolean') {
        convertedValue = value ? 'true' : 'false';
      }
      else {
        throw new Error('Not Allowed Header value type');
      }
      if(!this.headers.has(name)) {
        this.headers.set(name, []);
      }
      this.headers.get(name).push(convertedValue);
    }
  }
  
  hasHeader(name) {
    return !!this.headers.get(name);
  }
  
  containsHeaderValue(name, value) {
    const values = this.headers.get(name);
    if(!values) {
      return false;
    }
    else {
      for(let i = 0; i < values.length; ++i) {
        if(-1 !== values[i].indexOf(value)) {
          return true;
        }
      }
      return false;
    }
  }
  
  getHeader(name) {
    const headers = this.headers.get(name);
    if(undefined !== headers) {
      return headers[0];
    }
  }
  
  getHeaderList(name) {
    return this.headers.get(name);
  }

  getHeaderNumber(name) {
    const headers = this.headers.get(name);
    if(undefined !== headers) {
      return Number.parseFloat(headers[0]);
    }
  }
  
  getHeaderBoolean(name) {
    const headers = this.headers.get(name);
    if(undefined !== headers) {
      if('true' === headers[0]) {
        return true;
      }
      else if('false' === headers[0]) {
        return false;
      }
    }
  }
  
  deleteHeader(name) {
    this.headers.delete(name);
  }
  
  hasBody() {
    return 0 !== this.contents.length;
  }
  
  addBody(content) {
    if(content instanceof StackApi.ContentBase) {
      this.contents.push(content);
    }
    else {
      const newContent = new StackApi.ContentBinary();
      newContent.addBuffer(content);
      this.contents.push(newContent);
    }
  }
  
  deleteBody() {
    this.contents = [];
  }
  
  getBody(index = 0) {
    return this.contents[index];
  }
}


module.exports = HttpMsg;
