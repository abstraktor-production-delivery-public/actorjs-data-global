
'use strict';

const StackApi = require('stack-api');
const HttpEncoder = require('./http-encoder');
const HttpConst = require('./http-const');
const HttpInnerLog = require('./http-inner-log');


class HttpEncoderRequest extends HttpEncoder {
  constructor(msg, command = HttpEncoder.SEND_ALL) {
    super(msg, command);
  }
  
  encodeFirstLine() {
    const msg = this.msg;
    this.offset += this.buffer.write(`${msg.method} ${msg.requestTarget} ${msg.httpVersion}${HttpConst.CR_CL}`, this.offset);
    if(this.isLogIp) {
      const innerLogHttpRequest = HttpInnerLog.createRequestLine(this.msg);
      if(innerLogHttpRequest.logParts[0]) {
        const log = innerLogHttpRequest.logParts[0].text;
        this.setCaption(log.substring(0, log.lastIndexOf(' ')));
      }
      this.addLog(innerLogHttpRequest);
    }
  }
  
  getFirstLineLength() {
    const msg = this.msg;
    return msg.method.length + msg.requestTarget.length + msg.httpVersion.length + HttpConst.FIRST_LINE;
  }
}

module.exports = HttpEncoderRequest;
