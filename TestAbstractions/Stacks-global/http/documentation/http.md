# **Http**
[[API-STATUS=STABLE, Most functionallity verified]]
[[DOC-STATUS=MOSTLY, ]]

[[NOTE={"guid":"acd4d6ff-a52f-4800-99ce-67916790f5b7"}]]
[[ANCHOR={"id":"9f447a0b-cb5b-42ff-bba2-f56fab69861d","visible":true}]]
## **Description**
The http [[REF=, ABS_Stack]] implements HTTP/1.1, specifically the requirements in: [RFC7230](https://tools.ietf.org/html/rfc7230), [RFC7231](https://tools.ietf.org/html/rfc7231), [RFC7232](https://tools.ietf.org/html/rfc7232), [RFC7233](https://tools.ietf.org/html/rfc7233), [RFC7234](https://tools.ietf.org/html/rfc7234), [RFC7235](https://tools.ietf.org/html/rfc7235).

[[NOTE={"guid":"9429030e-438b-484c-8087-2002c8960a1c"}]]
[[ANCHOR={"id":"5355b4de-42ee-4615-8412-66cb168e3acf","visible":true}]]
## **Objects**
* [HttpConnectionClient](#client-connection)
* [HttpConnectionServer](#server-connection)
* [HttpMsg](#http-message)
  * [HttpMsgRequest](#http-message-request)
  * [HttpMsgResponse](#http-message-response)

See the [[REF=, API_Actor_Client_Stack]] and [[REF=, API_Actor_Server_Stack]] for more information about how to create and close connections. `TODO: Add description for close connection`

Further reading in [[REF=, ABS_Stack]] abstractions.

***

[[NOTE={"guid":"7c9632b4-d442-4615-92e4-419d89f75dcb"}]]
[[ANCHOR={"id":"b65a57b3-d60f-467e-8770-f55b77004691","visible":true}]]
## **Example**
```seq
Config(nodeWidth: 150, nodeMessageHeight: 17, nodeEventHeight: 10, widthBias: 30, heightBias: 30, lineNumbers: false, border: true, backgroundColor: default)

Nodes[orig, term]
orig => term[http]: Get
term => orig[http]: 200 OK
```

[[NOTE={"guid":"495005de-d3c6-49e5-83a1-6ce6474d08da"}]]
[[ANCHOR={"id":"ca380c5e-3893-40c6-acb0-10d26cd48ad6","visible":true}]]
```javascript




const ActorApi = require('actor-api');
const HttpApi = require('http-stack-api');
const HttpMsgGetRequest = require('./msg/HttpMsgGetRequest');


class HttpGetOkOrig extends ActorApi.ActorOriginating {
  constructor() {
    super();
    this.httpConnection = null;
    this.requistUri = '';
  }
  
  *data() {
    this.requistUri = this.getTestDataString('Request-URI', '/demo');
  }
    
  *initClient() {
    this.httpConnection = this.createConnection('http');
  }
  
  *run() {
    this.httpConnection.send(new HttpMsgGetRequest(this.requistUri));
    
    const response = this.httpConnection.receive();
    VERIFY_VALUE(HttpApi.StatusCode.OK, response.statusCode, ' HTTP response line status code:');
    VERIFY_CONTENT_OPTIONAL('content-name', response.getHeaderNumber(HttpApi.Header.CONTENT_LENGTH), response.getBody());
  }
  
  *exit(interrupted) {
    this.closeConnection(this.httpConnection);
  }
}

module.exports = HttpGetOkOrig;


```

***
[[NOTE={"guid":"6c1c915d-1cc3-40a1-b42f-b1a4f02eb239"}]]
[[ANCHOR={"id":"client-connection","visible":true}]]
## **HttpConnectionClient**
In the ***initClient()** and ***run()** methods, it is possible to make an [[REF=, ABS_Actor]] with client capacity connect to a socket. The implementation of the HttpClientConnection API starts in [http-connection-client.js](/stack-editor/Stacks-global/http/http-connection-client.js).

[[NOTE={"guid":"408bd1ac-b028-4371-8ca9-deb0abd606ef"}]]
[[ANCHOR={"id":"5f740bf0-b971-4287-adec-1e71b893e9b3","visible":true}]]
### **Methods**
* [HttpConnectionClient.send](#client-connection-send)
* [HttpConnectionClient.receive](#client-connection-receive)

***
[[NOTE={"guid":"dfacf45a-715c-4ba7-b1db-230ff1049e01"}]]
[[ANCHOR={"id":"client-connection-send","visible":true}]]
### **HttpConnectionClient.send**
```
httpConnectionClient.send(msg);
```
Sends an HTTP request.

[[NOTE={"guid":"ab87fb39-6c11-4f81-8492-9943d389cf14"}]]
[[ANCHOR={"id":"aebcc515-b384-4f04-b271-a7b8229f8c11","visible":true}]]
#### **Method Description**


```table
Config(classHeading: )

|Parameters                                                                         |
|Name|Type               |Description                                               |
|msg |[[REF=,MDN_Object]]|An [[REF=,MDN_Object]] inherited from &lt;**HttpMsg**&gt;.|
```

```table
Config(classHeading: )

|Async                                                                    |
|type |Description                                                        |
|yield|Generated if the name fulfills RegExp ***/this.(.\*)Connection/***.|
```

#### **Example**
```javascript




this.httpConnection.send(new HttpMsgGetRequest(this.requistUri));

```

[[NOTE={"guid":"f55872ac-2a67-468f-b214-5d7d4503ee00"}]]
[[ANCHOR={"id":"611df18f-0040-4c51-9f51-d756e1516b4e","visible":true}]]
#### **Test Cases using HttpConnectionClient.send**
+ [StackHttpGetImage](/../test-cases/Actor/StackHttp/StackHttpGetImage)
+ [DemoMobileBrowsingImagePng](/../test-cases/Actor/Demo/DemoMobileBrowsingImagePng)

***
[[NOTE={"guid":"37abaf8e-9f18-4b87-95b9-c1552ba77b26"}]]
[[ANCHOR={"id":"client-connection-receive","visible":true}]]
### **HttpConnectionClient.receive**
```
httpConnectionClient.receive();
```
Receives an HTTP response.

[[NOTE={"guid":"1b281423-ac33-47d1-9bd0-6809826d5f2c"}]]
[[ANCHOR={"id":"efa62cea-df24-403f-b832-f0f80a8c6552","visible":true}]]
#### **Method Description**


```table
Config(classHeading: )

|Returns                                                      |
|Type               |Description                              |
|[[REF=,MDN_Object]]|The received message &lt;**HttpMsg**&gt;.|
```
```table
Config(classHeading: )

|Async                                                                    |
|type |Description                                                        |
|yield|Generated if the name fulfills RegExp ***/this.(.\*)Connection/***.|
```
#### **Example**
```javascript




const response = this.httpConnection.receive();

```

[[NOTE={"guid":"772adaaa-3ce9-4274-b207-ad887194c696"}]]
[[ANCHOR={"id":"d6b3fad9-caa3-4bf7-9260-6ea148a2b1e5","visible":true}]]
#### **Test Cases using HttpConnectionClient.receive**
+ [StackHttpGetImage](/../test-cases/Actor/StackHttp/StackHttpGetImage)
+ [DemoMobileBrowsingImagePng](/../test-cases/Actor/Demo/DemoMobileBrowsingImagePng)

***
[[NOTE={"guid":"0f782b09-ee2a-4b74-be1d-cdde3cc7755d"}]]
[[ANCHOR={"id":"server-connection","visible":true}]]
## **HttpConnectionServer**
In the ***initServer()** and ***run()** methods, it is possible to make an [[REF=, ABS_Actor]] with server capacity listen to a socket. The implementation of the HttpServerConnection API starts in [http-connection-server.js](/stack-editor/Stacks-global/http/http-connection-server.js).

[[NOTE={"guid":"8673afd8-0fc5-4082-b021-3e77afb2bbe6"}]]
[[ANCHOR={"id":"344e4edd-73b5-422d-aac2-090315c36026","visible":true}]]
### **Methods**
* [HttpConnectionServer.send](#server-connection-send)
* [HttpConnectionServer.receive](#server-connection-receive)

***

[[NOTE={"guid":"f3a8a440-c1c5-44ac-9a44-6c6cd8013837"}]]
[[ANCHOR={"id":"server-connection-send","visible":true}]]
### **HttpConnectionServer.send**
```
httpConnectionServer.send(msg);
```
Sends an HTTP response.

[[NOTE={"guid":"9ddb3065-0d09-45a6-be04-e8fb0137a3ad"}]]
[[ANCHOR={"id":"1f9985b2-f835-4766-bbaf-4b9fe81c4653","visible":true}]]
#### **Method Description**


```table
Config(classHeading: )

|Parameters                                                                         |
|Name|Type               |Description                                               |
|msg |[[REF=,MDN_Object]]|An [[REF=,MDN_Object]] inherited from &lt;**HttpMsg**&gt;.|
```

```table
Config(classHeading: )

|Async                                                                    |
|type |Description                                                        |
|yield|Generated if the name fulfills RegExp ***/this.(.\*)Connection/***.|
```

#### **Example**
```javascript




this.httpConnection.send(new HttpGetImage200OkResp(this.content));

```

[[NOTE={"guid":"5f4cf565-9e6b-4d62-bfd2-1ba546dd0516"}]]
[[ANCHOR={"id":"ac45dc98-e042-486c-981c-3cd59d201495","visible":true}]]
#### **Test Cases using HttpConnectionServer.send**
+ [StackHttpGetImage](/../test-cases/Actor/StackHttp/StackHttpGetImage)
+ [DemoMobileBrowsingImagePng](/../test-cases/Actor/Demo/DemoMobileBrowsingImagePng)

***
[[NOTE={"guid":"80d4e342-56ed-4fb8-9c75-2faaf497420c"}]]
[[ANCHOR={"id":"server-connection-receive","visible":true}]]
### **HttpConnectionServer.receive**
```
httpConnectionServer.receive();
```
Receives an HTTP response.

[[NOTE={"guid":"60250105-c82f-42d2-849f-6aff578a069f"}]]
[[ANCHOR={"id":"a510d767-e215-44b4-a7c3-639e46075539","visible":true}]]
#### **Method Description**


```table
Config(classHeading: )

|Returns                                                      |
|Type               |Description                              |
|[[REF=,MDN_Object]]|The received message &lt;**HttpMsg**&gt;.|
```
```table
Config(classHeading: )

|Async                                                                    |
|type |Description                                                        |
|yield|Generated if the name fulfills RegExp ***/this.(.\*)Connection/***.|
```
#### **Example**
```javascript




const response = this.httpConnection.receive();

```

[[NOTE={"guid":"1255efac-fe4c-4626-8f89-ff9138fa0f6e"}]]
[[ANCHOR={"id":"ac0e40c3-f088-4926-bb94-5f3ff00c7b34","visible":true}]]
#### **Test Cases using HttpConnectionServer.receive**
+ [StackHttpGetImage](/../test-cases/Actor/StackHttp/StackHttpGetImage)
+ [DemoMobileBrowsingImagePng](/../test-cases/Actor/Demo/DemoMobileBrowsingImagePng)

***

[[NOTE={"guid":"cee86b64-d68c-40d6-8c4b-f0e2de8221c0"}]]
[[ANCHOR={"id":"http-message","visible":true}]]
## **HttpMsg**
The HttpMsg contains either the data to send to a connection or the data received from a connection. The Http [[REF=, ABS_Stack_Encoder]] will read the data and the Http [[REF=, ABS_Stack_Decoder]] will write data. The HttpMsg must obey the rules of the [[REF=, ABS_Stack_Message]] abstraction. The implementation of the HttpMsg API starts in [http-msg.js](/stack-editor/Stacks-global/http/http-msg.js).

The HttpMsg has two subclasses:
* [HttpMsgRequest](#http-message-request)
* [HttpMsgResponse](#http-message-response)

that should be inherited and specialized when a request or response is defined. These specialized classes are the ones to instantiate, not one of the three mentioned here.

***

### **Example - create an HTTP request object**

```javascript




class HttpGetReq extends HttpApi.Request {
  constructor(requetTarget, accept) {
    super(HttpApi.Method.GET, requetTarget, HttpApi.Version.HTTP_1_1);
    this.addHeader(HttpApi.Header.HOST, 'www.example.com');
    this.addHeader(HttpApi.Header.CONNECTION, 'keep-alive');
    this.addHeader(HttpApi.Header.USER_AGENT, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) HeadlessChrome/92.0.4512.0 Safari/537.36');
    this.addHeader(HttpApi.Header.ACCEPT_ENCODING, '');
    this.addHeader(HttpApi.Header.ACCEPT_LANGUAGE, 'en-GB');
    this.addHeader(HttpApi.Header.ACCEPT, accept);
  }
}
```
We want the [[REF=, ABS_Actor]]s to be as compact as possible so we can send a request as a one-liner. Using the HTTP request above could resemble the following.
```javascript




this.httpConnection.send(new HttpGetReq('www.example.com/my-image', 'image/png'));

```

Or it could look like the following, if we remove hard coding and use [[REF=, ABS_Test_Data]].
```javascript




this.httpConnection.send(new HttpGetReq(this.requetTarget, this.acceptHeader));

```

[[NOTE={"guid":"81ebbfe6-71be-4373-be34-1ecb1ae0a4fd"}]]
[[ANCHOR={"id":"3f35129a-3ac4-4d9b-b2bd-bbde4e619b15","visible":true}]]
### **Methods**
* [HttpMsg.addHeader](#http-message-addHeader)
* [HttpMsg.hasHeader](#http-message-hasHeader)
* [HttpMsg.containsHeaderValue](#http-message-containsHeaderValue)
* [HttpMsg.getHeader](#http-message-getHeader)
* [HttpMsg.getHeaderList](#http-message-getHeaderList)
* [HttpMsg.getHeaderNumber](#http-message-getHeaderNumber)
* [HttpMsg.getHeaderBoolean](#http-message-getHeaderBoolean)
* [HttpMsg.deleteHeader](#http-message-deleteHeader)
* [HttpMsg.addBody](#http-message-addBody)
* [HttpMsg.getBody](#http-message-getBody)

***
[[NOTE={"guid":"7514e8f9-61fc-4ce0-b8bc-f8d9f27c36dc"}]]
[[ANCHOR={"id":"http-message-addHeader","visible":true}]]
### **HttpMsg.addHeader**
```
httpMsg.addHeader(name, value);
```
Adds an HTTP header to the message.

[[NOTE={"guid":"a1642ba8-878e-4648-a717-9c2971870bf6"}]]
[[ANCHOR={"id":"3ee18111-34b2-4ac8-aae2-05fe8954ecd6","visible":true}]]
#### **Method Description**


```table
Config(classHeading: )

|Parameters                                                                                                                        |
|Name |Type                                                                                          |Description                  |
|name |[[REF=,MDN_string]]                                                                           |The name of the HTTP header. |
|value|[[REF=,MDN_string]]&#160;&#124;&#160;[[REF=,MDN_number]]&#160;&#124;&#160;[[REF=,MDN_boolean]]|The value of the HTTP header.|
```

***
[[NOTE={"guid":"80ced38e-20c6-4278-9c0e-b895e435bbfe"}]]
[[ANCHOR={"id":"http-message-hasHeader","visible":true}]]
### **HttpMsg.hasHeader**
```
httpMsg.hasHeader(name);
```
Checks whether an HTTP header exists.

[[NOTE={"guid":"5dc2c112-633b-4609-8817-2d1d3b81347b"}]]
[[ANCHOR={"id":"74a7d961-8b10-4268-81ea-8287a372e49a","visible":true}]]
#### **Method Description**


```table
Config(classHeading: )

|Parameters                                           |
|Name|Type               |Description                 |
|name|[[REF=,MDN_string]]|The name of the HTTP header.|
```

```table
Config(classHeading: )

|Returns                                        |
|Type                |Description               |
|[[REF=,MDN_boolean]]|The resulting ***value***.|
```

***
[[NOTE={"guid":"02107fab-da2c-4b17-a6f6-b45026fbd985"}]]
[[ANCHOR={"id":"http-message-containsHeaderValue","visible":true}]]
### **HttpMsg.containsHeaderValue**
```
httpMsg.containsHeaderValue(name, value);
```
Check whether an HTTP header contains a value.

[[NOTE={"guid":"2edc1c89-05ef-4f05-bad9-3f116a84b5b3"}]]
[[ANCHOR={"id":"1e67ef5f-9a44-4080-bbb8-9bca7811c458","visible":true}]]
#### **Method Description**


```table
Config(classHeading: )

|Parameters                                                     |
|Name |Type               |Description                          |
|name |[[REF=,MDN_string]]|The name of the HTTP header.         |
|value|[[REF=,MDN_string]]|The value to find in the HTTP header.|
```

```table
Config(classHeading: )

|Returns                                        |
|Type                |Description               |
|[[REF=,MDN_boolean]]|The resulting ***value***.|
```
***
[[NOTE={"guid":"eb93fe97-3c2f-499b-81a1-6bcad83469de"}]]
[[ANCHOR={"id":"http-message-getHeader","visible":true}]]
### **HttpMsg.getHeader**
```
httpMsg.getHeader(name);
```
Retrieves the value of an HTTP header as a [[REF=, MDN_string]].

[[NOTE={"guid":"30bb580e-1e16-4a4d-a986-a1cbee2e5aef"}]]
[[ANCHOR={"id":"377a0057-a684-4e95-ad72-81066f81504d","visible":true}]]
#### **Method Description**


```table
Config(classHeading: )

|Parameters                                           |
|Name|Type               |Description                 |
|name|[[REF=,MDN_string]]|The name of the HTTP header.|
```

```table
Config(classHeading: )

|Returns                                       |
|Type               |Description               |
|[[REF=,MDN_string]]|The resulting ***value***.|
```

***
[[NOTE={"guid":"cb07d389-a7ed-415f-b6b1-e2aea5c0a6fb"}]]
[[ANCHOR={"id":"http-message-getHeaderList","visible":true}]]
### **HttpMsg.getHeaderList**
```
httpMsg.getHeaderList(name);
```
Retrieves the values of an HTTP header as a [[REF=, MDN_strings]].
[[NOTE={"guid":"9607a857-ca92-4f39-889c-a1fdb2582b9e"}]]
[[ANCHOR={"id":"5daf8e42-0a11-42ac-b231-d924af134529","visible":true}]]
#### **Method Description**


```table
Config(classHeading: )

|Parameters                                           |
|Name|Type               |Description                 |
|name|[[REF=,MDN_string]]|The name of the HTTP header.|
```

```table
Config(classHeading: )

|Returns                                        |
|Type                |Description               |
|[[REF=,MDN_strings]]|The resulting ***value***.|
```

***
[[NOTE={"guid":"8c2f3c2d-343e-4405-ba12-51a677722fcf"}]]
[[ANCHOR={"id":"http-message-getHeaderNumber","visible":true}]]
### **HttpMsg.getHeaderNumber**
```
httpMsg.getHeaderNumber(name);
```
Retrieves the value of an HTTP header as a [[REF=, MDN_number]].

[[NOTE={"guid":"192e80ec-b159-4141-a20b-e608196d7733"}]]
[[ANCHOR={"id":"9b6761fd-703a-4bbd-8866-955607ed238b","visible":true}]]
#### **Method Description**


```table
Config(classHeading: )

|Parameters                                           |
|Name|Type               |Description                 |
|name|[[REF=,MDN_string]]|The name of the HTTP header.|
```

```table
Config(classHeading: )

|Returns                                       |
|Type               |Description               |
|[[REF=,MDN_number]]|The resulting ***value***.|
```

***
[[NOTE={"guid":"2566b319-f656-41ad-9905-295a5e6f98df"}]]
[[ANCHOR={"id":"http-message-getHeaderBoolean","visible":true}]]
### **HttpMsg.getHeaderBoolean**
```
httpMsg.getHeaderBoolean(name);
```
Retrieves the value of an HTTP header as a [[REF=, MDN_boolean]].

[[NOTE={"guid":"6c1c81c6-d990-4350-8b18-7c63149f5e3d"}]]
[[ANCHOR={"id":"e94eefa2-3c02-4ebc-b57d-befc22a19757","visible":true}]]
#### **Method Description**


```table
Config(classHeading: )

|Parameters                                           |
|Name|Type               |Description                 |
|name|[[REF=,MDN_string]]|The name of the HTTP header.|
```

```table
Config(classHeading: )

|Returns                                        |
|Type                |Description               |
|[[REF=,MDN_boolean]]|The resulting ***value***.|
```

***
[[NOTE={"guid":"fb04963f-542a-485e-9986-02852f92b6d7"}]]
[[ANCHOR={"id":"http-message-deleteHeader","visible":true}]]
### **HttpMsg.deleteHeader**
```
httpMsg.deleteHeader(name);
```
Removes an HTTP header from the message.

[[NOTE={"guid":"59af1bc7-6de0-4e9a-a18b-b6580076f5dc"}]]
[[ANCHOR={"id":"fb3b7780-abed-4ba6-8d1c-1651f5bafec2","visible":true}]]
#### **Method Description**


```table
Config(classHeading: )

|Parameters                                                     |
|Name|Type               |Description                           |
|name|[[REF=,MDN_string]]|The name of the HTTP header to delete.|
```


***
[[NOTE={"guid":"0552437b-c967-4125-9e4f-f41424343876"}]]
[[ANCHOR={"id":"http-message-addBody","visible":true}]]
### **HttpMsg.addBody**
```
httpMsg.addBody(content);
```
Adds an HTTP body to the message.

[[NOTE={"guid":"b33669c0-d1ae-4948-afc3-a5e20c73b996"}]]
[[ANCHOR={"id":"dc9f673e-a4ff-41ed-ae34-9f70af25b86b","visible":true}]]
#### **Method Description**


```table
Config(classHeading: )

|Parameters                                                                                                                                                |
|Name|Type                                                            |Description                                                                         |
|name|&lt;**any**&gt;&#160;&#124;&#160;[[REF=,API_Stack_Content_Base]]|The body as &lt;**any**&gt; type or encapsulated in [[REF=,API_Stack_Content_Base]].|
```

***
[[NOTE={"guid":"dfacf45a-715c-4ba7-b1db-230ff1049e01"}]]
[[ANCHOR={"id":"http-message-getBody","visible":true}]]
### **HttpMsg.getBody**
```
httpMsg.getBody([index]);
```
Retrieves the HTTP body.

[[NOTE={"guid":"e9fc941e-fdc9-4656-b4ef-65d43a549fac"}]]
[[ANCHOR={"id":"f568cf96-8870-4d89-a202-f0b2f0955514","visible":true}]]
#### **Method Description**


```table
Config(classHeading: )

|Parameters                                                              |
|Name|Type               |Default|Description                            |
|name|[[REF=,MDN_number]]|0      |The index of the HTTP body to retrieve.|
```

```table
Config(classHeading: )

|Returns                                                                                  |
|Type                           |Description                                              |
|[[REF=,API_Stack_Content_Base]]|The body encapsulated in [[REF=,API_Stack_Content_Base]].|
```

***
[[NOTE={"guid":"1ee4e2f8-fac6-4073-ac0d-b3c9c4748629"}]]
[[ANCHOR={"id":"http-message-request","visible":true}]]
## **HttpMsgRequest**
The HttpMsgRequest is a subclass of [HttpMsg](#http-message). The implementation of the HttpMsgRequest starts in [http-msg-request.js](/stack-editor/Stacks-global/http/http-msg-request.js).

[[NOTE={"guid":"5793445c-9019-49d7-a207-f1e95272e122"}]]
[[ANCHOR={"id":"cfba2dfd-87e2-4d9e-847a-feca80ce8fab","visible":true}]]
### **Methods**
* [HttpMsgRequest.constructor](#http-message-request-constructor)
* [HttpMsgRequest.addRequestLine](#http-message-request-addRequestLine)

***
[[NOTE={"guid":"bbb2a01c-8abf-4a7b-a9a8-5fca609fe7d5"}]]
[[ANCHOR={"id":"http-message-request-constructor","visible":true}]]
### **HttpMsgRequest.constructor**
```
httpMsgRequest.constructor(method, requestTarget, httpVersion);
```

[[NOTE={"guid":"26c89130-8308-4d4d-b5c3-049e57e30780"}]]
[[ANCHOR={"id":"1e7e1910-bb02-400b-9401-60b82ccb29f7","visible":true}]]
Creates the HttpMsgRequest and adds the data for the HTTP request line.
#### **Method Description**

```table
Config(classHeading: )

|Parameters                                               |
|Name         |Type               |Description            |
|method       |[[REF=,MDN_string]]|The HTTP method.       |
|requestTarget|[[REF=,MDN_string]]|The HTTP requestTarget.|
|httpVersion  |[[REF=,MDN_string]]|The HTTP version.      |
```

***
[[NOTE={"guid":"dfdfcaf8-a622-4380-b589-276d433cfaef"}]]
[[ANCHOR={"id":"http-message-request-addRequestLine","visible":true}]]
### **HttpMsgRequest.addRequestLine**
```
httpMsgRequest.addRequestLine(method, requestTarget, httpVersion);
```

[[NOTE={"guid":"a28fd65c-005b-4912-98e2-bd8355b4d25b"}]]
[[ANCHOR={"id":"01a2ada6-9778-4284-a381-211ecdd71113","visible":true}]]
Adds the data for the HTTP request line.
#### **Method Description**

```table
Config(classHeading: )

|Parameters                                               |
|Name         |Type               |Description            |
|method       |[[REF=,MDN_string]]|The HTTP method.       |
|requestTarget|[[REF=,MDN_string]]|The HTTP requestTarget.|
|httpVersion  |[[REF=,MDN_string]]|The HTTP version.      |
```

***

[[NOTE={"guid":"e9e64e3c-d6c7-44bc-b2ae-aabe06190ae9"}]]
[[ANCHOR={"id":"http-message-response","visible":true}]]
## **HttpMsgResponse**
The HttpMsgResponse is a subclass of [HttpMsg](#http-message). The implementation of the HttpMsgResponse starts in [http-msg-response.js](/stack-editor/Stacks-global/http/http-msg-response.js).

[[NOTE={"guid":"0b7f27ad-b20c-4df1-a497-6a7c827537f3"}]]
[[ANCHOR={"id":"d49a5a66-f7c1-4c36-ad00-e17c3ccee8e7","visible":true}]]
### **Methods**
* [HttpMsgResponse.constructor](#http-message-response-constructor)
* [HttpMsgResponse.addStatusLine](#http-message-response-addStatusLine)

***
[[NOTE={"guid":"a0023c63-9260-4293-aac8-82f6076ae4a0"}]]
[[ANCHOR={"id":"http-message-response-constructor","visible":true}]]
### **HttpMsgResponse.constructor**
```
httpMsgResponse.constructor(httpVersion, statusCode, reasonPhrase);
```

[[NOTE={"guid":"4317dade-93d8-4baf-b4a7-b41efb896757"}]]
[[ANCHOR={"id":"a9b16e8b-21a3-4119-8139-e6cc0161c88a","visible":true}]]
Creates the HttpMsgResponse and adds the data for the HTTP status line.
#### **Method Description**

```table
Config(classHeading: )

|Parameters                                        |
|Name        |Type               |Description      |
|httpVersion |[[REF=,MDN_string]]|The HTTP version.|
|statusCode  |[[REF=,MDN_string]]|The status code. |
|reasonPhrase|[[REF=,MDN_string]]|The HTTP phrase. |
```

***
[[NOTE={"guid":"ae1a78ce-a993-4157-9994-bada3cb4f9d3"}]]
[[ANCHOR={"id":"http-message-response-addStatusLine","visible":true}]]
### **HttpMsgResponse.addStatusLine**
```
httpMsgResponse.addStatusLine(httpVersion, statusCode, reasonPhrase);
```

[[NOTE={"guid":"554012c4-8101-4142-a439-767c9ae97efa"}]]
[[ANCHOR={"id":"bc3ce4fe-69b0-4349-9cfd-6e25beb9ce0c","visible":true}]]
Adds the data for the HTTP status line.
#### **Method Description**

```table
Config(classHeading: )

|Parameters                                              |
|Name        |Type               |Description            |
|httpVersion |[[REF=,MDN_string]]|The HTTP version.      |
|statusCode  |[[REF=,MDN_string]]|The HTTP status code.  |
|reasonPhrase|[[REF=,MDN_string]]|The HTTP reason phrase.|
```

***

