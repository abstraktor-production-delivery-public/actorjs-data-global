
'use strict';

const HttpConstStatusCode = require('./http-const-status-code');


class HttpConstReasonPhrase {}

HttpConstReasonPhrase.Continue = 'Continue';
HttpConstReasonPhrase.SwitchingProtocols = 'Switching Protocols';

HttpConstReasonPhrase.OK = 'OK';
HttpConstReasonPhrase.Created = 'Created';
HttpConstReasonPhrase.Accepted = 'Accepted';
HttpConstReasonPhrase.NonAuthoritativeInformation = 'Non-Authoritative Information';
HttpConstReasonPhrase.NoContent = 'No Content';
HttpConstReasonPhrase.ResetContent = 'Reset Content';
HttpConstReasonPhrase.PartialContent = 'Partial Content';

HttpConstReasonPhrase.MultipleChoices = 'Multiple Choices';
HttpConstReasonPhrase.MovedPermanently = 'Moved Permanently ';
HttpConstReasonPhrase.Found = 'Found';
HttpConstReasonPhrase.SeeOther = 'See Other';
HttpConstReasonPhrase.NotModified = 'Not Modified';
HttpConstReasonPhrase.UseProxy = 'UseProxy ';
HttpConstReasonPhrase.TemporaryRedirect = 'Temporary Redirect';

HttpConstReasonPhrase.BadRequest = 'Bad Request';
HttpConstReasonPhrase.Unauthorized = 'Unauthorized';
HttpConstReasonPhrase.PaymentRequired = 'Payment Required';
HttpConstReasonPhrase.Forbidden = 'Forbidden';
HttpConstReasonPhrase.NotFound = 'Not Found';
HttpConstReasonPhrase.MethodNotAllowed = 'Method Not Allowed';
HttpConstReasonPhrase.NotAcceptable = 'Not Acceptable';
HttpConstReasonPhrase.ProxyAuthenticationRequired = 'Proxy Authentication Required';
HttpConstReasonPhrase.RequestTimeout = 'Request Timeout';
HttpConstReasonPhrase.Conflict = 'Conflict';
HttpConstReasonPhrase.Gone = 'Gone';
HttpConstReasonPhrase.LengthRequired = 'Length Required';
HttpConstReasonPhrase.PreconditionFailed = 'Precondition Failed';
HttpConstReasonPhrase.PayloadTooLarge = 'Payload Too Large';
HttpConstReasonPhrase.URITooLong = 'URI Too Long';
HttpConstReasonPhrase.UnsupportedMediaType = 'Unsupported Media Type';
HttpConstReasonPhrase.RangeNotSatisfiable = 'Range Not Satisfiable';
HttpConstReasonPhrase.ExpectationFailed = 'Expectation Failed';
HttpConstReasonPhrase.UpgradeRequired = 'Upgrade Required';

HttpConstReasonPhrase.InternalServerError = 'Internal Server Error';
HttpConstReasonPhrase.NotImplemented = 'Not Implemented';
HttpConstReasonPhrase.BadGateway = 'Bad Gateway';
HttpConstReasonPhrase.ServiceUnavailable = 'Service Unavailable';
HttpConstReasonPhrase.GatewayTimeout = 'Gateway Timeout';
HttpConstReasonPhrase.HTTPVersionNotSupported = 'HTTP Version Not Supported';

HttpConstReasonPhrase.DATA_REASON_PHRASE = {links: ['rfc7231#section-6']};
HttpConstReasonPhrase.DATA = new Map([
  [HttpConstStatusCode.Continue, {links: ['rfc7231#section-6.2.1', 'rfc7231#section-6']}],
  [HttpConstStatusCode.SwitchingProtocols, {links: ['rfc7231#section-6.2.2', 'rfc7231#section-6']}],

  [HttpConstStatusCode.OK, {links: ['rfc7231#section-6.3.1', 'rfc7231#section-6']}],
  [HttpConstStatusCode.Created, {links: ['rfc7231#section-6.3.2', 'rfc7231#section-6']}],
  [HttpConstStatusCode.Accepted, {links: ['rfc7231#section-6.3.3', 'rfc7231#section-6']}],
  [HttpConstStatusCode.NonAuthoritativeInformation, {links: ['rfc7231#section-6.3.4', 'rfc7231#section-6']}],
  [HttpConstStatusCode.NoContent, {links: ['rfc7231#section-6.3.5', 'rfc7231#section-6']}],
  [HttpConstStatusCode.ResetContent, {links: ['rfc7231#section-6.3.6', 'rfc7231#section-6']}],
  [HttpConstStatusCode.PartialContent, {links: ['rfc7233#section-4.1', 'rfc7231#section-6']}],

  [HttpConstStatusCode.MultipleChoices, {links: ['rfc7231#section-6.4.1', 'rfc7231#section-6']}],
  [HttpConstStatusCode.MovedPermanently, {links: ['rfc7231#section-6.4.2', 'rfc7231#section-6']}],
  [HttpConstStatusCode.Found, {links: ['rfc7231#section-6.4.3', 'rfc7231#section-6']}],
  [HttpConstStatusCode.SeeOther, {links: ['rfc7231#section-6.4.4', 'rfc7231#section-6']}],
  [HttpConstStatusCode.NotModified, {links: ['rfc7232#section-4.1', 'rfc7231#section-6']}],
  [HttpConstStatusCode.UseProxy, {links: ['rfc7231#section-6.4.5', 'rfc7231#section-6']}],
  [HttpConstStatusCode.TemporaryRedirect, {links: ['rfc7231#section-6.4.7', 'rfc7231#section-6']}],

  [HttpConstStatusCode.BadRequest, {links: ['', 'rfc7231#section-6']}],
  [HttpConstStatusCode.Unauthorized, {links: ['', 'rfc7231#section-6']}],
  [HttpConstStatusCode.PaymentRequired, {links: ['', 'rfc7231#section-6']}],
  [HttpConstStatusCode.Forbidden, {links: ['', 'rfc7231#section-6']}],
  [HttpConstStatusCode.NotFound, {links: ['', 'rfc7231#section-6']}],
  [HttpConstStatusCode.MethodNotAllowed, {links: ['', 'rfc7231#section-6']}],
  [HttpConstStatusCode.NotAcceptable, {links: ['', 'rfc7231#section-6']}],
  [HttpConstStatusCode.ProxyAuthenticationRequired, {links: ['', 'rfc7231#section-6']}],
  [HttpConstStatusCode.RequestTimeout, {links: ['', 'rfc7231#section-6']}],
  [HttpConstStatusCode.Conflict, {links: ['', 'rfc7231#section-6']}],
  [HttpConstStatusCode.Gone, {links: ['', 'rfc7231#section-6']}],
  [HttpConstStatusCode.LengthRequired, {links: ['', 'rfc7231#section-6']}],
  [HttpConstStatusCode.PreconditionFailed, {links: ['', 'rfc7231#section-6']}],
  [HttpConstStatusCode.PayloadTooLarge, {links: ['', 'rfc7231#section-6']}],
  [HttpConstStatusCode.URITooLong, {links: ['', 'rfc7231#section-6']}],
  [HttpConstStatusCode.UnsupportedMediaType, {links: ['', 'rfc7231#section-6']}],
  [HttpConstStatusCode.RangeNotSatisfiable, {links: ['', 'rfc7231#section-6']}],
  [HttpConstStatusCode.ExpectationFailed, {links: ['', 'rfc7231#section-6']}],
  [HttpConstStatusCode.UpgradeRequired, {links: ['', 'rfc7231#section-6']}],

  [HttpConstStatusCode.InternalServerError, {links: ['', 'rfc7231#section-6']}],
  [HttpConstStatusCode.NotImplemented, {links: ['', 'rfc7231#section-6']}],
  [HttpConstStatusCode.BadGateway, {links: ['', 'rfc7231#section-6']}],
  [HttpConstStatusCode.ServiceUnavailable, {links: ['', 'rfc7231#section-6']}],
  [HttpConstStatusCode.GatewayTimeout, {links: ['', 'rfc7231#section-6']}],
  [HttpConstStatusCode.HTTPVersionNotSupported, {links: ['', 'rfc7231#section-6']}],
]);


module.exports = HttpConstReasonPhrase;
