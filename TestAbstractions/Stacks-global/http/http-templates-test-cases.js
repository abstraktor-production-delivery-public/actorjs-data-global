
'use strict';

const StackApi = require('stack-api');
const HttpGetTriggerMan = require('./templates-test-cases/http-get-trigger-man');
//const HttpApiTriggerMan = require('./templates-test-cases/http-api-trigger-man');


class HttpTemplatesTestCases extends StackApi.StackComponentsTemplateBaseTestCases {
  constructor() {
    super([
      HttpGetTriggerMan/*,
      HttpApiTriggerMan*/
    ]);
  }
}


module.exports = HttpTemplatesTestCases;
