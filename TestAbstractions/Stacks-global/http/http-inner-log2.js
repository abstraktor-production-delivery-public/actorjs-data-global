
'use strict';

const StackApi = require('stack-api');
const DiameterConst = require('../diameter/diameter-const');
const DiameterConstHeader = require('../diameter/diameter-const-header');
const HttpConst = require('./http-const');
const HttpConstMethod = require('./http-const-method');
const HttpConstHeader = require('./http-const-header');


class HttpInnerLog2 {
  static createFrameHeader(frame) {
    const header = new StackApi.LogInner('Frame Header', [], false);
    
    const length = [];
    length.push(new StackApi.LogPartRef('Length', `${DiameterConst.DOCUMENTATION_LINK_ROOT}${DiameterConstHeader.MESSAGE_LENGTH}`));
    length.push(new StackApi.LogPartText(`: ${frame.length}`));
    header.add(new StackApi.LogInner(length));
    
    const type = [];
    type.push(new StackApi.LogPartRef('Type', `${DiameterConst.DOCUMENTATION_LINK_ROOT}${DiameterConstHeader.MESSAGE_LENGTH}`));
    type.push(new StackApi.LogPartText(`: ${frame.type}`));
    header.add(new StackApi.LogInner(type));
    
    const flags = [];
    const commandFlag = new StackApi.LogInner(flags, []);
    flags.push(new StackApi.LogPartRef('Flags', `${DiameterConst.DOCUMENTATION_LINK_ROOT}${DiameterConstHeader.COMMAND_FLAGS}`));
    flags.push(new StackApi.LogPartText(`: binary: ${frame.flags.toString(2).padStart(8, '0')} - decimal: ${frame.flags}`));
    header.add(commandFlag);
    
    switch(frame.type) {
      case HttpInnerLog2.FRAME_TYPE_HEADERS: { 
        const priorityFlagbit = [];
        priorityFlagbit.push(new StackApi.LogPartRef(HttpInnerLog2.HEADER_FLAGS[HttpInnerLog2.HEADER_FLAGS_PRIORITY], `${DiameterConst.DOCUMENTATION_LINK_ROOT}${DiameterConstHeader.R_BIT}`));
        priorityFlagbit.push(new StackApi.LogPartText(`: ${StackApi.BitByte.getBit(frame.flags, HttpInnerLog2.HEADER_FLAGS_PRIORITY)} - `));
        commandFlag.add(new StackApi.LogInner(priorityFlagbit));
        
        const paddedFlagbit = [];
        paddedFlagbit.push(new StackApi.LogPartRef(HttpInnerLog2.HEADER_FLAGS[HttpInnerLog2.HEADER_FLAGS_PADDED], `${DiameterConst.DOCUMENTATION_LINK_ROOT}${DiameterConstHeader.R_BIT}`));
        paddedFlagbit.push(new StackApi.LogPartText(`: ${StackApi.BitByte.getBit(frame.flags, HttpInnerLog2.HEADER_FLAGS_PADDED)} - `));
        commandFlag.add(new StackApi.LogInner(paddedFlagbit));
        
        const endHeadersFlagbit = [];
        endHeadersFlagbit.push(new StackApi.LogPartRef(HttpInnerLog2.HEADER_FLAGS[HttpInnerLog2.HEADER_FLAGS_END_HEADERS], `${DiameterConst.DOCUMENTATION_LINK_ROOT}${DiameterConstHeader.R_BIT}`));
        endHeadersFlagbit.push(new StackApi.LogPartText(`: ${StackApi.BitByte.getBit(frame.flags, HttpInnerLog2.HEADER_FLAGS_END_HEADERS)} - `));
        commandFlag.add(new StackApi.LogInner(endHeadersFlagbit));
        
        const endStreamFlagbit = [];
        endStreamFlagbit.push(new StackApi.LogPartRef(HttpInnerLog2.HEADER_FLAGS[HttpInnerLog2.HEADER_FLAGS_END_STREAM], `${DiameterConst.DOCUMENTATION_LINK_ROOT}${DiameterConstHeader.R_BIT}`));
        endStreamFlagbit.push(new StackApi.LogPartText(`: ${StackApi.BitByte.getBit(frame.flags, HttpInnerLog2.HEADER_FLAGS_END_STREAM)} - `));
        commandFlag.add(new StackApi.LogInner(endStreamFlagbit));
        break;
      }
      case HttpInnerLog2.FRAME_TYPE_SETTINGS: {
        const settingsFlagAckbit = [];
        settingsFlagAckbit.push(new StackApi.LogPartRef(HttpInnerLog2.HEADER_FLAGS_END_STREAM, `${DiameterConst.DOCUMENTATION_LINK_ROOT}${DiameterConstHeader.R_BIT}`));
        settingsFlagAckbit.push(new StackApi.LogPartText(`: ${StackApi.BitByte.getBit(frame.flags, 7)} - `));
        commandFlag.add(new StackApi.LogInner(settingsFlagAckbit));
        break;
      }
    }
    
    const r = [];
    r.push(new StackApi.LogPartRef('Reserved', `${DiameterConst.DOCUMENTATION_LINK_ROOT}${DiameterConstHeader.MESSAGE_LENGTH}`));
    r.push(new StackApi.LogPartText(`: ${frame.r}`));
    header.add(new StackApi.LogInner(r));
    
    const streamIdentifier = [];
    streamIdentifier.push(new StackApi.LogPartRef('Stream Identifier', `${DiameterConst.DOCUMENTATION_LINK_ROOT}${DiameterConstHeader.MESSAGE_LENGTH}`));
    streamIdentifier.push(new StackApi.LogPartText(`: ${frame.streamIdentifier}`));
    header.add(new StackApi.LogInner(streamIdentifier));
    
    return header;
  }
  
  static createFramePayloadData(frame) {
    const data = frame.data;
    if(undefined !== data.padLength) {
      const dataPayload = new StackApi.LogInner('Frame Payload - DATA', [], false);
      const padLength = [];
      padLength.push(new StackApi.LogPartRef('Pad Length', `${DiameterConst.DOCUMENTATION_LINK_ROOT}${DiameterConstHeader.MESSAGE_LENGTH}`));
      padLength.push(new StackApi.LogPartText(`: ${data.padLength}`));
      dataPayload.add(new StackApi.LogInner(padLength));
    }
  }
  
  static createData(frame) {
    if(frame.data.data) {
      const dataLog = new StackApi.LogInner('[data]', [], false);
      const contentType = frame.data.contentType ? frame.data.contentType : 'UNKNOWN';
      const dataInnerLogContentType = [];
      dataInnerLogContentType.push(new StackApi.LogPartRef('ContentType', `${DiameterConst.DOCUMENTATION_LINK_ROOT}${DiameterConstHeader.MESSAGE_LENGTH}`));
      dataInnerLogContentType.push(new StackApi.LogPartText(`: ${contentType}`));
      dataLog.add(new StackApi.LogInner(dataInnerLogContentType));

      const contentEncoding = frame.data.contentEncoding ? frame.data.contentEncoding : 'UNKNOWN';
      const dataInnerLogContentEncoding = [];
      dataInnerLogContentEncoding.push(new StackApi.LogPartRef('ContentEncoding', `${DiameterConst.DOCUMENTATION_LINK_ROOT}${DiameterConstHeader.MESSAGE_LENGTH}`));
      dataInnerLogContentEncoding.push(new StackApi.LogPartText(`: ${contentEncoding}`));
      dataLog.add(new StackApi.LogInner(dataInnerLogContentEncoding));
      return result;
    }
  }
  
  static createFramePayloadHeaders(frame) {
    const data = frame.data;
    if(undefined !== data.padLength || undefined !== data.exclusive || undefined !== data.streamDependency || undefined !== data.weight) {
      const headersPayload = new StackApi.LogInner('Frame Payload - HEADERS', [], false);
      if(undefined !== data.padLength) {
        const padLength = [];
        padLength.push(new StackApi.LogPartRef('Pad Length', `${DiameterConst.DOCUMENTATION_LINK_ROOT}${DiameterConstHeader.MESSAGE_LENGTH}`));
        padLength.push(new StackApi.LogPartText(`: ${data.padLength}`));
        headersPayload.add(new StackApi.LogInner(padLength));
      }
      if(undefined !== data.exclusive) {
        const exclusive = [];
        exclusive.push(new StackApi.LogPartRef('Exclusive', `${DiameterConst.DOCUMENTATION_LINK_ROOT}${DiameterConstHeader.MESSAGE_LENGTH}`));
        exclusive.push(new StackApi.LogPartText(`: ${data.exclusive}`));
        headersPayload.add(new StackApi.LogInner(exclusive));
      }
      if(undefined !== data.streamDependency) {
        const streamDependency = [];
        streamDependency.push(new StackApi.LogPartRef('Stream Dependency', `${DiameterConst.DOCUMENTATION_LINK_ROOT}${DiameterConstHeader.MESSAGE_LENGTH}`));
        streamDependency.push(new StackApi.LogPartText(`: ${data.streamDependency}`));
        headersPayload.add(new StackApi.LogInner(streamDependency));
      }
      if(undefined !== data.weight) {
        const weight = [];
        weight.push(new StackApi.LogPartRef('Weight', `${DiameterConst.DOCUMENTATION_LINK_ROOT}${DiameterConstHeader.MESSAGE_LENGTH}`));
        weight.push(new StackApi.LogPartText(`: ${data.weight}`));
        headersPayload.add(new StackApi.LogInner(weight));
      }
      return headersPayload;
    }
  }
  
  static createLine(frame) {
    const pseudoHeader = frame.data.pseudoHeader;
    if(pseudoHeader.status) {
      const statusLineLog = new StackApi.LogInner(pseudoHeader.status, [], false);
      const statusLineInnerLogStatus = [];
      statusLineInnerLogStatus.push(new StackApi.LogPartRef('status', `${HttpConst.DOCUMENTATION_LINK_ROOT}`));
      statusLineInnerLogStatus.push(new StackApi.LogPartText(`: ${pseudoHeader.status}`));
      statusLineLog.add(new StackApi.LogInner(statusLineInnerLogStatus));
      return {
        log: statusLineLog,
        caption: pseudoHeader.status
      };
    }
    else {
      const requestLine = `${pseudoHeader.method} ${pseudoHeader.scheme}://${pseudoHeader.authority}${pseudoHeader.path}`;
      const requestLineLog = new StackApi.LogInner(requestLine, [], false);
      
      const requestLineInnerLogMethod = [];
      requestLineInnerLogMethod.push(new StackApi.LogPartRef('method', `${HttpConst.DOCUMENTATION_LINK_ROOT}`));
      requestLineInnerLogMethod.push(new StackApi.LogPartText(`: ${pseudoHeader.method}`));
      requestLineLog.add(new StackApi.LogInner(requestLineInnerLogMethod));
      
      const requestLineInnerLogScheme = [];
      requestLineInnerLogScheme.push(new StackApi.LogPartRef('scheme', `${HttpConst.DOCUMENTATION_LINK_ROOT}`));
      requestLineInnerLogScheme.push(new StackApi.LogPartText(`: ${pseudoHeader.scheme}`));
      requestLineLog.add(new StackApi.LogInner(requestLineInnerLogScheme));
      
      const requestLineInnerLogAuthority = [];
      requestLineInnerLogAuthority.push(new StackApi.LogPartRef('authority', `${HttpConst.DOCUMENTATION_LINK_ROOT}`));
      requestLineInnerLogAuthority.push(new StackApi.LogPartText(`: ${pseudoHeader.authority}`));
      requestLineLog.add(new StackApi.LogInner(requestLineInnerLogAuthority));
      
      const requestLineInnerLogPath = [];
      requestLineInnerLogPath.push(new StackApi.LogPartRef('path', `${HttpConst.DOCUMENTATION_LINK_ROOT}`));
      requestLineInnerLogPath.push(new StackApi.LogPartText(`: ${pseudoHeader.path}`));
      requestLineLog.add(new StackApi.LogInner(requestLineInnerLogPath));
      
      return {
        log: requestLineLog,
        caption: requestLine
      };
    }
  }
  
  static createHeaders(frame) {
    if(frame.data && 0 !== frame.data.headers.length) {
      const headersInnerLog = new StackApi.LogInner('[headers]', [], true);
      frame.data.headers.forEach((header) => {
        const headerName = `${header.name}: ${header.value}`;
        const headerData = HttpConstHeader.HEADER_DATA.get(header.name);
        if(headerData) {
          const headerLog = [];
          headersInnerLog.add(new StackApi.LogInner(headerLog));
          if(headerData.links[0]) {
            headerLog.push(new StackApi.LogPartRef(header.name, `${HttpConst.DOCUMENTATION_LINK_ROOT}${headerData.links[0]}`));
          }
          else {
            headerLog.push(new StackApi.LogPartText(header.name));
          }
          headerLog.push(new StackApi.LogPartText(`: ${header.value}`));
        }
        else {
          headersInnerLog.add(new StackApi.LogInner(headerName));
        }
      });
      return headersInnerLog;
    }
  }
  
  static createPriority(frame) {

  }
  
  static createRstStream(frame) {
    const rtsStreamLog = new StackApi.LogInner('Frame Payload - RTS_STREAM', [], true);
    const rtsStreamInnerLog = [];
    const errorCode = frame.data.errorCode;
    rtsStreamInnerLog.push(new StackApi.LogPartRef('Error Code', `${DiameterConst.DOCUMENTATION_LINK_ROOT}`));
    rtsStreamInnerLog.push(new StackApi.LogPartText(`: ${errorCode}`));
    rtsStreamLog.add(new StackApi.LogInner(rtsStreamInnerLog));
    return rtsStreamLog;
  }
  
  static createSettings(frame) {
    if(frame.data && 0 !== frame.data.length) {
      const settings = new StackApi.LogInner('Frame Payload - SETTINGS', [], true);
      frame.data.forEach((setting) => {
        const settingLog = [];
        let heading = HttpInnerLog2.SETTINGS[setting.identifier];
        if(!heading) {
          heading = `UNKNOWN (0x${setting.identifier.toString(16)})`;
        }
        settingLog.push(new StackApi.LogPartRef(heading, `${DiameterConst.DOCUMENTATION_LINK_ROOT}${DiameterConstHeader.MESSAGE_LENGTH}`));
        settingLog.push(new StackApi.LogPartText(`: ${setting.value}`));
        const settingInnerLog = new StackApi.LogInner(settingLog, []);
        settings.add(settingInnerLog);
        
        const identifier = [];
        identifier.push(new StackApi.LogPartRef('Identifier', `${DiameterConst.DOCUMENTATION_LINK_ROOT}`));
        identifier.push(new StackApi.LogPartText(`: ${setting.identifier}`));
        settingInnerLog.add(new StackApi.LogInner(identifier));
        
        const value = [];
        value.push(new StackApi.LogPartRef('Value', `${DiameterConst.DOCUMENTATION_LINK_ROOT}`));
        value.push(new StackApi.LogPartText(`: ${setting.value}`));
        settingInnerLog.add(new StackApi.LogInner(value));
      });
      return settings;
    }
  }
  
  static createPushPromise(frame) {

  }
  
  static createPing(frame, dataBuffers) {
    const pingLog = new StackApi.LogInner('Frame Payload - PING', [], true);
    const ipLogs = [];
    StackApi.BinaryLog.generateLog(Buffer.from(dataBuffers[0]), ipLogs);
    const pingInnerLog = [];
    pingInnerLog.push(new StackApi.LogPartRef('Opaque Data', `${DiameterConst.DOCUMENTATION_LINK_ROOT}`));
    pingInnerLog.push(ipLogs[0]);
    pingLog.add(new StackApi.LogInner(pingInnerLog));
    return pingLog;
  }
  
  static createGoaway(frame) {
    
  }
  
  static createWindowUpdate(frame) {
    const windowUpdateLog = new StackApi.LogInner('Frame Payload - WINDOW_UPDATE', [], true);
    if(frame.data) {
      const windowUpdate = frame.data;
      const reservedInnerLog = [];
      reservedInnerLog.push(new StackApi.LogPartRef('Reserved', `${DiameterConst.DOCUMENTATION_LINK_ROOT}`));
      reservedInnerLog.push(new StackApi.LogPartText(`: ${windowUpdate.reserved}`));
      windowUpdateLog.add(new StackApi.LogInner(reservedInnerLog));
       
      const windowSizeIncrementInnerLog = [];
      windowSizeIncrementInnerLog.push(new StackApi.LogPartRef('Window Size Increment', `${DiameterConst.DOCUMENTATION_LINK_ROOT}`));
      windowSizeIncrementInnerLog.push(new StackApi.LogPartText(`: ${windowUpdate.windowSizeIncrement}`));
      windowUpdateLog.add(new StackApi.LogInner(windowSizeIncrementInnerLog));
    }
    return windowUpdateLog;
  }
  
  static createContinuation(frame) {

  }
}

HttpInnerLog2.FRAME_TYPE_DATA = 0;
HttpInnerLog2.FRAME_TYPE_HEADERS = 1;
HttpInnerLog2.FRAME_TYPE_PRIORITY = 2;
HttpInnerLog2.FRAME_TYPE_RST_STREAM = 3;
HttpInnerLog2.FRAME_TYPE_SETTINGS = 4;
HttpInnerLog2.FRAME_TYPE_PUSH_PROMISE = 5;
HttpInnerLog2.FRAME_TYPE_PING = 6;
HttpInnerLog2.FRAME_TYPE_GOAWAY = 7;
HttpInnerLog2.FRAME_TYPE_WINDOW_UPDATE = 8;
HttpInnerLog2.FRAME_TYPE_CONTINUATION = 9;

HttpInnerLog2.FRAME_NAMES = [
  'DATA',
  'HEADERS',
  'PRIORITY',
  'RST_STREAM',
  'SETTINGS',
  'PUSH_PROMISE',
  'PING',
  'GOAWAY',
  'WINDOW_UPDATE',
  'CONTINUATION'
];

HttpInnerLog2.HEADER_FLAGS_PRIORITY = 2;
HttpInnerLog2.HEADER_FLAGS_PADDED = 4;
HttpInnerLog2.HEADER_FLAGS_END_HEADERS = 5;
HttpInnerLog2.HEADER_FLAGS_END_STREAM = 7;

HttpInnerLog2.HEADER_FLAGS = [
  'Unused Flag',
  'Unused Flag',
  'PRIORITY',
  'Unused Flag',
  'PADDED',
  'END_HEADERS',
  'Unused Flag',
  'END_STREAM'
];

HttpInnerLog2.SETTINGS = [
  '',
  'SETTINGS_HEADER_TABLE_SIZE (0x01)',
  'SETTINGS_ENABLE_PUSH (0x02)',
  'SETTINGS_MAX_CONCURRENT_STREAMS (0x03)',
  'SETTINGS_INITIAL_WINDOW_SIZE (0x04)',
  'SETTINGS_MAX_FRAME_SIZE (0x05)',
  'SETTINGS_MAX_HEADER_LIST_SIZE (0x06)'
];

module.exports = HttpInnerLog2;
