
'use strict';


class HttpConstHeader {}

// --- Representation Metadata ---

HttpConstHeader.CONTENT_TYPE = 'Content-Type';
HttpConstHeader.CONTENT_ENCODING = 'Content-Encoding';
HttpConstHeader.CONTENT_LANGUAGE = 'Content-Language';
HttpConstHeader.CONTENT_LOCATION = 'Content-Location';
  
// --- Payload Semantics ---
  
HttpConstHeader.CONTENT_LENGTH = 'Content-Length';
HttpConstHeader.CONTENT_RANGE = 'Content-Range';
HttpConstHeader.TRAILER = 'Trailer';
HttpConstHeader.TRANSFER_ENCODING = 'Transfer-Encoding';

// *** REQUEST HEADER FIELDS ***
  
// --- CONTROLS ---
  
HttpConstHeader.CACHE_CONTROL = 'Cache-Control';
HttpConstHeader.EXPECT = 'Expect';
HttpConstHeader.HOST = 'Host';
HttpConstHeader.MAX_FORWARDS = 'Max-Forwards';
HttpConstHeader.PRAGMA = 'Pragma';
HttpConstHeader.RANGE = 'Range';
HttpConstHeader.TE = 'TE';

// --- Conditionals ---
  
HttpConstHeader.IF_MATCH = 'If-Match';
HttpConstHeader.IF_NONE_MATCH = 'If-None-Match';
HttpConstHeader.IF_MODIFIED_SINCE = 'If-Modified-Since';
HttpConstHeader.IF_UNMODIFIED_SINCE = 'If-Unmodified-Since';
HttpConstHeader.IF_RANGE = 'If-Range';

// --- Content Negotiation ---
    
HttpConstHeader.ACCEPT = 'Accept';
HttpConstHeader.ACCEPT_CHARSET = 'Accept-Charset';
HttpConstHeader.ACCEPT_ENCODING = 'Accept-Encoding';
HttpConstHeader.ACCEPT_LANGUAGE = 'Accept-Language';
  
// --- Authentication Credentials ---
    
HttpConstHeader.AUTHORIZATION = 'Authorization';
HttpConstHeader.PROXY_AUTHORIZATION = 'Proxy-Authorization';
    
// --- Request Context ---
  
HttpConstHeader.FROM = 'From';
HttpConstHeader.ORIGIN = 'Origin';
HttpConstHeader.REFERER = 'Referer';
HttpConstHeader.USER_AGENT = 'User-Agent';

// --- Header Field Registration ---
HttpConstHeader.CONNECTION = 'Connection';
HttpConstHeader.PROXY_CONNECTION = 'Proxy-Connection';
HttpConstHeader.UPGRADE = 'Upgrade';
HttpConstHeader.VIA = 'Via';
   
// *** RESONSE HEADER FIELDS ***
 
// --- Control Data ---
  
HttpConstHeader.AGE = 'Age';
HttpConstHeader.CACHE_CONTROL = 'Cache-Control';
HttpConstHeader.EXPIRES = 'Expires';
HttpConstHeader.DATE = 'Date';
HttpConstHeader.LOCATION = 'Location';
HttpConstHeader.RETRY_AFTER = 'Retry-After';
HttpConstHeader.VARY = 'Vary';
HttpConstHeader.WARNING = 'Warning';
  
// --- Validator Header Fields ---
  
HttpConstHeader.ETAG = 'ETag';
HttpConstHeader.LAST_MODIFIED = 'Last-Modified';
    
// --- Authentication Challenges ---
  
HttpConstHeader.WWW_AUTHENTICATE = 'WWW-Authenticate';
HttpConstHeader.PROXY_AUTHENTICATE = 'Proxy-Authenticate';
    
// --- Response Context ---
  
HttpConstHeader.ACCEPT_RANGES = 'Accept-Ranges';
HttpConstHeader.ALLOW = 'Allow';
HttpConstHeader.SERVER = 'Server';


HttpConstHeader.HEADER_DATA = new Map([
  // --- Representation Metadata ---
  [HttpConstHeader.CONTENT_TYPE.toLowerCase(), {links: ['rfc7231#section-3.1.1.5', 'rfc7231#section-3.1']}],
  [HttpConstHeader.CONTENT_ENCODING.toLowerCase(), {links: ['rfc7231#section-3.1.2.2', 'rfc7231#section-3.1']}],
  [HttpConstHeader.CONTENT_LANGUAGE.toLowerCase(), {links: ['rfc7231#section-3.1.3.2', 'rfc7231#section-3.1']}],
  [HttpConstHeader.CONTENT_LOCATION.toLowerCase(), {links: ['rfc7231#section-3.1.4.2', 'rfc7231#section-3.1']}],
  // --- Payload Semantics ---
  [HttpConstHeader.CONTENT_LENGTH.toLowerCase(), {links: ['rfc7230#section-3.3.2', 'rfc7231#section-3.3']}],
  [HttpConstHeader.CONTENT_RANGE.toLowerCase(), {links: ['rfc7233#section-4.2', 'rfc7231#section-3.3']}],
  [HttpConstHeader.TRAILER.toLowerCase(), {links: ['rfc7230#section-4.4', 'rfc7231#section-3.3']}],
  [HttpConstHeader.TRANSFER_ENCODING.toLowerCase(), {links: ['rfc7230#section-3.3.1', 'rfc7231#section-3.3']}],
  // *** REQUEST HEADER FIELDS ***
  // --- CONTROLS ---
  [HttpConstHeader.CACHE_CONTROL.toLowerCase(), {links: ['rfc7234#section-5.2', 'rfc7231#section-5.1']}],
  [HttpConstHeader.EXPECT.toLowerCase(), {links: ['rfc7231#section-5.1.1', 'rfc7231#section-5.1']}],
  [HttpConstHeader.HOST.toLowerCase(), {links: ['rfc7230#section-5.4', 'rfc7231#section-5.1']}],
  [HttpConstHeader.MAX_FORWARDS.toLowerCase(), {links: ['rfc7231#section-5.1.2', 'rfc7231#section-5.1']}],
  [HttpConstHeader.PRAGMA.toLowerCase(), {links: ['rfc7234#section-5.4', 'rfc7231#section-5.1']}],
  [HttpConstHeader.RANGE.toLowerCase(), {links: ['rfc7233#section-3.1', 'rfc7231#section-5.1']}],
  [HttpConstHeader.TE.toLowerCase(), {links: ['rfc7230#section-4.3', 'rfc7231#section-5.1']}],
  // --- Conditionals ---
  [HttpConstHeader.IF_MATCH.toLowerCase(), {links: ['rfc7232#section-3.1', 'rfc7231#section-5.2']}],
  [HttpConstHeader.IF_NONE_MATCH.toLowerCase(), {links: ['rfc7232#section-3.2', 'rfc7231#section-5.2']}],
  [HttpConstHeader.IF_MODIFIED_SINCE.toLowerCase(), {links: ['rfc7232#section-3.3', 'rfc7231#section-5.2']}],
  [HttpConstHeader.IF_UNMODIFIED_SINCE.toLowerCase(), {links: ['rfc7232#section-3.4', 'rfc7231#section-5.2']}],
  [HttpConstHeader.IF_RANGE.toLowerCase(), {links: ['rfc7233#section-3.2', 'rfc7231#section-5.2']}],
  // --- Content Negotiation ---
  [HttpConstHeader.ACCEPT.toLowerCase(), {links: ['rfc7231#section-5.3.2', 'rfc7231#section-5.3']}],
  [HttpConstHeader.ACCEPT_CHARSET.toLowerCase(), {links: ['rfc7231#section-5.3.3', 'rfc7231#section-5.3']}],
  [HttpConstHeader.ACCEPT_ENCODING.toLowerCase(), {links: ['rfc7231#section-5.3.4', 'rfc7231#section-5.3']}],
  [HttpConstHeader.ACCEPT_LANGUAGE.toLowerCase(), {links: ['rfc7231#section-5.3.5', 'https//tools.ietf.org/html/rfc7231#section-5.3']}],
  // --- Authentication Credentials ---
  [HttpConstHeader.AUTHORIZATION.toLowerCase(), {links: ['rfc7235#section-4.2', 'rfc7231#section-5.4']}],
  [HttpConstHeader.PROXY_AUTHORIZATION.toLowerCase(), {links: ['rfc7235#section-4.4', 'rfc7231#section-5.4']}],
  // --- Request Context ---
  [HttpConstHeader.FROM.toLowerCase(), {links: ['rfc7231#section-5.5.1', 'rfc7231#section-5.5']}],
  [HttpConstHeader.ORIGIN.toLowerCase(), {links: ['rfc6454#section-7', 'rfc6454#section-7']}],
  [HttpConstHeader.REFERER.toLowerCase(), {links: ['rfc7231#section-5.5.2', 'rfc7231#section-5.5']}],
  [HttpConstHeader.USER_AGENT.toLowerCase(), {links: ['rfc7231#section-5.5.3', 'rfc7231#section-5.5']}],
  // --- Header Field Registration ---
  [HttpConstHeader.CONNECTION.toLowerCase(), {links: ['rfc7230#section-6.1', 'rfc7230#section-6']}],
  [HttpConstHeader.PROXY_CONNECTION.toLowerCase(), {links: ['rfc7230#appendix-A.1.2', 'rfc7230#appendix-A']}],
  
  [HttpConstHeader.UPGRADE.toLowerCase(), {links: ['rfc7230#section-6.7', 'rfc7230#section-6.7']}],
  [HttpConstHeader.VIA.toLowerCase(), {links: ['rfc7230#section-5.7.1', 'rfc7230#section-5.7']}],
  // *** RESONSE HEADER FIELDS ***
  // --- Control Data ---
  [HttpConstHeader.AGE.toLowerCase(), {links: ['rfc7234#section-5.1', 'rfc7231#section-7.1']}],
  [HttpConstHeader.CACHE_CONTROL.toLowerCase(), {links: ['rfc7234#section-5.2', 'rfc7231#section-7.1']}],
  [HttpConstHeader.EXPIRES.toLowerCase(), {links: ['rfc7234#section-5.3', 'rfc7231#section-7.1']}],
  [HttpConstHeader.DATE.toLowerCase(), {links: ['rfc7231#section-7.1.1.2', 'rfc7231#section-7.1']}],
  [HttpConstHeader.LOCATION.toLowerCase(), {links: ['rfc7231#section-7.1.2', 'rfc7231#section-7.1']}],
  [HttpConstHeader.RETRY_AFTER.toLowerCase(), {links: ['rfc7231#section-7.1.3', 'rfc7231#section-7.1']}],
  [HttpConstHeader.VARY.toLowerCase(), {links: ['rfc7231#section-7.1.4', 'rfc7231#section-7.1']}],
  [HttpConstHeader.WARNING.toLowerCase(), {links: ['rfc7234#section-5.5', 'rfc7231#section-7.1']}],
  // --- Validator Header Fields ---
  [HttpConstHeader.ETAG.toLowerCase(), {links: ['rfc7232#section-2.3', 'rfc7231#section-7.2']}],
  [HttpConstHeader.LAST_MODIFIED.toLowerCase(), {links: ['rfc7232#section-2.2', 'rfc7231#section-7.2']}],
  // --- Authentication Challenges ---
  [HttpConstHeader.WWW_AUTHENTICATE.toLowerCase(), {links: ['rfc7235#section-4.1', 'rfc7231#section-7.3']}],
  [HttpConstHeader.PROXY_AUTHENTICATE.toLowerCase(), {links: ['rfc7235#section-4.3', 'rfc7231#section-7.3']}],
  // --- Response Context ---
  [HttpConstHeader.ACCEPT_RANGES.toLowerCase(), {links: ['rfc7233#section-2.3', 'rfc7231#section-7.4']}],
  [HttpConstHeader.ALLOW.toLowerCase(), {links: ['rfc7231#section-7.4.1', 'rfc7231#section-7.4']}],
  [HttpConstHeader.SERVER.toLowerCase(), {links: ['rfc7231#section-7.4.2', 'rfc7231#section-7.4']}],
]);


module.exports = HttpConstHeader;
