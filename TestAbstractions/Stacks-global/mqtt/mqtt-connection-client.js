
'use strict';

const StackApi = require('stack-api');
const MqttConnectionClientOptions = require('./mqtt-connection-client-options');
const MqttEncoder = require('./mqtt-encoder');
const MqttDecoder = require('./mqtt-decoder');


class MqttConnectionClientConnectionClient extends StackApi.ConnectionClient {
  constructor(id, type, actor, connectionOptions, shareMessageSelector, connectionWorker) {
    super(id, type, 'mqtt', actor, StackApi.NetworkType.TCP, connectionOptions, MqttConnectionClientOptions, shareMessageSelector, connectionWorker);
  }
  
  send(msg) {
    this.sendMessage(new MqttEncoder(msg));
  }
  
  receive() {
    this.receiveMessage(new MqttDecoder());
  } 
}

module.exports = MqttConnectionClientConnectionClient;
