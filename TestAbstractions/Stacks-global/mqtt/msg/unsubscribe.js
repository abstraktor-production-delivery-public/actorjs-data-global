
'use strict';

const MqttMsg = require('../mqtt-msg');
const MqttConst = require('../mqtt-const');


class Unsubscribe extends MqttMsg {
  constructor(flags = 0b0010) {
    super(MqttConst.UNSUBSCRIBE, flags);
  }
}

module.exports = Unsubscribe;
