
'use strict';

const MqttMsg = require('../mqtt-msg');
const MqttConst = require('../mqtt-const');


class Disconnect extends MqttMsg {
  constructor(flags = 0b0000) {
    super(MqttConst.DISCONNECT, flags);
  }
}

module.exports = Disconnect;
