
'use strict';

const MqttMsg = require('../mqtt-msg');
const MqttConst = require('../mqtt-const');


class Auth extends MqttMsg {
  constructor(flags = 0b0000) {
    super(MqttConst.AUTH, flags);
  }
}

module.exports = Auth;
