
'use strict';

const StackApi = require('stack-api');
const MqttConnectionServerOptions = require('./mqtt-connection-server-options');
const MqttEncoder = require('./mqtt-encoder');
const MqttDecoder = require('./mqtt-decoder');


class MqttConnectionServerConnectionServer extends StackApi.ConnectionServer {
  constructor(id, type, actor, connectionOptions, shareMessageSelector) {
    super(id, type, 'mqtt', actor, StackApi.NetworkType.TCP, connectionOptions, MqttConnectionServerOptions, shareMessageSelector);
  }
  
  receive() {
    this.receiveMessage(new MqttDecoder());
  }
  
  send(msg) {
    this.sendMessage(new MqttEncoder(msg));
  }
}

module.exports = MqttConnectionServerConnectionServer;
