
'use strict';

const StackApi = require('stack-api');
const MqttInnerLog = require('./mqtt-inner-log');


class MqttEncoderEncoder extends StackApi.Encoder {
  constructor(msg, command = MqttEncoderEncoder.SEND_ALL) {
    super();
    this.msg = msg;
    this.offset = 0;
    this.buffer = null;
    this.buffers = [];
  }
  
  *encode() {
    switch(this.command) {
      case this.SEND_ALL:
        yield* this._sendAll();
        break;
      default:
        throw new Error('NOT IMPLEMENTD');
        break;
    };
  }
  
  *_sendAll() {
    const remainingLength = this._getVariableHeaderLength() + this._getPayloadLength();
    const remainingLengthSize = this._getRemainingLengthSize(remainingLength);
    const size = this._getSize(remainingLengthSize, remainingLength);
    this._createBuffer(size);
    
    this.offset = this.buffer.writeUInt8(this.controlPackageType << 4 + this.flags, this.offset);
    this._encodeRemainingLength(remainingLength).forEach((buf) => {
      this.offset = this.buffer.writeUInt8(buf, this.offset);
    });
    
    while(0 !== this.buffers.length) {
      yield* this.send(this.buffers.shift());
    }
    
    if(this.isLogIp) {
      this.addLog(MqttInnerLog.createFixedHeader(this.msg));
      //this.addLog(DiameterInnerLog.createHeader(this.msg));
      //this.addLog(DiameterInnerLog.createAvps(this.msg.avps));
      //this.setCaption(DiameterInnerLog.createCaption(this.msg, this.diameterExternalConstCommandCode));
      this.logMessage();
    }
  }
  
  encodeVariableHeader() {
    
  }
  
  encodePayload() {
    
  }
  
  _createBuffer(size) {
    if(0 !== size) {
      this.offset = 0;
      this.buffer = Buffer.allocUnsafe(size);
      this.buffers.push(this.buffer);
      return size;
    }
    else {
      return 0;
    }
  }
  
  _getRemainingLengthSize(remainingLength) {
    if(127 >= remainingLength) {
      return 1;
    }
    else if(16383 >= remainingLength) {
      return 2;
    }
    else if(2097151 >= remainingLength) {
      return 3;
    }
    else if(268435455 >= remainingLength) {
      return 4;
    }
    else {
      // throw
    }
  }
  
  _encodeRemainingLength(remainingLength) {
    const buffers = [];
    do {
      let encodedByte = remainingLength % 128;
      remainingLength = Math.floor(remainingLength / 128);
      if(remainingLength > 0) {
        encodedByte = encodedByte | 128;
      }
      buffers.push(encodedByte);
    } while(remainingLength > 0);
    return buffers;
  }
  
  _getSize(remainingLengthSize, remainingLength) {
    return 1 + remainingLengthSize + remainingLength;
  }
  
  _getVariableHeaderLength() {
    const msg = this.msg;
    return 0;
  }
  
  _getPayloadLength() {
    const msg = this.msg;
    return 0;
  }
}

MqttEncoderEncoder.SEND_ALL = 0;


module.exports = MqttEncoderEncoder;
