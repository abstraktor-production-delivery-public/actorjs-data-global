
'use strict';


class IcapMsg {
  constructor() {
    this.headers = new Map();
    this.httpRequest = null;
    this.httpResponse = null;
  }
  
  addHeader(name, value) {
    if(undefined !== value) {
      let convertedValue = value;
      if(typeof value === 'string') {}
      else if(typeof value === 'number') {
        convertedValue = '' + value;
      }
      else if(typeof value === 'boolean') {
        convertedValue = value ? 'true' : 'false';
      }
      else {
        throw new Error('Not Allowed Header value type');
      }
      if(!this.headers.has(name)) {
        this.headers.set(name, []);
      }
      this.headers.get(name).push(convertedValue);
    }
  }
  
  hasHeader(name) {
    return this.headers.has(name);
  }
  
  getHeader(name) {
    const headers = this.headers.get(name);
    if(undefined !== headers) {
      return headers[0];
    }
  }
  
  addHttpRequest(httpRequest) {
    this.httpRequest = httpRequest;
  }
  
  getHttpRequest() {
    return this.httpRequest;
  }
  
  hasHttpRequest() {
    return !!this.httpRequest;
  }
  
  hasHttpRequestBody() {
    if(this.httpRequest) {
      return this.httpRequest.hasBody();
    }
    else {
      return false;
    }
  }
  
  addHttpResponse(httpResponse) {
    this.httpResponse = httpResponse;
  }
  
  getHttpResponse() {
    return this.httpResponse;
  }
  
  hasHttpResponse() {
    return !!this.httpResponse;
  }

  hasHttpResponseBody() {
    if(this.httpResponse) {
      return this.httpResponse.hasBody();
    }
    else {
      return false;
    }
  }
}

module.exports = IcapMsg;
