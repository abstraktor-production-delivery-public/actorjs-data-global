
'use strict';

const StackApi = require('stack-api');
const IcapConst = require('./icap-const');
const IcapConstHeader = require('./icap-const-header');
const IcapInnerLog = require('./icap-inner-log');
const HttpDecoderRequest = require('../http/http-decoder-request');
const HttpDecoderResponse = require('../http/http-decoder-response');
const HttpDecoder = require('../http/http-decoder');


class IcapDecoder extends StackApi.Decoder {
  constructor(msg, command) {
    super();
    this.msg = msg;
    this.command = command;
    this.content = null;
    this.contentLength = -1;
    this.contentLengthToRead = -1;
  }
  
  *decode() {
    switch(this.command) {
      case IcapDecoder.RECEIVE_ALL:
        yield* this._receiveAll();
    }
    return this.msg;
  }
  
  *decodeHeaders() {
    while(this._parseHeaderLine(yield* this.receiveLine()));
    if(this.isLogIp) {
      this.addLog(IcapInnerLog.createHeaders(this.msg));
    }
  }
  
  _getRequest(encapsulatedObjects) {
    const o1 = encapsulatedObjects[0];
    if(!o1 || 'req-hdr' !== o1.name) {
      return null;
    }
    else {
      const request = {
        headerStart: o1.start,
        headerEnd: -1,
        bodyStart: -1,
        bodyEnd: -1
      };
      const o2 = encapsulatedObjects[1];
      if(!o2) {
        console.log('ERROR');
        return null;
      }
      if('null-body' !== o2.name || 'res-hdr' !== o2.name) {
        request.headerEnd = o2.start;
      }
      else if('res-body' !== o2.name) {
        request.headerEnd = o2.start;
        request.bodyStart = o2.start;
      }
      else {
        console.log('ERROR');
      }
      return request;
    }
  }
  
  _getResponse(encapsulatedObjects) {
    let foundIndex = -1;
    for(let i = 0; i < encapsulatedObjects.length; ++i) {
      if('res-hdr' === encapsulatedObjects[i].name) {
        foundIndex = i;
        break;
      }
    }
    if(-1 === foundIndex) {
      return null;
    }
    const response = {
      headerStart: encapsulatedObjects[foundIndex].start,
      headerEnd: -1,
      bodyStart: -1,
      bodyEnd: -1
    };
    const o2 = encapsulatedObjects[foundIndex + 1];
    if(!o2) {
      console.log('ERROR');
      return null;
    }
    if('null-body' === o2.name) {
      response.headerEnd = o2.start;
    }
    else if('res-body' === o2.name) {
      response.headerEnd = o2.start;
      response.bodyStart = o2.start;
    }
    return response;
  }
  
  _innerHttpLogMessage(coder, text, caption) {
    const bodyLog = IcapInnerLog.createInnerHttp(text);
    bodyLog.add(coder.ipLog.innerLogs);
    this.ipLog.addLog(bodyLog);
    this.ipLog.setCaption(caption);
    coder.clearLog();
    this.logMessage();

  }
  
  *decodeBody() {
    const msg = this.msg;
    const encapsulated = msg.getHeader(IcapConstHeader.ENCAPSULATED);
    if(encapsulated) {
      const encapsulatedDatas = encapsulated.split(', ');
      if(0 === encapsulatedDatas.length) {
        return;
      }
      const encapsulatedObjects = [];
      encapsulatedDatas.forEach((encapsulatedData) => {
        const data = encapsulatedData.split('=');
        encapsulatedObjects.push({
          name: data[0],
          start: data[1]
        });
      });
      const request = this._getRequest(encapsulatedObjects);
      const response = this._getResponse(encapsulatedObjects);    
      if(request) {
        const httpDecoderRequest = new HttpDecoderRequest(HttpDecoder.RECEIVE_FIRST_LINE_AND_HEADERS);
        httpDecoderRequest.setConnection(this.connection, this.isLogIp, this.pendingContext);
        if(this.isLogIp) {
          httpDecoderRequest.logMessage = () => {
            this._innerHttpLogMessage(httpDecoderRequest, 'http request', `${this.caption} - req`);
          };
        }
        yield* httpDecoderRequest.decode();
        this.msg.addHttpRequest(httpDecoderRequest.msg);
        if(-1 !== request.bodyStart) {
          httpDecoderRequest.command = HttpDecoder.RECEIVE_BODY_CHUNK;
          yield* httpDecoderRequest.decode();
        }
      }
      
      if(response) {
        const httpDecoderResponse = new HttpDecoderResponse(HttpDecoder.RECEIVE_FIRST_LINE_AND_HEADERS);
        httpDecoderResponse.setConnection(this.connection, this.isLogIp, this.pendingContext);
        if(this.isLogIp) {
          httpDecoderResponse.logMessage = () => {
            this._innerHttpLogMessage(httpDecoderResponse, 'http response', `${this.caption} - res`);
          };
        }
        yield* httpDecoderResponse.decode();
        this.msg.addHttpResponse(httpDecoderResponse.msg);
        if(-1 !== response.bodyStart) {
          httpDecoderResponse.command = HttpDecoder.RECEIVE_BODY_CHUNK;
          yield* httpDecoderResponse.decode();
        }
      }
    }
  }
  
  _parseHeaderLine(headerLine) {
    if(0 !== headerLine.length) {
      let parameters = headerLine.split(IcapConst.COLON_SP);
      this.msg.addHeader(...parameters);
      return true;
    }
    else {
      return false;
    }
  }
  
  _setContentLength(contentLength) {
    this.contentLength = contentLength;
    this.contentLengthToRead = contentLength;
  }
  
  _getNextRead() {
    const nextRead = Math.min(this.contentLengthToRead, IcapConst.MAX_CONTENT_CHUNK_SIZE);
    this.contentLengthToRead -= nextRead;
    return nextRead;
  }
  
  _parseBody(buffer) {
    this.content.addBuffer(buffer);
    return this.contentLengthToRead > 0; 
  }
  
  *_receiveAll() {
    yield* this.decodeFirstLine();
    yield* this.decodeHeaders();
    if(this.isLogIp) {
      this.logMessage();
    }
    yield* this.decodeBody();
    return this.msg;
  }
}

IcapDecoder.RECEIVE_ALL = 0;


module.exports = IcapDecoder;
