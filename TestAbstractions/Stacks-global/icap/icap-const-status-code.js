
'use strict';


class IcapConstStatusCode {}

IcapConstStatusCode.Continue = 100;
IcapConstStatusCode.SwitchingProtocols = 101;

IcapConstStatusCode.OK = 200;
IcapConstStatusCode.Created = 201;
IcapConstStatusCode.Accepted = 202;
IcapConstStatusCode.NonAuthoritativeInformation = 203;
IcapConstStatusCode.NoContent = 204;


IcapConstStatusCode.DATA_STATUS_CODE = {links: ['rfc7231#section-6']};
IcapConstStatusCode.DATA = new Map([
  [IcapConstStatusCode.Continue, {links: ['rfc7231#section-6.2.1', 'rfc7231#section-6']}],
  [IcapConstStatusCode.SwitchingProtocols, {links: ['rfc7231#section-6.2.2', 'rfc7231#section-6']}],

  [IcapConstStatusCode.OK, {links: ['rfc7231#section-6.3.1', 'rfc7231#section-6']}],
  [IcapConstStatusCode.Created, {links: ['rfc7231#section-6.3.2', 'rfc7231#section-6']}],
  [IcapConstStatusCode.Accepted, {links: ['rfc7231#section-6.3.3', 'rfc7231#section-6']}],
  [IcapConstStatusCode.NonAuthoritativeInformation, {links: ['rfc7231#section-6.3.4', 'rfc7231#section-6']}],
  [IcapConstStatusCode.NoContent, {links: ['rfc7231#section-6.3.5', 'rfc7231#section-6']}]
]);
  

module.exports = IcapConstStatusCode;
