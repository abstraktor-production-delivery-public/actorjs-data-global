
'use strict';

const StackApi = require('stack-api');
const IcapEncoder = require('./icap-encoder');
const IcapConst = require('./icap-const');
const IcapInnerLog = require('./icap-inner-log');


class IcapEncoderResponse extends IcapEncoder {
  constructor(msg, command = IcapEncoder.SEND_ALL) {
    super(msg, command);
    this.caption = '';
  }
  
  encodeFirstLine() {
    const msg = this.msg;
    this.offset += this.buffer.write(`${msg.icapVersion} ${msg.statusCodeString} ${msg.reasonPhrase}${IcapConst.CR_CL}`, this.offset);
    if(this.isLogIp) {
      const innerLogIcapResponse = IcapInnerLog.createStatusLine(this.msg);
      if(innerLogIcapResponse.logParts[0]) {
        const log = innerLogIcapResponse.logParts[0].text;
        this.caption = log.substring(log.indexOf(' ') + 1);
        this.setCaption(this.caption);
      }
      this.addLog(innerLogIcapResponse);
    }
  }
    
  getFirstLineLength() {
    const msg = this.msg;
    return msg.icapVersion.length + msg.statusCodeString.length + msg.reasonPhrase.length + IcapConst.FIRST_LINE;
  }
}


module.exports = IcapEncoderResponse;
