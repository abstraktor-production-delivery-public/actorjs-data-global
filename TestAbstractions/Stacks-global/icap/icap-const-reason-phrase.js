
'use strict';

const IcapConstStatusCode = require('./icap-const-status-code');


class IcapConstReasonPhrase {}

IcapConstReasonPhrase.Continue = 'Continue';
IcapConstReasonPhrase.SwitchingProtocols = 'Switching Protocols';

IcapConstReasonPhrase.OK = 'OK';
IcapConstReasonPhrase.Created = 'Created';
IcapConstReasonPhrase.Accepted = 'Accepted';
IcapConstReasonPhrase.NonAuthoritativeInformation = 'Non-Authoritative Information';
IcapConstReasonPhrase.NoContent = 'No Content';

IcapConstReasonPhrase.DATA_REASON_PHRASE = {links: ['rfc7231#section-6']};
IcapConstReasonPhrase.DATA = new Map([
  [IcapConstStatusCode.Continue, {links: ['rfc7231#section-6.2.1', 'rfc7231#section-6']}],
  [IcapConstStatusCode.SwitchingProtocols, {links: ['rfc7231#section-6.2.2', 'rfc7231#section-6']}],

  [IcapConstStatusCode.OK, {links: ['rfc7231#section-6.3.1', 'rfc7231#section-6']}],
  [IcapConstStatusCode.Created, {links: ['rfc7231#section-6.3.2', 'rfc7231#section-6']}],
  [IcapConstStatusCode.Accepted, {links: ['rfc7231#section-6.3.3', 'rfc7231#section-6']}],
  [IcapConstStatusCode.NonAuthoritativeInformation, {links: ['rfc7231#section-6.3.4', 'rfc7231#section-6']}],
  [IcapConstStatusCode.NoContent, {links: ['rfc7231#section-6.3.5', 'rfc7231#section-6']}],
]);


module.exports = IcapConstReasonPhrase;
