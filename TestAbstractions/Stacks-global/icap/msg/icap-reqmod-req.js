
'use strict';

const Request = require('../icap-msg-request');
const IcapConstMethod = require('../icap-const-method');
const IcapConstHeader = require('../icap-const-header');
const IcapConstVersion = require('../icap-const-version');


class IcapReqmodReq extends Request {
  constructor(requestTarget, headers) {
    super(IcapConstMethod.REQMOD, requestTarget, IcapConstVersion.HTTP_1_1);
    if(headers) {
      headers.forEach((header) => {
        this.addHeader(header[0], header[1]);
      });
    }
  }
}

module.exports = IcapReqmodReq;
