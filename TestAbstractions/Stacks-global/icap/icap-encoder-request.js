
'use strict';

const StackApi = require('stack-api');
const IcapEncoder = require('./icap-encoder');
const IcapConst = require('./icap-const');
const IcapInnerLog = require('./icap-inner-log');


class IcapEncoderRequest extends IcapEncoder {
  constructor(msg, command = IcapEncoder.SEND_ALL) {
    super(msg, command);
    this.caption = '';
  }
  
  encodeFirstLine() {
    const msg = this.msg;
    this.offset += this.buffer.write(`${msg.method} ${msg.requestTarget} ${msg.icapVersion}${IcapConst.CR_CL}`, this.offset);
    if(this.isLogIp) {
      const innerLogIcapRequest = IcapInnerLog.createRequestLine(this.msg);
      if(innerLogIcapRequest.logParts[0]) {
        const log = innerLogIcapRequest.logParts[0].text;
        this.caption = log.substring(0, log.lastIndexOf(' '));
        this.setCaption(this.caption);
      }
      this.addLog(innerLogIcapRequest);
    }
  }
  
  getFirstLineLength() {
    const msg = this.msg;
    return msg.method.length + msg.requestTarget.length + msg.icapVersion.length + IcapConst.FIRST_LINE;
  }
}

module.exports = IcapEncoderRequest;
