
'use strict';

const IcapMsg = require('./icap-msg');


class IcapMsgRequest extends IcapMsg {
  constructor(method, requestTarget, icapVersion) {
    super();
    this.method = method;
    this.requestTarget = requestTarget;
    this.icapVersion = icapVersion;
  }
 
  addRequestLine(method, requestTarget, icapVersion) {
    this.method = method;
    this.requestTarget = requestTarget;
    this.icapVersion = icapVersion;
  }
}

module.exports = IcapMsgRequest;
