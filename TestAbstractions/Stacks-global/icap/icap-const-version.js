
'use strict';


class IcapConstVersion {}

IcapConstVersion.ICAP_1_0 = 'ICAP/1.0',

IcapConstVersion.ICAP_VERSION_DATA = {links: ['rfc7230#section-2.6']};


module.exports = IcapConstVersion;
