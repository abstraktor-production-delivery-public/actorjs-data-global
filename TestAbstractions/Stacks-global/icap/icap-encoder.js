
'use strict';

const StackApi = require('stack-api');
const IcapConst = require('./icap-const');
const IcapConstHeader = require('./icap-const-header');
const IcapInnerLog = require('./icap-inner-log');
const HttpEncoderRequest = require('../http/http-encoder-request');
const HttpEncoderResponse = require('../http/http-encoder-response');
const HttpEncoder = require('../http/http-encoder');


class IcapEncoder extends StackApi.Encoder {
  constructor(msg, command) {
    super();
    this.msg = msg;
    this.command = command;
    this.offset = 0;
    this.buffer = null;
    this.buffers = [];
    this.bodySize = 0;
  }
  
  setCommand(command, options) {
    this.command = command;
    this.options = options;
    return this;
  }
  
  *encode() {
    switch(this.command) {
      case IcapEncoder.SEND_ALL:
        yield* this._sendAll();
        break;
    }
  }
  
  encodeHeaders() {
    const msg = this.msg;
    msg.headers.forEach((values, name) => {
      values.forEach((value) => {
        this.offset += this.buffer.write(name, this.offset);
        this.offset += this.buffer.write(': ', this.offset);
        this.offset += this.buffer.write(value, this.offset);
        this.offset += this.buffer.write(IcapConst.CR_CL, this.offset);
      });
    });
    this.offset += this.buffer.write(IcapConst.CR_CL, this.offset);
    if(this.isLogIp) {
      this.ipLog.addLog(IcapInnerLog.createHeaders(this.msg));
    }
  }
  
  _innerHttpLogMessage(coder, text, caption) {
    const bodyLog = IcapInnerLog.createInnerHttp(text);
    bodyLog.add(coder.ipLog.innerLogs);
    this.ipLog.addLog(bodyLog);
    this.ipLog.setCaption(caption);
    coder.clearLog();
    this.logMessage();
  }
  
  getHeadersLength() {
    let length = 0;
    const msg = this.msg;
    msg.headers.forEach((values, name) => {
      values.forEach((value) => {
        length += name.length + value.length + IcapConst.HEADER_LINE;
      });
    });
    length += IcapConst.CR_CL_LENGTH;
    return length;
  }
    
  *_sendAll() {
    const msg = this.msg;
    let httpEncoderRequest = null;
    let bodySize = 0;
    if(msg.httpRequest) {
      httpEncoderRequest = new HttpEncoderRequest(msg.httpRequest, HttpEncoder.SEND_FIRST_LINE_AND_HEADERS);
      httpEncoderRequest.setConnection(this.connection, this.isLogIp, this.pendingContext);
      if(this.isLogIp) {
        httpEncoderRequest.logMessage = () => {
          this._innerHttpLogMessage(httpEncoderRequest, 'http request', `${this.caption} - req`);
        };
      }
    }
    let httpEncoderResponse = null;
    if(msg.httpResponse) {
      httpEncoderResponse = new HttpEncoderResponse(msg.httpResponse, HttpEncoder.SEND_FIRST_LINE_AND_HEADERS);
      httpEncoderResponse.setConnection(this.connection, this.isLogIp, this.pendingContext);
      let resType = 'res';
      if(this.isLogIp) {
        httpEncoderResponse.logMessage = () => {
          this._innerHttpLogMessage(httpEncoderResponse, 'http response', `${this.caption} - ${resType}`);
          resType = 'res-body';
        };
      }
    }
    if(!msg.hasHeader(IcapConstHeader.ENCAPSULATED)) {
      let encapuladed = '';
      let offset = 0;
      let reqHeadersLength = 0;
      let reqBodyLength = 0;
      let resHeadersLength = 0;
      let resBodyLength = 0;
      if(httpEncoderRequest) {
        reqHeadersLength = httpEncoderRequest.getHeadersLength();
        reqBodyLength = httpEncoderRequest.getBodyLength();
      }
      if(httpEncoderResponse) {
        resHeadersLength = httpEncoderResponse.getHeadersLength();
        resBodyLength = httpEncoderResponse.getBodyLength();
      }
      if(0 !== reqHeadersLength) {
        encapuladed += `req-hdr=${offset}`;
        offset += reqHeadersLength;
        if(0 === reqBodyLength) {
          if(0 === resHeadersLength) {
            encapuladed += `, null-body=${offset}`;
          }
        }
        else {
          encapuladed += `, req-body=${offset}`;
          offset += reqBodyLength;
        }
      }
      if(0 !== resHeadersLength) {
        encapuladed += `${encapuladed ? ', ' : ''}res-hdr=${offset}`;
        offset += resHeadersLength;
        if(0 !== resBodyLength) {
          encapuladed += `, res-body=${offset}`;
        }
        else {
          encapuladed += `, null-body=${offset}`;
        }
      }
      if(encapuladed) {
        msg.addHeader(IcapConstHeader.ENCAPSULATED, encapuladed);
      }
      bodySize = reqHeadersLength + reqBodyLength + resHeadersLength + resBodyLength;
    }
    if(0 !== this._createBuffer(this.getFirstLineLength() + this.getHeadersLength())) {
      this.encodeFirstLine();
      this.encodeHeaders();
      while(0 !== this.buffers.length) {
        yield* this.send(this.buffers.shift());
      }
      if(this.isLogIp) {
        this.logMessage();
      }
      if(0 !== bodySize) {
        if(httpEncoderRequest) {
          yield* httpEncoderRequest.encode();
          if(msg.hasHttpRequestBody()) {
            httpEncoderRequest.command = HttpEncoder.SEND_BODY_CHUNK;
            yield* httpEncoderRequest.encode();
          }
        }
        if(httpEncoderResponse) {
          yield* httpEncoderResponse.encode();
          if(msg.hasHttpResponseBody()) {
            httpEncoderResponse.command = HttpEncoder.SEND_BODY_CHUNK;
            yield* httpEncoderResponse.encode();
          }
        }
      }
    }
  }
  
  _createBuffer(size) {
    if(0 !== size) {
      this.offset = 0;
      this.buffer = Buffer.allocUnsafe(size);
      this.buffers.push(this.buffer);
      return size;
    }
    else {
      return 0;
    }
  }
}


IcapEncoder.SEND_ALL = 0;


module.exports = IcapEncoder;
