
'use strict';

const IcapMsg = require('./icap-msg');


class IcapMsgResponse extends IcapMsg {
  constructor(icapVersion, statusCode, reasonPhrase) {
    super();
    this.icapVersion = icapVersion;
    this.statusCode = 0
    this.statusCodeString = '';
    this.reasonPhrase = reasonPhrase;
    this._updateStatusCode(statusCode);
  }
  
  addStatusLine(icapVersion, statusCode, reasonPhrase) {
    this.icapVersion = icapVersion;
    this._updateStatusCode(statusCode);
    this.reasonPhrase = reasonPhrase;
  }
  
  _updateStatusCode(statusCode) {
    if(typeof statusCode === "string") {
      this.statusCode = Number.parseInt(statusCode);
      this.statusCodeString = statusCode;
    }
    else if(typeof statusCode === "number") {
      this.statusCode = statusCode;
      this.statusCodeString = "" + statusCode;
    }
  }
}

module.exports = IcapMsgResponse;
