
'use strict';


class IcapConst {}

IcapConst.CR_CL = '\r\n';
IcapConst.SP = ' ';
IcapConst.COLON_SP = ': ';
IcapConst.COMMA_SP = ': ';

IcapConst.CR_CL_LENGTH = IcapConst.CR_CL.length;
IcapConst.SP_LENGTH = IcapConst.SP.length;
IcapConst.COLON_SP_LENGTH = IcapConst.COLON_SP.length;
IcapConst.COMMA_SP_LENGTH = IcapConst.COMMA_SP.length;

IcapConst.FIRST_LINE = IcapConst.SP_LENGTH + IcapConst.SP_LENGTH + IcapConst.CR_CL_LENGTH;
IcapConst.HEADER_LINE = IcapConst.COLON_SP_LENGTH + IcapConst.CR_CL_LENGTH;


IcapConst.REQUEST_LINE_HREF = "https://tools.ietf.org/html/rfc7230#section-3.1.1";
IcapConst.DOCUMENTATION_LINK_ROOT = 'https://datatracker.ietf.org/doc/html/';

IcapConst.MAX_CONTENT_CHUNK_SIZE = 16384;


module.exports = IcapConst;
