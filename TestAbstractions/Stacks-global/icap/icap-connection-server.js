
'use strict';

const StackApi = require('stack-api');
const IcapConnectionServerOptions = require('./icap-connection-server-options');
const IcapDecoderRequest = require('./icap-decoder-request');
const IcapEncoderResponse = require('./icap-encoder-response');


class IcapConnectionServer extends StackApi.ConnectionServer {
  constructor(id, type, actor, options) {
    super(id, type, 'icap', actor, StackApi.NetworkType.TCP, IcapConnectionServerOptions, options);
  }

  receive() {
    this.receiveMessage(new IcapDecoderRequest());
  }

  send(msg) {
    this.sendMessage(new IcapEncoderResponse(msg));
  }
}

module.exports = IcapConnectionServer;
