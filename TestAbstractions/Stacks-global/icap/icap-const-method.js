
'use strict';


class IcapConstMethod {}

IcapConstMethod.REQMOD = 'REQMOD';
IcapConstMethod.RESPMOD = 'RESPMOD';
IcapConstMethod.OPTIONS = 'OPTIONS';

IcapConstMethod.METHOD_DATA = new Map([
  [IcapConstMethod.REQMOD, {links: ['rfc3507#section-4.8']}],
  [IcapConstMethod.RESPMOD, {links: ['rfc3507#section-4.9']}],
  [IcapConstMethod.OPTIONS, {links: ['rfc3507#section-4.10']}]
]);

IcapConstMethod.METHOD = {links: ['rfc7231#section-4']};
IcapConstMethod.REQUEST_TARGET = {links: ['rfc7230#section-5.3']};


module.exports = IcapConstMethod;
