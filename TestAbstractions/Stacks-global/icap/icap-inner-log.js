
'use strict';

const StackApi = require('stack-api');
const IcapConst = require('./icap-const');
const IcapConstMethod = require('./icap-const-method');
const IcapConstHeader = require('./icap-const-header');
const IcapConstVersion = require('./icap-const-version');
const IcapConstReasonPhrase = require('./icap-const-reason-phrase');
const IcapConstStatusCode = require('./icap-const-status-code');


class IcapInnerLog {
  static createRequestLine(msg) {
    const methodLog = [];
    methodLog.push(new StackApi.LogPartRef('method', `${IcapConst.DOCUMENTATION_LINK_ROOT}${IcapConstMethod.METHOD.links[0]}`));
    const methodData = IcapConstMethod.METHOD_DATA.get(msg.method);
    if(methodData) {
      methodLog.push(new StackApi.LogPartText(': '));
      methodLog.push(new StackApi.LogPartRef(msg.method, `${IcapConst.DOCUMENTATION_LINK_ROOT}${methodData.links[0]}`));
    }
    else {
      methodLog.push(new StackApi.LogPartText(`: ${msg.method}`));
    }
    
    const requestTargetLog = [];
    requestTargetLog.push(new StackApi.LogPartRef('request-target', `${IcapConst.DOCUMENTATION_LINK_ROOT}${IcapConstMethod.REQUEST_TARGET.links[0]}`));
    requestTargetLog.push(new StackApi.LogPartText(`: ${msg.requestTarget}`));

    const icapVersionLog = [];
    icapVersionLog.push(new StackApi.LogPartRef('ICAP-version', `${IcapConst.DOCUMENTATION_LINK_ROOT}${IcapConstVersion.ICAP_VERSION_DATA.links[0]}`));
    icapVersionLog.push(new StackApi.LogPartText(`: ${msg.icapVersion}`));

    return new StackApi.LogInner(`${msg.method} ${msg.requestTarget} ${msg.icapVersion}`, [new StackApi.LogInner(methodLog), new StackApi.LogInner(requestTargetLog), new StackApi.LogInner(icapVersionLog)], true);
  }
  
  static createStatusLine(msg) {
    const icapVersionLog = [];
    icapVersionLog.push(new StackApi.LogPartRef('ICAP-version', `${IcapConst.DOCUMENTATION_LINK_ROOT}${IcapConstVersion.ICAP_VERSION_DATA.links[0]}`));
    icapVersionLog.push(new StackApi.LogPartText(`: ${msg.icapVersion}`));

    const statusCodeLog = [];
    statusCodeLog.push(new StackApi.LogPartRef('status-code', `${IcapConst.DOCUMENTATION_LINK_ROOT}${IcapConstStatusCode.DATA_STATUS_CODE.links[0]}`));
    const statusCodeData = IcapConstStatusCode.DATA.get(msg.statusCode);
    if(statusCodeData) {
      statusCodeLog.push(new StackApi.LogPartText(': '));
      statusCodeLog.push(new StackApi.LogPartRef(msg.statusCode, `${IcapConst.DOCUMENTATION_LINK_ROOT}${statusCodeData.links[0]}`));
    }
    else {
      statusCodeLog.push(new StackApi.LogPartText(`: ${msg.statusCode}`));
    }
    
    const reasonPhraseLog = [];
    reasonPhraseLog.push(new StackApi.LogPartRef('reason-phrase', `${IcapConst.DOCUMENTATION_LINK_ROOT}${IcapConstReasonPhrase.DATA_REASON_PHRASE.links[0]}`));
    const reasonPhraseData = IcapConstReasonPhrase.DATA.get(msg.statusCode);
    if(reasonPhraseData) {
      reasonPhraseLog.push(new StackApi.LogPartText(': '));
      reasonPhraseLog.push(new StackApi.LogPartRef(msg.reasonPhrase, `${IcapConst.DOCUMENTATION_LINK_ROOT}${reasonPhraseData.links[0]}`));
    }
    else {
      reasonPhraseLog.push(new StackApi.LogPartText(`: ${msg.reasonPhrase}`));
    }

    return new StackApi.LogInner(`${msg.icapVersion} ${msg.statusCode} ${msg.reasonPhrase}`, [new StackApi.LogInner(icapVersionLog), new StackApi.LogInner(statusCodeLog), new StackApi.LogInner(reasonPhraseLog)], true);
  }
  
  static createHeaders(msg) {
    const headersInnerLog = new StackApi.LogInner('[headers]', [], true);
    msg.headers.forEach((values, name) => {
      values.forEach((value) => {
        const header = `${name}: ${value}`;
        const headerData = IcapConstHeader.HEADER_DATA.get(name);
        if(headerData) {
          const headerLog = [];
          headersInnerLog.add(new StackApi.LogInner(headerLog));
          if(headerData.links[0]) {
            headerLog.push(new StackApi.LogPartRef(name, `${IcapConst.DOCUMENTATION_LINK_ROOT}${headerData.links[0]}`));
          }
          else {
            headerLog.push(new StackApi.LogPartText(name));
          }
          headerLog.push(new StackApi.LogPartText(`: ${value}`));
        }
        else {
          headersInnerLog.add(new StackApi.LogInner(header));
        }
      });
    });
    return headersInnerLog;
  }
  
  static createInnerHttp(text) {
    return new StackApi.LogInner(`[body] - ${text}`);
  }
}

module.exports = IcapInnerLog;
