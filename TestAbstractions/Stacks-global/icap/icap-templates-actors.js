
'use strict';

const IcapOrigReqmod = require('./templates-actors/icap-orig-reqmod');
const IcapTermReqmod = require('./templates-actors/icap-term-reqmod');
const IcapOrigRespmod = require('./templates-actors/icap-orig-respmod');
const IcapTermRespmod = require('./templates-actors/icap-term-respmod');
const IcapRequestReqmod = require('./templates-actors/icap-request-reqmod');
const IcapRequestRespmod = require('./templates-actors/icap-request-respmod');
const IcapResponseReqmod = require('./templates-actors/icap-response-reqmod');
const IcapResponseRespmod = require('./templates-actors/icap-response-respmod');
const StackApi = require('stack-api');


class IcapTemplatesActors extends StackApi.StackComponentsTemplateBaseActors {
  constructor() {
    super([
      IcapOrigReqmod,
      IcapTermReqmod,
      IcapOrigRespmod,
      IcapTermRespmod,
      IcapRequestReqmod,
      IcapRequestRespmod,
      IcapResponseReqmod,
      IcapResponseRespmod
    ]);
  }
}


module.exports = IcapTemplatesActors;
