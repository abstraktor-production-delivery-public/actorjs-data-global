
'use strict';

const StackApi = require('stack-api');


class IcapTermRespmod extends StackApi.StackComponentsTemplatesActors {}

IcapTermRespmod.displayName = 'respmod';
IcapTermRespmod.type = 'term';
IcapTermRespmod.template = IcapTermRespmod._template`
'use strict';

const ActorApi = require('actor-api');
const IcapApi = require('icap-stack-api');


class ${'name'}${'actorEnding'} extends ActorApi.Actor${'actorType'} {
  constructor() {
    super();
    this.icapConnection = null;
  }
  
  *data() {
  }
    
  *initServer() {
    this.icapConnection = this.createServer('icap');
  }
  
  *run() {
    this.icapConnection.accept();
    
    const request = this.icapConnection.receive();
  }
  
  *exit(interrupted) {
    this.closeConnection(this.icapConnection);
  }
}

module.exports = ${'name'}${'actorEnding'};
`;

IcapTermRespmod.markupNodes = 2;
IcapTermRespmod.markup = `\`\`\`seq
Config(nodeWidth: 126, nodeMessageHeight: 17, nodeEventHeight: 10, widthBias: 23, heightBias: 30, lineNumbers: false, border: false, backgroundColor: transparent)

Title: Respmod icap
Nodes[client, term]
client -o term[icap]: connect
client => term[icap]: GET
term => client[icap]: 200 OK
client -x term[icap]: disconnect

\`\`\`
`;


module.exports = IcapTermRespmod;
