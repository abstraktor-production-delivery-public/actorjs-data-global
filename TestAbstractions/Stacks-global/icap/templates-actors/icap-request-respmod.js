
'use strict';

const StackApi = require('stack-api');


class IcapRequestRespmod extends StackApi.StackComponentsTemplatesActors {}

IcapRequestRespmod.displayName = 'respmod-request';
IcapRequestRespmod.type = 'msg';
IcapRequestRespmod.markupStyle = {
  fontSize: '10px',
  fontWeight: 'bold',
  overflow: 'hidden',
  whiteSpace: 'nowrap',
  padding: '2px'
};
IcapRequestRespmod.template = IcapRequestRespmod._template`
'use strict';

const IcapApi = require('icap-stack-api');


class ${'name'}${'actorEnding'} extends IcapApi.Request {
  constructor(requestTarget, host, httpRequest, httpResponse) {
    super(IcapApi.Method.REQMOD, requestTarget, IcapApi.Version.ICAP_1_0);
    this.addHeader(IcapApi.Header.HOST, host);
    if(httpRequest) {
      this.addHttpRequest(httpRequest);
    }
    if(httpResponse) {
      this.addHttpResponse(httpResponse);
    }
  }
}

module.exports = ${'name'}${'actorEnding'};
`;

IcapRequestRespmod.markupNodes = 2;
IcapRequestRespmod.markup = `

RESPMOD [requestTarget] ICAP/1.0
Host: [host]
(Add more headers on your own)

[body - httpRequest]
[body - httpResponse]
`;

module.exports = IcapRequestRespmod;
