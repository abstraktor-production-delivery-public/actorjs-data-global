
'use strict';

const StackApi = require('stack-api');


class IcapOrigReqmod extends StackApi.StackComponentsTemplatesActors {}

IcapOrigReqmod.displayName = 'reqmod';
IcapOrigReqmod.type = 'orig';
IcapOrigReqmod.template = IcapOrigReqmod._template`
'use strict';

const ActorApi = require('actor-api');
const IcapApi = require('icap-stack-api');


class ${'name'}${'actorEnding'} extends ActorApi.Actor${'actorType'} {
  constructor() {
    super();
    this.icapConnection = null;
    this.requistUri = 'www.example.com';
  }
  
  *data() {
    this.requistUri = this.getTestDataString('request-uri', this.requistUri);
  }
  
  *initClient() {
    this.icapConnection = this.createConnection('icap');
  }
  
  *run() {
    this.icapConnection.send(new IcapApi.GetHtmlReq(this.requistUri));
  }
  
  *exit(interrupted) {
    this.closeConnection(this.icapConnection);
  }
}

module.exports = ${'name'}${'actorEnding'};
`;

IcapOrigReqmod.markupNodes = 2;
IcapOrigReqmod.markup = `\`\`\`seq
Config(nodeWidth: 126, nodeMessageHeight: 17, nodeEventHeight: 10, widthBias: 23, heightBias: 30, lineNumbers: false, border: false, backgroundColor: transparent)

Title: Reqmod icap
Nodes[orig, server]
orig -o server[icap]: connect
orig => server[icap]: REQMOD
server => orig[icap]: 200 OK
orig -x server[icap]: disconnect
\`\`\`
`;


module.exports = IcapOrigReqmod;
