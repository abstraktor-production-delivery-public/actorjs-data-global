
'use strict';

const StackApi = require('stack-api');


class IcapOrigRespmod extends StackApi.StackComponentsTemplatesActors {}

IcapOrigRespmod.displayName = 'respmod';
IcapOrigRespmod.type = 'orig';
IcapOrigRespmod.template = IcapOrigRespmod._template`
'use strict';

const ActorApi = require('actor-api');
const IcapApi = require('icap-stack-api');


class ${'name'}${'actorEnding'} extends ActorApi.Actor${'actorType'} {
  constructor() {
    super();
    this.icapConnection = null;
    this.requistUri = 'www.example.com';
  }
  
  *data() {
    this.requistUri = this.getTestDataString('request-uri', this.requistUri);
  }
  
  *initClient() {
    this.icapConnection = this.createConnection('icap');
  }
  
  *run() {
    this.icapConnection.send(new IcapApi.RespmodReq(this.requistUri));
  }
  
  *exit(interrupted) {
    this.closeConnection(this.icapConnection);
  }
}

module.exports = ${'name'}${'actorEnding'};
`;

IcapOrigRespmod.markupNodes = 2;
IcapOrigRespmod.markup = `\`\`\`seq
Config(nodeWidth: 126, nodeMessageHeight: 17, nodeEventHeight: 10, widthBias: 23, heightBias: 30, lineNumbers: false, border: false, backgroundColor: transparent)

Title: Reapmod icap
Nodes[orig, server]
orig -o server[icap]: connect
orig => server[icap]: RESPMOD
server => orig[icap]: 200 OK
orig -x server[icap]: disconnect
\`\`\`
`;


module.exports = IcapOrigRespmod;
