
'use strict';

const StackApi = require('stack-api');


class IcapTermReqmod extends StackApi.StackComponentsTemplatesActors {}

IcapTermReqmod.displayName = 'reqmod';
IcapTermReqmod.type = 'term';
IcapTermReqmod.template = IcapTermReqmod._template`
'use strict';

const ActorApi = require('actor-api');
const IcapApi = require('icap-stack-api');


class ${'name'}${'actorEnding'} extends ActorApi.Actor${'actorType'} {
  constructor() {
    super();
    this.icapConnection = null;
  }
  
  *data() {
  }
    
  *initServer() {
    this.icapConnection = this.createServer('icap');
  }
  
  *run() {
    this.icapConnection.accept();
    
    const request = this.icapConnection.receive();
  }
  
  *exit(interrupted) {
    this.closeConnection(this.icapConnection);
  }
}

module.exports = ${'name'}${'actorEnding'};
`;

IcapTermReqmod.markupNodes = 2;
IcapTermReqmod.markup = `\`\`\`seq
Config(nodeWidth: 126, nodeMessageHeight: 17, nodeEventHeight: 10, widthBias: 23, heightBias: 30, lineNumbers: false, border: false, backgroundColor: transparent)

Title: Reqmod icap
Nodes[client, term]
client -o term[icap]: connect
client => term[icap]: GET
term => client[icap]: 200 OK
client -x term[icap]: disconnect

\`\`\`
`;


module.exports = IcapTermReqmod;
