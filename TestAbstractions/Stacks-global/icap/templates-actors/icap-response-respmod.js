
'use strict';

const StackApi = require('stack-api');


class IcapResponseRespmod extends StackApi.StackComponentsTemplatesActors {}

IcapResponseRespmod.displayName = 'respmod-response';
IcapResponseRespmod.type = 'msg';
IcapResponseRespmod.markupStyle = {
  fontSize: '10px',
  fontWeight: 'bold',
  overflow: 'hidden',
  whiteSpace: 'nowrap',
  padding: '2px'
};
IcapResponseRespmod.template = IcapResponseRespmod._template`
'use strict';

const IcapApi = require('icap-stack-api');


class ${'name'}${'actorEnding'} extends IcapApi.Response {
  constructor(httpResponse, statusCode=IcapApi.StatusCode.OK, reasonPhrase=IcapApi.ReasonPhrase.OK) {
    super(IcapApi.Version.ICAP_1_0, statusCode, reasonPhrase);
    if(httpResponse) {
      this.addHttpResponse(httpResponse);
    }
  }
}

module.exports = ${'name'}${'actorEnding'};
`;

IcapResponseRespmod.markupNodes = 2;
IcapResponseRespmod.markup = `

ICAP/1.0 200 OK
(Add more headers on your own)

[body - modified httpResponse]
`;

module.exports = IcapResponseRespmod;
