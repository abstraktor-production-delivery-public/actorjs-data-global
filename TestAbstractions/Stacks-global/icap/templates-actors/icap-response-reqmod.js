
'use strict';

const StackApi = require('stack-api');


class IcapResponseReqmod extends StackApi.StackComponentsTemplatesActors {}

IcapResponseReqmod.displayName = 'reqmod-response';
IcapResponseReqmod.type = 'msg';
IcapResponseReqmod.markupStyle = {
  fontSize: '10px',
  fontWeight: 'bold',
  overflow: 'hidden',
  whiteSpace: 'nowrap',
  padding: '2px'
};
IcapResponseReqmod.template = IcapResponseReqmod._template`
'use strict';

const IcapApi = require('icap-stack-api');


class ${'name'}${'actorEnding'} extends IcapApi.Response {
  constructor(httpRequest, statusCode=IcapApi.StatusCode.OK, reasonPhrase=IcapApi.ReasonPhrase.OK) {
    super(IcapApi.Version.ICAP_1_0, statusCode, reasonPhrase);
    if(httpRequest) {
      this.addHttpRequest(httpRequest);
    }
  }
}

module.exports = ${'name'}${'actorEnding'};
`;

IcapResponseReqmod.markupNodes = 2;
IcapResponseReqmod.markup = `

ICAP/1.0 200 OK
(Add more headers on your own)

[body - modified httpRequest]
`;

module.exports = IcapResponseReqmod;
