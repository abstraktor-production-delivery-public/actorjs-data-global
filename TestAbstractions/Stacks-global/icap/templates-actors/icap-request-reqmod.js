
'use strict';

const StackApi = require('stack-api');


class IcapRequestReqmod extends StackApi.StackComponentsTemplatesActors {}

IcapRequestReqmod.displayName = 'reqmod-request';
IcapRequestReqmod.type = 'msg';
IcapRequestReqmod.markupStyle = {
  fontSize: '10px',
  fontWeight: 'bold',
  overflow: 'hidden',
  whiteSpace: 'nowrap',
  padding: '2px'
};
IcapRequestReqmod.template = IcapRequestReqmod._template`
'use strict';

const IcapApi = require('icap-stack-api');


class ${'name'}${'actorEnding'} extends IcapApi.Request {
  constructor(requestTarget, host, httpRequest) {
    super(IcapApi.Method.REQMOD, requestTarget, IcapApi.Version.ICAP_1_0);
    this.addHeader(IcapApi.Header.HOST, host);
    if(httpRequest) {
      this.addHttpRequest(httpRequest);
    }
  }
}

module.exports = ${'name'}${'actorEnding'};
`;

IcapRequestReqmod.markupNodes = 2;
IcapRequestReqmod.markup = `

REQMOD [requestTarget] ICAP/1.0
Host: [host]
(Add more headers on your own)

[body - httpRequest]
`;

module.exports = IcapRequestReqmod;
