
'use strict';

const StackApi = require('stack-api');
const IcapConnectionClientOptions = require('./icap-connection-client-options');
const IcapEncoderRequest = require('./icap-encoder-request');
const IcapDecoderResponse = require('./icap-decoder-response');


class IcapConnectionClient extends StackApi.ConnectionClient {
  constructor(id, type, actor, options) {
    super(id, type, 'icap', actor, StackApi.NetworkType.TCP, IcapConnectionClientOptions, options);
  }

  send(msg) {
    this.sendMessage(new IcapEncoderRequest(msg));
  }
  
  receive() {
    this.receiveMessage(new IcapDecoderResponse());
  }
}

module.exports = IcapConnectionClient;
