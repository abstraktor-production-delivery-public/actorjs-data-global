
'use strict';


class IcapConstHeader {}

// --- General - Same as HTTP ---

IcapConstHeader.CACHE_CONTROL = 'Cache-Control';
IcapConstHeader.CONNECTION = 'Connection';
IcapConstHeader.DATE = 'Date';
IcapConstHeader.EXPIRES = 'Expires';
IcapConstHeader.PRAGMA = 'Pragma';
IcapConstHeader.TRAILER = 'Trailer';
IcapConstHeader.UPGRADE = 'Upgrade';

// --- Request - Same as HTTP ---
IcapConstHeader.AUTHORIZATION = 'Authorization';
IcapConstHeader.ALLOW = 'Allow';
IcapConstHeader.FROM = 'From';
IcapConstHeader.HOST = 'Host';
IcapConstHeader.REFERER = 'Referer';
IcapConstHeader.USER_AGENT = 'User-Agent';

// --- Request - ICAP specific ---
IcapConstHeader.PREVIEW = 'Preview';

// --- Response - ICAP specific ---
IcapConstHeader.SERVER = 'Server';
IcapConstHeader.IS_TAG = 'ISTag';
IcapConstHeader.ENCAPSULATED = 'Encapsulated';


IcapConstHeader.HEADER_DATA = new Map([
  // --- Same as HTTP ---
  [IcapConstHeader.CACHE_CONTROL, {links: ['rfc7231#section-3.1.1.5', 'rfc7231#section-3.1']}],
  [IcapConstHeader.CONNECTION, {links: ['rfc7231#section-3.1.2.2', 'rfc7231#section-3.1']}],
  [IcapConstHeader.DATE, {links: ['rfc7231#section-3.1.3.2', 'rfc7231#section-3.1']}],
  [IcapConstHeader.EXPIRES, {links: ['rfc7231#section-3.1.4.2', 'rfc7231#section-3.1']}],
  [IcapConstHeader.PRAGMA, {links: ['rfc7231#section-3.1.1.5', 'rfc7231#section-3.1']}],
  [IcapConstHeader.TRAILER, {links: ['rfc7231#section-3.1.2.2', 'rfc7231#section-3.1']}],
  [IcapConstHeader.UPGRADE, {links: ['rfc7231#section-3.1.3.2', 'rfc7231#section-3.1']}],
  // --- Request - Same as HTTP ---
  [IcapConstHeader.AUTHORIZATION, {links: ['rfc7231#section-3.1.1.5', 'rfc7231#section-3.1']}],
  [IcapConstHeader.ALLOW, {links: ['rfc7231#section-3.1.2.2', 'rfc7231#section-3.1']}],
  [IcapConstHeader.FROM, {links: ['rfc7231#section-3.1.3.2', 'rfc7231#section-3.1']}],
  [IcapConstHeader.HOST, {links: ['rfc7231#section-3.1.4.2', 'rfc7231#section-3.1']}],
  [IcapConstHeader.REFERER, {links: ['rfc7231#section-3.1.1.5', 'rfc7231#section-3.1']}],
  [IcapConstHeader.USER_AGENT, {links: ['rfc7231#section-3.1.2.2', 'rfc7231#section-3.1']}],
  // --- Request - ICAP specific ---
  [IcapConstHeader.PREVIEW, {links: ['rfc7231#section-3.1.2.2', 'rfc7231#section-3.1']}],
  // --- Response - ICAP specific ---
  [IcapConstHeader.SERVER, {links: ['rfc7231#section-3.1.4.2', 'rfc7231#section-3.1']}],
  [IcapConstHeader.IS_TAG, {links: ['rfc7231#section-3.1.1.5', 'rfc7231#section-3.1']}],
  [IcapConstHeader.ENCAPSULATED, {links: ['rfc7231#section-3.1.2.2', 'rfc7231#section-3.1']}]
]);


module.exports = IcapConstHeader;
