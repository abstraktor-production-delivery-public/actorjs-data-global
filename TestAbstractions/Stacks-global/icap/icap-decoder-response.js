
'use strict';

const StackApi = require('stack-api');
const IcapDecoder = require('./icap-decoder');
const IcapConst = require('./icap-const');
const IcapInnerLog = require('./icap-inner-log');
const IcapMsgResponse = require('./icap-msg-response');


class IcapDecoderResponse extends IcapDecoder {
  constructor(command = IcapDecoder.RECEIVE_ALL) {
    super(new IcapMsgResponse(), command);
    this.caption = '';
  }
  
  *decodeFirstLine() {
    this._parseStatusLine(yield* this.receiveLine());
    if(this.isLogIp) {
      const innerLogIcapResponse = IcapInnerLog.createStatusLine(this.msg);
      if(innerLogIcapResponse.logParts[0]) {
        const log = innerLogIcapResponse.logParts[0].text;
        this.caption = log.substring(log.indexOf(' ') + 1);
        this.setCaption(this.caption);
      }
      this.addLog(innerLogIcapResponse);
    }
  }
  
  _parseStatusLine(statusLine) {
    const parameters = statusLine.split(IcapConst.SP);
    this.msg.addStatusLine(...parameters);
  }
}

module.exports = IcapDecoderResponse;
