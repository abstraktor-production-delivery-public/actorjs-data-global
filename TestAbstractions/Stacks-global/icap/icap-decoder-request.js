
'use strict';

const StackApi = require('stack-api');
const IcapDecoder = require('./icap-decoder');
const IcapConst = require('./icap-const');
const IcapInnerLog = require('./icap-inner-log');
const IcapMsgRequest = require('./icap-msg-request');


class IcapDecoderRequest extends IcapDecoder {
  constructor(command = IcapDecoder.RECEIVE_ALL) {
    super(new IcapMsgRequest(), command);
    this.caption = '';
  }
  
  *decodeFirstLine() {
    this._parseRequestLine(yield* this.receiveLine());
    if(this.isLogIp) {
      const innerLogIcapRequest = IcapInnerLog.createRequestLine(this.msg);
      if(innerLogIcapRequest.logParts[0]) {
        const log = innerLogIcapRequest.logParts[0].text;
        this.caption = log.substring(0, log.lastIndexOf(' '));
        this.setCaption(this.caption);
      }
      this.addLog(innerLogIcapRequest);
    }
  }
  
  _parseRequestLine(requestLine) {
    let parameters = requestLine.split(IcapConst.SP);
    this.msg.addRequestLine(...parameters);
  }
}


module.exports = IcapDecoderRequest;
