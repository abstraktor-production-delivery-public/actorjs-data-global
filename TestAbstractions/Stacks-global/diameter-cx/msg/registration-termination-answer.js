
'use strict';

const DiameterApi = require('diameter-stack-api');
const DiameterCxConstCommandCode = require('../diameter-cx-const-command-code');


class RegistrationTerminationAnswer extends DiameterApi.DiameterMsg {
  constructor(applicationID, version = 1, commandFlags = 0b00000000, messageLength, hopByHopIdentifier, endToEndIdentifier) {
    super(DiameterCxConstCommandCode.REGISTRATION_TERMINATION, applicationID, version, commandFlags, messageLength, hopByHopIdentifier, endToEndIdentifier);
  }
}


module.exports = RegistrationTerminationAnswer;
