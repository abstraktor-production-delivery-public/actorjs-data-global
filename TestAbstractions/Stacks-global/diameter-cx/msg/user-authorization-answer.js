
'use strict';

const DiameterApi = require('diameter-stack-api');
const DiameterCxConstCommandCode = require('../diameter-cx-const-command-code');

const Drmp = require('../avp/drmp');
const OCSupportedFeatures = require('../avp/oc-supported-features');
const OCOLR = require('../avp/oc-olr');
const Load = require('../avp/load');
const SupportedFeatures = require('../avp/supported-features');
const ServerName = require('../avp/server-name');
const ServerCapabilities = require('../avp/server-capabilities');


class UserAuthorizationAnswer extends DiameterApi.DiameterMsg {
  constructor(applicationID, version = 1, commandFlags = 0b00000000, messageLength, hopByHopIdentifier, endToEndIdentifier) {
    super(DiameterCxConstCommandCode.USER_AUTHORIZATION, applicationID, version, commandFlags, messageLength, hopByHopIdentifier, endToEndIdentifier);
  }
  
  addAvpSessionId(sessionId) {                                               // < Session-Id >
    this.addAvp(DiameterApi.avp.SessionId, sessionId);
  }
  
  addAvpDRMP(drmp) {                                                         // [ DRMP ]
    this.addAvp(DRMP, drmp);
  }
  
  addAvpVendorSpecificApplicationId(vendorSpecificApplicationId) {           // { Vendor-Specific-Application-Id }
    this.addAvp(DiameterApi.avp.VendorSpecificApplicationId, vendorSpecificApplicationId);
  }
  
  addAvpResultCode(resultCode) {                                             // {Result-Code }
    this.addAvp(DiameterApi.avp.ResultCode, resultCode);
  }
  
  addAvpExperimentalResult(experimentalResult) {                             // [ Experimental-Result ]
    this.addAvp(DiameterApi.avp.ExperimentalResult, experimentalResult);
  }
  
  addAvpAuthSessionState(authSessionState) {                                 // { Auth-Session-State }
    this.addAvp(DiameterApi.avp.AuthSessionState, authSessionState);
  }

  addAvpOriginHost(originHost) {                                             // { Origin-Host }
    this.addAvp(DiameterApi.avp.OriginHost, originHost);
  }
  
  addAvpOriginRealm(originRealm) {                                           // { Origin-Realm }
    this.addAvp(DiameterApi.avp.OriginRealm, originRealm);
  }

  addAvpOCSupportedFeatures(ocSupportedFeatures) {                           // [ OC-Supported-Features ]
    this.addAvp(OCSupportedFeatures, ocSupportedFeatures);
  }
  
  addAvpOCOLR(ocOLR) {                                                       // [ OC-OLR ]
    this.addAvp(OCOLR, ocOLR);
  }
  
  addAvpLoad(load) {                                                         // *[ Load ]
    this.addAvp(Load, load);
  }
  
  addAvpSupportedFeatures(supportedFeatures) {                               // *[ Supported-Features ]
    this.addAvp(SupportedFeatures, supportedFeatures);
  }
  
  addAvpServerName(serverName) {                                             // [ Server-Name ]
    this.addAvp(ServerName, serverName);
  }
  
  addAvpServerCapabilities(serverCapabilities) {                             // [ Server-Capabilities ]
    this.addAvp(ServerCapabilities, serverCapabilities);
  }
  
  
                                                                             // *[ AVP ]
  
  addAvpFailedAVP(failedAVP) {                                               // [ Failed-AVP ]
    this.addAvp(DiameterApi.avp.FailedAVP, failedAVP);
  }
  
  addAvpProxyInfo(proxyInfo) {                                               // *[ Proxy-Info ]
    this.addAvp(DiameterApi.avp.ProxyInfo, proxyInfo);
  }

  addAvpRouteRecord(routeRecord) {                                           // *[ Route-Record ]	
    this.addAvp(DiameterApi.avp.RouteRecord, routeRecord);
  }
}


module.exports = UserAuthorizationAnswer;
