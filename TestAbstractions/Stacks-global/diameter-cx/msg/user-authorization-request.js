
'use strict';

const DiameterApi = require('diameter-stack-api');
const DiameterCxConstCommandCode = require('../diameter-cx-const-command-code');
const Drmp = require('../avp/drmp');
const OCSupportedFeatures = require('../avp/oc-supported-features');
const SupportedFeatures = require('../avp/supported-features');
const PublicIdentity = require('../avp/public-identity');
const VisitedNetworkIdentifier = require('../avp/visited-network-identifier');
const UserAuthorizationType = require('../avp/user-authorization-type');
const UARFlags = require('../avp/uar-flags');


class UserAuthorizationRequest extends DiameterApi.DiameterMsg {
  constructor(applicationID, version = 1, commandFlags = 0b10000000, messageLength, hopByHopIdentifier, endToEndIdentifier) {
    super(DiameterCxConstCommandCode.USER_AUTHORIZATION, applicationID, version, commandFlags, messageLength, hopByHopIdentifier, endToEndIdentifier);
  }
  
  addAvpSessionId(sessionId) {                                               // < Session-Id >
    this.addAvp(DiameterApi.avp.SessionId, sessionId);
  }
  
  addAvpDRMP(drmp) {                                                         // [ DRMP ]
    this.addAvp(Drmp, drmp);
  }
  
  addAvpVendorSpecificApplicationId(vendorSpecificApplicationId) {           // { Vendor-Specific-Application-Id }
    this.addAvp(DiameterApi.avp.VendorSpecificApplicationId, vendorSpecificApplicationId);
  }
  
  addAvpAuthSessionState(authSessionState) {                                 // { Auth-Session-State }
    this.addAvp(DiameterApi.avp.AuthSessionState, authSessionState);
  }
  
  addAvpOriginHost(originHost) {                                             // { Origin-Host }
    this.addAvp(DiameterApi.avp.OriginHost, originHost);
  }
  
  addAvpOriginRealm(originRealm) {                                           // { Origin-Realm }
    this.addAvp(DiameterApi.avp.OriginRealm, originRealm);
  }
  
  addAvpDestinationHost(destinationHost) {                                   // [ Destination-Host ]
    this.addAvp(DiameterApi.avp.DestinationHost, destinationHost);
  }
  
  addAvpDestinationRealm(destinationRealm) {                                 // { Destination-Realm }
    this.addAvp(DiameterApi.avp.DestinationRealm, destinationRealm);
  }
  
  addAvpUserName(UserName) {                                                 // { User-Name }
    this.addAvp(DiameterApi.avp.UserName, UserName);
  }
  
  addAvpOCSupportedFeatures(ocSupportedFeatures) {                           // [ OC-Supported-Features ]
    this.addAvp(OCSupportedFeatures, ocSupportedFeatures);
  }
  
  addAvpSupportedFeatures(supportedFeatures) {                               // *[ Supported-Features ]
    this.addAvp(SupportedFeatures, supportedFeatures);
  }
  
  addAvpPublicIdentity(publicIdentity) {                                     // { Public-Identity }
    this.addAvp(PublicIdentity, publicIdentity);
  }
  
  addAvpVisitedNetworkIdentifier(visitedNetworkIdentifier) {                 // { Visited-Network-Identifier }
    this.addAvp(VisitedNetworkIdentifier, visitedNetworkIdentifier);
  }
  
  addAvpUserAuthorizationType(userAuthorizationType) {                       // [ User-Authorization-Type ]
    this.addAvp(UserAuthorizationType, userAuthorizationType);
  }
  
  addAvpUARFlags(uarFlags) {                                                 // [ UAR-Flags ]
    this.addAvp(UARFlags, uarFlags);
  }

                                                                             // *[ AVP ]
  
  addAvpProxyInfo(proxyInfo) {                                               // *[ Proxy-Info ]
    this.addAvp(ProxyInfo, proxyInfo);
  }

  addAvpRouteRecord(routeRecord) {                                           // *[ Route-Record ]	
    this.addAvp(DiameterApi.avp.RouteRecord, routeRecord);
  }
}


module.exports = UserAuthorizationRequest;
