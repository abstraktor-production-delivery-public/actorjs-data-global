
'use strict';

const StackApi = require('stack-api');
const DiameterApi = require('diameter-stack-api');
const DiameterCxConnectionServerOptions = require('./diameter-cx-connection-server-options');
const DiameterCxConstCommandCode = require('./diameter-cx-const-command-code');
const DiameterCxAvpFactory = require('./diameter-cx-avp-factory');


class DiameterCxConnectionServer extends StackApi.ConnectionServer {
  constructor(id, type, actor, options) {
    super(id, type, 'diameter-cx', actor, StackApi.NetworkType.TCP, DiameterCxConnectionServerOptions, options);
  }
  
  receive() {
    this.receiveMessage(new DiameterApi.DiameterDecoder(DiameterApi.DiameterDecoder.RECEIVE_ALL, {
      avpFactory: DiameterCxAvpFactory,
      commandCode: DiameterCxConstCommandCode
    }));
  }
  
  send(msg) {
    this.sendMessage(new DiameterApi.DiameterEncoder(msg, DiameterApi.DiameterEncoder.SEND_ALL, DiameterCxConstCommandCode));
  }
}

module.exports = DiameterCxConnectionServer;
