
'use strict';

const AllowedWafWwsfIdentities = require('./avp/allowed-waf-wwsf-identities');
const AssociatedIdentities = require('./avp/associated-identities');
const AssociatedRegisteredIdentities = require('./avp/associated-registered-identities');
const CallIdSipHeader = require('./avp/call-id-sip-header');
const ChargingInformation = require('./avp/charging-information');
const ConfidentialityKey = require('./avp/confidentiality-key');
const Contact = require('./avp/contact');
const DeregistrationReason = require('./avp/deregistration-reason');
const DigestAlgorithm = require('./avp/digest-algorithm');
const DigestHa1 = require('./avp/digest-ha1');
const DigestQop = require('./avp/digest-qop');
const DigestRealm = require('./avp/digest-realm');
const Drmp = require('./avp/drmp');
const FeatureListId = require('./avp/feature-list-id');
const FeatureList = require('./avp/feature-list');
const FromSipHeader = require('./avp/from-sip-header');
const IdentityWithEmergencyRegistration = require('./avp/identity-with-emergency-registration');
const InitialCseqSequenceNumber = require('./avp/initial-cseq-sequence-number');
const IntegrityKey = require('./avp/integrity-key');
const LiaFlags = require('./avp/lia-flags');
const Load = require('./avp/load');
const LooseRouteIndication = require('./avp/loose-route-indication');
const MandatoryCapability = require('./avp/mandatory-capability');
const MultipleRegistrationIndication = require('./avp/multiple-registration-indication');
const OcOlr = require('./avp/oc-olr');
const OcSupportedFeatures = require('./avp/oc-supported-features');
const OptionalCapability = require('./avp/optional-capability');
const OriginatingRequest = require('./avp/originating-request');
const PCscfSubscriptionInfo = require('./avp/p-cscf-subscription-info');
const Path = require('./avp/path');
const PrimaryChargingCollectionFunctionName = require('./avp/primary-charging-collection-function-name');
const PrimaryEventChargingFunctionName = require('./avp/primary-event-charging-function-name');
const PriviledgedSenderIndication = require('./avp/priviledged-sender-indication');
const PublicIdentity = require('./avp/public-identity');
const ReasonCode = require('./avp/reason-code');
const ReasonInfo = require('./avp/reason-info');
const RecordRoute = require('./avp/record-route');
const RestorationInfo = require('./avp/restoration-info');
const RtrFlags = require('./avp/rtr-flags');
const SarFlags = require('./avp/sar-flags');
const ScscfRestorationInfo = require('./avp/scscf-restoration-info');
const ServerCapabilities = require('./avp/server-capabilities');
const ServerName = require('./avp/server-name');
const SessionPriority = require('./avp/session-priority');
const SipAuthDataItem = require('./avp/sip-auth-data-item');
const SipAuthenticate = require('./avp/sip-authenticate');
const SipAuthenticationContext = require('./avp/sip-authentication-context');
const SipAuthenticationScheme = require('./avp/sip-authentication-scheme');
const SipAuthorization = require('./avp/sip-authorization');
const SipDigestAuthenticate = require('./avp/sip-digest-authenticate');
const SipItemNumber = require('./avp/sip-item-number');
const SipNumberAuthItems = require('./avp/sip-number-auth-items');
const SubscriptionInfo = require('./avp/subscription-info');
const SupportedApplications = require('./avp/supported-applications');
const SupportedFeatures = require('./avp/supported-features');
const ToSipHeader = require('./avp/to-sip-header');
const UarFlags = require('./avp/uar-flags');
const UserAuthorizationType = require('./avp/user-authorization-type');
const UserDataAlreadyAvailable = require('./avp/user-data-already-available');
const UserData = require('./avp/user-data');
const VisitedNetworkIdentifier = require('./avp/visited-network-identifier');
const WebrtcAuthenticationFunctionName = require('./avp/webrtc-authentication-function-name');
const WebrtcWebServerFunctionName = require('./avp/webrtc-web-server-function-name');
const WildcardedPublicIdentity = require('./avp/wildcarded-public-identity');


class DiameterCxAvpFactory {
  static create(avpCode) {
    const avpFactory = DiameterCxAvpFactory.AVP_DATA.get(avpCode);
    if(undefined !== avpFactory) {
      return avpFactory;
    }
  }
}

DiameterCxAvpFactory.AVP_DATA = new Map([
  [AllowedWafWwsfIdentities.CODE, AllowedWafWwsfIdentities],
  [AssociatedIdentities.CODE, AssociatedIdentities],
  [AssociatedRegisteredIdentities.CODE, AssociatedRegisteredIdentities],
  [CallIdSipHeader.CODE, CallIdSipHeader],
  [ChargingInformation.CODE, ChargingInformation],
  [ConfidentialityKey.CODE, ConfidentialityKey],
  [Contact.CODE, Contact],
  [DeregistrationReason.CODE, DeregistrationReason],
  [DigestAlgorithm.CODE, DigestAlgorithm],
  [DigestHa1.CODE, DigestHa1],
  [DigestQop.CODE, DigestQop],
  [DigestRealm.CODE, DigestRealm],
  [Drmp.CODE, Drmp],
  [FeatureListId.CODE, FeatureListId],
  [FeatureList.CODE, FeatureList],
  [FromSipHeader.CODE, FromSipHeader],
  [IdentityWithEmergencyRegistration.CODE, IdentityWithEmergencyRegistration],
  [InitialCseqSequenceNumber.CODE, InitialCseqSequenceNumber],
  [IntegrityKey.CODE, IntegrityKey],
  [LiaFlags.CODE, LiaFlags],
  [Load.CODE, Load],
  [LooseRouteIndication.CODE, LooseRouteIndication],
  [MandatoryCapability.CODE, MandatoryCapability],
  [MultipleRegistrationIndication.CODE, MultipleRegistrationIndication],
  [OcOlr.CODE, OcOlr],
  [OcSupportedFeatures.CODE, OcSupportedFeatures],
  [OptionalCapability.CODE, OptionalCapability],
  [OriginatingRequest.CODE, OriginatingRequest],
  [PCscfSubscriptionInfo.CODE, PCscfSubscriptionInfo],
  [PrimaryChargingCollectionFunctionName.CODE, PrimaryChargingCollectionFunctionName],
  [PrimaryEventChargingFunctionName.CODE, PrimaryEventChargingFunctionName],
  [PriviledgedSenderIndication.CODE, PriviledgedSenderIndication],
  [PublicIdentity.CODE, PublicIdentity],
  [ReasonCode.CODE, ReasonCode],
  [ReasonInfo.CODE, ReasonInfo],
  [RecordRoute.CODE, RecordRoute],
  [RestorationInfo.CODE, RestorationInfo],
  [RtrFlags.CODE, RtrFlags],
  [SarFlags.CODE, SarFlags],
  [ScscfRestorationInfo.CODE, ScscfRestorationInfo],
  [ServerCapabilities.CODE, ServerCapabilities],
  [ServerName.CODE, ServerName],
  [SessionPriority.CODE, SessionPriority],
  [SipAuthDataItem.CODE, SipAuthDataItem],
  [SipAuthenticate.CODE, SipAuthenticate],
  [SipAuthenticationContext.CODE, SipAuthenticationContext],
  [SipAuthenticationScheme.CODE, SipAuthenticationScheme],
  [SipAuthorization.CODE, SipAuthorization],
  
  [SipDigestAuthenticate.CODE, SipDigestAuthenticate],
  [SipItemNumber.CODE, SipItemNumber],
  [SipNumberAuthItems.CODE, SipNumberAuthItems],
  [SubscriptionInfo.CODE, SubscriptionInfo],
  [SupportedApplications.CODE, SupportedApplications],
  [SupportedFeatures.CODE, SupportedFeatures],
  [ToSipHeader.CODE, ToSipHeader],
  [UarFlags.CODE, UarFlags],
  [UserAuthorizationType.CODE, UserAuthorizationType],
  [UserDataAlreadyAvailable.CODE, UserDataAlreadyAvailable],
  [UserData.CODE, UserData],
  [VisitedNetworkIdentifier.CODE, VisitedNetworkIdentifier],
  [WebrtcAuthenticationFunctionName.CODE, WebrtcAuthenticationFunctionName],
  [WebrtcWebServerFunctionName.CODE, WebrtcWebServerFunctionName],
  [WildcardedPublicIdentity.CODE, WildcardedPublicIdentity]
]);


module.exports = DiameterCxAvpFactory;
