
'use strict';


class DiameterCxConstCommandCode {
  static get(commandCode) {
    const commandCodeData = DiameterCxConstCommandCode.HEADER_DATA.get(commandCode);
    if(undefined !== commandCodeData) {
      return commandCodeData;
    }
    else {
      return DiameterCxConstCommandCode.UNKNOWN;
    }
  }
}

DiameterCxConstCommandCode.USER_AUTHORIZATION = 300;
DiameterCxConstCommandCode.SERVER_ASSIGNMENT = 301;
DiameterCxConstCommandCode.LOCATION_INFO = 302;
DiameterCxConstCommandCode.MULTIMEDIA_AUTH = 303;
DiameterCxConstCommandCode.REGISTRATION_TERMINATION = 304;
DiameterCxConstCommandCode.PUSH_PROFILE = 405;

DiameterCxConstCommandCode.HEADER_DATA = new Map([
  [DiameterCxConstCommandCode.USER_AUTHORIZATION, [{links: ['rfc6733#section-8.5.2', 'rfc6733#section-8.5'], abbrev: 'UAA', name: 'User-Authorization-Answer'}, {links: ['rfc6733#section-8.5.1', 'rfc6733#section-8.5'], abbrev: 'UAR', name: 'User-Authorization-Request'}]],
  [DiameterCxConstCommandCode.SERVER_ASSIGNMENT, [{links: ['rfc6733#section-8.5.2', 'rfc6733#section-8.5'], abbrev: 'SAA', name: 'Server-Assignment-Answer'}, {links: ['rfc6733#section-8.5.1', 'rfc6733#section-8.5'], abbrev: 'SAR', name: 'Server-Assignment-Request'}]],
  [DiameterCxConstCommandCode.LOCATION_INFO, [{links: ['rfc6733#section-8.5.2', 'rfc6733#section-8.5'], abbrev: 'LIA', name: 'Location-Info-Answer'}, {links: ['rfc6733#section-8.5.1', 'rfc6733#section-8.5'], abbrev: 'LIR', name: 'Location-Info-Request'}]],
  [DiameterCxConstCommandCode.MULTIMEDIA_AUTH, [{links: ['rfc6733#section-8.5.2', 'rfc6733#section-8.5'], abbrev: 'MAA', name: 'Multimedia-Auth-Answer'}, {links: ['rfc6733#section-8.5.1', 'rfc6733#section-8.5'], abbrev: 'MAR', name: 'Multimedia-Auth-Request'}]],
  [DiameterCxConstCommandCode.REGISTRATION_TERMINATION, [{links: ['rfc6733#section-8.5.2', 'rfc6733#section-8.5'], abbrev: 'RTA', name: 'Registration-Termination-Answer'}, {links: ['rfc6733#section-8.5.1', 'rfc6733#section-8.5'], abbrev: 'RTR', name: 'Registration-Termination-Request'}]],
  [DiameterCxConstCommandCode.PUSH_PROFILE, [{links: ['rfc6733#section-8.5.2', 'rfc6733#section-8.5'], abbrev: 'PPA', name: 'Push-Profile-Answer'}, {links: ['rfc6733#section-8.5.1', 'rfc6733#section-8.5'], abbrev: 'PPR', name: 'Push-Profile-Request'}]]
]);

DiameterCxConstCommandCode.UNKNOWN = [{links: ['', ''], abbrev: 'UNKNOWN', name: 'UNKNOWN'}, {links: ['', ''], abbrev: 'UNKNOWN', name: 'UNKNOWN'}];


module.exports = DiameterCxConstCommandCode;
