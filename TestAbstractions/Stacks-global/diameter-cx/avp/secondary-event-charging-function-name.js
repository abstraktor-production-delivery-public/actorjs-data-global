
'use strict';

const DiameterApi = require('diameter-stack-api');


class SecondaryEventChargingFunctionName extends DiameterApi.avpDerived.DiameterURI {
  constructor(value, mBit=1, vbit=1, pBit=0, vendorId=0) {
    super(SecondaryEventChargingFunctionName.CODE, value, mBit, vbit, pBit, vendorId);
  }
}

SecondaryEventChargingFunctionName.CODE = 620;


module.exports = SecondaryEventChargingFunctionName;
