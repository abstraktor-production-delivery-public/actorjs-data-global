
'use strict';

const DiameterApi = require('diameter-stack-api');


class SubscriptionInfo extends DiameterApi.avpBasic.Grouped {
  constructor(value, mBit=1, vbit=1, pBit=0, vendorId=0) {
    super(SubscriptionInfo.CODE, value, mBit, vbit, pBit, vendorId);
  }
}

SubscriptionInfo.CODE = 642;


module.exports = SubscriptionInfo;
