
'use strict';

const DiameterApi = require('diameter-stack-api');


class SarFlags extends DiameterApi.avpBasic.Unsigned32 {
  constructor(value, mBit=1, vbit=1, pBit=0, vendorId=0) {
    super(SarFlags.CODE, value, mBit, vbit, pBit, vendorId);
  }
}

SarFlags.CODE = 655;


module.exports = SarFlags;
