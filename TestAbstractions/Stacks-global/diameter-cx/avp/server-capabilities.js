
'use strict';

const DiameterApi = require('diameter-stack-api');


class ServerCapabilities extends DiameterApi.avpBasic.Grouped {
  constructor(value, mBit=1, vbit=1, pBit=0, vendorId=0) {
    super(ServerCapabilities.CODE, value, mBit, vbit, pBit, vendorId);
  }
}

ServerCapabilities.CODE = 603;


module.exports = ServerCapabilities;
