
'use strict';

const DiameterApi = require('diameter-stack-api');


class DigestAlgorithm extends DiameterApi.avpDerived.UTF8String {
  constructor(value, mBit=1, vbit=1, pBit=0, vendorId=0) {
    super(DigestAlgorithm.CODE, value, mBit, vbit, pBit, vendorId);
  }
}

DigestAlgorithm.CODE = 111;


module.exports = DigestAlgorithm;
