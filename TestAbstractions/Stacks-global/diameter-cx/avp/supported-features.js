
'use strict';

const DiameterApi = require('diameter-stack-api');


class SupportedFeatures extends DiameterApi.avpBasic.Grouped {
  constructor(value, mBit=1, vbit=1, pBit=0, vendorId=0) {
    super(SupportedFeatures.CODE, value, mBit, vbit, pBit, vendorId);
  }
}

SupportedFeatures.CODE = 628;


module.exports = SupportedFeatures;
