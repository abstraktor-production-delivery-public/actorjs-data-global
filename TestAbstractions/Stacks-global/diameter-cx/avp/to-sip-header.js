
'use strict';

const DiameterApi = require('diameter-stack-api');


class ToSipHeader extends DiameterApi.avpBasic.OctetString {
  constructor(value, mBit=1, vbit=1, pBit=0, vendorId=0) {
    super(ToSipHeader.CODE, value, mBit, vbit, pBit, vendorId);
  }
}

ToSipHeader.CODE = 645;


module.exports = ToSipHeader;
