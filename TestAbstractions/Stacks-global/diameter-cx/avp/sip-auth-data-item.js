
'use strict';

const DiameterApi = require('diameter-stack-api');


class SipAuthDataItem extends DiameterApi.avpBasic.Grouped {
  constructor(value, mBit=1, vbit=1, pBit=0, vendorId=0) {
    super(SipAuthDataItem.CODE, value, mBit, vbit, pBit, vendorId);
  }
}

SipAuthDataItem.CODE = 612;


module.exports = SipAuthDataItem;
