
'use strict';

const DiameterApi = require('diameter-stack-api');


class RecordRoute extends DiameterApi.avpBasic.OctetString {
  constructor(value, mBit=1, vbit=1, pBit=0, vendorId=0) {
    super(RecordRoute.CODE, value, mBit, vbit, pBit, vendorId);
  }
}

RecordRoute.CODE = 646;


module.exports = RecordRoute;
