
'use strict';

const DiameterApi = require('diameter-stack-api');


class ServerAssignmentType extends DiameterApi.avpDerived.Enumerated {
  constructor(value, mBit=1, vbit=1, pBit=0, vendorId=0) {
    super(ServerAssignmentType.CODE, value, mBit, vbit, pBit, vendorId);
  }
}

ServerAssignmentType.CODE = 614;


module.exports = ServerAssignmentType;
