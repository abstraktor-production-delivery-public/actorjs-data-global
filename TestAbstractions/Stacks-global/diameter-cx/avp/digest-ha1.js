
'use strict';

const DiameterApi = require('diameter-stack-api');


class DigestHa1 extends DiameterApi.avpDerived.UTF8String {
  constructor(value, mBit=1, vbit=1, pBit=0, vendorId=0) {
    super(DigestHa1.CODE, value, mBit, vbit, pBit, vendorId);
  }
}

DigestHa1.CODE = 121;


module.exports = DigestHa1;
