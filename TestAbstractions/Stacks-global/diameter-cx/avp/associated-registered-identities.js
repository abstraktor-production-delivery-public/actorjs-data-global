
'use strict';

const DiameterApi = require('diameter-stack-api');


class AssociatedRegisteredIdentities extends DiameterApi.avpBasic.Grouped {
  constructor(value, mBit=1, vbit=1, pBit=0, vendorId=0) {
    super(AssociatedRegisteredIdentities.CODE, value, mBit, vbit, pBit, vendorId);
  }
}

AssociatedRegisteredIdentities.CODE = 647;


module.exports = AssociatedRegisteredIdentities;
