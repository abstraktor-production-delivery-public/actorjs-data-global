
'use strict';

const DiameterApi = require('diameter-stack-api');


class WebrtcWebServerFunctionName extends DiameterApi.avpDerived.UTF8String {
  constructor(value, mBit=1, vbit=1, pBit=0, vendorId=0) {
    super(WebrtcWebServerFunctionName.CODE, value, mBit, vbit, pBit, vendorId);
  }
}

WebrtcWebServerFunctionName.CODE = 658;


module.exports = WebrtcWebServerFunctionName;
