
'use strict';

const DiameterApi = require('diameter-stack-api');


class UserDataAlreadyAvailable extends DiameterApi.avpDerived.Enumerated {
  constructor(value, mBit=1, vbit=1, pBit=0, vendorId=0) {
    super(UserDataAlreadyAvailable.CODE, value, mBit, vbit, pBit, vendorId);
  }
}

UserDataAlreadyAvailable.CODE = 624;


module.exports = UserDataAlreadyAvailable;
