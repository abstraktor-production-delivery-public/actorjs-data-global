
'use strict';

const DiameterApi = require('diameter-stack-api');


class SipAuthenticate extends DiameterApi.avpBasic.OctetString {
  constructor(value, mBit=1, vbit=1, pBit=0, vendorId=0) {
    super(SipAuthenticate.CODE, value, mBit, vbit, pBit, vendorId);
  }
}

SipAuthenticate.CODE = 609;


module.exports = SipAuthenticate;
