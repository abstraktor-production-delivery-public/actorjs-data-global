
'use strict';

const DiameterApi = require('diameter-stack-api');


class LiaFlags extends DiameterApi.avpBasic.Unsigned32 {
  constructor(value, mBit=1, vbit=1, pBit=0, vendorId=0) {
    super(LiaFlags.CODE, value, mBit, vbit, pBit, vendorId);
  }
}

LiaFlags.CODE = 653;


module.exports = LiaFlags;
