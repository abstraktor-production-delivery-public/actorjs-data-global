
'use strict';

const DiameterApi = require('diameter-stack-api');


class AssociatedIdentities extends DiameterApi.avpBasic.Grouped {
  constructor(value, mBit=1, vbit=1, pBit=0, vendorId=0) {
    super(AssociatedIdentities.CODE, value, mBit, vbit, pBit, vendorId);
  }
}

AssociatedIdentities.CODE = 632;


module.exports = AssociatedIdentities;
