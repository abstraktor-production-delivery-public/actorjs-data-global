
'use strict';

const DiameterApi = require('diameter-stack-api');


class WebrtcAuthenticationFunctionName extends DiameterApi.avpDerived.UTF8String {
  constructor(value, mBit=1, vbit=1, pBit=0, vendorId=0) {
    super(WebrtcAuthenticationFunctionName.CODE, value, mBit, vbit, pBit, vendorId);
  }
}

WebrtcAuthenticationFunctionName.CODE = 657;


module.exports = WebrtcAuthenticationFunctionName;
