
'use strict';

const DiameterApi = require('diameter-stack-api');


class SipAuthenticationScheme extends DiameterApi.avpDerived.UTF8String {
  constructor(value, mBit=1, vbit=1, pBit=0, vendorId=0) {
    super(SipAuthenticationScheme.CODE, value, mBit, vbit, pBit, vendorId);
  }
}

SipAuthenticationScheme.CODE = 608;


module.exports = SipAuthenticationScheme;
