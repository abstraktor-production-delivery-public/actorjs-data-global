
'use strict';

const DiameterApi = require('diameter-stack-api');


class SipItemNumber extends DiameterApi.avpBasic.Unsigned32 {
  constructor(value, mBit=1, vbit=1, pBit=0, vendorId=0) {
    super(SipItemNumber.CODE, value, mBit, vbit, pBit, vendorId);
  }
}

SipItemNumber.CODE = 613;


module.exports = SipItemNumber;
