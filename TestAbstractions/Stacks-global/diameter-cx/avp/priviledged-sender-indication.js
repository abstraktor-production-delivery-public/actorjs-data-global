
'use strict';

const DiameterApi = require('diameter-stack-api');


class PriviledgedSenderIndication extends DiameterApi.avpDerived.Enumerated {
  constructor(value, mBit=1, vbit=1, pBit=0, vendorId=0) {
    super(PriviledgedSenderIndication.CODE, value, mBit, vbit, pBit, vendorId);
  }
}

PriviledgedSenderIndication.CODE = 652;


module.exports = PriviledgedSenderIndication;
