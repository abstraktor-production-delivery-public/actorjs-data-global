
'use strict';

const DiameterApi = require('diameter-stack-api');


class ScscfRestorationInfo extends DiameterApi.avpBasic.Grouped {
  constructor(value, mBit=1, vbit=1, pBit=0, vendorId=0) {
    super(ScscfRestorationInfo.CODE, value, mBit, vbit, pBit, vendorId);
  }
}

ScscfRestorationInfo.CODE = 639;


module.exports = ScscfRestorationInfo;
